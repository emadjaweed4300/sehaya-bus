import { Component, OnInit } from '@angular/core';
import { ChartDB } from '../../../fack-db/chart-data';
import { ApexChartService } from '../../../theme/shared/components/chart/apex-chart/apex-chart.service';
import { GlobalService } from 'src/app/services/global.service';
import { DashboardApiService } from '../shared/dashboard-api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-dash-analytics',
	templateUrl: './dash-analytics.component.html',
	styleUrls: ['./dash-analytics.component.scss']
})
export class DashAnalyticsComponent implements OnInit {

	public bar1CAC = {
		// chart: {
		// 	height: 350,
		// 	type: 'bar',
		// },
		// plotOptions: {
		// 	bar: {
		// 		horizontal: false,
		// 		columnWidth: '55%',
		// 		endingShape: 'rounded'
		// 	},
		// },
		// dataLabels: {
		// 	enabled: false
		// },
		// colors: ['#0e9e4a', '#323258', '#ff5252'],
		// stroke: {
		// 	show: true,
		// 	width: 2,
		// 	colors: ['transparent']
		// },
		// series: [],
		// xaxis: {
		// 	categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
		// },
		// yaxis: {
		// 	title: {
		// 		text: 'mad (thousands)'
		// 	}
		// },
		// fill: {
		// 	opacity: 1

		// },
		// tooltip: {
		// 	y: {
		// 		formatter: (val) => 'MAD ' + val + ' thousands'
		// 	}
		// }
	};

	// seriesArr: any[] = [
	// 	{ name: "Cash In", data: [1000, 2000, 3000, 4000, 5000, 5500, 8500, 35000, 2000, 1000, 2500, 15000] },
	// 	{ name: "Profit", data: [500, 500, 2500, 3000, 2000, 5000, 0, 35000, 2000, 1000, 2500, 5000] },
	// 	{ name: "Expense", data: [500, 1500, 500, 1000, 3000, 500, 8500, 0, 0, 0, 0, 10000] }
	// ];

	// categoriesArr: any[] = [
	// 	{ categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] },
	// ];

	public chartDB: any;
	dashboard_list: any = [];
	cities_list: any = [];
	clients_list: any = [];
	tourism_company_lists: any = [];
	// graph_data_list: any = [];

	constructor(
		public apexEvent: ApexChartService,
		public global: GlobalService,
		private dashboardApi: DashboardApiService
	) {
		this.chartDB = ChartDB;
	}

	ngOnInit() {
		this.get_dashboard_deatils();
		this.city_list();
		this.client_list();
		// this.graph_data();
		this.tourism_company_list();
	}

	// Get Cash Desk Card Details
	get_dashboard_deatils() {
		this.dashboardApi.dashboard().then((response: any) => {
			console.log('Response:', response);
			this.dashboard_list = response.dashboard;
		})
			.catch((error: any) => { 
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
			
	}


	// List City
	city_list() {
		this.dashboardApi.dashboard().then((response: any) => {
			console.log('Response:', response);
			this.cities_list = response.dashboard.cities
		})
			.catch((error: any) => {
				console.error(error);
			});
	}

	// List clients
	client_list() {
		this.dashboardApi.dashboard().then((response: any) => {
			console.log('Response:', response['dashboard']);
			this.clients_list = response.dashboard.clients
		})
			.catch((error: any) => {
				console.error(error);
			});
	}

	// Graph API Functionality
	// graph_data() {
	// 	this.dashboardApi.dashboard().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.graph_data_list = response.dashboard
	// 		// this.bar1CAC.series = this.seriesArr
	// 		// this.bar1CAC.xaxis.categories = this.graph_data_list.xaxis.categories
	// 		// this.bar1CAC.xaxis.categories = ['Fab', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec', 'Jan']
	// 		// console.log(this.bar1CAC.series)
	// 		// console.log(this.bar1CAC.xaxis.categories)
	// 	})
	// 		.catch((error: any) => {
	// 			console.error(error);
	// 		});
	// 	console.log(this.bar1CAC)
	// }

	// List Tourism Company
	tourism_company_list() {
		this.dashboardApi.dashboard().then((response: any) => {
			console.log('Response:', response);
			this.tourism_company_lists = response.dashboard.clients
		})
			.catch((error: any) => {
				console.error(error);
			});
	}

}
