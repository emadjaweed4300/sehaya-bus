import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
	selector: 'app-change-password',
	templateUrl: './change-password.component.html',
	styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

	Change_Password_frm: FormGroup;

	constructor(
		private formBuilder: FormBuilder
	) { }

	ngOnInit() {
		this.Change_Password_frm = this.formBuilder.group({
			old_password: [null, [Validators.required]],
			new_password: [null, [Validators.required]],
			confirm_password: [null, [Validators.required, (control) => this.Match(control, this.Change_Password_frm, 'new_password')]],
		});
	}

	// Passwrod Match Function
	Match(fc: FormControl, za: FormGroup, zb: string) {
		if (fc.value === null) {
			return (null)
		}
		if (fc.value !== za.get(zb).value) {
			return { Match: true }
		} else {
			return (null)
		}
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minlengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.Change_Password_frm.get(formControlName).errors &&
			(this.Change_Password_frm.get(formControlName).touched ||
				this.Change_Password_frm.get(formControlName).dirty);
		if (errors) {
			required = this.Change_Password_frm.get(formControlName).errors
				.required;
			patternValidate = this.Change_Password_frm.get(formControlName).errors
				.pattern;
			minlengthValidate = this.Change_Password_frm.get(formControlName)
				.errors.minLength;
			notEquivalentValidate = this.Change_Password_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minlengthValidate: minlengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	changePassword() {
		this.markFormGroupTouched(this.Change_Password_frm);
		if (this.Change_Password_frm.valid) {
		}
	}

	/**
	   * Marks all controls in a form group as touched
	   * @param formGroup The form group to touch
	   */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
