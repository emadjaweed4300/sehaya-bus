import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordsRoutingModule } from './change-passwords-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ChangePasswordsRoutingModule,
  ],
  declarations: []
})
export class ChangePasswordsModule { }
