import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Change Password list,add,view,edit components
			{
				path: 'change-password',
				loadChildren: () => import('./change-password/change-password.module').then(module => module.ChangePasswordModule)
			}
		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ChangePasswordsRoutingModule { }
