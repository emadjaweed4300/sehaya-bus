import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditClaimCategoryRoutingModule } from './edit-claim-category-routing.module';
import { EditClaimCategoryComponent } from './edit-claim-category.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    EditClaimCategoryRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditClaimCategoryComponent]
})
export class EditClaimCategoryModule { }
