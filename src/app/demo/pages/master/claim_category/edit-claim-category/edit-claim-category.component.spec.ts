import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditClaimCategoryComponent } from './edit-claim-category.component';

describe('EditClaimCategoryComponent', () => {
  let component: EditClaimCategoryComponent;
  let fixture: ComponentFixture<EditClaimCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditClaimCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditClaimCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
