import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-claim-category',
	templateUrl: './edit-claim-category.component.html',
	styleUrls: ['./edit-claim-category.component.scss']
})
export class EditClaimCategoryComponent implements OnInit {

	edit_claim_category_frm: FormGroup;
	id
	claim_type: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) {
		this.id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_claim_category_frm = this.__fb.group({
			claim_category: ["", Validators.required]
		});
		this.get_claim_type();
	}

	// Get All Claim Type API
	get_claim_type(){
		this.masterApi.get_claim_type(this.id).then((response: any) => {
			console.log('Response:', response);
			this.claim_type = response.claim_type;
			// claim_category (formControlName) + response.claim_type.claim_type(Postman)
			console.log(response.claim_type.type_name)
			this.edit_claim_category_frm.get('claim_category').setValue(response.claim_type.type_name);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_claim_category_frm.get(formControlName).errors &&
			(this.edit_claim_category_frm.get(formControlName).touched ||
				this.edit_claim_category_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_claim_category_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_claim_category_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_claim_category_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_claim_category_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_claim_category_frm);
		if (this.edit_claim_category_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('user_id', null);
			// id(postman) 
			form_data.append('claim_id', this.id);
			// parameter "claim_type" (postman) + formcontrolname (claim_category)
			form_data.append('claim_type', this.edit_claim_category_frm.get('claim_category').value);
			this.masterApi.edit_claim_type(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-claim-category'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
