import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddClaimCategoryComponent } from './add-claim-category.component';


const routes: Routes = [
  {
    path: '',
    component: AddClaimCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddClaimCategoryRoutingModule { }
