import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClaimCategoryComponent } from './add-claim-category.component';

describe('AddClaimCategoryComponent', () => {
  let component: AddClaimCategoryComponent;
  let fixture: ComponentFixture<AddClaimCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClaimCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClaimCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
