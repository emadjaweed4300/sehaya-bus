import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddClaimCategoryRoutingModule } from './add-claim-category-routing.module';
import { AddClaimCategoryComponent } from './add-claim-category.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    AddClaimCategoryRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddClaimCategoryComponent]
})
export class AddClaimCategoryModule { }
