import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListClaimCategoryRoutingModule } from './list-claim-category-routing.module';
import { ListClaimCategoryComponent } from './list-claim-category.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    ListClaimCategoryRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListClaimCategoryComponent]
})
export class ListClaimCategoryModule { }
