import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListClaimCategoryComponent } from './list-claim-category.component';

describe('ListClaimCategoryComponent', () => {
  let component: ListClaimCategoryComponent;
  let fixture: ComponentFixture<ListClaimCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListClaimCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListClaimCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
