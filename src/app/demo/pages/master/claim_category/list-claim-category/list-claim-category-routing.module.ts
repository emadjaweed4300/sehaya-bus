import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListClaimCategoryComponent } from './list-claim-category.component';


const routes: Routes = [
  {
    path: '',
    component: ListClaimCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListClaimCategoryRoutingModule { }
