import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditLocationRoutingModule } from './edit-location-routing.module';
import { EditLocationComponent } from './edit-location.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditLocationRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditLocationComponent]
})
export class EditLocationModule { }
