import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-location',
	templateUrl: './edit-location.component.html',
	styleUrls: ['./edit-location.component.scss']
})
export class EditLocationComponent implements OnInit {

	edit_location_frm: FormGroup;
	id
	location: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) {
		this.id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		console.log(this.id)
		/** Form builder instance */
		this.edit_location_frm = this.__fb.group({
			location_type: ["", Validators.required]
		});
		this.get_location();
	}

	// Get All Location API
	get_location(){
		this.masterApi.get_location(this.id).then((response: any) => {
			console.log('Response:', response);
			this.location = response.location_type;
			this.edit_location_frm.get('location_type').setValue(response.location_type.location_type);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_location_frm.get(formControlName).errors &&
			(this.edit_location_frm.get(formControlName).touched ||
				this.edit_location_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_location_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_location_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_location_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_location_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_location_frm);
		if (this.edit_location_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData(); 
			form_data.append('location_id', this.id);
			form_data.append('location_type', this.edit_location_frm.get('location_type').value);
			this.masterApi.edit_location(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-location'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
