import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListLocationRoutingModule } from './list-location-routing.module';
import { ListLocationComponent } from './list-location.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListLocationRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListLocationComponent]
})
export class ListLocationModule { }
