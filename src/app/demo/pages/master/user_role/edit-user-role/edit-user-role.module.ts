import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditUserRoleRoutingModule } from './edit-user-role-routing.module';
import { EditUserRoleComponent } from './edit-user-role.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditUserRoleRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditUserRoleComponent]
})
export class EditUserRoleModule { }
