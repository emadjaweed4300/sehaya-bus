import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';
@Component({
	selector: 'app-edit-user-role',
	templateUrl: './edit-user-role.component.html',
	styleUrls: ['./edit-user-role.component.scss']
})
export class EditUserRoleComponent implements OnInit {

	edit_user_role_frm: FormGroup;
	user_role_id
	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi : MasterApiService,
		private __route: ActivatedRoute,
	) { 
		this.user_role_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_user_role_frm = this.__fb.group({
			user_role: ["", Validators.required]
		});
		this.get_user_role();
	}

	get_user_role(){
		this.masterApi.get_user_role(this.user_role_id).then((response: any) => {
			console.log('Response:', response);
			this.edit_user_role_frm.get('user_role').setValue(response.user_role.role);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_user_role_frm.get(formControlName).errors &&
			(this.edit_user_role_frm.get(formControlName).touched ||
				this.edit_user_role_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_user_role_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_user_role_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_user_role_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_user_role_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_user_role_frm);
		if (this.edit_user_role_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('user_role_id', this.user_role_id);
			form_data.append('role_name', this.edit_user_role_frm.get('user_role').value);
			this.masterApi.edit_user_role(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-user-role'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});		
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
