import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListUserRoleRoutingModule } from './list-user-role-routing.module';
import { ListUserRoleComponent } from './list-user-role.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListUserRoleRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListUserRoleComponent]
})
export class ListUserRoleModule { }
