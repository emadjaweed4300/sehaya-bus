import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUserRoleComponent } from './list-user-role.component';

const routes: Routes = [
  {
    path: '',
    component: ListUserRoleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListUserRoleRoutingModule { }
