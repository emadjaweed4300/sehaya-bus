import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddUserRoleRoutingModule } from './add-user-role-routing.module';
import { AddUserRoleComponent } from './add-user-role.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddUserRoleRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddUserRoleComponent]
})
export class AddUserRoleModule { }
