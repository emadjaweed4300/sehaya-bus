import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Brand list,add,view,edit components
			{
				path: 'list-brand',
				loadChildren: () => import('./brand/list-brand/list-brand.module').then(module => module.ListBrandModule)
			},
			{
				path: 'add-brand',
				loadChildren: () => import('./brand/add-brand/add-brand.module').then(module => module.AddBrandModule)
			},
			{
				path: 'view-brand',
				loadChildren: () => import('./brand/view-brand/view-brand.module').then(module => module.ViewBrandModule)
			},
			{
				path: 'edit-brand/:id',
				loadChildren: () => import('./brand/edit-brand/edit-brand.module').then(module => module.EditBrandModule)
			},

			// Region list,add,view,edit components
			{
				path: 'list-region',
				loadChildren: () => import('./region/list-region/list-region.module').then(module => module.ListRegionModule)
			},
			{
				path: 'add-region',
				loadChildren: () => import('./region/add-region/add-region.module').then(module => module.AddRegionModule)
			},
			{
				path: 'view-region',
				loadChildren: () => import('./region/view-region/view-region.module').then(module => module.ViewRegionModule)
			},
			{
				path: 'edit-region/:id',
				loadChildren: () => import('./region/edit-region/edit-region.module').then(module => module.EditRegionModule)
			},

			// City list,add,view,edit components
			{
				path: 'list-city',
				loadChildren: () => import('./city/list-city/list-city.module').then(module => module.ListCityModule)
			},
			{
				path: 'add-city',
				loadChildren: () => import('./city/add-city/add-city.module').then(module => module.AddCityModule)
			},
			{
				path: 'view-city',
				loadChildren: () => import('./city/view-city/view-city.module').then(module => module.ViewCityModule)
			},
			{
				path: 'edit-city/:id',
				loadChildren: () => import('./city/edit-city/edit-city.module').then(module => module.EditCityModule)
			},

			// Veicle_Category list,add,view,edit components
			{
				path: 'list-vehicle-category',
				loadChildren: () => import('./vehicle_category/list-vehicle-category/list-vehicle-category.module').then(module => module.ListVehicleCategoryModule)
			},
			{
				path: 'add-vehicle-category',
				loadChildren: () => import('./vehicle_category/add-vehicle-category/add-vehicle-category.module').then(module => module.AddVehicleCategoryModule)
			},
			{
				path: 'edit-vehicle-category/:id',
				loadChildren: () => import('./vehicle_category/edit-vehicle-category/edit-vehicle-category.module').then(module => module.EditVehicleCategoryModule)
			},

			// Claim_Category list,add,view,edit components
			{
				path: 'list-claim-category',
				loadChildren: () => import('./claim_category/list-claim-category/list-claim-category.module').then(module => module.ListClaimCategoryModule)
			},
			{
				path: 'add-claim-category',
				loadChildren: () => import('./claim_category/add-claim-category/add-claim-category.module').then(module => module.AddClaimCategoryModule)
			},
			// {
			// 	path: 'view-claim-category',
			// 	loadChildren: () => import('./claim-category/view-claim-category/view-claim-category.module').then(module => module.ViewclaimCategoryModule)
			// },
			{
				path: 'edit-claim-category/:id',
				loadChildren: () => import('./claim_category/edit-claim-category/edit-claim-category.module').then(module => module.EditClaimCategoryModule)
			},
			// User Role list,add,view,edit components
			{
				path: 'list-user-role',
				loadChildren: () => import('./user_role/list-user-role/list-user-role.module').then(module => module.ListUserRoleModule)
			},
			{
				path: 'add-user-role',
				loadChildren: () => import('./user_role/add-user-role/add-user-role.module').then(module => module.AddUserRoleModule)
			},
			// {
			// 	path: 'view-user-role',
			// 	loadChildren: () => import('./user-role/view-user-role/view-user-role.module').then(module => module.ViewUserRoleModule)
			// },
			{
				path: 'edit-user-role/:id',
				loadChildren: () => import('./user_role/edit-user-role/edit-user-role.module').then(module => module.EditUserRoleModule)
			},
			// Client_Category list,add,view,edit components
			{
				path: 'list-client-category',
				loadChildren: () => import('./client_category/list-client-category/list-client-category.module').then(module => module.ListClientCategoryModule)
			},
			{
				path: 'add-client-category',
				loadChildren: () => import('./client_category/add-client-category/add-client-category.module').then(module => module.AddClientCategoryModule)
			},
			// {
			// 	path: 'view-client-category',
			// 	loadChildren: () => import('./client-category/view-client-category/view-client-category.module').then(module => module.ViewClientCategoryModule)
			// },
			{
				path: 'edit-client-category/:id',
				loadChildren: () => import('./client_category/edit-client-category/edit-client-category.module').then(module => module.EditClientCategoryModule)
			},
			// function list,add,view,edit components
			{
				path: 'list-function',
				loadChildren: () => import('./function/list-function/list-function.module').then(module => module.ListFunctionModule)
			},
			{
				path: 'add-function',
				loadChildren: () => import('./function/add-function/add-function.module').then(module => module.AddFunctionModule)
			},
			{
				path: 'edit-function/:id',
				loadChildren: () => import('./function/edit-function/edit-function.module').then(module => module.EditFunctionModule)
			},
			// Deadline list,add,edit components
			{
				path: 'list-deadline',
				loadChildren: () => import('./deadline/list-deadline/list-deadline.module').then(module => module.ListDeadlineModule)
			},
			{
				path: 'add-deadline',
				loadChildren: () => import('./deadline/add-deadline/add-deadline.module').then(module => module.AddDeadlineModule)
			},
			{
				path: 'edit-deadline/:id',
				loadChildren: () => import('./deadline/edit-deadline/edit-deadline.module').then(module => module.EditDeadlineModule)
			},
			// Features list,add,edit components
			{
				path: 'list-features',
				loadChildren: () => import('./features/list-features/list-features.module').then(module => module.ListFeaturesModule)
			},
			{
				path: 'add-features',
				loadChildren: () => import('./features/add-features/add-features.module').then(module => module.AddFeaturesModule)
			},
			{
				path: 'edit-features/:id',
				loadChildren: () => import('./features/edit-features/edit-features.module').then(module => module.EditFeaturesModule)
			},
			// Unit list,add,edit components
			{
				path: 'list-unit',
				loadChildren: () => import('./unit/list-unit/list-unit.module').then(module => module.ListUnitModule)
			},
			{
				path: 'add-unit',
				loadChildren: () => import('./unit/add-unit/add-unit.module').then(module => module.AddUnitModule)
			},
			{
				path: 'edit-unit/:id',
				loadChildren: () => import('./unit/edit-unit/edit-unit.module').then(module => module.EditUnitModule)
			},
			// Vehicle Provider list, add, edit, view components
			{
				path: 'list-vehicle-provider',
				loadChildren: () => import('./vehicle-provider/list-vehicle-provider/list-vehicle-provider.module').then(module => module.ListVehicleProviderModule)
			},
			{
				path: 'add-vehicle-provider',
				loadChildren: () => import('./vehicle-provider/add-vehicle-provider/add-vehicle-provider.module').then(module => module.AddVehicleProviderModule)
			},
			{
				path: 'edit-vehicle-provider/:id',
				loadChildren: () => import('./vehicle-provider/edit-vehicle-provider/edit-vehicle-provider.module').then(module => module.EditVehicleProviderModule)
			},
			{
				path: 'view-vehicle-provider/:id',
				loadChildren: () => import('./vehicle-provider/view-vehicle-provider/view-vehicle-provider.module').then(module => module.ViewVehicleProviderModule)
			},
			// event list,add,view,edit components
			{
				path: 'list-event',
				loadChildren: () => import('./event/list-event/list-event.module').then(module => module.ListEventModule)
			},
			{
				path: 'add-event',
				loadChildren: () => import('./event/add-event/add-event.module').then(module => module.AddEventModule)
			},
			{
				path: 'edit-event/:id',
				loadChildren: () => import('./event/edit-event/edit-event.module').then(module => module.EditEventModule)
			},
			// movement list,add,view,edit components
			{
				path: 'list-movement',
				loadChildren: () => import('./movement/list-movement/list-movement.module').then(module => module.ListMovementModule)
			},
			{
				path: 'add-movement',
				loadChildren: () => import('./movement/add-movement/add-movement.module').then(module => module.AddMovementModule)
			},
			{
				path: 'edit-movement/:id',
				loadChildren: () => import('./movement/edit-movement/edit-movement.module').then(module => module.EditMovementModule)
			},
			// location list,add,edit components
			{
				path: 'list-location',
				loadChildren: () => import('./location/list-location/list-location.module').then(module => module.ListLocationModule)
			},
			{
				path: 'add-location',
				loadChildren: () => import('./location/add-location/add-location.module').then(module => module.AddLocationModule)
			},
			{
				path: 'edit-location/:id',
				loadChildren: () => import('./location/edit-location/edit-location.module').then(module => module.EditLocationModule)
			},
		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MasterRoutingModule { }
