import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListBrandRoutingModule } from './list-brand-routing.module';
import { ListBrandComponent } from './list-brand.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    ListBrandRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListBrandComponent]
})
export class ListBrandModule { }
