import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MasterApiService } from '../../shared/master_api.service';

@Component({
	selector: 'app-add-brand',
	templateUrl: './add-brand.component.html',
	styleUrls: ['./add-brand.component.scss']
})
export class AddBrandComponent implements OnInit {

	add_brand_frm: FormGroup;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_brand_frm = this.__fb.group({
			brand_name: ["", Validators.required]
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_brand_frm.get(formControlName).errors &&
			(this.add_brand_frm.get(formControlName).touched ||
				this.add_brand_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_brand_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_brand_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_brand_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_brand_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_brand_frm);
		if (this.add_brand_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('brand_name', this.add_brand_frm.get('brand_name').value);
			this.masterApi.add_brands(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-brand'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
