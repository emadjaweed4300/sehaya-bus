import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddBrandRoutingModule } from './add-brand-routing.module';
import { AddBrandComponent } from './add-brand.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddBrandRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddBrandComponent]
})
export class AddBrandModule { }
