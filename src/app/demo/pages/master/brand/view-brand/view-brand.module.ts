import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewBrandRoutingModule } from './view-brand-routing.module';
import { ViewBrandComponent } from './view-brand.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';



@NgModule({
  imports: [
    CommonModule,
    ViewBrandRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule
  ],
  declarations: [ViewBrandComponent]
})
export class ViewBrandModule { }
