import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditBrandRoutingModule } from './edit-brand-routing.module';
import { EditBrandComponent } from './edit-brand.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    EditBrandRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditBrandComponent]
})
export class EditBrandModule { }
