import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-brand',
	templateUrl: './edit-brand.component.html',
	styleUrls: ['./edit-brand.component.scss']
})
export class EditBrandComponent implements OnInit {

	edit_brand_frm: FormGroup;
	id
	brand: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) {
		this.id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		console.log(this.id)
		/** Form builder instance */
		this.edit_brand_frm = this.__fb.group({
			brand_name: ["", Validators.required]
		});
		this.get_brand();
	}

	// Get All Brand API
	get_brand(){
		this.masterApi.get_brand(this.id).then((response: any) => {
			console.log('Response:', response);
			this.brand = response.brand;
			// brand_name (formControlName) + response.brand.brand(Postman)
			this.edit_brand_frm.get('brand_name').setValue(response.brand.brand_name);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_brand_frm.get(formControlName).errors &&
			(this.edit_brand_frm.get(formControlName).touched ||
				this.edit_brand_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_brand_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_brand_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_brand_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_brand_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_brand_frm);
		if (this.edit_brand_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// brand_id(postman) 
			form_data.append('brand_id', this.id);
			// parameter "brand" (postman) + formcontrolname (brand_name)
			form_data.append('brand_name', this.edit_brand_frm.get('brand_name').value);
			this.masterApi.edit_brand(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-brand'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
