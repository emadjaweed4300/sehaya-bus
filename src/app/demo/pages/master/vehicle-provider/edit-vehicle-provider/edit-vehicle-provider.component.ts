import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-vehicle-provider',
	templateUrl: './edit-vehicle-provider.component.html',
	styleUrls: ['./edit-vehicle-provider.component.scss']
})
export class EditVehicleProviderComponent implements OnInit {

	edit_vehicle_provider_frm: FormGroup;
	vehicle_provider_id
	provider: any;
	cities = [];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) {
		this.vehicle_provider_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		/** Form builder instance */
		this.edit_vehicle_provider_frm = this.__fb.group({
			name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			mobile: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			telephone: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9,/]*$'), Validators.maxLength(21)]],
			city_name: ["", Validators.required],
			address: ["", Validators.required],
		});
		this.get_vehicle_provider();
		this.get_cities();
	}

	// Get Cities Dropdown
	get_cities(){
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	get_vehicle_provider(){
		this.masterApi.get_vehicle_provider(this.vehicle_provider_id).then((response: any) => {
			console.log('Response:', response);
			this.provider = response.provider;
			// formControlName & postman response
			this.edit_vehicle_provider_frm.get('name').setValue(response.provider.name);
			this.edit_vehicle_provider_frm.get('address').setValue(response.provider.address);
			this.edit_vehicle_provider_frm.get('mobile').setValue(response.provider.mobile);
			this.edit_vehicle_provider_frm.get('telephone').setValue(response.provider.telephone);
			this.edit_vehicle_provider_frm.get('email').setValue(response.provider.email);
			this.edit_vehicle_provider_frm.get('city_name').setValue(response.provider.city);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let maxLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_vehicle_provider_frm.get(formControlName).errors &&
			(this.edit_vehicle_provider_frm.get(formControlName).touched ||
				this.edit_vehicle_provider_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_vehicle_provider_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_vehicle_provider_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_vehicle_provider_frm.get(formControlName)
				.errors.minlength;
			maxLengthValidate = this.edit_vehicle_provider_frm.get(formControlName)
				.errors.maxlength;
			notEquivalentValidate = this.edit_vehicle_provider_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			maxLengthValidate: maxLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call edit service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_vehicle_provider_frm);
		if (this.edit_vehicle_provider_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// form_data.append('user_id', null);
			form_data.append('name', this.edit_vehicle_provider_frm.get('name').value);
			form_data.append('email', this.edit_vehicle_provider_frm.get('email').value);
			form_data.append('mobile', this.edit_vehicle_provider_frm.get('mobile').value);
			form_data.append('telephone', this.edit_vehicle_provider_frm.get('telephone').value);
			form_data.append('city_id', this.edit_vehicle_provider_frm.get('city_name').value);
			form_data.append('address', this.edit_vehicle_provider_frm.get('address').value);
			form_data.append('provider_id', this.vehicle_provider_id);
			this.masterApi.edit_vehicle_provider(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-vehicle-provider'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
