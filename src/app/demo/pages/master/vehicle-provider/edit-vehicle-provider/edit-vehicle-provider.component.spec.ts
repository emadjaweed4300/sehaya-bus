import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVehicleProviderComponent } from './edit-vehicle-provider.component';

describe('EditVehicleProviderComponent', () => {
  let component: EditVehicleProviderComponent;
  let fixture: ComponentFixture<EditVehicleProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVehicleProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVehicleProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
