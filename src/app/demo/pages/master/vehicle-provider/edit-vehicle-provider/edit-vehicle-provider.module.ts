import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditVehicleProviderRoutingModule } from './edit-vehicle-provider-routing.module';
import { EditVehicleProviderComponent } from './edit-vehicle-provider.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    EditVehicleProviderRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditVehicleProviderComponent]
})
export class EditVehicleProviderModule { }
