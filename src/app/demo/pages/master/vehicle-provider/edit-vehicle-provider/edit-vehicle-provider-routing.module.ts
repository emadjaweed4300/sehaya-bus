import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditVehicleProviderComponent } from './edit-vehicle-provider.component';


const routes: Routes = [
  {
    path: '',
    component: EditVehicleProviderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditVehicleProviderRoutingModule { }
