import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewVehicleProviderComponent } from './view-vehicle-provider.component';

describe('ViewVehicleProviderComponent', () => {
  let component: ViewVehicleProviderComponent;
  let fixture: ComponentFixture<ViewVehicleProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewVehicleProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewVehicleProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
