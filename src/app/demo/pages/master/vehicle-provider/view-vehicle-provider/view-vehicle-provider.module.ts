import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewVehicleProviderRoutingModule } from './view-vehicle-provider-routing.module';
import { ViewVehicleProviderComponent } from './view-vehicle-provider.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    ViewVehicleProviderRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ViewVehicleProviderComponent]
})
export class ViewVehicleProviderModule { }
