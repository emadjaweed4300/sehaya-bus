import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewVehicleProviderComponent } from './view-vehicle-provider.component';


const routes: Routes = [
  {
    path: '',
    component: ViewVehicleProviderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewVehicleProviderRoutingModule { }
