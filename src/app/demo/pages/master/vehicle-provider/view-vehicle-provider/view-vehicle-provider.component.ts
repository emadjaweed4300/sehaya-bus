import { Component, OnInit } from '@angular/core';
import { MasterApiService } from '../../shared/master_api.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-view-vehicle-provider',
	templateUrl: './view-vehicle-provider.component.html',
	styleUrls: ['./view-vehicle-provider.component.scss']
})
export class ViewVehicleProviderComponent implements OnInit {

	vehicle_providers_list: any = [];
	vehicle_provider_id
	provider: any;

	constructor(
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) {
		this.vehicle_provider_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		this.get_vehicle_provider();
	}

	// Get All FeatureAPI
	get_vehicle_provider(){
		// this.vehicle_provider_id = this.__route.snapshot.paramMap.get('id');
		this.masterApi.get_vehicle_provider(this.vehicle_provider_id).then((response: any) => {
			console.log('Response:', response);
			this.provider = response.provider;
			// formControlName & postman response
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

}
