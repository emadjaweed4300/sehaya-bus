import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListVehicleProviderRoutingModule } from './list-vehicle-provider-routing.module';
import { ListVehicleProviderComponent } from './list-vehicle-provider.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    ListVehicleProviderRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListVehicleProviderComponent]
})
export class ListVehicleProviderModule { }
