import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListVehicleProviderComponent } from './list-vehicle-provider.component';


const routes: Routes = [
  {
    path: '',
    component: ListVehicleProviderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListVehicleProviderRoutingModule { }
