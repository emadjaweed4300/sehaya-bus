import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MasterApiService } from '../../shared/master_api.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
	selector: 'app-list-vehicle-provider',
	templateUrl: './list-vehicle-provider.component.html',
	styleUrls: ['./list-vehicle-provider.component.scss']
})
export class ListVehicleProviderComponent implements OnInit {

	@ViewChild(DataTableDirective, {static: false})
  	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	vehicle_providers_list: any = [];
	isDtInitialized:boolean = false
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private router: Router,
		private masterApi: MasterApiService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			// 		ajax: 'fake-data/datatable-data.json',
			// 		columns: [
			// 			{
			// 				title: 'Id',
			// 				data: 'id'
			// 			},
			// 			{
			// 				title: 'Name',
			// 				data: 'name'
			// 			},
			// 			{
			// 				title: 'Office',
			// 				data: 'office'
			// 			},
			// 			{
			// 				title: 'Age',
			// 				data: 'age'
			// 			}, 
			// 			{
			// 				title: 'Start Date',
			// 				data: 'date'
			// 			}, 
			// 			{
			// 				title: 'Salary',
			// 				data: 'salary'
			// 			}
			// 		],
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.list_vehicle_providers();
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	list_vehicle_providers() {
		this.masterApi.vehicle_providers().then((response: any) => {
			console.log('Response:', response);
			this.vehicle_providers_list = response.providers
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
				  dtInstance.destroy();
				  this.dtTrigger.next();
				});
			  } else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			  }
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Status Change API
	statusChange(i, id: string, event) {
		let formData = new FormData()
		formData.append('id', id)
		if (event.target.checked == true) {
			this.masterApi.vehicle_provider_active(formData).then((response: any) => {
				console.log('Response:', response);
				this.vehicle_providers_list[i].status = '1'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		} else {
			this.masterApi.vehicle_provider_inactive(formData).then((response: any) => {
				console.log('Response:', response);
				this.vehicle_providers_list[i].status = '0'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}
	}

	// delete Vehicle Provider
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if(result.value){
				this.masterApi.delete_vehicle_provider(id).then((response: any) => {
					console.log('Response:', response);
					this.list_vehicle_providers();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
			}
		})
	}

}
