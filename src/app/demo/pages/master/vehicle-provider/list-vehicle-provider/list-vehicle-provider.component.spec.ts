import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListVehicleProviderComponent } from './list-vehicle-provider.component';

describe('ListVehicleProviderComponent', () => {
  let component: ListVehicleProviderComponent;
  let fixture: ComponentFixture<ListVehicleProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListVehicleProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListVehicleProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
