import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';


@Component({
	selector: 'app-add-vehicle-provider',
	templateUrl: './add-vehicle-provider.component.html',
	styleUrls: ['./add-vehicle-provider.component.scss']
})
export class AddVehicleProviderComponent implements OnInit {

	add_vehicle_provider_frm: FormGroup;
	cities = [];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_vehicle_provider_frm = this.__fb.group({
			name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			mobile: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			telephone: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9,/]*$'), Validators.maxLength(21)]],
			city_name: ["", Validators.required], 
			address: ["", Validators.required],
		});
		this.get_cities();
	}

	// Get Cities Dropdown
	get_cities(){
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let maxLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_vehicle_provider_frm.get(formControlName).errors &&
			(this.add_vehicle_provider_frm.get(formControlName).touched ||
				this.add_vehicle_provider_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_vehicle_provider_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_vehicle_provider_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_vehicle_provider_frm.get(formControlName)
				.errors.minlength;
			maxLengthValidate = this.add_vehicle_provider_frm.get(formControlName)
				.errors.maxlength;
			notEquivalentValidate = this.add_vehicle_provider_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			maxLengthValidate: maxLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_vehicle_provider_frm);
		if (this.add_vehicle_provider_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('name', this.add_vehicle_provider_frm.get('name').value);
			form_data.append('email', this.add_vehicle_provider_frm.get('email').value);
			form_data.append('mobile', this.add_vehicle_provider_frm.get('mobile').value);
			form_data.append('telephone', this.add_vehicle_provider_frm.get('telephone').value);
			form_data.append('city_id', this.add_vehicle_provider_frm.get('city_name').value);
			form_data.append('address', this.add_vehicle_provider_frm.get('address').value);

			// console.log(this.add_vehicle_provider_frm.get('city').value)
			this.masterApi.add_vehicle_provider(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-vehicle-provider'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
			
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
