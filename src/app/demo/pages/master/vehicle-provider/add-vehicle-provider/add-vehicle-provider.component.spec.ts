import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVehicleProviderComponent } from './add-vehicle-provider.component';

describe('AddVehicleProviderComponent', () => {
  let component: AddVehicleProviderComponent;
  let fixture: ComponentFixture<AddVehicleProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVehicleProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVehicleProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
