import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddVehicleProviderComponent } from './add-vehicle-provider.component';


const routes: Routes = [
  {
    path: '',
    component: AddVehicleProviderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddVehicleProviderRoutingModule { }
