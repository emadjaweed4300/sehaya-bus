import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddDeadlineComponent } from './add-deadline.component';

const routes: Routes = [
  {
    path: '',
    component: AddDeadlineComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddDeadlineRoutingModule { }
