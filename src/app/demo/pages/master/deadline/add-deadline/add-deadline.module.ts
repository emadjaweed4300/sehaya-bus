import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddDeadlineRoutingModule } from './add-deadline-routing.module';
import { AddDeadlineComponent } from './add-deadline.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddDeadlineRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddDeadlineComponent]
})
export class AddDeadlineModule { }
