import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListDeadlineComponent } from './list-deadline.component';

const routes: Routes = [
  {
    path: '',
    component: ListDeadlineComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListDeadlineRoutingModule { }
