import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListDeadlineRoutingModule } from './list-deadline-routing.module';
import { ListDeadlineComponent } from './list-deadline.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListDeadlineRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListDeadlineComponent]
})
export class ListDeadlineModule { }
