import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDeadlineComponent } from './list-deadline.component';

describe('ListDeadlineComponent', () => {
  let component: ListDeadlineComponent;
  let fixture: ComponentFixture<ListDeadlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDeadlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDeadlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
