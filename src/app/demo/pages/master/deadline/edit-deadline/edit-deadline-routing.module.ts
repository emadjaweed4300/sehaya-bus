import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditDeadlineComponent } from './edit-deadline.component';

const routes: Routes = [
  {
    path: '',
    component: EditDeadlineComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditDeadlineRoutingModule { }
