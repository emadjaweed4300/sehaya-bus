import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';
@Component({
	selector: 'app-edit-deadline',
	templateUrl: './edit-deadline.component.html',
	styleUrls: ['./edit-deadline.component.scss']
})
export class EditDeadlineComponent implements OnInit {

	edit_deadline_frm: FormGroup;
	deadline_id
	deadline: any;
	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) { 
		this.deadline_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_deadline_frm = this.__fb.group({
			deadline: ["", Validators.required],
			radiobutton: ["", Validators.required],
			// nb_month: ["", Validators.required],
			// nb_killometer: ["", Validators.required],
		});
		this.get_deadline_by_id()
	}

	get_deadline_by_id(){
		this.masterApi.get_deadline_by_id(this.deadline_id).then((response: any) => {
			console.log('Response:', response);
			this.deadline = response.deadline;
			this.edit_deadline_frm.get('deadline').setValue(response.deadline.deadlines);
			this.edit_deadline_frm.get('radiobutton').setValue(response.deadline.type);
			// this.edit_deadline_frm.get('nb_month').setValue(response.deadline.no_of_month);
			// this.edit_deadline_frm.get('nb_killometer').setValue(response.deadline.no_of_km);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_deadline_frm.get(formControlName).errors &&
			(this.edit_deadline_frm.get(formControlName).touched ||
				this.edit_deadline_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_deadline_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_deadline_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_deadline_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_deadline_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_deadline_frm);
		if (this.edit_deadline_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('deadline_id', this.deadline_id);
			form_data.append('deadlines', this.edit_deadline_frm.get('deadline').value);
			form_data.append('type', this.edit_deadline_frm.get('radiobutton').value);
			// form_data.append('no_of_month', this.edit_deadline_frm.get('nb_month').value);
			// form_data.append('no_of_km', this.edit_deadline_frm.get('nb_killometer').value);
			this.masterApi.edit_deadline(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-deadline'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});		
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}


}
