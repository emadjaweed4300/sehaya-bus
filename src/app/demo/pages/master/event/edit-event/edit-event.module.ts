import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditEventRoutingModule } from './edit-event-routing.module';
import { EditEventComponent } from './edit-event.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditEventRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditEventComponent]
})
export class EditEventModule { }
