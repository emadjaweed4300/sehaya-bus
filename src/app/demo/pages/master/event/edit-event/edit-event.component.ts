import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { EventApiService } from '../../../events/shared/event-api.service';

@Component({
	selector: 'app-edit-event',
	templateUrl: './edit-event.component.html',
	styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {

	edit_event_manage_frm: FormGroup;
	event_id
	event_master: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private eventApi: EventApiService,
	) { 
		this.event_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_event_manage_frm = this.__fb.group({
			even_code: ["", [Validators.required, Validators.pattern('^[a-zA-Z ]+$'), Validators.maxLength(4)]],
			designation: ["", Validators.required],
			automatic: ["", Validators.required],
		});
		this.get_event_master();
	}

	get_event_master(){
		this.eventApi.get_event_master(this.event_id).then((response: any) => {
			console.log('Response:', response);
			this.event_master = response.event;
			this.edit_event_manage_frm.get('even_code').setValue(response.event.event_code);
			this.edit_event_manage_frm.get('designation').setValue(response.event.designation);
			this.edit_event_manage_frm.get('automatic').setValue(response.event.automatic);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}


	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let maxLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_event_manage_frm.get(formControlName).errors &&
			(this.edit_event_manage_frm.get(formControlName).touched ||
				this.edit_event_manage_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_event_manage_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_event_manage_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_event_manage_frm.get(formControlName)
				.errors.minlength;
			maxLengthValidate = this.edit_event_manage_frm.get(formControlName)
			.errors.maxlength;
			notEquivalentValidate = this.edit_event_manage_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			maxLengthValidate: maxLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_event_manage_frm);
		if (this.edit_event_manage_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('event_id', this.event_id);
			form_data.append('event_code', this.edit_event_manage_frm.get('even_code').value);
			form_data.append('designation', this.edit_event_manage_frm.get('designation').value);
			form_data.append('automatic', this.edit_event_manage_frm.get('automatic').value);
			this.eventApi.edit_event_master(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-event'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
