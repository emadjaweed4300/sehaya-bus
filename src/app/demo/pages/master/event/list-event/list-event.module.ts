import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListEventRoutingModule } from './list-event-routing.module';
import { ListEventComponent } from './list-event.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListEventRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListEventComponent]
})
export class ListEventModule { }
