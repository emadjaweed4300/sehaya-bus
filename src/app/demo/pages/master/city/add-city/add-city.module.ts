import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddCityRoutingModule } from './add-city-routing.module';
import { AddCityComponent } from './add-city.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { SelectModule } from 'ng-select';


@NgModule({
  imports: [
    CommonModule,
    AddCityRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    SelectModule
  ],
  declarations: [AddCityComponent]
})
export class AddCityModule { }
