import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListCityRoutingModule } from './list-city-routing.module';
import { ListCityComponent } from './list-city.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListCityRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListCityComponent]
})
export class ListCityModule { }
