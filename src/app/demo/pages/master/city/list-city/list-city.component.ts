import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MasterApiService } from '../../shared/master_api.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
	selector: 'app-list-city',
	templateUrl: './list-city.component.html',
	styleUrls: ['./list-city.component.scss']
})
export class ListCityComponent implements OnInit {

	@ViewChild(DataTableDirective, {static: false})
  	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	cities_list: any = [];
	isDtInitialized:boolean = false
	regions = [];
	// for filter
	region_id = ''
	
	clear(){
		this.region_id = '';
	}

	dtTrigger: Subject<any> = new Subject();
	constructor(
		private router: Router,
		private masterApi: MasterApiService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.list_cities();
		this.get_regions();
	}

	get_regions(){
		this.masterApi.regions().then((response: any) => {
			console.log('Response:', response);
			this.regions = response.regions
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	list_cities() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.masterApi.list_cities_filter(formData).then((response: any) => {
			console.log('Response:', response);
			this.cities_list = response.cities
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
				  dtInstance.destroy();
				  this.dtTrigger.next();
				});
			  } else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			  }
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('region_id', this.region_id);
		formData.append('add_filter', '1');
		console.log(this.region_id);
		this.masterApi.list_cities_filter(formData).then((response: any) => {
			console.log('Response:', response);
			this.cities_list = response.cities
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.list_cities()
	}

	statusChange(i, city_id: string, event) {
		let formData = new FormData()
		formData.append('id', city_id)
		if (event.target.checked == true) {
			this.masterApi.city_active(formData).then((response: any) => {
				console.log('Response:', response);
				this.cities_list[i].status = '1'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		} else {
			this.masterApi.city_inactive(formData).then((response: any) => {
				console.log('Response:', response);
				this.cities_list[i].status = '0'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}
	}

	// delete city
	public deleteAction(id: number) {
		console.log(id)
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if(result.value){
				this.masterApi.delete_city(id).then((response: any) => {
					console.log('Response:', response);
					this.list_cities();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
			}
		})
	}

}
