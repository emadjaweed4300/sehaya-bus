import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';
@Component({
	selector: 'app-edit-city',
	templateUrl: './edit-city.component.html',
	styleUrls: ['./edit-city.component.scss']
})
export class EditCityComponent implements OnInit {

	edit_city_frm: FormGroup;
	city_id
	city: any;
	// countries = [];
	regions = [];
	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) { 
		this.city_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		console.log(this.city_id)
		/** Form builder instance */
		this.edit_city_frm = this.__fb.group({
			city_name: ["", Validators.required],
			region_name: ["", Validators.required],
			// country_name: ["", Validators.required],
		});
		this.get_city();
		// this.get_countries();
		this.get_regions();
	}
	
	// get_countries(){
	// 	this.masterApi.countries().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.countries = response.countries
	// 	})
	// 	.catch((error: any) => {
	// 		console.error(error);
	// 		Swal.fire({
				// 	type: 'error',
				// 	title: 'Error!',
				// 	text: error,
				// 	confirmButtonColor: '#323258',
				// 	confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				// });
	// 	});	
	// }

	get_regions(){
		this.masterApi.regions().then((response: any) => {
			console.log('Response:', response);
			this.regions = response.regions
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	get_city(){
		this.masterApi.get_city(this.city_id).then((response: any) => {
			console.log('Response:', response);
			this.city = response.city;
			this.edit_city_frm.get('city_name').setValue(response.city.city_name);
			this.edit_city_frm.get('region_name').setValue(response.city.region_id);
			// this.edit_city_frm.get('country_name').setValue(response.city.country_id);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_city_frm.get(formControlName).errors &&
			(this.edit_city_frm.get(formControlName).touched ||
				this.edit_city_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_city_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_city_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_city_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_city_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_city_frm);
		if (this.edit_city_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('city', this.edit_city_frm.get('city_name').value);
			form_data.append('region_id', this.edit_city_frm.get('region_name').value);
			form_data.append('country_id', '0')
			form_data.append('city_id', this.city_id);
			this.masterApi.edit_city(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-city'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});		
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
