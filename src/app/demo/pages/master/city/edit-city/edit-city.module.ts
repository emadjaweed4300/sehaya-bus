import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditCityRoutingModule } from './edit-city-routing.module';
import { EditCityComponent } from './edit-city.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [ 
    CommonModule,
    EditCityRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditCityComponent]
})
export class EditCityModule { }
