import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewCityRoutingModule } from './view-city-routing.module';
import { ViewCityComponent } from './view-city.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  imports: [
    CommonModule,
    ViewCityRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule
  ],
  declarations: [ViewCityComponent]
})
export class ViewCityModule { }
