import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRegionRoutingModule } from './edit-region-routing.module';
import { EditRegionComponent } from './edit-region.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditRegionRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditRegionComponent]
})
export class EditRegionModule { }
