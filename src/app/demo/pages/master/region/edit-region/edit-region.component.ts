import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';
@Component({
	selector: 'app-edit-region',
	templateUrl: './edit-region.component.html',
	styleUrls: ['./edit-region.component.scss']
})
export class EditRegionComponent implements OnInit {

	edit_region_frm: FormGroup;
	region_id
	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) {
		this.region_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		/** Form builder instance */
		this.edit_region_frm = this.__fb.group({
			region_name: ["", Validators.required]
		});
		this.get_region();
	}

	get_region(){
		this.masterApi.get_region(this.region_id).then((response: any) => {
			console.log('Response:', response);
			this.edit_region_frm.get('region_name').setValue(response.region.regions);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_region_frm.get(formControlName).errors &&
			(this.edit_region_frm.get(formControlName).touched ||
				this.edit_region_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_region_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_region_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_region_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_region_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_region_frm);
		if (this.edit_region_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('region_id', this.region_id);
			form_data.append('region', this.edit_region_frm.get('region_name').value);
			this.masterApi.edit_region(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-region'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});		
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
