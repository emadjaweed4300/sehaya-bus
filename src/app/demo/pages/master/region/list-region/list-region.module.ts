import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRegionRoutingModule } from './list-region-routing.module';
import { ListRegionComponent } from './list-region.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListRegionRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListRegionComponent]
})
export class ListRegionModule { }
