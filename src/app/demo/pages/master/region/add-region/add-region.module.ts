import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddRegionRoutingModule } from './add-region-routing.module';
import { AddRegionComponent } from './add-region.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddRegionRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddRegionComponent]
})
export class AddRegionModule { }
