import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewRegionRoutingModule } from './view-region-routing.module';
import { ViewRegionComponent } from './view-region.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  imports: [
    CommonModule,
    ViewRegionRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule
  ],
  declarations: [ViewRegionComponent]
})
export class ViewRegionModule { }
