import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewRegionComponent } from './view-region.component';

const routes: Routes = [
  {
    path: '',
    component: ViewRegionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewRegionRoutingModule { }
