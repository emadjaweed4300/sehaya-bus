import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditVehicleCategoryComponent } from './edit-vehicle-category.component';

import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { EditVehicleCategoryRoutingModule } from './edit-vehicle-category-routing.module';




@NgModule({
  imports: [
    CommonModule,
    EditVehicleCategoryRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditVehicleCategoryComponent]
})
export class EditVehicleCategoryModule { }
