import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-vehicle-category',
	templateUrl: './edit-vehicle-category.component.html',
	styleUrls: ['./edit-vehicle-category.component.scss']
})
export class EditVehicleCategoryComponent implements OnInit {

	edit_vehicle_category_frm: FormGroup;
	id
	vehicle_Categorys: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) {
		this.id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		console.log(this.id)
		/** Form builder instance */
		this.edit_vehicle_category_frm = this.__fb.group({
			vehicle_category_name: ["", Validators.required]
		});
		this.get_vehicle_category();
	}

	// Get All Vehicle Category API
	get_vehicle_category(){
		this.masterApi.get_vehicle_category(this.id).then((response: any) => {
			console.log('Response:', response);
			this.vehicle_Categorys = response.vehicle_category;
			this.edit_vehicle_category_frm.get('vehicle_category_name').setValue(response.vehicle_category.category_name);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_vehicle_category_frm.get(formControlName).errors &&
			(this.edit_vehicle_category_frm.get(formControlName).touched ||
				this.edit_vehicle_category_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_vehicle_category_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_vehicle_category_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_vehicle_category_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_vehicle_category_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_vehicle_category_frm);
		if (this.edit_vehicle_category_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('user_id', null);
			// category_id (postman)
			form_data.append('category_id', this.id);
			// category (Postman) and vehicle_category_name (FormCOnrolName)
			form_data.append('category', this.edit_vehicle_category_frm.get('vehicle_category_name').value);
			this.masterApi.edit_vehicle_category(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-vehicle-category'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
