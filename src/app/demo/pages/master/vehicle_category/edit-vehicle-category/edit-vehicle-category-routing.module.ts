import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditVehicleCategoryComponent } from './edit-vehicle-category.component';

const routes: Routes = [
  {
    path: '',
    component: EditVehicleCategoryComponent 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]  
})
export class EditVehicleCategoryRoutingModule { }
 