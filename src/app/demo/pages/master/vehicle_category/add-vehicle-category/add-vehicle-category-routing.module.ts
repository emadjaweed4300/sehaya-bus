import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddVehicleCategoryComponent } from './add-vehicle-category.component';


const routes: Routes = [
  {
    path: '',
    component: AddVehicleCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddVehicleCategoryRoutingModule { }
