import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddVehicleCategoryRoutingModule } from './add-vehicle-category-routing.module';
import { AddVehicleCategoryComponent } from './add-vehicle-category.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    AddVehicleCategoryRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddVehicleCategoryComponent]
})
export class AddVehicleCategoryModule { }
