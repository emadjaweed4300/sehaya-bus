import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { ListVehicleCategoryComponent } from './list-vehicle-category.component';
import { ListVehicleCategoryComponent } from './list-vehicle-category.component'
import { ListVehicleCategoryRoutingModule } from './list-vehicle-category-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';




@NgModule({
  imports: [
    CommonModule,
    ListVehicleCategoryRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListVehicleCategoryComponent] 
})
export class ListVehicleCategoryModule { }
