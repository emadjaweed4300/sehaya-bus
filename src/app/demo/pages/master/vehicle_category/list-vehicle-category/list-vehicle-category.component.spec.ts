import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListVehicleCategoryComponent } from './list-vehicle-category.component';

describe('ListVehicleCategoryComponent', () => {
  let component: ListVehicleCategoryComponent;
  let fixture: ComponentFixture<ListVehicleCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListVehicleCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListVehicleCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
