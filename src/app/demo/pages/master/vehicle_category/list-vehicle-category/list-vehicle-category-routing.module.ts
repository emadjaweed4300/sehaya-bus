import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListVehicleCategoryComponent } from './list-vehicle-category.component';


const routes: Routes = [
  {
    path: '',
    component: ListVehicleCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListVehicleCategoryRoutingModule { }
