import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListMovementRoutingModule } from './list-movement-routing.module';
import { ListMovementComponent } from './list-movement.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListMovementRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListMovementComponent]
})
export class ListMovementModule { }
