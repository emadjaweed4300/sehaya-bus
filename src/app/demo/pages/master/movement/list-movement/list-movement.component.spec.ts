import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMovementComponent } from './list-movement.component';

describe('ListMovementComponent', () => {
  let component: ListMovementComponent;
  let fixture: ComponentFixture<ListMovementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMovementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMovementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
