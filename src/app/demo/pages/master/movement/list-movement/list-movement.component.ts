import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ManageMovementApiService } from '../../../manage-movement/shared/manage-movement-api.service';

@Component({
	selector: 'app-list-movement',
	templateUrl: './list-movement.component.html',
	styleUrls: ['./list-movement.component.scss']
})
export class ListMovementComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	movementList: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	constructor(
		private movementApi: ManageMovementApiService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.list_movement()
	}

	list_movement() {
		this.movementApi.movement_list().then((response: any) => {
			console.log('Response:', response);
			this.movementList = response.routes
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	statusChange(i, movement_id: string, event) {
		let formData = new FormData()
		formData.append('route_id', movement_id)
		if (event.target.checked == true) {
			this.movementApi.movement_active(formData).then((response: any) => {
				console.log('Response:', response);
				this.movementList[i].status = '1'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		} else {
			this.movementApi.movement_inactive(formData).then((response: any) => {
				console.log('Response:', response);
				this.movementList[i].status = '0'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}
	}

	// delete Brand
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		})
	}

}
