import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListMovementComponent } from './list-movement.component';

const routes: Routes = [
  {
    path: '',
    component: ListMovementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListMovementRoutingModule { }
