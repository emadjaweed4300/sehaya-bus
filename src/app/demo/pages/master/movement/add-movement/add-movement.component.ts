import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { ManageMovementApiService } from '../../../manage-movement/shared/manage-movement-api.service';
export class DynamicGrid {
	city: string;
	NbDays: string;
}
@Component({
	selector: 'app-add-movement',
	templateUrl: './add-movement.component.html',
	styleUrls: ['./add-movement.component.scss']
})
export class AddMovementComponent implements OnInit {

	add_movement_frm: FormGroup;
	cities = [];
	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService,
		private movementApi: ManageMovementApiService,
		private translate: TranslateService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_movement_frm = this.__fb.group({
			movement_name: ["", Validators.required],
			no_of_days: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			arrayRow: this.__fb.array([
				this.getRowArray()
			])
		});
		this.get_cities()
	}

	// Get Cities Dropdown 
	get_cities() {
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	getRowArray() {
		const numberPatern = '^[0-9]+$';
		return this.__fb.group({
			city_id: ["", Validators.required],
			no_of_days: ["", [Validators.required, Validators.pattern(numberPatern)]],
		});
	}

	addRow() {
		const control = <FormArray>this.add_movement_frm.controls['arrayRow'];
		control.push(this.getRowArray());
		return true;
	}

	deleteRow(index) {
		const control = <FormArray>this.add_movement_frm.controls['arrayRow'];
		if (control.length == 1) {

		} else {
			control.removeAt(index);
		}
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_movement_frm.get(formControlName).errors &&
			(this.add_movement_frm.get(formControlName).touched ||
				this.add_movement_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_movement_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_movement_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_movement_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_movement_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save() {
		this.markFormGroupTouched(this.add_movement_frm);
		if (this.add_movement_frm.valid) {
			let totalNbDays
			this.add_movement_frm.get('arrayRow').value.map(tag => tag.no_of_days).reduce((a, b) => totalNbDays = parseInt(a) + parseInt(b), 0);
			console.log(totalNbDays)
			if (totalNbDays !== parseInt(this.add_movement_frm.get('no_of_days').value)) {
				var msg = ''
				this.translate.get('add_movement.Please provide total number days in fields').subscribe(val => {
					msg = val
				})
				return Swal.fire({
					type: 'warning',
					title: 'Warning!',
					text: msg,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			}
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('route_name', this.add_movement_frm.get('movement_name').value);
			form_data.append('no_of_days', this.add_movement_frm.get('no_of_days').value);
			form_data.append('route', JSON.stringify(this.add_movement_frm.get('arrayRow').value));
			console.log(this.add_movement_frm.get('arrayRow').value[0].no_of_days)
			this.movementApi.add_movement(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				this.router.navigate(['/master/list-movement'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
