import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { data } from 'jquery';
import { ManageMovementApiService } from '../../../manage-movement/shared/manage-movement-api.service';
@Component({
	selector: 'app-edit-movement',
	templateUrl: './edit-movement.component.html',
	styleUrls: ['./edit-movement.component.scss']
})
export class EditMovementComponent implements OnInit {

	edit_movement_frm: FormGroup;
	cities = [];
	movement_id
	movement: any
	constructor(
		private __fb: FormBuilder,
		private __route: ActivatedRoute,
		private router: Router,
		private masterApi: MasterApiService,
		private movementApi: ManageMovementApiService,
		private translate: TranslateService
	) {
		this.movement_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_movement_frm = this.__fb.group({
			movement_name: ["", Validators.required],
			no_of_days: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			arrayRow: this.__fb.array([
				// this.getRowArray()
			])
		});
		this.get_cities();
		this.get_movement_by_id();
	}

	// Get Cities Dropdown
	get_cities() {
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	getRowArray() {
		const numberPatern = '^[0-9]+$';
		return this.__fb.group({
			mapping_id: ["0"],
			city_id: ["", Validators.required],
			no_of_days: ["", [Validators.required, Validators.pattern(numberPatern)]],
		});
	}

	addRow() {
		const control = <FormArray>this.edit_movement_frm.controls['arrayRow'];
		control.push(this.getRowArray());
		return true;
	}

	deleteRow(index, mapping_id) {
		const control = <FormArray>this.edit_movement_frm.controls['arrayRow'];
		if (control.length == 1) {

		} else {
			Swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#323258',
				cancelButtonColor: '#ff1f1f',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
			}).then(result => {
				if (result.value) {
					let form_data = new FormData();
					form_data.append('route_master', this.movement_id);
					form_data.append('route_id', mapping_id);
					console.log(form_data.get('route_id'),form_data.get('route_master'))
					this.movementApi.delete_root_row(form_data).then((response: any) => {
						console.log('Response:', response);
						control.removeAt(index);
						Swal.fire({
							type: 'success',
							title: 'Deleted!',
							text: response.message,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
	
					})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
							type: 'error',
							title: 'Error!',
							text: error,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
					});
				}
			})
		}

	}

	get_movement_by_id() {
		this.movementApi.get_movement_by_id(this.movement_id).then((response: any) => {
			console.log('Response:', response);
			this.movement = response.route;
			this.edit_movement_frm.get('movement_name').setValue(response.route.master_route.route_name);
			this.edit_movement_frm.get('no_of_days').setValue(response.route.master_route.no_of_days);

			const control = <FormArray>this.edit_movement_frm.controls['arrayRow'];
			for (let item of this.movement.mapping_route) {
				control.push(this.patchValues(item.id, item.destination, item.no_of_days_planned))
			}
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	patchValues(id, city_id, days) {
		const numberPatern = '^[0-9]+$';
		return this.__fb.group({
			mapping_id: [id],
			city_id: [city_id, Validators.required],
			no_of_days: [days, [Validators.required, Validators.pattern(numberPatern)]]
		})
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_movement_frm.get(formControlName).errors &&
			(this.edit_movement_frm.get(formControlName).touched ||
				this.edit_movement_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_movement_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_movement_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_movement_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_movement_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save() {
		console.log(this.edit_movement_frm.get('arrayRow').value);
		this.markFormGroupTouched(this.edit_movement_frm);
		if (this.edit_movement_frm.valid) {
			let totalNbDays
			this.edit_movement_frm.get('arrayRow').value.map(tag => tag.no_of_days).reduce((a, b) => totalNbDays = parseInt(a) + parseInt(b), 0);
			console.log(totalNbDays)
			if (totalNbDays !== parseInt(this.edit_movement_frm.get('no_of_days').value)) {
				var msg = ''
				this.translate.get('add_movement.Please provide total number days in fields').subscribe(val => {
					msg = val
				})
				return Swal.fire({
					type: 'warning',
					title: 'Warning!',
					text: msg,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			}
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('route_id', this.movement_id);
			// form_data.append('mapping_route_id', mapping_id);
			form_data.append('route_name', this.edit_movement_frm.get('movement_name').value);
			form_data.append('no_of_days', this.edit_movement_frm.get('no_of_days').value);
			// form_data.append('destination', city_id);
			// form_data.append('no_of_days_planned', no_of_days_planned);
			form_data.append('route', JSON.stringify(this.edit_movement_frm.get('arrayRow').value));
			console.log((this.edit_movement_frm.get('arrayRow').value));
			this.movementApi.edit_movement(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				this.router.navigate(['/master/list-movement'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
