import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditMovementRoutingModule } from './edit-movement-routing.module';
import { EditMovementComponent } from './edit-movement.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditMovementRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditMovementComponent]
})
export class EditMovementModule { }
