import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditMovementComponent } from './edit-movement.component';

const routes: Routes = [
  {
    path: '',
    component: EditMovementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditMovementRoutingModule { }
