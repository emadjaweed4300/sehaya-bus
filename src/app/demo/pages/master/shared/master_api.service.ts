import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { END_POINT } from '../../../../constant';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global.service';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class MasterApiService {

	// API path
	//   base_path = 'http://nooridev.in/hapush/sehayabus';

	data = new FormData();
	constructor(
		private http: HttpClient,
		private translate: TranslateService,
		private global: GlobalService,
		private router: Router
	) {
		this.data.append('user_id', this.global.currentUser.id);
		this.data.append('user_role', this.global.currentUser.user_role);
		this.data.append('security_key', this.global.currentUser.token);
		// this.data.append('email', this.global.currentUser.email);
	}

	// Http Options
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	}

	// Handle API errors
	handleError(error: HttpErrorResponse) {

		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${JSON.stringify(error.status)}, ` +
				`body was: ${JSON.stringify(error.message)}`);
		}
		// return an observable with a user-facing error message
		if (error.status == 0) {
			return throwError(
				Swal.fire(
					'The Internet?',
					'Please try again later',
					'question'
				)
			);
		}
	};

	// Add city
	add_city(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-city', data)
	}

	edit_city(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-city', data)
	}

	list_cities() {
		return this._callAPI('/cities', this.data)
	}
	list_cities_filter(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/cities', data)
	}

	city_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/city-active', data)
	}

	city_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/city-inactive', data)
	}

	delete_city(id) {
		return this._callAPI('/delete-city/' + id, this.data)
	}

	get_city(id) {
		return this._callAPI('/get-city/' + id, this.data)
	}

	countries() {
		return this._callAPI('/countries', this.data)
	}

	regions() {
		return this._callAPI('/regions', this.data)
	}

	add_region(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-region', data)
	}

	delete_region(id) {
		return this._callAPI('/delete-region/' + id, this.data)
	}

	region_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/region-active', data)
	}

	region_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/region-inactive', data)
	}

	get_region(id) {
		return this._callAPI('/get-region/' + id, this.data)
	}

	edit_region(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-region', data)
	}

	add_user_role(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-user-role', data)
	}

	list_user_role() {
		return this._callAPI('/user-roles', this.data)
	}

	delete_user_role(id) {
		return this._callAPI('/delete-user-role/' + id, this.data)
	}

	user_role_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/user-role-active', data)
	}

	user_role_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/user-role-inactive', data)
	}

	get_user_role(id) {
		return this._callAPI('/get-user-role/' + id, this.data)
	}

	edit_user_role(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-user-role', data)
	}

	// Add Brand
	add_brands(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-brand', data)
	}
	// Brands
	brands() {
		return this._callAPI('/brands', this.data)
	}
	// Delete Brand
	delete_brand(id) {
		return this._callAPI('/delete-brand/' + id, this.data)
	}
	// Status Active
	brand_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/brand-active', data)
	}
	// Status Deactive
	brand_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/brand-inactive', data)
	}
	// Edit Brand
	edit_brand(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-brand', data)
	}
	// Get Brand
	get_brand(id) {
		return this._callAPI('/get-brand/' + id, this.data)
	}

	// Add Vehicle Category
	add_vehicle_category(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-vehicle-category', data)
	}
	// Vehicle Categorys
	vehicle_categorys() {
		return this._callAPI('/vehicle-categories', this.data)
	}
	// Delete Vehicle Category
	delete_vehicle_category(id) {
		return this._callAPI('/delete-vehicle-category/' + id, this.data)
	}
	// Status Active
	vehicle_category_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/vehicle-category-active', data)
	}
	// Status Deactive
	vehicle_category_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/vehicle-category-inactive', data)
	}
	// Edit Vehicle Category
	edit_vehicle_category(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-vehicle-category', data)
	}
	// Get Vehicle Category
	get_vehicle_category(id) {
		return this._callAPI('/get-vehicle_category/' + id, this.data)
	}

	// Add Client Category
	add_client_category(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-client-category', data)
	}
	// Client Categorys
	client_categorys() {
		return this._callAPI('/client-category', this.data)
	}
	// Delete Client Category
	delete_client_category(id) {
		return this._callAPI('/delete-client-category/' + id, this.data)
	}
	// Status Active
	client_category_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/client-category-active', data)
	}
	// Status Deactive
	client_category_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/client-category-inactive', data)
	}
	// Edit Client Category
	edit_client_category(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-client-category', data)
	}
	// Get Client Category
	get_client_category(id) {
		return this._callAPI('/get-client-category/' + id, this.data)
	}


	// Claim Type API's
	// Add Claim Type
	add_claim_type(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-claim-type', data)
	}
	// Claim Types
	list_claim_types() {
		return this._callAPI('/claim-type', this.data)
	}
	// Delete Claim Type
	delete_claim_type(id) {
		return this._callAPI('/delete-claim-type/' + id, this.data)
	}
	// Status Active
	claim_type_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/claim-type-active', data)
	}
	// Status Deactive
	claim_type_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/claim-type-inactive', data)
	}
	// Edit Claim Type
	edit_claim_type(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-claim-type', data)
	}
	// Get Claim Type
	get_claim_type(id) {
		return this._callAPI('/get-claim-type/' + id, this.data)
	}



	// Vehicle Provider API's
	// Add Claim Type
	add_vehicle_provider(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-vehicle-provider', data)
	}
	// Claim Types
	vehicle_providers() {
		return this._callAPI('/vehicle-providers', this.data)
	}
	// Delete Claim Type
	delete_vehicle_provider(id) {
		return this._callAPI('/delete-vehicle-provider/' + id, this.data)
	}
	// Status Active
	vehicle_provider_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/vehicle-provider-active', data)
	}
	// Status Deactive
	vehicle_provider_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/vehicle-provider-inactive', data)
	}
	// Edit Claim Type
	edit_vehicle_provider(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-vehicle-provider', data)
	}
	// Get Claim Type
	get_vehicle_provider(id) {
		return this._callAPI('/get-vehicle-provider/' + id, this.data)
	}



	// Features API's
	// Add Features
	add_features(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-feature', data)
	}
	// Features
	features() {
		return this._callAPI('/features', this.data)
	}
	// Delete Features
	delete_features(id) {
		return this._callAPI('/delete-feature/' + id, this.data)
	}
	// Status Active
	features_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/feature-active', data)
	}
	// Status Deactive
	features_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/feature-inactive', data)
	}
	// Edit Features
	edit_features(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-feature', data)
	}
	// Get Features
	get_features(id) {
		return this._callAPI('/get-feature/' + id, this.data)
 	}
  
   // Add Staff Function
   add_staff_function(data) {
    data.append('user_id', this.global.currentUser.id);
    data.append('user_role', this.global.currentUser.user_role);
	data.append('security_key', this.global.currentUser.token)
    return this._callAPI('/add-staff_function', data)
  }

  // Staff Functions list
  list_staff_function() {
    return this._callAPI('/staff-functions', this.data)
  }
  
  // Staff Functions active
  staff_function_active(data) {
    data.append('user_id', this.global.currentUser.id);
    data.append('user_role', this.global.currentUser.user_role);
	data.append('security_key', this.global.currentUser.token)
    return this._callAPI('/staff-function-active', data)
  }

  // Staff Functions inactive
  staff_function_inactive(data) {
    data.append('user_id', this.global.currentUser.id);
    data.append('user_role', this.global.currentUser.user_role);
	data.append('security_key', this.global.currentUser.token)
    return this._callAPI('/staff-function-inactive', data)
  }

  // Staff Functions delete
  delete_staff_function(id) {
    return this._callAPI('/delete-staff-function/' + id, this.data)
  }
  // get staff function
  get_staff_function(id) {
    return this._callAPI('/get-staff-function/' + id, this.data)
  }

  // edit staff function
  edit_staff_function(data) {
    data.append('user_id', this.global.currentUser.id);
    data.append('user_role', this.global.currentUser.user_role);
	data.append('security_key', this.global.currentUser.token)
    return this._callAPI('/edit-staff-function', data)
  }

  // Unit API's
	// Add Unit
	add_unit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-unit', data)
	}
	// Unit
	units() {
		return this._callAPI('/units', this.data)
	}
	// Delete Unit
	delete_unit(id) {
		return this._callAPI('/delete-unit/' + id, this.data)
	}
	// Status Active
	unit_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/unit-active', data)
	}
	// Status Deactive
	unit_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/unit-inactive', data)
	}
	// Edit Unit
	edit_unit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-unit', data)
	}
	// Get Unit
	get_unit(id) {
		return this._callAPI('/get-unit/' + id, this.data)
  }
 	 //Deadline API
	// Add city
	add_deadline(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-deadline', data)
	}

	// List Deadline
	list_deadline() {
		return this._callAPI('/deadlines', this.data)
	}

	// active Deadline
	deadline_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/deadline-active', data)
	}

	// Inactive Deadline
	deadline_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/deadline-inactive', data)
	}

	// delete Deadline 
	delete_deadline(id) {
		return this._callAPI('/delete-deadline/' + id, this.data)
	}

	// get deadline by id
	get_deadline_by_id(id) {
		return this._callAPI('/get-deadline/' + id, this.data)
	}

	// Edit deadline 
	edit_deadline(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-deadline', data)
	}

	// Add Location
	add_locations(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-location-type', data)
	}
	// locations
	locations() {
		return this._callAPI('/location-types', this.data)
	}
	// Delete location
	delete_location(id) {
		return this._callAPI('/delete-location-type/' + id, this.data)
	}
	// Status Active
	location_active(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/location-type-active', data)
	}
	// Status Deactive
	location_inactive(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/location-type-inactive', data)
	}
	// Edit location
	edit_location(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-location-type', data)
	}
	// Get location
	get_location(id) {
		return this._callAPI('/get-location-type/' + id, this.data)
	}

	_callAPI(url: string, params?) {
		return new Promise((resolve, reject) => {
			this.http.post(END_POINT + url, params)
				.subscribe(res => {
					if (res['response'] === 'success') {
						resolve(res)
					} else {
						if (res['response_code'] == 405 || res['response_code'] == 403 || res['response_code'] == 204){
							localStorage.clear();
							return window.location.replace('/auth/signin');
						}
						reject(res['message'])
					}
				}, (this.handleError))
		})
	}

}