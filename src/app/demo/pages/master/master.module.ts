import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterRoutingModule } from './master-routing.module';

// import { AddBrandComponent } from './add-brand/add-brand.component';
// import { ViewBrandComponent } from './view-brand/view-brand.component';
// import { ListBrandComponent } from './list-brand/list-brand.component';
// import { EditBrandComponent } from './edit-brand/edit-brand.component';


@NgModule({
  imports: [
    CommonModule,
    MasterRoutingModule,
  ],
  declarations: []
})
export class MasterModule { }
