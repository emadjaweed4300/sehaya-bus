import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClientCategoryComponent } from './add-client-category.component';

describe('AddClientCategoryComponent', () => {
  let component: AddClientCategoryComponent;
  let fixture: ComponentFixture<AddClientCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClientCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClientCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
