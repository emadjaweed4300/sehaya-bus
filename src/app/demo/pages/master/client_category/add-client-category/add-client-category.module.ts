import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddClientCategoryRoutingModule } from './add-client-category-routing.module';
import { AddClientCategoryComponent } from './add-client-category.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    AddClientCategoryRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddClientCategoryComponent]
})
export class AddClientCategoryModule { }
