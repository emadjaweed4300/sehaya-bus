import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddClientCategoryComponent } from './add-client-category.component';


const routes: Routes = [
  {
    path: '',
    component: AddClientCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddClientCategoryRoutingModule { }
