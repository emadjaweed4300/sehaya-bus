import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-add-client-category',
	templateUrl: './add-client-category.component.html',
	styleUrls: ['./add-client-category.component.scss']
})
export class AddClientCategoryComponent implements OnInit {

	add_client_category_frm: FormGroup;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_client_category_frm = this.__fb.group({
			client_category: ["", Validators.required]
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_client_category_frm.get(formControlName).errors &&
			(this.add_client_category_frm.get(formControlName).touched ||
				this.add_client_category_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_client_category_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_client_category_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_client_category_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_client_category_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_client_category_frm);
		if (this.add_client_category_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('client_category', this.add_client_category_frm.get('client_category').value);
			this.masterApi.add_client_category(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-client-category'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
