import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListClientCategoryComponent } from './list-client-category.component';


const routes: Routes = [
  {
    path: '',
    component: ListClientCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListClientCategoryRoutingModule { }
