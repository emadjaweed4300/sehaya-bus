import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListClientCategoryRoutingModule } from './list-client-category-routing.module';
import { ListClientCategoryComponent } from './list-client-category.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    ListClientCategoryRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListClientCategoryComponent]
})
export class ListClientCategoryModule { }
