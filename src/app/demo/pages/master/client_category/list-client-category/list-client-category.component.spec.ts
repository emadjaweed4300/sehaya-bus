import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListClientCategoryComponent } from './list-client-category.component';

describe('ListClientCategoryComponent', () => {
  let component: ListClientCategoryComponent;
  let fixture: ComponentFixture<ListClientCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListClientCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListClientCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
