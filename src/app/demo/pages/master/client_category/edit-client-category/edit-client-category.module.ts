import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditClientCategoryRoutingModule } from './edit-client-category-routing.module';
import { EditClientCategoryComponent } from './edit-client-category.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    EditClientCategoryRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditClientCategoryComponent]
})
export class EditClientCategoryModule { }
