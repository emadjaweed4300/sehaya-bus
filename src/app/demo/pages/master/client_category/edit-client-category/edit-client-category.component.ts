import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-client-category',
	templateUrl: './edit-client-category.component.html',
	styleUrls: ['./edit-client-category.component.scss']
})
export class EditClientCategoryComponent implements OnInit {

	edit_client_category_frm: FormGroup;
	id
	client_Categorys: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) { 
		this.id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_client_category_frm = this.__fb.group({
			client_category: ["", Validators.required]
		});
		this.get_client_category();
	}

	// Get All Client Category API
	get_client_category(){
		this.masterApi.get_client_category(this.id).then((response: any) => {
			console.log('Response:', response);
			this.client_Categorys = response.client_category;
			this.edit_client_category_frm.get('client_category').setValue(response.client_category.client_category);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_client_category_frm.get(formControlName).errors &&
			(this.edit_client_category_frm.get(formControlName).touched ||
				this.edit_client_category_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_client_category_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_client_category_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_client_category_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_client_category_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_client_category_frm);
		if (this.edit_client_category_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// id(postman) 
			form_data.append('category_id', this.id);
			// client_category (postman)
			form_data.append('client_category', this.edit_client_category_frm.get('client_category').value);
			this.masterApi.edit_client_category(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-client-category'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
