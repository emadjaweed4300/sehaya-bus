import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditClientCategoryComponent } from './edit-client-category.component';


const routes: Routes = [
  {
    path: '',
    component: EditClientCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditClientCategoryRoutingModule { }
