import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditClientCategoryComponent } from './edit-client-category.component';

describe('EditClientCategoryComponent', () => {
  let component: EditClientCategoryComponent;
  let fixture: ComponentFixture<EditClientCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditClientCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditClientCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
