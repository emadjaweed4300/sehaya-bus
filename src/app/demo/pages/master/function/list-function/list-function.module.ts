import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListFunctionRoutingModule } from './list-function-routing.module';
import { ListFunctionComponent } from './list-function.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListFunctionRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListFunctionComponent]
})
export class ListFunctionModule { }
