import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MasterApiService } from '../../shared/master_api.service';

@Component({
	selector: 'app-list-function',
	templateUrl: './list-function.component.html',
	styleUrls: ['./list-function.component.scss']
})
export class ListFunctionComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	staff_function_list: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private masterApi: MasterApiService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.list_staff_function()
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	list_staff_function() {
		this.masterApi.list_staff_function().then((response: any) => {
			console.log('Response:', response);
			this.staff_function_list = response.functions
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	statusChange(i, staff_function_id: string, event) {
		let formData = new FormData()
		formData.append('id', staff_function_id)
		if (event.target.checked == true) {
			this.masterApi.staff_function_active(formData).then((response: any) => {
				console.log('Response:', response);
				this.staff_function_list[i].status = '1'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		} else {
			this.masterApi.staff_function_inactive(formData).then((response: any) => {
				console.log('Response:', response);
				this.staff_function_list[i].status = '0'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}
	}

	// delete staff function
	public deleteAction(id: number) {
		console.log(id)
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.masterApi.delete_staff_function(id).then((response: any) => {
					console.log('Response:', response);
					this.list_staff_function();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
					});
			}
		})
	}

}
