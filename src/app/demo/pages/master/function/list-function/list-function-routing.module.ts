import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListFunctionComponent } from './list-function.component';

const routes: Routes = [
  {
    path: '',
    component: ListFunctionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],  
  exports: [RouterModule]
})
export class ListFunctionRoutingModule { }
