import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-edit-function',
  templateUrl: './edit-function.component.html',
  styleUrls: ['./edit-function.component.scss']
})
export class EditFunctionComponent implements OnInit {

  edit_function_frm: FormGroup;
  staff_function_id
	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) { 
		this.staff_function_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_function_frm = this.__fb.group({
			function_name: ["", Validators.required]
		});
		this.get_staff_function()
	}

	get_staff_function(){
		this.masterApi.get_staff_function(this.staff_function_id).then((response: any) => {
			console.log('Response:', response);
			this.edit_function_frm.get('function_name').setValue(response.function.function_name);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_function_frm.get(formControlName).errors &&
			(this.edit_function_frm.get(formControlName).touched ||
				this.edit_function_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_function_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_function_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_function_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_function_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call edit service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_function_frm);
		if (this.edit_function_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('function_id', this.staff_function_id);
			form_data.append('function', this.edit_function_frm.get('function_name').value);
			this.masterApi.edit_staff_function(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-function'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});		
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
