import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditFunctionRoutingModule } from './edit-function-routing.module';
import { EditFunctionComponent } from './edit-function.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditFunctionRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditFunctionComponent]
})
export class EditFunctionModule { }
