import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditFunctionComponent } from './edit-function.component';

const routes: Routes = [
  {
    path: '',
    component: EditFunctionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditFunctionRoutingModule { }
