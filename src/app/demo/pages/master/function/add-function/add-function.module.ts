import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddFunctionRoutingModule } from './add-function-routing.module';
import { AddFunctionComponent } from './add-function.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddFunctionRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddFunctionComponent]
})
export class AddFunctionModule { }
