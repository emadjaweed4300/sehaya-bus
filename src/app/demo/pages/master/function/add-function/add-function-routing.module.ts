import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddFunctionComponent } from './add-function.component';

const routes: Routes = [
  {
    path: '',
    component: AddFunctionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddFunctionRoutingModule { }
