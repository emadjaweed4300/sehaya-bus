import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddUnitRoutingModule } from './add-unit-routing.module';
import { AddUnitComponent } from './add-unit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddUnitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddUnitComponent]
})
export class AddUnitModule { }
