import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditUnitComponent } from './edit-unit.component';

const routes: Routes = [
  {
    path: '',
    component: EditUnitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditUnitRoutingModule { }
