import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditUnitRoutingModule } from './edit-unit-routing.module';
import { EditUnitComponent } from './edit-unit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditUnitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditUnitComponent]
})
export class EditUnitModule { }
