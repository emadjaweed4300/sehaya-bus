import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-unit',
	templateUrl: './edit-unit.component.html',
	styleUrls: ['./edit-unit.component.scss']
})
export class EditUnitComponent implements OnInit {

	edit_unit_frm: FormGroup;
	id
	unit: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) {
		this.id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_unit_frm = this.__fb.group({
			unit: ["", Validators.required]
		});
		this.get_unit();
	}

	// Get All Unit API
	get_unit(){
		this.masterApi.get_unit(this.id).then((response: any) => {
			console.log('Response:', response);
			this.unit = response.unit;
			// unit(formControlName) & unit(response:unit) & unit (response:unit.unit)
			this.edit_unit_frm.get('unit').setValue(response.unit.unit);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_unit_frm.get(formControlName).errors &&
			(this.edit_unit_frm.get(formControlName).touched ||
				this.edit_unit_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_unit_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_unit_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_unit_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_unit_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_unit_frm);
		if (this.edit_unit_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('unit_id', this.id);
			form_data.append('unit', this.edit_unit_frm.get('unit').value);
			this.masterApi.edit_unit(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-unit'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
