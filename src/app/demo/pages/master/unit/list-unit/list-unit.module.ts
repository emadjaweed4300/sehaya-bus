import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListUnitRoutingModule } from './list-unit-routing.module';
import { ListUnitComponent } from './list-unit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListUnitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListUnitComponent]
})
export class ListUnitModule { }
