import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MasterApiService } from '../../shared/master_api.service';

@Component({
	selector: 'app-list-unit',
	templateUrl: './list-unit.component.html',
	styleUrls: ['./list-unit.component.scss']
})
export class ListUnitComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	unit_list: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	dtExportButtonOptions: any = {};

	constructor(
		private masterApi: MasterApiService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			// 		ajax: 'fake-data/datatable-data.json',
			// 		columns: [
			// 			{
			// 				title: 'Id',
			// 				data: 'id'
			// 			},
			// 			{
			// 				title: 'Name',
			// 				data: 'name'
			// 			},
			// 			{
			// 				title: 'Office',
			// 				data: 'office'
			// 			},
			// 			{
			// 				title: 'Age',
			// 				data: 'age'
			// 			}, 
			// 			{
			// 				title: 'Start Date',
			// 				data: 'date'
			// 			}, 
			// 			{
			// 				title: 'Salary',
			// 				data: 'salary'
			// 			}
			// 		],
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.list_Units();
	}

	// Table Data not found message function
	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// List Data API
	list_Units() {
		this.masterApi.units().then((response: any) => {
			console.log('Response:', response);
			this.unit_list = response.units
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	// Status API
	statusChange(i, id: string, event) {
		let formData = new FormData()
		formData.append('id', id)
		if (event.target.checked == true) {
			this.masterApi.unit_active(formData).then((response: any) => {
				console.log('Response:', response);
				this.unit_list[i].status = '1'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		} else {
			this.masterApi.unit_inactive(formData).then((response: any) => {
				console.log('Response:', response);
				this.unit_list[i].status = '0'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}
	}

	// delete Unit
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.masterApi.delete_unit(id).then((response: any) => {
					console.log('Response:', response);
					this.list_Units();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
			}
		})
	}

}
