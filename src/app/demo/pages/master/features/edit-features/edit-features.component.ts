import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-features',
	templateUrl: './edit-features.component.html',
	styleUrls: ['./edit-features.component.scss']
})
export class EditFeaturesComponent implements OnInit {

	edit_features_frm: FormGroup;
	id
	features: any;
	units = [];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService
	) { 
		this.id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		console.log(this.id)
		/** Form builder instance */
		this.edit_features_frm = this.__fb.group({
			features: ["", Validators.required],
			unit: ["", Validators.required],
		});
		this.get_feature();
		this.get_unit();
	}

	// Get All FeatureAPI
	get_feature(){
		this.masterApi.get_features(this.id).then((response: any) => {
			console.log('Response:', response);
			this.features = response.feature;
			// features(formControlName) & feature(response:feature) & feature (response:feature.feature)
			this.edit_features_frm.get('features').setValue(response.feature.feature);
			this.edit_features_frm.get('unit').setValue(response.feature.unit_id);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	get_unit(){
		this.masterApi.units().then((response: any) => {
			console.log('Response:', response);
			this.units = response.units
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});	
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_features_frm.get(formControlName).errors &&
			(this.edit_features_frm.get(formControlName).touched ||
				this.edit_features_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_features_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_features_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_features_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_features_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_features_frm);
		if (this.edit_features_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// feature_id(postman) 
			form_data.append('feature_id', this.id);
			// feature (postman) & features (formCOntrolName)
			form_data.append('feature', this.edit_features_frm.get('features').value);
			form_data.append('unit_id', this.edit_features_frm.get('unit').value);
			this.masterApi.edit_features(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-features'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
