import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditFeaturesComponent } from './edit-features.component';

const routes: Routes = [
  {
    path: '',
    component: EditFeaturesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditFeaturesRoutingModule { }
