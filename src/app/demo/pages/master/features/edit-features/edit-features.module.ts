import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditFeaturesRoutingModule } from './edit-features-routing.module';
import { EditFeaturesComponent } from './edit-features.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditFeaturesRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditFeaturesComponent]
})
export class EditFeaturesModule { }
