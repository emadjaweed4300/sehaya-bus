import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListFeaturesComponent } from './list-features.component';

const routes: Routes = [
  {
    path: '',
    component: ListFeaturesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListFeaturesRoutingModule { }
