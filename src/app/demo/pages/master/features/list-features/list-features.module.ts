import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListFeaturesRoutingModule } from './list-features-routing.module';
import { ListFeaturesComponent } from './list-features.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListFeaturesRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListFeaturesComponent]
})
export class ListFeaturesModule { }
