import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MasterApiService } from '../../shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-add-features',
	templateUrl: './add-features.component.html',
	styleUrls: ['./add-features.component.scss']
})
export class AddFeaturesComponent implements OnInit {

	add_features_frm: FormGroup;
	units = [];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_features_frm = this.__fb.group({
			features: ["", Validators.required],
			unit: ["", Validators.required],
		});
		this.get_unit();
	}

	get_unit(){
		this.masterApi.units().then((response: any) => {
			console.log('Response:', response);
			this.units = response.units
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_features_frm.get(formControlName).errors &&
			(this.add_features_frm.get(formControlName).touched ||
				this.add_features_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_features_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_features_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_features_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_features_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_features_frm);
		if (this.add_features_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// feature(postman) & features (formControlName)
			form_data.append('feature', this.add_features_frm.get('features').value);
			form_data.append('unit_id', this.add_features_frm.get('unit').value);
			this.masterApi.add_features(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/master/list-features'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}


}
