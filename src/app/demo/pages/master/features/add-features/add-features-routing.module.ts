import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddFeaturesComponent } from './add-features.component';

const routes: Routes = [
  {
    path: '',
    component: AddFeaturesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddFeaturesRoutingModule { }
