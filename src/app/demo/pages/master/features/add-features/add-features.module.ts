import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddFeaturesRoutingModule } from './add-features-routing.module';
import { AddFeaturesComponent } from './add-features.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddFeaturesRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddFeaturesComponent]
})
export class AddFeaturesModule { }
