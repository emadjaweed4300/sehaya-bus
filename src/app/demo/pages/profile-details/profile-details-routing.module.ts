import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Profile view,edit components
			{
				path: 'view-profile-info',
				loadChildren: () => import('./profile-info/view-profile-info/view-profile-info.module').then(module => module.ViewProfileInfoModule)
			},

			// Profile Personal Info edit components
			{
				path: 'edit-personal-info',
				loadChildren: () => import('./profile-info/edit-personal-info/edit-personal-info.module').then(module => module.EditPersonalInfoModule)
			},

			// Profile Contact Info edit components
			{
				path: 'edit-contact-info',
				loadChildren: () => import('./profile-info/edit-contact-info/edit-contact-info.module').then(module => module.EditContactInfoModule)
			},

			// Profile Admin Personal Info edit components
			{
				path: 'edit-admin-personal-info',
				loadChildren: () => import('./profile-info/edit-admin-personal-info/edit-admin-personal-info.module').then(module => module.EditAdminPersonalInfoModule)
			},
			// Profile staff Member Personal Info edit components
			{
				path: 'edit-staff-member-personal-info',
				loadChildren: () => import('./profile-info/edit-staff-member-personal-info/edit-staff-member-personal-info.module').then(module => module.EditStaffMemberPersonalInfoModule)
			},
		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ProfileDetailsRoutingModule { }
