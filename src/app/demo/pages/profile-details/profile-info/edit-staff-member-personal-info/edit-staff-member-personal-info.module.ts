import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { NgbTabsetModule, NgbDropdownModule, NgbTooltipModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { LightboxModule } from 'ngx-lightbox';
import { EditStaffMemberPersonalInfoRoutingModule } from './edit-staff-member-personal-info-routing.module';
import { EditStaffMemberPersonalInfoComponent } from './edit-staff-member-personal-info.component';


@NgModule({
  imports: [
    CommonModule,
    EditStaffMemberPersonalInfoRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    NgbTabsetModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbCarouselModule,
    LightboxModule
  ],
  declarations: [EditStaffMemberPersonalInfoComponent]
})
export class EditStaffMemberPersonalInfoModule { }
