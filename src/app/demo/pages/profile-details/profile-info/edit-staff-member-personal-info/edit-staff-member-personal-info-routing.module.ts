import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditStaffMemberPersonalInfoComponent } from './edit-staff-member-personal-info.component';

const routes: Routes = [
  {
    path: '',
    component: EditStaffMemberPersonalInfoComponent 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditStaffMemberPersonalInfoRoutingModule { }
