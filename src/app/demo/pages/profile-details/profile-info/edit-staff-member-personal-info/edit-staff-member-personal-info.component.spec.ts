import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStaffMemberPersonalInfoComponent } from './edit-staff-member-personal-info.component';

describe('EditStaffMemberPersonalInfoComponent', () => {
  let component: EditStaffMemberPersonalInfoComponent;
  let fixture: ComponentFixture<EditStaffMemberPersonalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditStaffMemberPersonalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStaffMemberPersonalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
