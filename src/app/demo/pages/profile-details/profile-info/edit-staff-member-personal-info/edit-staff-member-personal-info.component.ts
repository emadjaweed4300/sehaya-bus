import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileApiService } from '../../shared/profile-api.service';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { END_POINT } from 'src/app/constant';

@Component({
	selector: 'app-edit-staff-member-personal-info',
	templateUrl: './edit-staff-member-personal-info.component.html',
	styleUrls: ['./edit-staff-member-personal-info.component.scss']
})
export class EditStaffMemberPersonalInfoComponent implements OnInit {

	edit_admin_personal_info_frm: FormGroup;
	cities = [];
	countries = [];
	personal_info: any;

	imageSrc: any;
	file: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService,
		private profileApiService: ProfileApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.edit_admin_personal_info_frm = this.__fb.group({
			full_name: ["", Validators.required],
			city_name: ["", Validators.required],
			marital_status: ["", Validators.required],
			logo: [""],
			phone:['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
		});
		this.get_staff_member_personal_info();
		this.get_cities();
	}

	// Get All Profile Detail API
	get_staff_member_personal_info() {
		this.profileApiService.get_staff_member_profile_details().then((response: any) => {
			console.log('Response:', response);
			console.log(response.staff_member.companies_logo);
			this.imageSrc = END_POINT + '/' + response.staff_member.companies_logo
			this.personal_info = response.staff_member;
			this.edit_admin_personal_info_frm.get('full_name').setValue(response.staff_member.name);
			this.edit_admin_personal_info_frm.get('city_name').setValue(response.staff_member.city);
			this.edit_admin_personal_info_frm.get('marital_status').setValue(response.staff_member.marital_status);
			this.edit_admin_personal_info_frm.get('phone').setValue(response.staff_member.phone);
			console.log(this.edit_admin_personal_info_frm.get('logo').value)
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// get All Countries
	get_cities() {
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_admin_personal_info_frm.get(formControlName).errors &&
			(this.edit_admin_personal_info_frm.get(formControlName).touched ||
				this.edit_admin_personal_info_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_admin_personal_info_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_admin_personal_info_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_admin_personal_info_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_admin_personal_info_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_admin_personal_info_frm);
		if (this.edit_admin_personal_info_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('name', this.edit_admin_personal_info_frm.get('full_name').value);
			form_data.append('city_id', this.edit_admin_personal_info_frm.get('city_name').value);
			if (this.edit_admin_personal_info_frm.get('logo').value !== '') {
				form_data.append('picture', this.file, this.file.name);
			}
			// form_data.append('picture', this.edit_admin_personal_info_frm.get('logo').value);
			// console.log(this.edit_admin_personal_info_frm.get('logo').value)
			form_data.append('marital_status', this.edit_admin_personal_info_frm.get('marital_status').value);
			form_data.append('phone', this.edit_admin_personal_info_frm.get('phone').value);
			this.profileApiService.edit_staff_member_personal_info(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				this.router.navigate(['/profile-details/view-profile-info'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	/**
	 * @name onFileChange
	 * @param file_event
	 * @param file_name
	 * @description This function will call on file on change and allocate the file value to formgroup's
	 * formcontrol.
	 */
	public onFileChange(file_event: any, file_name: string) {
		this.file = file_event.target.files[0];

		const reader = new FileReader();
		reader.onload = e => this.imageSrc = reader.result;

		reader.readAsDataURL(this.file);
		console.log(this.file)
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
