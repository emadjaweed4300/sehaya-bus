import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { Router } from '@angular/router';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import {IAlbum, IEvent, Lightbox, LIGHTBOX_EVENT, LightboxConfig, LightboxEvent} from 'ngx-lightbox';
import {Subscription} from 'rxjs';
import { ProfileApiService } from '../../shared/profile-api.service';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global.service';

@Component({
	selector: 'app-view-profile-info',
	templateUrl: './view-profile-info.component.html',
	styleUrls: ['./view-profile-info.component.scss']
})
export class ViewProfileInfoComponent implements OnInit {

	// url: string | ArrayBuffer;

	// onSelectFile(event) {
	// 	if (event.target.files && event.target.files[0]) {
	// 	var reader = new FileReader();

	// 	reader.readAsDataURL(event.target.files[0]); // read file as data url

	// 	reader.onload = (event) => { 
	// 		console.log(event);
	// 		// called once readAsDataURL is completed
	// 		this.url = event.target.result;
	// 	}
	// 	}
	// }

	// edit_profile_frm: FormGroup;

	// constructor(
	// 	private __fb: FormBuilder,
	// 	private router: Router,
	// ) { }

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	profile_details: any;
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	staff_functions = [];
	claim_types = [];
	all_claim_users = [];
	cities = [];

	public activeTab: string;

	public editProfile: boolean;
	public editProfileIcon: string;

	public editContact: boolean;
	public editContactIcon: string;

	public editOtherInfo: boolean;
	public editOtherInfoIcon: string;

	public albums: Array<IAlbum>;
	private subscription: Subscription;

	Change_Password_frm: FormGroup;

	constructor(
		private lightbox: Lightbox, 
		private lightboxEvent: LightboxEvent, 
		private lighboxConfig: LightboxConfig,
		private formBuilder: FormBuilder,
		private profiletApi: ProfileApiService,
		private masterApi: MasterApiService,
		private translate: TranslateService,
		private router: Router,
		public global: GlobalService,
		) {
		this.activeTab = 'profile';

		this.editProfile = false;
		this.editProfileIcon = 'icon-edit-2';

		this.editContact = false;
		this.editContactIcon = 'icon-edit-2';

		this.editOtherInfo = false;
		this.editOtherInfoIcon = 'icon-edit-2';

		this.albums = [];
		for (let i = 1; i <= 6; i++) {
			const album = {
				src: 'assets/images/light-box/l' + i + '.jpg',
				caption: 'Image ' + i + ' caption here',
				thumb: 'assets/images/light-box/sl' + i + '.jpg'
			};

			this.albums.push(album);
		}
		lighboxConfig.fadeDuration = 1;
	}

	ngOnInit() {
		this.Change_Password_frm = this.formBuilder.group({
			old_password: [null, [Validators.required]],
			// new_password: [null, [Validators.required, Validators.minLength(8)]], 
			new_password: ["", [Validators.required, Validators.minLength(8)]], 
			confirm_password: [null, [Validators.required, (control) => this.Match(control, this.Change_Password_frm, 'new_password')]],
		});
		this.get_profile();
	}

	open(index: number): void {
		this.subscription = this.lightboxEvent.lightboxEvent$.subscribe((event: IEvent) => this._onReceivedEvent(event));
		this.lightbox.open(this.albums, index, { wrapAround: true, showImageNumberLabel: true });
	}

	private _onReceivedEvent(event: IEvent): void {
		if (event.id === LIGHTBOX_EVENT.CLOSE) {
			this.subscription.unsubscribe();
		}
	}

	// Passwrod Match Function
	Match(fc: FormControl, za: FormGroup, zb: string) {
		if (fc.value === null) {
			return (null)
		}
		if (fc.value !== za.get(zb).value) {
			return { Match: true }
		} else {
			return (null)
		}
	}

		/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.Change_Password_frm.get(formControlName).errors &&
			(this.Change_Password_frm.get(formControlName).touched ||
				this.Change_Password_frm.get(formControlName).dirty);
		if (errors) {
			required = this.Change_Password_frm.get(formControlName).errors
				.required;
			patternValidate = this.Change_Password_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.Change_Password_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.Change_Password_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	// changePassword() {
	// 	this.markFormGroupTouched(this.Change_Password_frm);
	// 	if (this.Change_Password_frm.valid) {
	// 	}
	// }

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// List Data API
	// get_profile() {
	// 	this.profiletApi.get_profile_details().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.profile_details = response.user_details
	// 		if (this.isDtInitialized) {
	// 			this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
	// 				dtInstance.destroy();
	// 				this.dtTrigger.next();
	// 			});
	// 		} else {
	// 			this.isDtInitialized = true
	// 			this.dtTrigger.next();
	// 		}
	// 	})
	// 		.catch((error: any) => {
	// 			console.error(error);
	// 			Swal.fire({
				// 	type: 'error',
				// 	title: 'Error!',
				// 	text: error,
				// 	confirmButtonColor: '#323258',
				// 	confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				// });
	// 		});
	// }

	// Get Profile Data
	get_profile(){
		if(this.global.currentUser.user_role == '2' || this.global.currentUser.user_role == '3'){
			this.profiletApi.get_profile_details().then((response: any) => {
				console.log('Response:', response);
				this.profile_details = response.user_details;
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		} else if(this.global.currentUser.user_role == '1'){
			this.profiletApi.get_admin_profile_details().then((response: any) => {
				console.log('Response:', response);
				this.profile_details = response.admin;
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
		// Staff Member 
		else if(this.global.currentUser.user_role == '4'){
			this.profiletApi.get_staff_member_profile_details().then((response: any) => {
				console.log('Response:', response);
				this.profile_details = response.staff_member;
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
		
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public changePassword(): void {
		this.markFormGroupTouched(this.Change_Password_frm);
		if (this.Change_Password_frm.valid) {
			if(this.global.currentUser.user_role == '2' || this.global.currentUser.user_role == '3'){
				/** Prepare form data */
				const form_data = new FormData();
				form_data.append('old_password', this.Change_Password_frm.get('old_password').value);
				form_data.append('new_password', this.Change_Password_frm.get('new_password').value);
				form_data.append('confirm_password', this.Change_Password_frm.get('confirm_password').value);
				this.profiletApi.change_password(form_data).then((response: any) => {
					console.log('Response:', response);
					Swal.fire({
						type: 'success',
						title: 'Success',
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
						text: response.message
					});
					this.router.navigate(['/dashboard/analytics'])
				})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
			} else if(this.global.currentUser.user_role == '1'){
				/** Prepare form data */
				const form_data = new FormData();
				form_data.append('old_password', this.Change_Password_frm.get('old_password').value);
				form_data.append('new_password', this.Change_Password_frm.get('new_password').value);
				form_data.append('confirm_password', this.Change_Password_frm.get('confirm_password').value);
				this.profiletApi.admin_change_password(form_data).then((response: any) => {
					console.log('Response:', response);
					Swal.fire({
						type: 'success',
						title: 'Success',
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
						text: response.message
					});
					this.router.navigate(['/dashboard/analytics'])
				})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
			}
			// Staff Member Change Password
			else if(this.global.currentUser.user_role == '4'){
				/** Prepare form data */
				const form_data = new FormData();
				form_data.append('old_password', this.Change_Password_frm.get('old_password').value);
				form_data.append('new_password', this.Change_Password_frm.get('new_password').value);
				form_data.append('confirm_password', this.Change_Password_frm.get('confirm_password').value);
				this.profiletApi.staff_change_password(form_data).then((response: any) => {
					console.log('Response:', response);
					Swal.fire({
						type: 'success',
						title: 'Success',
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
						text: response.message
					});
					this.router.navigate(['/dashboard/analytics'])
				})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
			}
		}
	}

	/**
	   * Marks all controls in a form group as touched
	   * @param formGroup The form group to touch
	   */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
