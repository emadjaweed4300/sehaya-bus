import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewProfileInfoComponent } from './view-profile-info.component';

const routes: Routes = [
  {
    path: '',
    component: ViewProfileInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewProfileInfoRoutingModule { }
