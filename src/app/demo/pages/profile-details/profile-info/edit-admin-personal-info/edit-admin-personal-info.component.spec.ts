import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAdminPersonalInfoComponent } from './edit-admin-personal-info.component';

describe('EditAdminPersonalInfoComponent', () => {
  let component: EditAdminPersonalInfoComponent;
  let fixture: ComponentFixture<EditAdminPersonalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAdminPersonalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAdminPersonalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
