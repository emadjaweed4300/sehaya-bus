import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditAdminPersonalInfoComponent } from './edit-admin-personal-info.component';

const routes: Routes = [
  {
    path: '',
    component: EditAdminPersonalInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditAdminPersonalInfoRoutingModule { }
