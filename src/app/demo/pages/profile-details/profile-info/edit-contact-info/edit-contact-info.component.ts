import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { ProfileApiService } from '../../shared/profile-api.service';

@Component({
	selector: 'app-edit-contact-info',
	templateUrl: './edit-contact-info.component.html',
	styleUrls: ['./edit-contact-info.component.scss']
})
export class EditContactInfoComponent implements OnInit {

	edit_contact_info_frm: FormGroup;
	contact_info: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private profileApiService: ProfileApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.edit_contact_info_frm = this.__fb.group({
			phone: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			website: ["", Validators.required],
			fax_number: ["", Validators.required],
		});
		this.get_contact_info();
	}

	// Get All Profile Detail API
	get_contact_info() {
		this.profileApiService.get_profile_details().then((response: any) => {
			console.log('Response:', response);
			this.contact_info = response.user_details;
			this.edit_contact_info_frm.get('phone').setValue(response.user_details.phone);
			this.edit_contact_info_frm.get('email').setValue(response.user_details.email);
			this.edit_contact_info_frm.get('website').setValue(response.user_details.website);
			this.edit_contact_info_frm.get('fax_number').setValue(response.user_details.fax_number);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_contact_info_frm.get(formControlName).errors &&
			(this.edit_contact_info_frm.get(formControlName).touched ||
				this.edit_contact_info_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_contact_info_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_contact_info_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_contact_info_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_contact_info_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_contact_info_frm);
		if (this.edit_contact_info_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('phone', this.edit_contact_info_frm.get('phone').value);
			form_data.append('email', this.edit_contact_info_frm.get('email').value);
			form_data.append('website', this.edit_contact_info_frm.get('website').value);
			form_data.append('fax_number', this.edit_contact_info_frm.get('fax_number').value);
			this.profileApiService.edit_contact_info(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/profile-details/view-profile-info'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
