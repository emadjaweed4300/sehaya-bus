import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditContactInfoComponent } from './edit-contact-info.component';

const routes: Routes = [
  {
    path: '',
    component: EditContactInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditContactInfoRoutingModule { }
