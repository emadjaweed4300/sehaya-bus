import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditPersonalInfoComponent } from './edit-personal-info.component';

const routes: Routes = [
  {
    path: '',
    component: EditPersonalInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditPersonalInfoRoutingModule { }
