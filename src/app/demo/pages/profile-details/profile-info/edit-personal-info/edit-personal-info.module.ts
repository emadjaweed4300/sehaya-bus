import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditPersonalInfoRoutingModule } from './edit-personal-info-routing.module';
import { EditPersonalInfoComponent } from './edit-personal-info.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { NgbTabsetModule, NgbDropdownModule, NgbTooltipModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { LightboxModule } from 'ngx-lightbox';


@NgModule({
  imports: [
    CommonModule,
    EditPersonalInfoRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    NgbTabsetModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbCarouselModule,
    LightboxModule
  ],
  declarations: [EditPersonalInfoComponent]
})
export class EditPersonalInfoModule { }
