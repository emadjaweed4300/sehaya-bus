import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControlName } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { ProfileApiService } from '../../shared/profile-api.service';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { END_POINT } from 'src/app/constant';

@Component({
	selector: 'app-edit-personal-info',
	templateUrl: './edit-personal-info.component.html',
	styleUrls: ['./edit-personal-info.component.scss']
})
export class EditPersonalInfoComponent implements OnInit {

	edit_personal_info_frm: FormGroup;
	cities = [];
	regions = [];
	personal_info: any;

	imageSrc: any;
	file: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService,
		private profileApiService: ProfileApiService
	) {	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_personal_info_frm = this.__fb.group({
			full_name: ["", Validators.required],
			city_name: ["", Validators.required],
			region_name: ["", Validators.required],
			logo: [""],
			address: ["", Validators.required],
		});
		this.get_personal_info();
		this.get_cities();
		this.get_regions();
	}

	// Get All Profile Detail API
	get_personal_info() {
		this.profileApiService.get_profile_details().then((response: any) => {
			console.log('Response:', response);
			console.log(response.user_details.companies_logo);
			this.imageSrc=END_POINT + '/' + response.user_details.companies_logo
			this.personal_info = response.user_details;
			this.edit_personal_info_frm.get('full_name').setValue(response.user_details.name);
			this.edit_personal_info_frm.get('city_name').setValue(response.user_details.city);
			this.edit_personal_info_frm.get('region_name').setValue(response.user_details.region);
			this.edit_personal_info_frm.get('address').setValue(response.user_details.address);
			console.log(this.edit_personal_info_frm.get('logo').value)
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

	// get All Countries
	get_cities(){
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});	
	}

	// get All Regions
	get_regions(){
		this.masterApi.regions().then((response: any) => {
			console.log('Response:', response);
			this.regions = response.regions
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});	
	}


	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_personal_info_frm.get(formControlName).errors &&
			(this.edit_personal_info_frm.get(formControlName).touched ||
				this.edit_personal_info_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_personal_info_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_personal_info_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_personal_info_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_personal_info_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_personal_info_frm);
		if (this.edit_personal_info_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('name', this.edit_personal_info_frm.get('full_name').value);
			form_data.append('city_id', this.edit_personal_info_frm.get('city_name').value);
			form_data.append('region_id', this.edit_personal_info_frm.get('region_name').value);
			if (this.edit_personal_info_frm.get('logo').value !== '') {
				form_data.append('picture', this.file, this.file.name);
			}
			// form_data.append('picture', this.edit_personal_info_frm.get('logo').value);
			// console.log(this.edit_personal_info_frm.get('logo').value)
			form_data.append('location', this.edit_personal_info_frm.get('address').value);
			this.profileApiService.edit_personal_info(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/profile-details/view-profile-info'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * @name onFileChange
	 * @param file_event
	 * @param file_name
	 * @description This function will call on file on change and allocate the file value to formgroup's
	 * formcontrol.
	 */
	public onFileChange(file_event: any, file_name: string) {
			this.file = file_event.target.files[0];
		
			const reader = new FileReader();
			reader.onload = e => this.imageSrc = reader.result;

			reader.readAsDataURL(this.file);
			console.log(this.file)
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
