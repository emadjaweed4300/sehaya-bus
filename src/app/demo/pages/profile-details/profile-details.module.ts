import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileDetailsRoutingModule } from './profile-details-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ProfileDetailsRoutingModule,
  ],
  declarations: []
})
export class ProfileDetailsModule { }
