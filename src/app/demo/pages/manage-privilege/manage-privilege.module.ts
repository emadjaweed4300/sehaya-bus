import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagePrivilegeRoutingModule } from './manage-privilege-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ManagePrivilegeRoutingModule,
  ],
  declarations: []
})
export class ManagePrivilegeModule { }
