import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Privilege Tracking list,add,edit components
			{
				path: 'list-privilege',
				loadChildren: () => import('./privilege/list-privilege/list-privilege.module').then(module => module.ListPrivilegeModule)
			},
			{
				path: 'add-privilege',
				loadChildren: () => import('./privilege/add-privilege/add-privilege.module').then(module => module.AddPrivilegeModule)
			},
			{
				path: 'edit-privilege/:id',
				loadChildren: () => import('./privilege/edit-privilege/edit-privilege.module').then(module => module.EditPrivilegeModule)
			},
		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManagePrivilegeRoutingModule { }
