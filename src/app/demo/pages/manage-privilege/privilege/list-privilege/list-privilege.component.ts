import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { PriviligeApiService } from '../../shared/privilige-api.service';

@Component({
	selector: 'app-list-privilege',
	templateUrl: './list-privilege.component.html',
	styleUrls: ['./list-privilege.component.scss']
})
export class ListPrivilegeComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	privilege_list: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private priviligeApiService: PriviligeApiService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				"<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			buttons: [
				{
					extend: 'copy',
					text: '<i class="fa fa-copy" style="color: white;"></i>',
					titleAttr: 'Copy',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				},
				{
					extend: 'print',
					text: '<i class="fa fa-print" style="color: white;"></i>',
					titleAttr: 'Print',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				},
				{
					extend: 'excel',
					text: '<i class="fa fa-file-excel" style="color: white;"></i>',
					titleAttr: 'Excel',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				},
				{
					extend: 'csv',
					text: '<i class="fa fa-file-csv" style="color: white;"></i>',
					titleAttr: 'CSV',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				}
			],
			responsive: true
		};
		this.list_privileges();
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	list_privileges() {
		this.priviligeApiService.privileges().then((response: any) => {
			console.log('Response:', response);
			this.privilege_list = response.privileges
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
				  dtInstance.destroy();
				  this.dtTrigger.next();
				});
			  } else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			  }
		})
			.catch((error: any) => {
				console.error(error);
			});
	}

}
