import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListPrivilegeRoutingModule } from './list-privilege-routing.module';
import { ListPrivilegeComponent } from './list-privilege.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListPrivilegeRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListPrivilegeComponent]
})
export class ListPrivilegeModule { }
