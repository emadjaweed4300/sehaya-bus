import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPrivilegeComponent } from './list-privilege.component';

const routes: Routes = [
  {
    path: '',
    component: ListPrivilegeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListPrivilegeRoutingModule { }
