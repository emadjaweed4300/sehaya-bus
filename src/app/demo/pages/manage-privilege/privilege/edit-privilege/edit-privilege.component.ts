import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PriviligeApiService } from '../../shared/privilige-api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-privilege',
	templateUrl: './edit-privilege.component.html',
	styleUrls: ['./edit-privilege.component.scss']
})
export class EditPrivilegeComponent implements OnInit {

	edit_privilege_frm: FormGroup;
	staff_members = [];
	user_privilege_details: any;
	user_privilege_id

	// Ng-select accept only value (value: 1) and label (label: 'List')
	// Brand
	master = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '5', label: 'Active' },
		{ value: '6', label: 'Inactive' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	vehicle = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];

	clients = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '5', label: 'Active' },
		{ value: '6', label: 'Inactive' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	staff_member = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '5', label: 'Active' },
		{ value: '6', label: 'Inactive' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	circuits = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	claim_circuit = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	expense_circuit = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '9', label: 'Share' },
	];
	receipt_circuit = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '9', label: 'Share' },
	];
	event_circuit = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
	];
	bank_account = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '8', label: 'View' },
	];
	invoices = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '10', label: 'Download' },
		{ value: '11', label: 'View Cancel' },
	];
	pay_bills = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '10', label: 'Download' },
	];
	cash_movement = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
	];
	statement = [
		{ value: '10', label: 'Download' },
	];
	cash_desk = [
		{ value: '1', label: 'List' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '12', label: 'Opening' },
		{ value: '13', label: 'Closing' },
	]

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private priviligeApiService: PriviligeApiService,
		private __route: ActivatedRoute,
	) {
		this.user_privilege_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		/** Form builder instance */
		this.edit_privilege_frm = this.__fb.group({
			staff_member_name: [{ value: '', disabled: true }, Validators.required],
			// dashboard: ["", ""],
			Brand: ["", ""],
			Region : ["", ""],
			City : ["", ""],
			Vehicle_Category : ["", ""],
			Vehicle_Carrier: ["", ""],
			Claim_Category: ["", ""],
			User_Type: ["", ""],
			Client_Category: ["", ""],
			Functions: ["", ""],
			Vehicle_Deadline: ["", ""],
			Units: ["", ""],
			Vehicle_Features: ["", ""],
			Manage_Events: ["", ""],
			Movement: ["", ""],
			Location: ["", ""],
			Vehicles: ["", ""],
			Clients: ["", ""],
			Staff_Member: ["", ""],
			Circuits: ["", ""],
			Claim_Circuit: ["", ""],
			Expense_Circuit: ["", ""],
			Receipt_Circuit: ["", ""],
			Event_Circuit: ["", ""],
			Bank_Account: ["", ""],
			Invoices: ["", ""],
			Pay_Bills: ["", ""],
			Cash_Movement: ["", ""],
			Statement: ["", ""],
			Cash_Desk: ["", ""],
		});
		this.get_staff_member();
		this.get_user_privileges();
		console.log(this.user_privilege_id)
	}

	// Get Staff Member
	get_staff_member(){
		this.priviligeApiService.staff_members().then((response: any) => {
			console.log('Response:', response);
			this.staff_members = response.staff_member
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});	
	}

	
	get_user_privileges(){
		this.priviligeApiService.get_user_privileges(this.user_privilege_id).then((response: any) => {
			console.log('Response:', response);
			this.user_privilege_details = response.privilege;
			// name (formControlName) & name (postman)
			this.edit_privilege_frm.get('staff_member_name').setValue(response.privilege.staff_member_id);
			this.edit_privilege_frm.get('Brand').setValue(response.privilege.brand);
			// this.edit_privilege_frm.get('Brand').setValue([1,2]);
			this.edit_privilege_frm.get('Region').setValue(response.privilege.region);
			this.edit_privilege_frm.get('City').setValue(response.privilege.city);
			this.edit_privilege_frm.get('Vehicle_Category').setValue(response.privilege.vehicle_category);
			this.edit_privilege_frm.get('Vehicle_Carrier').setValue(response.privilege.vehicle_carrier);
			this.edit_privilege_frm.get('Claim_Category').setValue(response.privilege.claim_category);
			this.edit_privilege_frm.get('User_Type').setValue(response.privilege.user_type);
			this.edit_privilege_frm.get('Client_Category').setValue(response.privilege.client_category);
			this.edit_privilege_frm.get('Functions').setValue(response.privilege.functions);

			this.edit_privilege_frm.get('Vehicle_Deadline').setValue(response.privilege.vehicle_deadline);
			this.edit_privilege_frm.get('Units').setValue(response.privilege.units);
			this.edit_privilege_frm.get('Vehicle_Features').setValue(response.privilege.vehicle_features);
			this.edit_privilege_frm.get('Manage_Events').setValue(response.privilege.manage_events);
			this.edit_privilege_frm.get('Movement').setValue(response.privilege.movement);
			this.edit_privilege_frm.get('Location').setValue(response.privilege.location);
			this.edit_privilege_frm.get('Vehicles').setValue(response.privilege.vehicles);
			this.edit_privilege_frm.get('Clients').setValue(response.privilege.clients);
			this.edit_privilege_frm.get('Staff_Member').setValue(response.privilege.staff_member);
			this.edit_privilege_frm.get('Circuits').setValue(response.privilege.circuits);
			this.edit_privilege_frm.get('Claim_Circuit').setValue(response.privilege.claim_circuit);
			this.edit_privilege_frm.get('Expense_Circuit').setValue(response.privilege.expense_circuit);
			this.edit_privilege_frm.get('Receipt_Circuit').setValue(response.privilege.receipt_circuit);
			this.edit_privilege_frm.get('Event_Circuit').setValue(response.privilege.event_circuit);
			this.edit_privilege_frm.get('Bank_Account').setValue(response.privilege.bank_account);
			this.edit_privilege_frm.get('Invoices').setValue(response.privilege.invoices);
			this.edit_privilege_frm.get('Pay_Bills').setValue(response.privilege.pay_bills);
			this.edit_privilege_frm.get('Cash_Movement').setValue(response.privilege.cash_movement);
			this.edit_privilege_frm.get('Statement').setValue(response.privilege.statement);
			this.edit_privilege_frm.get('Cash_Desk').setValue(response.privilege.cash_desk);

		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_privilege_frm.get(formControlName).errors &&
			(this.edit_privilege_frm.get(formControlName).touched ||
				this.edit_privilege_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_privilege_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_privilege_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_privilege_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_privilege_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_privilege_frm);
		if (this.edit_privilege_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('privilege_id', this.user_privilege_id);
			console.log(this.user_privilege_id)
			form_data.append('id', this.edit_privilege_frm.get('staff_member_name').value);
			form_data.append('Brand', this.edit_privilege_frm.get('Brand').value);
			form_data.append('Region', this.edit_privilege_frm.get('Region').value);
			// console.log(this.edit_privilege_frm.get('Brand').value);
			// console.log(this.edit_privilege_frm.get('Region').value);

			form_data.append('City', this.edit_privilege_frm.get('City').value);
			form_data.append('Vehicle_Category', this.edit_privilege_frm.get('Vehicle_Category').value);
			form_data.append('Vehicle_Carrier', this.edit_privilege_frm.get('Vehicle_Carrier').value);
			form_data.append('Claim_Category', this.edit_privilege_frm.get('Claim_Category').value);
			form_data.append('User_Type', this.edit_privilege_frm.get('User_Type').value);
			form_data.append('Client_Category', this.edit_privilege_frm.get('Client_Category').value);
			form_data.append('Functions', this.edit_privilege_frm.get('Functions').value);
			form_data.append('Vehicle_Deadline', this.edit_privilege_frm.get('Vehicle_Deadline').value);
			form_data.append('Units', this.edit_privilege_frm.get('Units').value);
			form_data.append('Vehicle_Features', this.edit_privilege_frm.get('Vehicle_Features').value);
			form_data.append('Manage_Events', this.edit_privilege_frm.get('Manage_Events').value);
			form_data.append('Movement', this.edit_privilege_frm.get('Movement').value);
			form_data.append('Location', this.edit_privilege_frm.get('Location').value);
			form_data.append('Vehicles', this.edit_privilege_frm.get('Vehicles').value);
			form_data.append('Clients', this.edit_privilege_frm.get('Clients').value);
			form_data.append('Staff_Member', this.edit_privilege_frm.get('Staff_Member').value);
			form_data.append('Circuits', this.edit_privilege_frm.get('Circuits').value);
			form_data.append('Claim_Circuit', this.edit_privilege_frm.get('Claim_Circuit').value);
			form_data.append('Expense_Circuit', this.edit_privilege_frm.get('Expense_Circuit').value);
			form_data.append('Receipt_Circuit', this.edit_privilege_frm.get('Receipt_Circuit').value);
			form_data.append('Event_Circuit', this.edit_privilege_frm.get('Event_Circuit').value);
			form_data.append('Bank_Account', this.edit_privilege_frm.get('Bank_Account').value);
			form_data.append('Invoices', this.edit_privilege_frm.get('Invoices').value);
			form_data.append('Pay_Bills', this.edit_privilege_frm.get('Pay_Bills').value);
			form_data.append('Cash_Movement', this.edit_privilege_frm.get('Cash_Movement').value);
			form_data.append('Statement', this.edit_privilege_frm.get('Statement').value);
			form_data.append('Cash_Desk', this.edit_privilege_frm.get('Cash_Desk').value);
			console.log(this.edit_privilege_frm.value);
			console.log(this.edit_privilege_frm.get('staff_member_name').value)
			this.priviligeApiService.edit_privilege(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['manage-privilege/list-privilege'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
