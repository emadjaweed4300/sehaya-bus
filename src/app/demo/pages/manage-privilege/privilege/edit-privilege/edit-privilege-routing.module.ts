import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditPrivilegeComponent } from './edit-privilege.component';

const routes: Routes = [
  {
    path: '',
    component: EditPrivilegeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditPrivilegeRoutingModule { }
