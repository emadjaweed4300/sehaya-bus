import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditPrivilegeRoutingModule } from './edit-privilege-routing.module';
import { EditPrivilegeComponent } from './edit-privilege.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { SelectModule } from 'ng-select';


@NgModule({
  imports: [
    CommonModule,
    EditPrivilegeRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    SelectModule
  ],
  declarations: [EditPrivilegeComponent]
})
export class EditPrivilegeModule { }
