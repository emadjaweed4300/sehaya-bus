import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddPrivilegeRoutingModule } from './add-privilege-routing.module';
import { AddPrivilegeComponent } from './add-privilege.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { SelectModule } from 'ng-select';


@NgModule({
  imports: [
    CommonModule,
    AddPrivilegeRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    SelectModule
  ],
  declarations: [AddPrivilegeComponent]
})
export class AddPrivilegeModule { }
