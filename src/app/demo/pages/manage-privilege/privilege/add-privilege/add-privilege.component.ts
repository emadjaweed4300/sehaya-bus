import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { PriviligeApiService } from '../../shared/privilige-api.service';

@Component({
	selector: 'app-add-privilege',
	templateUrl: './add-privilege.component.html',
	styleUrls: ['./add-privilege.component.scss']
})
export class AddPrivilegeComponent implements OnInit {

	add_privilege_frm: FormGroup;
	staff_members = [];

	// Ng-select accept only value (value: 1) and label (label: 'List')
	// Brand
	master = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '5', label: 'Active' },
		{ value: '6', label: 'Inactive' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	vehicle = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];

	clients = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '5', label: 'Active' },
		{ value: '6', label: 'Inactive' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	staff_member = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '5', label: 'Active' },
		{ value: '6', label: 'Inactive' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	circuits = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	claim_circuit = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
	];
	expense_circuit = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '9', label: 'Share' },
	];
	receipt_circuit = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '9', label: 'Share' },
	];
	event_circuit = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
	];
	bank_account = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '8', label: 'View' },
	];
	invoices = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '10', label: 'Download' },
		{ value: '11', label: 'View Cancel' },
	];
	pay_bills = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '10', label: 'Download' },
	];
	cash_movement = [
		{ value: '1', label: 'List' },
		{ value: '2', label: 'Add' },
	];
	statement = [
		{ value: '10', label: 'Download' },
	];
	cash_desk = [
		{ value: '1', label: 'List' },
		{ value: '3', label: 'Update' },
		{ value: '4', label: 'Delete' },
		{ value: '7', label: 'All' },
		{ value: '8', label: 'View' },
		{ value: '12', label: 'Opening' },
		{ value: '13', label: 'Closing' },
	]

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private priviligeApiService: PriviligeApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_privilege_frm = this.__fb.group({
			// function: ["", Validators.required],
			staff_member_name: ["", Validators.required],
			// dashboard: ["", ""],
			Brand: ["", ""],
			Region: ["", ""],
			City: ["", ""],
			Vehicle_Category: ["", ""],
			Vehicle_Carrier: ["", ""],
			Claim_Category: ["", ""],
			User_Type: ["", ""],
			Client_Category: ["", ""],
			Functions: ["", ""],
			Vehicle_Deadline: ["", ""],
			Units: ["", ""],
			Vehicle_Features: ["", ""],
			Manage_Events: ["", ""],
			Movement: ["", ""],
			Location: ["", ""],
			Vehicles: ["", ""],
			Clients: ["", ""],
			Staff_Member: ["", ""],
			Circuits: ["", ""],
			Claim_Circuit: ["", ""],
			Expense_Circuit: ["", ""],
			Receipt_Circuit: ["", ""],
			Event_Circuit: ["", ""],
			Bank_Account: ["", ""],
			Invoices: ["", ""],
			Pay_Bills: ["", ""],
			Cash_Movement: ["", ""],
			Statement: ["", ""],
			Cash_Desk: ["", ""],
		});
		this.get_staff_member();
	}

	// Get Staff Member
	get_staff_member(){
		this.priviligeApiService.staff_members().then((response: any) => {
			console.log('Response:', response);
			this.staff_members = response.staff_member
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});	
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_privilege_frm.get(formControlName).errors &&
			(this.add_privilege_frm.get(formControlName).touched ||
				this.add_privilege_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_privilege_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_privilege_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_privilege_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_privilege_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_privilege_frm);
		if (this.add_privilege_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// form_data.append('function', this.add_privilege_frm.get('function').value);
			form_data.append('id', this.add_privilege_frm.get('staff_member_name').value);
			form_data.append('Brand', this.add_privilege_frm.get('Brand').value);
			form_data.append('Region', this.add_privilege_frm.get('Region').value);
			// console.log(this.add_privilege_frm.get('Brand').value);
			// console.log(this.add_privilege_frm.get('Region').value);

			form_data.append('City', this.add_privilege_frm.get('City').value);
			form_data.append('Vehicle_Category', this.add_privilege_frm.get('Vehicle_Category').value);
			form_data.append('Vehicle_Carrier', this.add_privilege_frm.get('Vehicle_Carrier').value);
			form_data.append('Claim_Category', this.add_privilege_frm.get('Claim_Category').value);
			form_data.append('User_Type', this.add_privilege_frm.get('User_Type').value);
			form_data.append('Client_Category', this.add_privilege_frm.get('Client_Category').value);
			form_data.append('Functions', this.add_privilege_frm.get('Functions').value);
			form_data.append('Vehicle_Deadline', this.add_privilege_frm.get('Vehicle_Deadline').value);
			form_data.append('Units', this.add_privilege_frm.get('Units').value);
			form_data.append('Vehicle_Features', this.add_privilege_frm.get('Vehicle_Features').value);
			form_data.append('Manage_Events', this.add_privilege_frm.get('Manage_Events').value);
			form_data.append('Movement', this.add_privilege_frm.get('Movement').value);
			form_data.append('Location', this.add_privilege_frm.get('Location').value);
			form_data.append('Vehicles', this.add_privilege_frm.get('Vehicles').value);
			form_data.append('Clients', this.add_privilege_frm.get('Clients').value);
			form_data.append('Staff_Member', this.add_privilege_frm.get('Staff_Member').value);
			form_data.append('Circuits', this.add_privilege_frm.get('Circuits').value);
			form_data.append('Claim_Circuit', this.add_privilege_frm.get('Claim_Circuit').value);
			form_data.append('Expense_Circuit', this.add_privilege_frm.get('Expense_Circuit').value);
			form_data.append('Receipt_Circuit', this.add_privilege_frm.get('Receipt_Circuit').value);
			form_data.append('Event_Circuit', this.add_privilege_frm.get('Event_Circuit').value);
			form_data.append('Bank_Account', this.add_privilege_frm.get('Bank_Account').value);
			form_data.append('Invoices', this.add_privilege_frm.get('Invoices').value);
			form_data.append('Pay_Bills', this.add_privilege_frm.get('Pay_Bills').value);
			form_data.append('Cash_Movement', this.add_privilege_frm.get('Cash_Movement').value);
			form_data.append('Statement', this.add_privilege_frm.get('Statement').value);
			form_data.append('Cash_Desk', this.add_privilege_frm.get('Cash_Desk').value);
			console.log(this.add_privilege_frm.value);
			console.log(this.add_privilege_frm.get('staff_member_name').value)

			this.priviligeApiService.add_privilege(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				this.router.navigate(['/manage-privilege/list-privilege'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
