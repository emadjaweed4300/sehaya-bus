import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global.service';
import { END_POINT } from 'src/app/constant';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class PriviligeApiService {

	// API path
	//   base_path = 'http://nooridev.in/hapush/sehayabus';

	data = new FormData();
	constructor(
		private http: HttpClient,
		private translate: TranslateService,
		private global: GlobalService,
		private router: Router
	) {
		this.data.append('user_id', this.global.currentUser.id);
		this.data.append('user_role', this.global.currentUser.user_role);
		this.data.append('security_key', this.global.currentUser.token)
	}

	// Http Options
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	}

	// Handle API errors
	handleError(error: HttpErrorResponse) {

		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${JSON.stringify(error.status)}, ` +
				`body was: ${JSON.stringify(error.message)}`);
		}
		// return an observable with a user-facing error message
		if (error.status == 0) {
			return throwError(
				Swal.fire(
					'The Internet?',
					'Please try again later',
					'question'
				)
			);
		}
	};

	staff_members() {
		return this._callAPI('/my-staff-members', this.data)
	}

	// Add Privilege
	add_privilege(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-privilege', data)
	}

	// User Pri
	privileges() {
		return this._callAPI('/privileges', this.data)
	}
	// Get Staff Members
	get_user_privileges(id) {
		return this._callAPI('/get-privileges/' + id, this.data)
	}

	// Edit Staff Member
	edit_privilege(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-privilege', data)
	}

	_callAPI(url: string, params?) {
		return new Promise((resolve, reject) => {
			this.http.post(END_POINT + url, params)
				.subscribe(res => {
					if (res['response'] === 'success') {
						resolve(res)
					} else {
						if (res['response_code'] == 405 || res['response_code'] == 403 || res['response_code'] == 204){
							localStorage.clear();
							return window.location.replace('/auth/signin');
						}
						reject(res['message'])
					}
				}, (this.handleError))
		})
	}
}
