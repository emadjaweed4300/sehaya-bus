import { TestBed } from '@angular/core/testing';

import { PriviligeApiService } from './privilige-api.service';

describe('PriviligeApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PriviligeApiService = TestBed.get(PriviligeApiService);
    expect(service).toBeTruthy();
  });
});
