import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptCircuitRoutingModule } from './receipt-circuit-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ReceiptCircuitRoutingModule,
  ],
  declarations: []
})
export class ReceiptCircuitModule { }
