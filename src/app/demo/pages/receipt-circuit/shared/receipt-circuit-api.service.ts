import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global.service';
import { END_POINT } from 'src/app/constant';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class ReceiptCircuitApiService {

    data = new FormData();
    constructor(
        private http: HttpClient,
        private translate: TranslateService,
        private global: GlobalService,
        private router: Router
    ) {
        this.data.append('user_id', this.global.currentUser.id);
        this.data.append('user_role', this.global.currentUser.user_role);
        this.data.append('security_key', this.global.currentUser.token)
    }

    // Http Options
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }

    // Handle API errors
    handleError(error: HttpErrorResponse) {

        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${JSON.stringify(error.status)}, ` +
                `body was: ${JSON.stringify(error.message)}`);
        }
        // return an observable with a user-facing error message
        if (error.status == 0) {
            return throwError(
                Swal.fire(
                    'The Internet?',
                    'Please try again later',
                    'question'
                )
            );
        }
    };

    // Receipt Circuit API's
	// Add Receipt Circuit
	add_receipt_circuit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-receipt-circuit', data)
    }
    
    // Receipt Circuit
	receipt_circuit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/receipt-circuit', data)
    }

    // Get Receipt Circuit By Id
	get_receipt_circuit(id) {
		return this._callAPI('/get-receipt-circuit/' + id, this.data)
	}
    
    // Edit Receipt Circuit
	edit_receipt_circuit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-receipt-circuit', data)
    }
    
    // Navigate receipt to cash desk API
	navigate_receipt_to_cash_desk(id) {
		return this._callAPI('/navigate-receipt-to-cash/' + id, this.data)
	} 

    _callAPI(url: string, params?) {
        return new Promise((resolve, reject) => {
            this.http.post(END_POINT + url, params)
                .subscribe(res => {
                    if (res['response'] === 'success') {
                        resolve(res)
                    } else {
                        if (res['response_code'] == 405 || res['response_code'] == 403 || res['response_code'] == 204) {
                            localStorage.clear();
                            return window.location.replace('/auth/signin');
                        }
                        reject(res['message'])
                    }
                }, (this.handleError))
        })
    }
}    