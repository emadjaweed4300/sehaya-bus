import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListReceiptComponent } from './list-receipt.component';

const routes: Routes = [
  {
    path: '',
    component: ListReceiptComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListReceiptRoutingModule { }
