import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { ReceiptCircuitApiService } from '../../shared/receipt-circuit-api.service';

@Component({
	selector: 'app-list-receipt',
	templateUrl: './list-receipt.component.html',
	styleUrls: ['./list-receipt.component.scss']
})
export class ListReceiptComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	receipt_circuit_list: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	dtExportButtonOptions: any = {};

	constructor(
		private __route: ActivatedRoute,
		private router: Router,
		private receiptCircuitApi: ReceiptCircuitApiService,
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			responsive: true
		};
		this.list_receipt_circuit()
	}

	// Table Data not found message function
	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// List Data API
	list_receipt_circuit() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.receiptCircuitApi.receipt_circuit(formData).then((response: any) => {
			console.log('Response:', response);
			this.receipt_circuit_list = response.reciept
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Delete
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			// if (result.value) {
			// 	this.expenseCircuitApi.delete_expence_circuit(id).then((response: any) => {
			// 		console.log('Response:', response);
			// 		this.list_expense_circuit();
			// 		Swal.fire({
			// 			type: 'success',
			// 			title: 'Deleted!',
			// 			text: response.message,
			// 			confirmButtonColor: '#323258',
			// 			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			// 		});

			// 	})
			// 	.catch((error: any) => {
			// 		console.error(error);
			// 		Swal.fire({
			// 		type: 'error',
			// 		title: 'Error!',
			// 		text: error,
			// 		confirmButtonColor: '#323258',
			// 		confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			// 	});
			// 	});
			// }
		})
	}

	// Share Function
	public shareAction(id: number) {
		// this.router.navigate(['/manage-cash-desk/cash-desk'])
		// console.log(id)
		// console.log(id)
		this.receiptCircuitApi.navigate_receipt_to_cash_desk(id).then((response: any) => {
			console.log('Response:', response);
			Swal.fire({
				type: 'success',
				title: 'Success',
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				text: response.message
			});
			this.router.navigate(['/manage-cash-desk/cash-desk']);
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

}
