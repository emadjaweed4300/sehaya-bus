import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';
import Swal from 'sweetalert2';
import { CashDeskApiService } from '../../../manage-cash-desk/shared/cash-desk-api.service';
import { ReceiptCircuitApiService } from '../../shared/receipt-circuit-api.service';

@Component({
	selector: 'app-add-receipt',
	templateUrl: './add-receipt.component.html',
	styleUrls: ['./add-receipt.component.scss']
})
export class AddReceiptComponent implements OnInit {

	add_receipt_circuit_frm: FormGroup;
	drivers = [];
	circuits = [];
	receipt_operations =[];
	setting_id

	paymentModeArr: any[] = [
		{ id: 1, name: 'Cash' },
		{ id: 2, name: 'Cheeque' },
		{ id: 3, name: 'Transfer' }
	];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private circuitApi: CircuitApiService,
		private cashDeskApi: CashDeskApiService,
		private receiptCircuitApi: ReceiptCircuitApiService,
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_receipt_circuit_frm = this.__fb.group({
			circuit: [null, Validators.required],
			driver: ["", Validators.required],
			operation: ["", Validators.required],
			reference: ["", Validators.required],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			description: ["", Validators.required],
			payment_mode: ["", Validators.required],
			opration_date:  ["", Validators.required],
		});
		// this.add_receipt_circuit_frm.get('circuit').setValue(this.setting_id);
		this.get_drivers();
		this.get_circuits();
		this.get_operaions_receipt();
	}

	// Get Driver list Dropdown
	get_drivers() {
		this.circuitApi.drivers_for_filter().then((response: any) => {
			console.log('Response:', response);
			this.drivers = response.drivers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	
	// Get Circuit list Dropdown
	get_circuits() {
		this.circuitApi.circuits().then((response: any) => {
			console.log('Response:', response);
			this.circuits = response.circuit
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// get operation dropdown
	get_operaions_receipt() {
		this.cashDeskApi.get_receipt_operation().then((response: any) => {
			console.log('Response:', response);
			this.receipt_operations = response.reciept
		})
		.catch((error: any) => {
			console.error(error);
		});
	}
	

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_receipt_circuit_frm.get(formControlName).errors &&
			(this.add_receipt_circuit_frm.get(formControlName).touched ||
				this.add_receipt_circuit_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_receipt_circuit_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_receipt_circuit_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_receipt_circuit_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_receipt_circuit_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_receipt_circuit_frm);
		if (this.add_receipt_circuit_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('circuit_id', this.add_receipt_circuit_frm.get('circuit').value);
			form_data.append('driver_id', this.add_receipt_circuit_frm.get('driver').value);
			form_data.append('operation', this.add_receipt_circuit_frm.get('operation').value);
			form_data.append('reference', this.add_receipt_circuit_frm.get('reference').value);
			form_data.append('amount', this.add_receipt_circuit_frm.get('amount').value);
			form_data.append('description', this.add_receipt_circuit_frm.get('description').value);
			form_data.append('operation_date', this.add_receipt_circuit_frm.get('opration_date').value);
			form_data.append('payment_mode', this.add_receipt_circuit_frm.get('payment_mode').value);

			this.receiptCircuitApi.add_receipt_circuit(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/receipt-circuit/list-receipt'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
