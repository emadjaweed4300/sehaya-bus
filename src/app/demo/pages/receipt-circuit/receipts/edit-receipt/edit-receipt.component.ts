import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { CashDeskApiService } from '../../../manage-cash-desk/shared/cash-desk-api.service';
import { ReceiptCircuitApiService } from '../../shared/receipt-circuit-api.service';

@Component({
	selector: 'app-edit-receipt',
	templateUrl: './edit-receipt.component.html',
	styleUrls: ['./edit-receipt.component.scss']
})
export class EditReceiptComponent implements OnInit {

	edit_receipt_circuit_frm: FormGroup;
	drivers = [];
	circuits = [];
	receipt_operations =[];
	receipt_circuit_id
	receipts: any;

	paymentModeArr: any[] = [
		{ id: 1, name: 'Cash' },
		{ id: 2, name: 'Cheeque' },
		{ id: 3, name: 'Transfer' }
	];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private circuitApi: CircuitApiService,
		private cashDeskApi: CashDeskApiService,
		public datepipe: DatePipe,
		private receiptCircuitApi: ReceiptCircuitApiService
	) {
		this.receipt_circuit_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		/** Form builder instance */
		this.edit_receipt_circuit_frm = this.__fb.group({
			circuit: ["", Validators.required],
			driver: ["", Validators.required],
			operation:  [{ value: '', disabled: true }],
			reference: ["", Validators.required],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			opration_date: ["", Validators.required],
			payment_mode: ["", Validators.required],
			description: ["", Validators.required],
		});
		this.get_drivers();
		this.get_circuits();
		// this.get_operaions_receipt();
		this.get_receipt_circuits();
	}

	// Get Driver list Dropdown
	get_drivers() {
		this.circuitApi.drivers_for_filter().then((response: any) => {
			console.log('Response:', response);
			this.drivers = response.drivers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	
	// Get Circuit list Dropdown
	get_circuits() {
		this.circuitApi.circuits().then((response: any) => {
			console.log('Response:', response);
			this.circuits = response.circuit
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// get operation dropdown
	// get_operaions_receipt() {
	// 	this.cashDeskApi.get_receipt_operation().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.receipt_operations = response.receipt
	// 	})
	// 		.catch((error: any) => {
	// 			console.error(error);
	// 			Swal.fire({
	// 				type: 'error',
	// 				title: 'Error!',
	// 				text: error,
	// 				confirmButtonColor: '#323258',
	// 				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// 			});
	// 		});
	// }

	// Get All Receipt Circuit API
	get_receipt_circuits(){
		this.receiptCircuitApi.get_receipt_circuit(this.receipt_circuit_id).then((response: any) => {
			console.log('Response:', response);
			this.receipts = response.receipt;
			this.edit_receipt_circuit_frm.get('circuit').setValue(response.receipt.circuit_id);
			this.edit_receipt_circuit_frm.get('driver').setValue(response.receipt.driver_id);
			this.edit_receipt_circuit_frm.get('operation').setValue(response.receipt.operation_name);
			console.log(response.receipt.operation)
			this.edit_receipt_circuit_frm.get('reference').setValue(response.receipt.reference);
			this.edit_receipt_circuit_frm.get('amount').setValue(response.receipt.amount);
			this.edit_receipt_circuit_frm.get('opration_date').setValue(this.DateFormate(response.receipt.opration_date));
			this.edit_receipt_circuit_frm.get('payment_mode').setValue(response.receipt.payment_mode);
			this.edit_receipt_circuit_frm.get('description').setValue(response.receipt.discription);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	DateFormate(Date) {
		return this.datepipe.transform(Date, 'yyyy-MM-dd');
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_receipt_circuit_frm.get(formControlName).errors &&
			(this.edit_receipt_circuit_frm.get(formControlName).touched ||
				this.edit_receipt_circuit_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_receipt_circuit_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_receipt_circuit_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_receipt_circuit_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_receipt_circuit_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call edit service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_receipt_circuit_frm);
		if (this.edit_receipt_circuit_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('receipt_id', this.receipt_circuit_id);
			form_data.append('circuit_id', this.edit_receipt_circuit_frm.get('circuit').value);
			form_data.append('driver_id', this.edit_receipt_circuit_frm.get('driver').value);
			form_data.append('operation', this.edit_receipt_circuit_frm.get('operation').value);
			form_data.append('reference', this.edit_receipt_circuit_frm.get('reference').value);
			form_data.append('amount', this.edit_receipt_circuit_frm.get('amount').value);
			form_data.append('operation_date', this.edit_receipt_circuit_frm.get('opration_date').value);
			form_data.append('payment_mode', this.edit_receipt_circuit_frm.get('payment_mode').value);
			form_data.append('description', this.edit_receipt_circuit_frm.get('description').value);

			this.receiptCircuitApi.edit_receipt_circuit(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/receipt-circuit/list-receipt'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
