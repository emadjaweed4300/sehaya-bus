import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditReceiptComponent } from './edit-receipt.component';

const routes: Routes = [
  {
    path: '',
    component: EditReceiptComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditReceiptRoutingModule { }
