import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Receipt circuit list,add,edit components
			{
				path: 'list-receipt',
				loadChildren: () => import('./receipts/list-receipt/list-receipt.module').then(module => module.ListReceiptModule)
			},
			{
				path: 'list-receipt/:setting_id',
				loadChildren: () => import('./receipts/list-receipt/list-receipt.module').then(module => module.ListReceiptModule)
			},
			{
				path: 'add-receipt',
				loadChildren: () => import('./receipts/add-receipt/add-receipt.module').then(module => module.AddReceiptModule)
			},
			{
				path: 'edit-receipt/:id',
				loadChildren: () => import('./receipts/edit-receipt/edit-receipt.module').then(module => module.EditReceiptModule)
			},
		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ReceiptCircuitRoutingModule { }
