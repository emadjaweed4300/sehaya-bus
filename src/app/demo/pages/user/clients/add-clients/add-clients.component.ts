import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import Swal from 'sweetalert2';
import { UserApiService } from '../../shared/user_api.service';

@Component({
	selector: 'app-add-clients',
	templateUrl: './add-clients.component.html',
	styleUrls: ['./add-clients.component.scss']
})
export class AddClientsComponent implements OnInit {

	add_clients_frm: FormGroup;
	cities = [];
	// regions = [];
	user_roles = [];
	client_categories= [];
	countries = [];

	invoiceModeArr: any[] = [
		{ id: 1, name: 'Cash' },
		{ id: 2, name: 'Term' },
	];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService,
		private userApi: UserApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_clients_frm = this.__fb.group({
			name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			phone:  ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			// password: ["", [Validators.required, Validators.minLength(8)]],
			user_role: ['', Validators.required],
			client_category: ['', Validators.required],
			address: ['', Validators.required],
			city_name: ['', Validators.required],
			country_name: ["", Validators.required],
			nb_day: ["", Validators.required],
			currency: ['Morocco Dhirham', Validators.required],
			invoice_mode: ["", Validators.required],
		});
		this.get_cities();
		// this.get_regions();
		this.get_user_role();
		this.get_client_category();
		this.get_countries();
	}

	// get All cities
	get_cities(){
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		// .catch((error: any) => {
		// 	console.error(error);
		// 	Swal.fire({
		// 			type: 'error',
		// 			title: 'Error!',
		// 			text: error,
		// 			confirmButtonColor: '#323258',
		// 			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
		// 		});
		// });	
		.catch((error: any) => {
			console.error(error);
		});
	}

	// get All Countries
	get_countries(){
		this.masterApi.countries().then((response: any) => {
			console.log('Response:', response);
			this.countries = response.countries
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// get All Regions
	// get_regions(){
	// 	this.masterApi.regions().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.regions = response.regions
	// 	})
	// 	.catch((error: any) => {
	// 		console.error(error);
	// 		Swal.fire({
	// 				type: 'error',
	// 				title: 'Error!',
	// 				text: error,
	// 				confirmButtonColor: '#323258',
	// 				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// 			});
	// 	});	
	// }

	// get All User Role
	get_user_role(){
		this.masterApi.list_user_role().then((response: any) => {
			console.log('Response:', response);
			this.user_roles = response.user_role
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// get All Client Category
	get_client_category(){
		this.masterApi.client_categorys().then((response: any) => {
			console.log('Response:', response);
			this.client_categories = response.client_category
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_clients_frm.get(formControlName).errors &&
			(this.add_clients_frm.get(formControlName).touched ||
				this.add_clients_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_clients_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_clients_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_clients_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_clients_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_clients_frm);
		if (this.add_clients_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('db_type', '3');
			form_data.append('role', '3');
			form_data.append('name', this.add_clients_frm.get('name').value);
			form_data.append('phone', this.add_clients_frm.get('phone').value);
			form_data.append('email', this.add_clients_frm.get('email').value);
			// form_data.append('password', this.add_clients_frm.get('password').value);
			form_data.append('password', null);
			form_data.append('user_type', this.add_clients_frm.get('user_role').value);
			form_data.append('client_category', this.add_clients_frm.get('client_category').value);
			// form_data.append('region_id', this.add_clients_frm.get('region_name').value);
			form_data.append('city_id', this.add_clients_frm.get('city_name').value);
			form_data.append('country_id', this.add_clients_frm.get('country_name').value);
			form_data.append('nb_day', this.add_clients_frm.get('nb_day').value);
			form_data.append('currency', this.add_clients_frm.get('currency').value);
			form_data.append('invoice_mode', this.add_clients_frm.get('invoice_mode').value);
			form_data.append('address', this.add_clients_frm.get('address').value);
			
			this.userApi.add_users(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/user/list-clients'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}
	
}
