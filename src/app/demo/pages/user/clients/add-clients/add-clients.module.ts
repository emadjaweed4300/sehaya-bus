import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddClientsRoutingModule } from './add-clients-routing.module';
import { AddClientsComponent } from './add-clients.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { SelectModule } from 'ng-select';


@NgModule({
  imports: [
    CommonModule,
    AddClientsRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    SelectModule
  ],
  declarations: [AddClientsComponent]
})
export class AddClientsModule { }
