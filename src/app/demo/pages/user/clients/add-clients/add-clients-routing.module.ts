import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddClientsComponent } from './add-clients.component';

const routes: Routes = [
  {
    path: '',
    component: AddClientsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddClientsRoutingModule { }
