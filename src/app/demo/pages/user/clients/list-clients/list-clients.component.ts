import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { UserApiService } from '../../shared/user_api.service';

@Component({
  selector: 'app-list-clients',
  templateUrl: './list-clients.component.html',
  styleUrls: ['./list-clients.component.scss']
})
export class ListClientsComponent implements OnInit {

	paginationTop
	@ViewChild(DataTableDirective, {static: false})
  	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	clients_list: any = [];
	isDtInitialized:boolean = false
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private router: Router,
		private masterApi: MasterApiService,
		private userApi: UserApiService
	) { }
  
	ngOnInit() {
		this.dtExportButtonOptions = {
			// 		ajax: 'fake-data/datatable-data.json',
			// 		columns: [
			// 			{
			// 				title: 'Id',
			// 				data: 'id'
			// 			},
			// 			{
			// 				title: 'Name',
			// 				data: 'name'
			// 			},
			// 			{
			// 				title: 'Office',
			// 				data: 'office'
			// 			},
			// 			{
			// 				title: 'Age',
			// 				data: 'age'
			// 			}, 
			// 			{
			// 				title: 'Start Date',
			// 				data: 'date'
			// 			}, 
			// 			{
			// 				title: 'Salary',
			// 				data: 'salary'
			// 			}
			// 		],
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.list_clients();
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	list_clients() {
		this.userApi.users().then((response: any) => {
			console.log('Response:', response);
			this.clients_list = response.list_of_users
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
				  dtInstance.destroy();
				  this.dtTrigger.next();
				});
			  } else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			  }
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Status API
	statusChange(i, id: string, event) {
		console.log(id)
		let formData = new FormData()
		formData.append('id', id)
		if (event.target.checked == true) {
			this.userApi.user_active(formData).then((response: any) => {
				console.log('Response:', response);
				// on Inactive status edit disbale. 
				this.clients_list[i].status = '1'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		} else {
			this.userApi.user_inactive(formData).then((response: any) => {
					console.log('Response:', response);
					// on Active status edit Enabale. 
					this.clients_list[i].status = '0'
				})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}
	}

  // delete Brand
	// public deleteAction(id: number) {
	// 	Swal.fire({
	// 		title: 'Are you sure?',
	// 		text: "You won't be able to revert this!",
	// 		type: 'warning',
	// 		showCancelButton: true,
	// 		confirmButtonColor: '#323258',
	// 		cancelButtonColor: '#ff1f1f',
	// 		confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
	// 		cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
	// 	})
	// }

}
