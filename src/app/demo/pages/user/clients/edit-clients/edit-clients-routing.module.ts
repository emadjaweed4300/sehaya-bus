import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditClientsComponent } from './edit-clients.component';

const routes: Routes = [
  {
    path: '',
    component: EditClientsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditClientsRoutingModule { }
