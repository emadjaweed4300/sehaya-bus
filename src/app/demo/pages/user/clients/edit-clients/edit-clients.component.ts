import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import Swal from 'sweetalert2';
import { UserApiService } from '../../shared/user_api.service';

@Component({
	selector: 'app-edit-clients',
	templateUrl: './edit-clients.component.html',
	styleUrls: ['./edit-clients.component.scss']
})
export class EditClientsComponent implements OnInit {

	edit_clients_frm: FormGroup;
	user_id
	user: any;
	cities = [];
	// regions = [];
	countries = [];
	user_roles = [];
	client_categories= [];

	invoiceModeArr: any[] = [
		{ id: 1, name: 'Cash' },
		{ id: 2, name: 'Term' },
	];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService,
		private userApi: UserApiService
	) {
		this.user_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		console.log(this.user_id)
		/** Form builder instance */
		this.edit_clients_frm = this.__fb.group({
			name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			phone: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			// password: ['', Validators.required],
			user_role: ['', Validators.required],
			client_category: ['', Validators.required],
			address: ['', Validators.required],
			city_name: ['', Validators.required],
			country_name: ["", Validators.required],
			nb_day: ["", Validators.required],
			currency: ['Morocco Dhirham', Validators.required],
			invoice_mode: ["", Validators.required],
		});
		this.get_Users();
		this.get_cities();
		// this.get_regions();
		this.get_countries();
		this.get_user_role();
		this.get_client_category();
	}

	// get All Cities
	get_cities(){
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// // get All Regions
	// get_regions(){
	// 	this.masterApi.regions().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.regions = response.regions
	// 	})
	// 	.catch((error: any) => {
	// 		console.error(error);
	// 		Swal.fire({
	// 				type: 'error',
	// 				title: 'Error!',
	// 				text: error,
	// 				confirmButtonColor: '#323258',
	// 				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// 			});
	// 	});	
	// }

	// get All Countries
	get_countries(){
		this.masterApi.countries().then((response: any) => {
			console.log('Response:', response);
			this.countries = response.countries
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// get All User Role
	get_user_role(){
		this.masterApi.list_user_role().then((response: any) => {
			console.log('Response:', response);
			this.user_roles = response.user_role
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// get All Client Category
	get_client_category(){
		this.masterApi.client_categorys().then((response: any) => {
			console.log('Response:', response);
			this.client_categories = response.client_category
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// get All client Data
	get_Users(){
		this.userApi.get_user(this.user_id).then((response: any) => {
			console.log('Response:', response);
			this.user = response.user_details;
			// user_role (formControlName) & user_type (API)
			this.edit_clients_frm.get('name').setValue(response.user_details.name);
			this.edit_clients_frm.get('phone').setValue(response.user_details.phone);
			this.edit_clients_frm.get('email').setValue(response.user_details.email);
			this.edit_clients_frm.get('country_name').setValue(response.user_details.country);
			this.edit_clients_frm.get('city_name').setValue(response.user_details.city);
			this.edit_clients_frm.get('user_role').setValue(response.user_details.user_type);
			this.edit_clients_frm.get('client_category').setValue(response.user_details.client_category);
			// this.edit_clients_frm.get('region_name').setValue(response.user_details.region);
			this.edit_clients_frm.get('invoice_mode').setValue(response.user_details.mode_invoicing);
			this.edit_clients_frm.get('nb_day').setValue(response.user_details.nb_days);
			this.edit_clients_frm.get('address').setValue(response.user_details.address);
				
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_clients_frm.get(formControlName).errors &&
			(this.edit_clients_frm.get(formControlName).touched ||
				this.edit_clients_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_clients_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_clients_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_clients_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_clients_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		console.log('testing')
		this.markFormGroupTouched(this.edit_clients_frm);
		if (this.edit_clients_frm.valid) {
			console.log('testing')
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('id', this.user_id);
			form_data.append('name', this.edit_clients_frm.get('name').value);
			form_data.append('phone', this.edit_clients_frm.get('phone').value);
			form_data.append('email', this.edit_clients_frm.get('email').value);
			// form_data.append('password', this.edit_clients_frm.get('password').value);
			form_data.append('user_type', this.edit_clients_frm.get('user_role').value);
			form_data.append('client_category', this.edit_clients_frm.get('client_category').value);
			// form_data.append('region_id', this.edit_clients_frm.get('region_name').value);
			form_data.append('city_id', this.edit_clients_frm.get('city_name').value);
			form_data.append('country_id', this.edit_clients_frm.get('country_name').value);
			form_data.append('nb_day', this.edit_clients_frm.get('nb_day').value);
			form_data.append('currency', this.edit_clients_frm.get('currency').value);
			form_data.append('invoice_mode', this.edit_clients_frm.get('invoice_mode').value);
			form_data.append('address', this.edit_clients_frm.get('address').value);
			this.userApi.edit_user(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/user/list-clients'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
