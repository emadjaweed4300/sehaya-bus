import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditClientsRoutingModule } from './edit-clients-routing.module';
import { EditClientsComponent } from './edit-clients.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditClientsRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditClientsComponent]
})
export class EditClientsModule { }
