import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewCientsRoutingModule } from './view-clients-routing.module';
import { ViewClientsComponent } from './view-clients.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ViewCientsRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ViewClientsComponent]
})
export class ViewClientsModule { }
