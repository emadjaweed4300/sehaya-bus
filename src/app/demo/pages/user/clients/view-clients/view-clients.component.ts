import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserApiService } from '../../shared/user_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-view-clients',
	templateUrl: './view-clients.component.html',
	styleUrls: ['./view-clients.component.scss']
})
export class ViewClientsComponent implements OnInit {

	user_list: any = [];
	user_id
	client: any;

	constructor(
		private router: Router,
		private __route: ActivatedRoute,
		private userApi: UserApiService
	) {
		this.user_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		this.get_Users();
	}

	get_Users(){
		this.userApi.get_user(this.user_id).then((response: any) => {
			console.log('Response:', response);
			this.client = response.user_details;
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

}
