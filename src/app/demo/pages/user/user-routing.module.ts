import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Clients list,add,view,edit components
			{
				path: 'list-clients',
				loadChildren: () => import('./clients/list-clients/list-clients.module').then(module => module.ListClientsModule)
			},
			{
				path: 'add-clients',
				loadChildren: () => import('./clients/add-clients/add-clients.module').then(module => module.AddClientsModule)
			},
			{
				path: 'view-clients/:id',
				loadChildren: () => import('./clients/view-clients/view-clients.module').then(module => module.ViewClientsModule)
			},
			{
				path: 'edit-clients/:id',
				loadChildren: () => import('./clients/edit-clients/edit-clients.module').then(module => module.EditClientsModule)
			},

			// Tourism Company list,add,view,edit components
			{
				path: 'list-tourism-company',
				loadChildren: () => import('./tourism-company/list-tourism-company/list-tourism-company.module').then(module => module.ListTourismCompanyModule)
			},
			{
				path: 'add-tourism-company',
				loadChildren: () => import('./tourism-company/add-tourism-company/add-tourism-company.module').then(module => module.AddTourismCompanyModule)
			},
			{
				path: 'view-tourism-company/:id',
				loadChildren: () => import('./tourism-company/view-tourism-company/view-tourism-company.module').then(module => module.ViewTourismCompanyModule)
			},
			{
				path: 'edit-tourism-company/:id',
				loadChildren: () => import('./tourism-company/edit-tourism-company/edit-tourism-company.module').then(module => module.EditTourismCompanyModule)
			},

		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class UserRoutingModule { }
