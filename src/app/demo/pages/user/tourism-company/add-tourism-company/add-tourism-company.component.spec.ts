import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTourismCompanyComponent } from './add-tourism-company.component';

describe('AddTourismCompanyComponent', () => {
  let component: AddTourismCompanyComponent;
  let fixture: ComponentFixture<AddTourismCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTourismCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTourismCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
