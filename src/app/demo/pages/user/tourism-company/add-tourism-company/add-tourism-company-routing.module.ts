import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTourismCompanyComponent } from './add-tourism-company.component';

const routes: Routes = [
  {
    path: '',
    component: AddTourismCompanyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddTourismCompanyRoutingModule { }
