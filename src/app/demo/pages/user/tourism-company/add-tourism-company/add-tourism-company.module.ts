import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddTourismCompanyRoutingModule } from './add-tourism-company-routing.module';
import { AddTourismCompanyComponent } from './add-tourism-company.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { SelectModule } from 'ng-select';
import { ArchwizardModule } from 'ng2-archwizard/dist';


@NgModule({
  imports: [
    CommonModule,
    AddTourismCompanyRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    SelectModule,
    ArchwizardModule
  ],
  declarations: [AddTourismCompanyComponent]
})
export class AddTourismCompanyModule { }
