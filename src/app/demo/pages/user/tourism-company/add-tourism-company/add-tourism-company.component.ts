import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserApiService } from '../../shared/user_api.service';
import { MasterApiService } from '../../../master/shared/master_api.service';
import Swal from 'sweetalert2';
@Component({
	selector: 'app-add-tourism-company',
	templateUrl: './add-tourism-company.component.html',
	styleUrls: ['./add-tourism-company.component.scss']
})
export class AddTourismCompanyComponent implements OnInit {

	databaseValue: any[] = [
		{ id: '1', viewValue: 'Local Databse'  },
		{ id: '2', viewValue: 'Cloud Databse' },
	];
	database_type = '';

	numberOfEmployeeArr: any[] = [
		{ id: 1, name: '1' },
		{ id: 2, name: '2-5' },
		{ id: 3, name: '6-10' },
		{ id: 4, name: '11-20' },
		{ id: 5, name: '21-50' },
		{ id: 6, name: '51-100' },
		{ id: 7, name: 'More than 100' },
	];

	add_tourism_company_frm: FormGroup;
	cities = [];
	regions = [];
	user_roles = [];
	countries= [];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService,
		private userApi: UserApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_tourism_company_frm = this.__fb.group({
			company_name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			phone: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			password: ["", [Validators.required, Validators.minLength(8)]],
			cp: ['', Validators.required],
			cut: ['', Validators.required],
			web_site: ['', Validators.required],
			fax_number: ['', Validators.required],

			ice: ['', Validators.required],
			rc: ['', Validators.required],
			lf: ['', Validators.required],
			cnss: ['', Validators.required],
			patent: ['', Validators.required],
			activity: ['', Validators.required],
			
			address: ['', Validators.required],
			country_name: ['', Validators.required],
			region_name: ['', Validators.required],
			city_name: ['', Validators.required],

			database_type: ['', Validators.required],
			director_name: ['', Validators.required],
			director_email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			director_phone_number: ['', Validators.required],
			manager_email: ['', Validators.required],
			// user_role: ['', Validators.required],
			
			host_name: [{value:'', disabled: true},],
			user_name: [{value:'', disabled: true},],
			passwords: [{value:'', disabled: true},],
			db_name: [{value:'', disabled: true},],
			ip_address: [{value:'', disabled: true},],
		});
		this.get_cities();
		this.get_regions();
		this.get_user_role();
		this.get_countries();
	}

	// get All cities
	get_cities(){
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// get All Countries
	get_countries(){
		this.masterApi.countries().then((response: any) => {
			console.log('Response:', response);
			this.countries = response.countries
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// get All Regions
	get_regions(){
		this.masterApi.regions().then((response: any) => {
			console.log('Response:', response);
			this.regions = response.regions
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// get All User Role
	get_user_role(){
		this.masterApi.list_user_role().then((response: any) => {
			console.log('Response:', response);
			this.user_roles = response.user_role
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	data(){
		console.log(this.database_type)
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_tourism_company_frm.get(formControlName).errors &&
			(this.add_tourism_company_frm.get(formControlName).touched ||
				this.add_tourism_company_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_tourism_company_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_tourism_company_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_tourism_company_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_tourism_company_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		console.log('Hello')
		this.markFormGroupTouched(this.add_tourism_company_frm);
		if (this.add_tourism_company_frm.valid) {
			console.log('Hi')
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('role', '2');
			form_data.append('client_category', '0');
			form_data.append('user_type', '0');
			form_data.append('db_name', '1');

			form_data.append('name', this.add_tourism_company_frm.get('company_name').value);
			form_data.append('phone', this.add_tourism_company_frm.get('phone').value);
			form_data.append('email', this.add_tourism_company_frm.get('email').value);
			form_data.append('password', this.add_tourism_company_frm.get('password').value);
			form_data.append('region_id', this.add_tourism_company_frm.get('region_name').value);
			form_data.append('city_id', this.add_tourism_company_frm.get('city_name').value);
			form_data.append('cp', this.add_tourism_company_frm.get('cp').value);
			form_data.append('website', this.add_tourism_company_frm.get('web_site').value);
			form_data.append('fax_number', this.add_tourism_company_frm.get('fax_number').value);
			form_data.append('cut', this.add_tourism_company_frm.get('cut').value);
			form_data.append('director_name', this.add_tourism_company_frm.get('director_name').value);
			form_data.append('director_email', this.add_tourism_company_frm.get('director_email').value);
			form_data.append('director_phone_number', this.add_tourism_company_frm.get('director_phone_number').value);
			form_data.append('manager_email', this.add_tourism_company_frm.get('manager_email').value);

			form_data.append('ice', this.add_tourism_company_frm.get('ice').value);
			form_data.append('rc', this.add_tourism_company_frm.get('rc').value);
			form_data.append('lf', this.add_tourism_company_frm.get('lf').value);
			form_data.append('cnss', this.add_tourism_company_frm.get('cnss').value);
			form_data.append('patent', this.add_tourism_company_frm.get('patent').value);
			form_data.append('activity', this.add_tourism_company_frm.get('activity').value);
			form_data.append('address', this.add_tourism_company_frm.get('address').value);
			form_data.append('country', this.add_tourism_company_frm.get('country_name').value);

			form_data.append('db_type', this.add_tourism_company_frm.get('database_type').value);
			form_data.append('db_host', this.add_tourism_company_frm.get('host_name').value);
			form_data.append('db_username', this.add_tourism_company_frm.get('user_name').value);
			form_data.append('db_password', this.add_tourism_company_frm.get('passwords').value);
			form_data.append('ip_address', this.add_tourism_company_frm.get('ip_address').value);
			form_data.append('db_name', this.add_tourism_company_frm.get('db_name').value);

			
			this.userApi.add_users(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/user/list-tourism-company'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

	/* Set County as Required based on DB Selected */
	onDatabseChange() {
		
		if (this.database_type == '2') {
			console.log(this.database_type)
			// this.add_tourism_company_frm.get('host_names').setValidators([Validators.required]); // 5.Set Required Validator
			// this.add_tourism_company_frm.get('host_names').updateValueAndValidity();
			this.add_tourism_company_frm.get('host_name').enable();
			this.add_tourism_company_frm.get('user_name').enable();
			this.add_tourism_company_frm.get('passwords').enable();
			this.add_tourism_company_frm.get('db_name').enable();
			this.add_tourism_company_frm.get('ip_address').enable();
			// this.add_tourism_company_frm.get('host_name').setValidators([Validators.required]); // 5.Set Required Validator
		} else {
			console.log(this.database_type)
			this.add_tourism_company_frm.get('host_name').disable();
			this.add_tourism_company_frm.get('user_name').disable();
			this.add_tourism_company_frm.get('passwords').disable();
			this.add_tourism_company_frm.get('db_name').disable();
			this.add_tourism_company_frm.get('ip_address').disable();
			// this.add_tourism_company_frm.get('host_names').clearValidators(); // 6. Clear All Validators
			// this.add_tourism_company_frm.get('host_names').updateValueAndValidity();
		}

	}
}	
