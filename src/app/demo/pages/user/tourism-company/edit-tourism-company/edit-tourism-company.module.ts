import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditTourismCompanyRoutingModule } from './edit-tourism-company-routing.module';
import { EditTourismCompanyComponent } from './edit-tourism-company.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { SelectModule } from 'ng-select';


@NgModule({
  imports: [
    CommonModule,
    EditTourismCompanyRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    SelectModule
  ],
  declarations: [EditTourismCompanyComponent]
})
export class EditTourismCompanyModule { }
