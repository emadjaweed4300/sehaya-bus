import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserApiService } from '../../shared/user_api.service';
import { MasterApiService } from '../../../master/shared/master_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-tourism-company',
	templateUrl: './edit-tourism-company.component.html',
	styleUrls: ['./edit-tourism-company.component.scss']
})
export class EditTourismCompanyComponent implements OnInit {

	edit_tourism_company_frm: FormGroup;
	user_id
	user: any;
	cities = [];
	regions = [];
	// user_roles = [];
	countries= [];

	numberOfEmployeeArr: any[] = [
		{ id: 1, name: '1' },
		{ id: 2, name: '2-5' },
		{ id: 3, name: '6-10' },
		{ id: 4, name: '11-20' },
		{ id: 5, name: '21-50' },
		{ id: 6, name: '51-100' },
		{ id: 7, name: 'More than 100' },
	];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService,
		private userApi: UserApiService
	) { 
		this.user_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_tourism_company_frm = this.__fb.group({
			company_name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			phone: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			// password: ["", [Validators.required, Validators.minLength(8)]],
			region_name: ['', Validators.required],
			city_name: ['', Validators.required],
			cp: ['', Validators.required],
			// user_role: ['', Validators.required],
			web_site: ['', Validators.required],
			fax_number: ['', Validators.required],
			cut: ['', Validators.required],
			director_name: ['', Validators.required],
			director_email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			director_phone_number: ['', Validators.required],
			manager_email: ['', Validators.required],
			ice: ['', Validators.required],
			rc: ['', Validators.required],
			lf: ['', Validators.required],
			cnss: ['', Validators.required],
			patent: ['', Validators.required],
			activity: ['', Validators.required],
			address: ['', Validators.required],
			country_name: ['', Validators.required],
		});
		this.get_cities();
		this.get_regions();
		// this.get_user_role();
		this.get_countries();
		this.get_Users();
	}

	// get All Countries
	get_cities(){
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// get All Regions
	get_regions(){
		this.masterApi.regions().then((response: any) => {
			console.log('Response:', response);
			this.regions = response.regions
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// get All User Role
	// get_user_role(){
	// 	this.masterApi.list_user_role().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.user_roles = response.user_role
	// 	})
	// 	.catch((error: any) => {
	// 		console.error(error);
	// 		Swal.fire({
				// 	type: 'error',
				// 	title: 'Error!',
				// 	text: error,
				// 	confirmButtonColor: '#323258',
				// 	confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				// });
	// 	});	
	// }

	// get All countries
	get_countries(){
		this.masterApi.countries().then((response: any) => {
			console.log('Response:', response);
			this.countries = response.countries
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get All tourism company data
	get_Users(){
		this.userApi.get_user(this.user_id).then((response: any) => {
			console.log('Response:', response);
			this.user = response.user_details;
			// user_role (formControlName) & user_type (API)
			this.edit_tourism_company_frm.get('company_name').setValue(response.user_details.name);
			this.edit_tourism_company_frm.get('phone').setValue(response.user_details.phone);
			this.edit_tourism_company_frm.get('email').setValue(response.user_details.email);
			this.edit_tourism_company_frm.get('city_name').setValue(response.user_details.city);

			this.edit_tourism_company_frm.get('web_site').setValue(response.user_details.website);
			this.edit_tourism_company_frm.get('fax_number').setValue(response.user_details.fax_number);
			this.edit_tourism_company_frm.get('cut').setValue(response.user_details.cut);
			this.edit_tourism_company_frm.get('director_name').setValue(response.user_details.director_name);
			this.edit_tourism_company_frm.get('director_email').setValue(response.user_details.director_email);
			this.edit_tourism_company_frm.get('director_phone_number').setValue(response.user_details.director_phone_number);
			this.edit_tourism_company_frm.get('manager_email').setValue(response.user_details.manager_email);
			this.edit_tourism_company_frm.get('ice').setValue(response.user_details.ICE);
			this.edit_tourism_company_frm.get('rc').setValue(response.user_details.RC);
			this.edit_tourism_company_frm.get('cnss').setValue(response.user_details.CNSS);
			this.edit_tourism_company_frm.get('lf').setValue(response.user_details.LF);
			this.edit_tourism_company_frm.get('patent').setValue(response.user_details.patent);
			this.edit_tourism_company_frm.get('activity').setValue(response.user_details.activity);
			this.edit_tourism_company_frm.get('address').setValue(response.user_details.address);
			this.edit_tourism_company_frm.get('country_name').setValue(response.user_details.country);
			this.edit_tourism_company_frm.get('cp').setValue(response.user_details.CP);
			
			this.edit_tourism_company_frm.get('region_name').setValue(response.user_details.region);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_tourism_company_frm.get(formControlName).errors &&
			(this.edit_tourism_company_frm.get(formControlName).touched ||
				this.edit_tourism_company_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_tourism_company_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_tourism_company_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_tourism_company_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_tourism_company_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_tourism_company_frm);
		if (this.edit_tourism_company_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('id', this.user_id);
			form_data.append('name', this.edit_tourism_company_frm.get('company_name').value);
			form_data.append('phone', this.edit_tourism_company_frm.get('phone').value);
			form_data.append('email', this.edit_tourism_company_frm.get('email').value);
			form_data.append('web_site', this.edit_tourism_company_frm.get('web_site').value);
			form_data.append('cp', this.edit_tourism_company_frm.get('cp').value);
			form_data.append('fax_number', this.edit_tourism_company_frm.get('fax_number').value);
			form_data.append('cut', this.edit_tourism_company_frm.get('cut').value);
			form_data.append('ice', this.edit_tourism_company_frm.get('ice').value);
			form_data.append('rc', this.edit_tourism_company_frm.get('rc').value);
			form_data.append('lf', this.edit_tourism_company_frm.get('lf').value);
			form_data.append('cnss', this.edit_tourism_company_frm.get('cnss').value);
			form_data.append('patent', this.edit_tourism_company_frm.get('patent').value);
			form_data.append('activity', this.edit_tourism_company_frm.get('activity').value);
			form_data.append('country_id', this.edit_tourism_company_frm.get('country_name').value);
			form_data.append('city_id', this.edit_tourism_company_frm.get('city_name').value);
			form_data.append('region_id', this.edit_tourism_company_frm.get('region_name').value);
			form_data.append('address', this.edit_tourism_company_frm.get('address').value);
			form_data.append('director_name', this.edit_tourism_company_frm.get('director_name').value);
			form_data.append('director_email', this.edit_tourism_company_frm.get('director_email').value);
			form_data.append('director_phone_number', this.edit_tourism_company_frm.get('director_phone_number').value);
			form_data.append('manager_email', this.edit_tourism_company_frm.get('manager_email').value);
			// form_data.append('user_role', this.edit_tourism_company_frm.get('user_role').value);		
			this.userApi.edit_user(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/user/list-tourism-company'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
