import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditTourismCompanyComponent } from './edit-tourism-company.component';

const routes: Routes = [
  {
    path: '',
    component: EditTourismCompanyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditTourismCompanyRoutingModule { }
