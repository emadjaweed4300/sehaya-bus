import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTourismCompanyComponent } from './edit-tourism-company.component';

describe('EditTourismCompanyComponent', () => {
  let component: EditTourismCompanyComponent;
  let fixture: ComponentFixture<EditTourismCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTourismCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTourismCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
