import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewTourismCompanyComponent } from './view-tourism-company.component';

const routes: Routes = [
  {
    path: '',
    component: ViewTourismCompanyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewTourismCompanyRoutingModule { }
