import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTourismCompanyComponent } from './view-tourism-company.component';

describe('ViewTourismCompanyComponent', () => {
  let component: ViewTourismCompanyComponent;
  let fixture: ComponentFixture<ViewTourismCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTourismCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTourismCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
