import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewTourismCompanyRoutingModule } from './view-tourism-company-routing.module';
import { ViewTourismCompanyComponent } from './view-tourism-company.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ViewTourismCompanyRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ViewTourismCompanyComponent]
})
export class ViewTourismCompanyModule { }
