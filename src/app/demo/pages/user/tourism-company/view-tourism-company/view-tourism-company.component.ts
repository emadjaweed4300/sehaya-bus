import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserApiService } from '../../shared/user_api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-view-clients',
	templateUrl: './view-tourism-company.component.html',
	styleUrls: ['./view-tourism-company.component.scss']
})
export class ViewTourismCompanyComponent implements OnInit {

	tourism_company_list: any = [];
	tourism_company_id
	company: any;

	constructor(
		private router: Router,
		private __route: ActivatedRoute,
		private userApi: UserApiService
	) { 
		this.tourism_company_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		this.get_tourism_comapny();
	}

	// Get All FeatureAPI
	get_tourism_comapny(){
		// this.vehicle_provider_id = this.__route.snapshot.paramMap.get('id');
		this.userApi.get_tourism_company(this.tourism_company_id).then((response: any) => {
			console.log('Response:', response);
			this.company = response.user_details;
			// formControlName & postman response
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

}
