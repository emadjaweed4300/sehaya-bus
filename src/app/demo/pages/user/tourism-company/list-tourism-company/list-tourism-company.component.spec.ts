import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTourismCompanyComponent } from './list-tourism-company.component';

describe('ListTourismCompanyComponent', () => {
  let component: ListTourismCompanyComponent;
  let fixture: ComponentFixture<ListTourismCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTourismCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTourismCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
