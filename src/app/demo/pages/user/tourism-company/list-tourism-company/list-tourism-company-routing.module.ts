import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListTourismCompanyComponent } from './list-tourism-company.component';

const routes: Routes = [
  {
    path: '',
    component: ListTourismCompanyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListTourismCompanyRoutingModule { }
 