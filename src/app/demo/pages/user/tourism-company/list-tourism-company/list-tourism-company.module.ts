import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListTourismCompanyRoutingModule } from './list-tourism-company-routing.module';
import { ListTourismCompanyComponent } from './list-tourism-company.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { SelectModule } from 'ng-select';


@NgModule({
  imports: [
    CommonModule,
    ListTourismCompanyRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    SelectModule
  ],
  declarations: [ListTourismCompanyComponent]
})
export class ListTourismCompanyModule { }
