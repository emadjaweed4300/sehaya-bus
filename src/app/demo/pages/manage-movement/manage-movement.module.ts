import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageMovementRoutingModule } from './manage-movement-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ManageMovementRoutingModule,
  ],
  declarations: []
})
export class ManageMovementModule { }
