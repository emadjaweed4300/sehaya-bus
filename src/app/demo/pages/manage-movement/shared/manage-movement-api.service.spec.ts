import { TestBed } from '@angular/core/testing';

import { ManageMovementApiService } from './manage-movement-api.service';

describe('ManageMovementApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageMovementApiService = TestBed.get(ManageMovementApiService);
    expect(service).toBeTruthy();
  });
});
