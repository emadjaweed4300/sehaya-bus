import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// // movement list,add,view,edit components
			// {
			// 	path: 'list-movement',
			// 	loadChildren: () => import('./movement/list-movement/list-movement.module').then(module => module.ListMovementModule)
			// },
			// {
			// 	path: 'add-movement',
			// 	loadChildren: () => import('./movement/add-movement/add-movement.module').then(module => module.AddMovementModule)
			// },
			// {
			// 	path: 'edit-movement/:id',
			// 	loadChildren: () => import('./movement/edit-movement/edit-movement.module').then(module => module.EditMovementModule)
			// },

		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManageMovementRoutingModule { }
