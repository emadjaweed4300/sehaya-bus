import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Event Circuit list,add,view,edit components
			{
				path: 'list-event-circuit',
				loadChildren: () => import('./event-circuit/list-event-circuit/list-event-circuit.module').then(module => module.ListEventCircuitModule)
			},
			{
				path: 'list-event-circuit/:setting_id',
				loadChildren: () => import('./event-circuit/list-event-circuit/list-event-circuit.module').then(module => module.ListEventCircuitModule)
			},
			// {
			// 	path: 'add-event-circuit/:setting_id',
			// 	loadChildren: () => import('./event-circuit/add-event-circuit/add-event-circuit.module').then(module => module.AddEventCircuitModule)
			// },
			{
				path: 'add-event-circuit',
				loadChildren: () => import('./event-circuit/add-event-circuit/add-event-circuit.module').then(module => module.AddEventCircuitModule)
			},
			// {
			// 	path: 'view-staff-members',
			// 	loadChildren: () => import('./event/view-event/view-event.module').then(module => module.ViewEventModule)
			// },
			{
				path: 'edit-event-circuit/:id',
				loadChildren: () => import('./event-circuit/edit-event-circuit/edit-event-circuit.module').then(module => module.EditEventCircuitModule)
			},

		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class EventsRoutingModule { }
