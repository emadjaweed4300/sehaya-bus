import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEventCircuitComponent } from './edit-event-circuit.component';

describe('EditEventCircuitComponent', () => {
  let component: EditEventCircuitComponent;
  let fixture: ComponentFixture<EditEventCircuitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditEventCircuitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEventCircuitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
