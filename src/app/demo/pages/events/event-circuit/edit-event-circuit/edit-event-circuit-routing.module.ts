import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditEventCircuitComponent } from './edit-event-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: EditEventCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditEventCircuitRoutingModule { }
