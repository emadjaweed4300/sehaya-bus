import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditEventCircuitRoutingModule } from './edit-event-circuit-routing.module';
import { EditEventCircuitComponent } from './edit-event-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditEventCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditEventCircuitComponent]
})
export class EditEventCircuitModule { }
