import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EventApiService } from '../../shared/event-api.service';
import Swal from 'sweetalert2';
import { formatDate, DatePipe } from '@angular/common';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';

@Component({
	selector: 'app-edit-event-circuit',
	templateUrl: './edit-event-circuit.component.html',
	styleUrls: ['./edit-event-circuit.component.scss']
})
export class EditEventCircuitComponent implements OnInit {

	edit_event_circuit_frm: FormGroup;
	current_date = (new Date()).toISOString().substring(0, 10)
	today= new Date();
	  current_time = '';
	  
	event_circuit_id
	event_circuit: any;
	circuits = [];
	event_circuits = [];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private eventApi: EventApiService,
		private circuitApi: CircuitApiService,
		public datepipe: DatePipe
	) { 
		this.event_circuit_id = this.__route.snapshot.paramMap.get('id');
		this.current_time = formatDate(this.today, 'hh:mm a', 'en-US', '+0530');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_event_circuit_frm = this.__fb.group({
			circuit: ["", Validators.required],
			event_circuit: ["", Validators.required],
			date: ["", Validators.required],
			hour: ["", Validators.required],
			// date_creation: [{ value: this.current_date, disabled: true }, [Validators.required]],
			// time_creation: [{ value: this.current_time, disabled: true }, [Validators.required]],
			description: ["", Validators.required],
		});
		this.get_circuits();
		this.get_evet_circuits();
		this.get_event_circuit();
	}

	// Get Circuit list Dropdown
	get_circuits() {
		this.circuitApi.circuits().then((response: any) => {
			console.log('Response:', response);
			this.circuits = response.circuit
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Circuit list Dropdown
	get_evet_circuits() {
		this.eventApi.get_event_circuit().then((response: any) => {
			console.log('Response:', response);
			this.event_circuits = response.event_masters
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Value By ID
	get_event_circuit(){
		this.eventApi.get_event_circuits(this.event_circuit_id).then((response: any) => {
			console.log('Response:', response);
			this.event_circuit = response.event;
			this.edit_event_circuit_frm.get('circuit').setValue(response.event.circuit_id);
			this.edit_event_circuit_frm.get('event_circuit').setValue(response.event.event_master);
			this.edit_event_circuit_frm.get('date').setValue(this.DateFormate(response.event.date));
			this.edit_event_circuit_frm.get('hour').setValue(response.event.hour);
			// this.edit_event_circuit_frm.get('date_creation').setValue(this.DateFormate(response.event.genration_date));
			// this.edit_event_circuit_frm.get('time_creation').setValue(response.event.genration_time);
			this.edit_event_circuit_frm.get('description').setValue(response.event.discription);
			console.log(response.event.genration_time)
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	// Date Formate
	DateFormate(Date) {
		return this.datepipe.transform(Date, 'yyyy-MM-dd');
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_event_circuit_frm.get(formControlName).errors &&
			(this.edit_event_circuit_frm.get(formControlName).touched ||
				this.edit_event_circuit_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_event_circuit_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_event_circuit_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_event_circuit_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_event_circuit_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_event_circuit_frm);
		if (this.edit_event_circuit_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('event_id', this.event_circuit_id);
			form_data.append('circuit_id', this.edit_event_circuit_frm.get('circuit').value);
			form_data.append('event_master', this.edit_event_circuit_frm.get('event_circuit').value);
			form_data.append('date', this.edit_event_circuit_frm.get('date').value);
			form_data.append('hour', this.edit_event_circuit_frm.get('hour').value);
			form_data.append('genration_date', null);
			form_data.append('time', null);
			form_data.append('discription', this.edit_event_circuit_frm.get('description').value);

			this.eventApi.edit_event_circuit(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/events/list-event-circuit'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}


}
