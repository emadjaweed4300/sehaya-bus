import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListEventCircuitRoutingModule } from './list-event-circuit-routing.module';
import { ListEventCircuitComponent } from './list-event-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListEventCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListEventCircuitComponent]
})
export class ListEventCircuitModule { }
