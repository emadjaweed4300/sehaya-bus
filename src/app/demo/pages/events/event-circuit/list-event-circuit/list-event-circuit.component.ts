import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { EventApiService } from '../../shared/event-api.service';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';
import { ActivatedRoute } from '@angular/router';


@Component({
	selector: 'app-list-event-circuit',
	templateUrl: './list-event-circuit.component.html',
	styleUrls: ['./list-event-circuit.component.scss']
})
export class ListEventCircuitComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	event_Circuit_List: any = [];

	circuits = [];
	event_circuits = [];
	// for circuit filter
	circuit_id = ''

	// for filter
	circuit = '';
	event_circuit = '';
	from_date = '';
	to_date = '';
	// For clear value of filter
	clear() {
		this.circuit = '';
		this.event_circuit = '';
		this.from_date = '';
		this.to_date = '';
	}

	constructor(
		private eventApi: EventApiService,
		private circuitApi: CircuitApiService,
		private __route: ActivatedRoute,
	) {
		this.circuit_id = this.__route.snapshot.paramMap.get('setting_id');
	}

	ngOnInit() {
		this.dtExportButtonOptions = {
			// 		ajax: 'fake-data/datatable-data.json',
			// 		columns: [
			// 			{
			// 				title: 'Id',
			// 				data: 'id'
			// 			},
			// 			{
			// 				title: 'Name',
			// 				data: 'name'
			// 			},
			// 			{
			// 				title: 'Office',
			// 				data: 'office'
			// 			},
			// 			{
			// 				title: 'Age',
			// 				data: 'age'
			// 			}, 
			// 			{
			// 				title: 'Start Date',
			// 				data: 'date'
			// 			}, 
			// 			{
			// 				title: 'Salary',
			// 				data: 'salary'
			// 			}
			// 		],
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.list_event_circuits();
		this.get_circuits();
		this.get_evet_circuits();
		if (this.circuit_id != null) {
			this.circuitFilterAction()
		}
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// Get Circuit list Dropdown
	get_circuits() {
		this.circuitApi.circuits().then((response: any) => {
			console.log('Response:', response);
			this.circuits = response.circuit
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Circuit list Dropdown
	get_evet_circuits() {
		this.eventApi.get_event_circuit().then((response: any) => {
			console.log('Response:', response);
			this.event_circuits = response.event_masters
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	list_event_circuits() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.eventApi.event_circuits(formData).then((response: any) => {
			console.log('Response:', response);
			this.event_Circuit_List = response.event_circuit
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('circuit', this.circuit);
		formData.append('event_circuit', this.event_circuit);
		formData.append('from_date', this.from_date);
		formData.append('to_date', this.to_date);
		formData.append('add_filter', '1');
		this.eventApi.event_circuits(formData).then((response: any) => {
			console.log('Response:', response);
			this.event_Circuit_List = response.event_circuit
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Event Navigation with Filter Function
	public circuitFilterAction() {
		let formData = new FormData()
		formData.append('circuit', this.circuit_id);
		formData.append('add_filter', '1');
		console.log(this.circuit_id)
		this.eventApi.event_circuits(formData).then((response: any) => {
			console.log('Response:', response);
			this.event_Circuit_List = response.event_circuit
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.list_event_circuits()
	}

	// delete Brand
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.eventApi.delete_event_circuit(id).then((response: any) => {
					console.log('Response:', response);
					this.list_event_circuits();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
							type: 'error',
							title: 'Error!',
							text: error,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
					});
			}
		})
	}

}
