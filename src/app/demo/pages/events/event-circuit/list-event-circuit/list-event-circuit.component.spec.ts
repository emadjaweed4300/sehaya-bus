import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEventCircuitComponent } from './list-event-circuit.component';

describe('ListEventCircuitComponent', () => {
  let component: ListEventCircuitComponent;
  let fixture: ComponentFixture<ListEventCircuitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEventCircuitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEventCircuitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
