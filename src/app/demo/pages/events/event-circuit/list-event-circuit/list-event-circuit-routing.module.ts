import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListEventCircuitComponent } from './list-event-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: ListEventCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListEventCircuitRoutingModule { }
