import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEventCircuitComponent } from './add-event-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: AddEventCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddEventCircuitRoutingModule { }
