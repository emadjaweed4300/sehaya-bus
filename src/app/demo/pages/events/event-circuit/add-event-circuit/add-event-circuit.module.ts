import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddEventCircuitRoutingModule } from './add-event-circuit-routing.module';
import { AddEventCircuitComponent } from './add-event-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddEventCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddEventCircuitComponent]
})
export class AddEventCircuitModule { }
