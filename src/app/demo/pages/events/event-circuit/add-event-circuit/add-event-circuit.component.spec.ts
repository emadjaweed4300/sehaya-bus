import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEventCircuitComponent } from './add-event-circuit.component';

describe('AddEventCircuitComponent', () => {
  let component: AddEventCircuitComponent;
  let fixture: ComponentFixture<AddEventCircuitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEventCircuitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEventCircuitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
