import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { EventApiService } from '../../shared/event-api.service';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';
import { formatDate } from '@angular/common';


@Component({
	selector: 'app-add-event-circuit',
	templateUrl: './add-event-circuit.component.html',
	styleUrls: ['./add-event-circuit.component.scss']
})
export class AddEventCircuitComponent implements OnInit {

	add_event_circuit_frm: FormGroup;
	current_date = (new Date()).toISOString().substring(0, 10)
	// current_time = (new Date()).toISOString().slice(0, 19);
	
	today= new Date();
  	current_time = '';

	circuits = [];
	event_circuits = [];
	setting_id

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private circuitApi: CircuitApiService,
		private eventApi: EventApiService,
	) { 
		this.current_time = formatDate(this.today, 'hh:mm a', 'en-US', '+0530');
		this.setting_id = this.__route.snapshot.paramMap.get('setting_id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.add_event_circuit_frm = this.__fb.group({
			circuit: [null, Validators.required],
			event_circuit: ["", Validators.required],
			date: ["", Validators.required],
			hour: ["", Validators.required],
			// date_creation: [{ value: this.current_date, disabled: true }, [Validators.required]],
			// time_creation: [{ value: this.current_time, disabled: true }, [Validators.required]],
			description: ["", Validators.required],
		});
		this.add_event_circuit_frm.get('circuit').setValue(this.setting_id);
		this.get_circuits();
		this.get_evet_circuits();
	}

	// Get Circuit list Dropdown
	get_circuits() {
		this.circuitApi.circuits().then((response: any) => {
			console.log('Response:', response);
			this.circuits = response.circuit
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Circuit list Dropdown
	get_evet_circuits() {
		this.eventApi.get_event_circuit().then((response: any) => {
			console.log('Response:', response);
			this.event_circuits = response.event_masters
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let maxLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_event_circuit_frm.get(formControlName).errors &&
			(this.add_event_circuit_frm.get(formControlName).touched ||
				this.add_event_circuit_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_event_circuit_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_event_circuit_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_event_circuit_frm.get(formControlName)
				.errors.minlength;
			maxLengthValidate = this.add_event_circuit_frm.get(formControlName)
				.errors.maxlength;
			notEquivalentValidate = this.add_event_circuit_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			maxLengthValidate: maxLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_event_circuit_frm);
		if (this.add_event_circuit_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();

			form_data.append('circuit_id', this.add_event_circuit_frm.get('circuit').value);
			form_data.append('event_master', this.add_event_circuit_frm.get('event_circuit').value);
			form_data.append('date', this.add_event_circuit_frm.get('date').value);
			form_data.append('hour', this.add_event_circuit_frm.get('hour').value);
			form_data.append('genration_date', null);
			form_data.append('time', null);
			form_data.append('discription', this.add_event_circuit_frm.get('description').value);

			this.eventApi.add_event_circuit(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				this.router.navigate(['/events/list-event-circuit'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});


		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
