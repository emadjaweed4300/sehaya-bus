import { TestBed } from '@angular/core/testing';

import { CircuitApiService } from './circuit-api.service';

describe('CircuitApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CircuitApiService = TestBed.get(CircuitApiService);
    expect(service).toBeTruthy();
  });
});
