import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global.service';
import { throwError } from 'rxjs';
import Swal from 'sweetalert2';
import { END_POINT } from 'src/app/constant';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class CircuitApiService {

	data = new FormData();
	constructor(
		private http: HttpClient,
		private translate: TranslateService,
		private global: GlobalService,
		private router: Router
	) {
		this.data.append('user_id', this.global.currentUser.id);
		this.data.append('user_role', this.global.currentUser.user_role);
		this.data.append('security_key', this.global.currentUser.token)
	}

	// Http Options
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	}

	// Handle API errors
	handleError(error: HttpErrorResponse) {

		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${JSON.stringify(error.status)}, ` +
				`body was: ${JSON.stringify(error.message)}`);
		}
		// return an observable with a user-facing error message
		if (error.status == 0) {
			return throwError(
				Swal.fire(
					'The Internet?',
					'Please try again later',
					'question'
				)
			);
		}
	};

	// reference id
	get_reference() {
		return this._callAPI('/get-circuit-reference', this.data)
	}

	// Users
	users() {
		return this._callAPI('/users', this.data)
	}

	// Movement List
	root_list() {
		return this._callAPI('/routes', this.data)
	}

	// Add Movement
	get_movement_list(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/get-route-destinations', data)
	}

	// Get Staff
	get_staff(function_id) {
		return this._callAPI('/get-staff/' + function_id, this.data)
	}

	// Circuit API's
	// Add Circuit
	add_circuit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-circuit', data)
	}

	// Use on the expense circuit
	circuits() {
		return this._callAPI('/circuits', this.data)
	}
	
	list_circuits(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/circuits', data)
	}

	// Delete Circuit
	delete_circuit(id) {
		return this._callAPI('/delete-circuit/' + id, this.data)
	}

	// get All driver from circuit
	drivers_for_filter() {
		return this._callAPI('/get-all-circuit-drivers', this.data)
	}

	// Get Circuit
	get_circuit(id) {
		return this._callAPI('/get-circuit/' + id, this.data)
	}

	// Edit Circuit Fileds
	edit_circuit_details(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-circuit-deatils', data)
	}

	// Edit circuit account feilds
	edit_circuit_account_details(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-circuit-account-deatils', data)
	}

	// Edit circuit root table
	edit_circuit_route_details(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-circuit-route-deatils', data)
	}

	// // Edit circuit affection table
	// edit_circuit_route_affection(data) {
	// 	data.append('user_id', this.global.currentUser.id);
	// 	data.append('user_role', this.global.currentUser.user_role);
	// 	data.append('security_key', this.global.currentUser.token)
	// 	return this._callAPI('/edit-circuit-route-affections', data)
	// }

	// get Circuit by ID
	get_circuit_by_id(id) {
		return this._callAPI('/get-circuit/' + id, this.data)
	}

	// // Edit Affection Row
	// edit_affection_details(data) {
	// 	data.append('user_id', this.global.currentUser.id);
	// 	data.append('user_role', this.global.currentUser.user_role);
	// 	data.append('security_key', this.global.currentUser.token)
	// 	return this._callAPI('/edit-circuit-route-affections', data)
	// }

	// Edit Passenger List
	edit_passenger_list(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-passenger-details', data)
	}

	// Edit root details
	edit_root_details(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-circuit-route-deatils', data)
	}

	// Status Changes
	circuit_status(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/change-circuit-status', data)
	}

	// Vehicle Avilibility API
	vehicle_avilibility(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/validate-availability-vehicle', data)
	}

	// delete passenger row API
	delete_passenger_row(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/delete-existing-passenger', data)
	}

	// delete root row API
	delete_root_row(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/delete-existing-destination', data)
	}

	// delete affection row API
	delete_affection_row(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/delete-existing-affections', data)
	}

	// Edit circuit root_details
	edit_circuit_root_details(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-circuit-route-details', data)
	}

	// Edit circuit affectiona_details
	edit_circuit_affectiona_details(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-circuit-route-affections', data)
	}

	// Get Location Type
	location_type() {
		return this._callAPI('/location-type-master', this.data)
	}

	_callAPI(url: string, params?) {
		return new Promise((resolve, reject) => {
			this.http.post(END_POINT + url, params)
				.subscribe(res => {
					if (res['response'] === 'success') {
						resolve(res)
					} else {
						if (res['response_code'] == 405 || res['response_code'] == 403 || res['response_code'] == 204){
							localStorage.clear();
							return window.location.replace('/auth/signin');
						}
						reject(res['message'])
					}
				}, (this.handleError))
		})
	}
}
