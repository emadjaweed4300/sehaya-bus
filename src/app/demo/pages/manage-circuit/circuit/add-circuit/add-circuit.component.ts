import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import Swal from 'sweetalert2';
import { CircuitApiService } from '../../shared/circuit-api.service';
import { VehicleApiService } from '../../../manage-vehicle/shared/vehicle-api.service';
import { StaffMemberApiService } from '../../../manage-staff/shared/staff-member-api.service';
import { data } from 'jquery';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
declare var $: any;

@Component({
	selector: 'app-add-circuit',
	templateUrl: './add-circuit.component.html',
	styleUrls: ['./add-circuit.component.scss']
})
export class AddCircuitComponent implements OnInit {

	add_circuit_frm: FormGroup
	current_date = (new Date()).toISOString().substring(0, 10)
	path_table: any = {}
	staff_functions = [];
	users = [];
	staff_members = [];
	root_list = [];
	driver = [];
	vehilces = [];
	cities = [];
	countries = [];
	location_types = [];

	// locationTypeArr: any[] = [
	// 	{ id: 1, name: 'Circuit' },
	// 	{ id: 2, name: 'Excursion' },
	// ];

	paymentModeArr: any[] = [
		{ id: 1, name: 'Cash' },
		{ id: 2, name: 'Cheeque' },
		{ id: 3, name: 'Transfer' }
	];

	invoiceModeArr: any[] = [
		{ id: 1, name: 'Flate Rate' },
		{ id: 2, name: 'Calculated' },
	];

	setting_id
	departureMinDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
	arrivalMinDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

	minimumDate
	maximumDate

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService,
		private circuitApi: CircuitApiService,
		private vehicleApi: VehicleApiService,
		private staffMemberApiService: StaffMemberApiService,
		private translate: TranslateService,
		private datePipe: DatePipe
	) {

	}

	ngOnInit() {
		/** Form builder instance */
		this.add_circuit_frm = this.__fb.group({
			reference: [{ value: '', disabled: true }, [Validators.required]],
			client: ['', [Validators.required]],
			date_creation: [{ value: this.current_date, disabled: true }, [Validators.required]],
			path: ['', [Validators.required]],
			location_type: ['', [Validators.required]],
			status: ['1'],
			status_name: [{ value: 'Creation', disabled: true }, [Validators.required]],
			departure_date: ['', [Validators.required]],
			arrival_date: ['', [Validators.required]],
			function_name: ['', [Validators.required]],
			driver: ['', [Validators.required]],
			vehicle: ['', [Validators.required]],
			passenger_list: this.__fb.array([
				// this.getRootArray()
			]),
			root_movement: this.__fb.array([
				// this.getRootArray()
			]),
			affection_details: this.__fb.array([
				// this.getAffectionArray()
			]),
			payment_mode: ['', [Validators.required]],
			invoice_type: [1, [Validators.required]],
			flat_rate: ['', [Validators.required, Validators.pattern("^[0-9]+$")]],
			passenger_number: ['', [Validators.required, Validators.pattern("^[0-9]+$")]],
			tourist_country: [148, [Validators.required]],
			// file_number: ['', [Validators.required]], 
			// authorization_number: ['', [Validators.required]],
			commarcial: ['', [Validators.required]],
			nb_km: [{ value: '', disabled: true }, [Validators.required, Validators.pattern("^[0-9]+$")]],
			location_description: ['', [Validators.required]],
		});
		this.path_table = {
			"paging": false,
			"ordering": false,
			"info": false,
			"filter": false

		}
		this.get_reference();
		this.getUsers();
		this.get_staff_functions();
		this.get_root_list();
		this.get_vehicle();
		this.getCommarcial();
		this.get_cities();
		this.get_countries();
		this.get_location_types();
		// $(".datetimepicker2").datepicker({ minDate: 0 });
		// $('.datetimepicker').datepicker({ startDate: new Date() });
	}

	getRootArray(destination_name, destination_id) {
		return this.__fb.group({
			schedule_date: ['', [Validators.required]],
			actual_date: ['', [Validators.required]],
			destination: [destination_id],
			destination_name: [destination_name],
		});
	}

	// New Row Array for Passenger Table
	getPassengerBlankArray() {
		return this.__fb.group({
			Full_Name: ["", [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			Mobile_No: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			Age: ['', [Validators.required, Validators.pattern("^[0-9]+$")]],
			Gender: ["", [Validators.required]],
		});
	}

	// New Row Array for Root Table
	getRootBlankArray() {
		return this.__fb.group({
			destination: [""],
			destination_name: [""],
			schedule_date: ["", [Validators.required]],
			actual_date: ["", [Validators.required]]
		});
	}

	getAffectionArray(destination) {
		return this.__fb.group({
			destination: [destination],
			vehicle: ["", Validators.required],
			from_date: ["", [Validators.required]],
			to_date: ["", [Validators.required]],
			km_start: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			km_arrival: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			driver: ["", [Validators.required]],
			comment: ["", [Validators.required]],
		});
	}

	// New Row Array for Affection Table
	getAffectionBlankArray() {
		return this.__fb.group({
			destination: [''],
			vehicle: ["", Validators.required],
			from_date: ["", [Validators.required]],
			to_date: ["", [Validators.required]],
			km_start: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			km_arrival: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			driver: ["", [Validators.required]],
			comment: ["", [Validators.required]],
		});
	}

	// reference id
	get_reference() {
		this.circuitApi.get_reference().then((response: any) => {
			console.log('Response:', response);
			this.add_circuit_frm.get('reference').setValue(response.reference);
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	getUsers() {
		this.circuitApi.users().then((response: any) => {
			console.log('Response:', response);
			this.users = response.list_of_users
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Commarcial field (get staff member API)
	getCommarcial() {
		let formData = new FormData()
		this.staffMemberApiService.staff_members(formData).then((response: any) => {
			console.log('Response:', response);
			this.staff_members = response.staff_members
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// reference id
	get_root_list() {
		this.circuitApi.root_list().then((response: any) => {
			console.log('Response:', response);
			this.root_list = response.routes
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Countries
	get_countries() {
		this.masterApi.countries().then((response: any) => {
			console.log('Response:', response);
			this.countries = response.countries
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	changeRoot() {
		if (this.add_circuit_frm.get('path').value !== '') {
			const control1 = <FormArray>this.add_circuit_frm.controls['root_movement'];
			const control2 = <FormArray>this.add_circuit_frm.controls['affection_details'];
			control1.clear();
			control2.clear();
			let form_data = new FormData();
			form_data.append('route_id', this.add_circuit_frm.get('path').value);
			this.circuitApi.get_movement_list(form_data).then((response: any) => {
				console.log('Response:', response);
				for (let item of response.destination) {
					control1.push(this.getRootArray(item.destination_name, item.destination))
					control2.push(this.getAffectionArray(item.destination))
					// console.log(control1['controls'])
				}
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	// Departure Date change function for Date Block
	departureDateChange() {
		this.minimumDate = this.add_circuit_frm.get('departure_date').value
		// console.log(this.minimumDate)
	}

	// Arrival Date change function for Date Block
	arrivalDateChange() {
		// Check Arrival Date must be greater Departure Date!
		if (new Date(this.add_circuit_frm.get('departure_date').value) >= new Date(this.add_circuit_frm.get('arrival_date').value)) {
			var errorMsg = ''
			this.translate.get('add_circuit.Arrival Date must be greater Departure Date!').subscribe(val => {
				errorMsg = val
			})
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: errorMsg,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		}

		this.maximumDate = this.add_circuit_frm.get('arrival_date').value
		// console.log(this.maximumDate)
	}

	// Get Staff Function Dropdown
	get_staff_functions() {
		this.masterApi.list_staff_function().then((response: any) => {
			console.log('Response:', response);
			this.staff_functions = response.driver
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	staffChange() {
		this.circuitApi.get_staff(this.add_circuit_frm.get('function_name').value).then((response: any) => {
			console.log('Response:', response);
			this.driver = response.$drivers

		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Get Vehicle Dropdown
	get_vehicle() {
		let formData = new FormData()
		this.vehicleApi.list_vehicles(formData).then((response: any) => {
			console.log('Response:', response);
			this.vehilces = response.vehicles
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Cities Dropdown
	get_cities() {
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Cities Dropdown
	get_location_types() {
		this.circuitApi.location_type().then((response: any) => {
			console.log('Response:', response);
			this.location_types = response.location_type
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Check Vehicle Avilibility for vehicle
	check_vehicle_avilibility() {
		let form_data = new FormData();
		form_data.append('vehicle_id', this.add_circuit_frm.get('vehicle').value);
		form_data.append('departure_date', this.add_circuit_frm.get('departure_date').value);
		form_data.append('arrival_date', this.add_circuit_frm.get('arrival_date').value);
		// let vehicle_id = this.add_circuit_frm.get('vehicle').value;
		// let departure_date = this.add_circuit_frm.get('departure_date').value;
		// let arrival_date = this.add_circuit_frm.get('arrival_date').value;
		// console.log(form_data)
		this.circuitApi.vehicle_avilibility(form_data).then((response: any) => {
			console.log('Response:', response);
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	enableFlatRate() {
		if (this.add_circuit_frm.get('invoice_type').value == 2) {
			this.add_circuit_frm.get('flat_rate').disable();
		} else {
			this.add_circuit_frm.get('flat_rate').enable();
		}
	}

	calcualteKM() {
		let first = this.add_circuit_frm['controls'].affection_details['controls'][0];
		let last = this.add_circuit_frm['controls'].affection_details['controls'][this.add_circuit_frm['controls'].affection_details['controls'].length - 1];
		if (last['controls'].km_arrival.value !== '') {
			let calculateValue = parseInt(last['controls'].km_arrival.value) - parseInt(first['controls'].km_start.value)
			this.add_circuit_frm.get('nb_km').setValue(calculateValue)
		}
	}

	// Add Passenger Button Functionality
	addPassengerRow() {
		const control = <FormArray>this.add_circuit_frm.controls['passenger_list'];
		control.push(this.getPassengerBlankArray());
		return true;
	}
	// Delete Passenger Button Functionality
	deletePassengerRow(index) {
		const control = <FormArray>this.add_circuit_frm.controls['passenger_list'];
		if (control.length == 1) {

		} else {
			control.removeAt(index);
		}
	}

	addRootRow() {
		const control = <FormArray>this.add_circuit_frm.controls['root_movement'];
		control.push(this.getRootBlankArray());
		return true;
	}

	deleteRootRow(index) {
		const control = <FormArray>this.add_circuit_frm.controls['root_movement'];
		if (control.length == 1) {

		} else {
			control.removeAt(index);
		}
	}

	addAffectionRow() {
		const control = <FormArray>this.add_circuit_frm.controls['affection_details'];
		control.push(this.getAffectionBlankArray());
		return true;
	}

	deleteAffectionRow(index) {
		const control = <FormArray>this.add_circuit_frm.controls['affection_details'];
		if (control.length == 1) {

		} else {
			control.removeAt(index);
		}
	}


	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_circuit_frm.get(formControlName).errors &&
			(this.add_circuit_frm.get(formControlName).touched ||
				this.add_circuit_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_circuit_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_circuit_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_circuit_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_circuit_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save() {
		this.markFormGroupTouched(this.add_circuit_frm);
		if (this.add_circuit_frm.valid) {
			if (this.add_circuit_frm.get('root_movement').value.length !== this.add_circuit_frm.get('affection_details').value.length) {
				var msg = ''
				this.translate.get('add_circuit.Root Table and Affection Table Rows are not Equal').subscribe(val => {
					msg = val
				})
				return Swal.fire({
					type: 'warning',
					title: 'Warning!',
					text: msg,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			}

			if (this.add_circuit_frm.get('passenger_number').value != this.add_circuit_frm.get('passenger_list').value.length) {
				console.log(this.add_circuit_frm.get('passenger_number').value)
				console.log(this.add_circuit_frm.get('passenger_list').value.length)
				var msg = ''
				this.translate.get('add_circuit.No of Passenger and Passenger List Row are not Equal').subscribe(val => {
					msg = val
				})
				return Swal.fire({
					type: 'warning',
					title: 'Warning!',
					text: msg,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			}	

			/** Prepare form data */
			const form_data = new FormData();
			// form_data.append('user_id', null);
			form_data.append('refrence', this.add_circuit_frm.get('reference').value);
			form_data.append('client_id', this.add_circuit_frm.get('client').value);
			form_data.append('current_date', this.add_circuit_frm.get('date_creation').value);

			form_data.append('route_id', this.add_circuit_frm.get('path').value);
			form_data.append('type_location', this.add_circuit_frm.get('location_type').value);
			form_data.append('circuit_status', this.add_circuit_frm.get('status').value);
			form_data.append('departure_date', this.add_circuit_frm.get('departure_date').value);
			form_data.append('arrival_date', this.add_circuit_frm.get('arrival_date').value);
			// form_data.append('path', this.add_circuit_frm.get('function_name').value);
			form_data.append('driver_id', this.add_circuit_frm.get('driver').value);
			form_data.append('vehicle_id', this.add_circuit_frm.get('vehicle').value);
			form_data.append('function_id', this.add_circuit_frm.get('function_name').value);
			// passengers (API Array parameter Name) & passenger_list (Array formControlName)
			form_data.append('passengers', JSON.stringify(this.add_circuit_frm.get('passenger_list').value));
			console.log(this.add_circuit_frm.get('passenger_list').value)
			// destination (API Array parameter Name) & root_movement (Array formControlName)
			form_data.append('destination', JSON.stringify(this.add_circuit_frm.get('root_movement').value));
			console.log(this.add_circuit_frm.get('root_movement').value)
			// affections (API Array parameter Name) & affection_details (Array formControlName)
			form_data.append('affections', JSON.stringify(this.add_circuit_frm.get('affection_details').value));
			console.log(this.add_circuit_frm.get('affection_details').value)

			form_data.append('payment_mode', this.add_circuit_frm.get('payment_mode').value);
			form_data.append('invoice_type', this.add_circuit_frm.get('invoice_type').value);
			form_data.append('flat_rate', this.add_circuit_frm.get('flat_rate').value);
			form_data.append('no_of_km', this.add_circuit_frm.get('nb_km').value);
			form_data.append('location_description', this.add_circuit_frm.get('location_description').value)

			form_data.append('passenger_number', this.add_circuit_frm.get('passenger_number').value);
			form_data.append('tourist_country', this.add_circuit_frm.get('tourist_country').value);
			form_data.append('commercial', this.add_circuit_frm.get('commarcial').value);
			console.log(form_data);
			this.circuitApi.add_circuit(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				this.router.navigate(['/manage-circuit/list-circuit'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
