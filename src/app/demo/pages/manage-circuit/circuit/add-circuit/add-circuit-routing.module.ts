import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddCircuitComponent } from './add-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: AddCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddCircuitRoutingModule { }
