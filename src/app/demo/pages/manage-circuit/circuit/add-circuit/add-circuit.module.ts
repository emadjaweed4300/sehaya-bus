import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddCircuitRoutingModule } from './add-circuit-routing.module';
import { AddCircuitComponent } from './add-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddCircuitComponent]
})
export class AddCircuitModule { }
