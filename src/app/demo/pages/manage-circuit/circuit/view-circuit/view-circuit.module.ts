import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewCircuitRoutingModule } from './view-circuit-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { ViewCircuitComponent } from './view-circuit.component';

 
@NgModule({ 
  imports: [
    CommonModule,
    ViewCircuitRoutingModule,
    SharedModule, 
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ViewCircuitComponent]
})
export class ViewCircuitModule { }
