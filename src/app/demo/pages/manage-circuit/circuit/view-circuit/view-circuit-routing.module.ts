import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewCircuitComponent } from './view-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: ViewCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewCircuitRoutingModule { }
