import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditCircuitRoutingModule } from './edit-circuit-routing.module';
import { EditCircuitComponent } from './edit-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditCircuitComponent]
})
export class EditCircuitModule { }
