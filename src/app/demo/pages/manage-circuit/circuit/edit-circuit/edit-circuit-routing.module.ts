import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditCircuitComponent } from './edit-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: EditCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditCircuitRoutingModule { }
