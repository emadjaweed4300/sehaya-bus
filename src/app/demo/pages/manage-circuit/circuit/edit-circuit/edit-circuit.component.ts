import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import Swal from 'sweetalert2';
import { CircuitApiService } from '../../shared/circuit-api.service';
import { VehicleApiService } from '../../../manage-vehicle/shared/vehicle-api.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { StaffMemberApiService } from '../../../manage-staff/shared/staff-member-api.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
	selector: 'app-edit-circuit',
	templateUrl: './edit-circuit.component.html',
	styleUrls: ['./edit-circuit.component.scss']
})
export class EditCircuitComponent implements OnInit {

	edit_circuit_frm: FormGroup
	current_date = (new Date()).toISOString().substring(0, 10)
	path_table: any = {}
	staff_functions = [];
	users = [];
	staff_members = [];
	root_list = [];
	driver = [];
	vehilces = [];
	cities = [];
	countries = [];
	location_types = [];

	circuit_id
	circuit: any

	// locationTypeArr: any[] = [
	// 	{ id: 1, name: 'Circuit' },
	// 	{ id: 2, name: 'Excursion' },
	// ];

	paymentModeArr: any[] = [
		{ id: 1, name: 'Cash' },
		{ id: 2, name: 'Cheeque' },
		{ id: 3, name: 'Transfer' }
	];

	invoiceModeArr: any[] = [
		{ id: 1, name: 'Flate Rate' },
		{ id: 2, name: 'Calculated' },
	];

	departureMinDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
	arrivalMinDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

	minimumDate
	maximumDate

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService,
		private circuitApi: CircuitApiService,
		private vehicleApi: VehicleApiService,
		public datepipe: DatePipe,
		private staffMemberApiService: StaffMemberApiService,
		private translate: TranslateService,
		private datePipe: DatePipe
	) {
		this.circuit_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_circuit_frm = this.__fb.group({
			reference: [{ value: '', disabled: true }],
			client: ['', [Validators.required]],
			date_creation: [{ value: this.current_date, disabled: true }],
			path: [{ value: '', disabled: true }],
			location_type: ['', [Validators.required]],
			status: ['1'],
			status_name: [{ value: 'Creation', disabled: true }],
			departure_date: ['', [Validators.required]],
			arrival_date: ['', [Validators.required]],
			function_name: [{ value: '', disabled: true }],
			driver: ['', [Validators.required]],
			vehicle: ['', [Validators.required]],
			passenger_list: this.__fb.array([
				// this.getPassengerArray()
			]),
			root_movement: this.__fb.array([
				// this.getRootArray()
			]),
			affection_details: this.__fb.array([
				// this.getAffectionArray()
			]),
			payment_mode: ['', [Validators.required]],
			invoice_type: ['', [Validators.required]],
			flat_rate: [{ value: '', disabled: true }],
			passenger_number: ['', [Validators.required, Validators.pattern("^[0-9]+$")]],
			tourist_country: ['', [Validators.required]],
			// file_number: ['', [Validators.required]],
			// authorization_number: ['', [Validators.required]],
			commarcial: ['', [Validators.required]],
			nb_km: [{ value: '', disabled: true }],
			location_description: ['', [Validators.required]],
		});
		this.path_table = {
			"paging": false,
			"ordering": false,
			"info": false,
			"filter": false
		}
		// this.get_reference();
		this.get_circuit_by_id();
		this.getUsers();
		this.get_staff_functions();
		this.get_root_list();
		this.get_vehicle();
		this.getCommarcial();
		this.get_cities();
		this.get_countries();
		this.get_location_types();
	}

	getPassengerArray(passenger_id, mobile_no, full_name, age, gender) {
		return this.__fb.group({
			passenger_id: [passenger_id],
			Full_Name: [full_name, [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			Mobile_No: [mobile_no, [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			Age: [age, [Validators.required, Validators.pattern("^[0-9]+$")]],
			Gender: [gender, [Validators.required]],
		});
	}

	// New Row Array for Passenger Table
	getPassengerBlankArray() {
		return this.__fb.group({
			// Adding New Row and set the id 0 by default --> (passenger_id: ["0"],)
			passenger_id: ["0"],
			Full_Name: ["", [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			Mobile_No: ["", [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			Age: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			Gender: ["", [Validators.required]],
		});
	}

	getRootArray(route_id, city_id, city_name, schedule_date, actual_date) {
		return this.__fb.group({
			route_id: [route_id],
			destination: [city_id],
			destination_name: [{ value: city_name, disabled: true }],
			schedule_date: [this.DateFormate(schedule_date), [Validators.required]],
			actual_date: [this.DateFormate(actual_date), [Validators.required]]
		});
	}

	// New Row Array for Root Table
	getRootBlankArray() {
		return this.__fb.group({
			route_id: ["0"],
			destination: [''],
			destination_name: [''],
			schedule_date: ['', [Validators.required]],
			actual_date: ['', [Validators.required]]
		});
	}

	getAffectionArray(id, vehicle_id, vehicle, from_date, to_date, km_departure, km_arrival, driver_id, driver, comment) {
		return this.__fb.group({
			id: [id],
			vehicle_id: [vehicle_id],
			vehicle: [vehicle_id, Validators.required],
			from_date: [this.DateFormate(from_date), [Validators.required]],
			to_date: [this.DateFormate(to_date), [Validators.required]],
			km_start: [km_departure, [Validators.required, Validators.pattern("^[0-9]+$")]],
			km_arrival: [km_arrival, [Validators.required, Validators.pattern("^[0-9]+$")]],
			driver_id: [driver_id],
			driver: [driver_id, [Validators.required]],
			comment: [comment, [Validators.required]],
		});
	}
	// New Row Array for Affection Table
	getAffectionBlankArray() {
		return this.__fb.group({
			id: ["0"],
			destination: [''],
			vehicle: ["", Validators.required],
			from_date: ["", [Validators.required]],
			to_date: ["", [Validators.required]],
			km_start: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			km_arrival: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			driver: ["", [Validators.required]],
			comment: ["", [Validators.required]],
		});
	}

	// reference id
	// get_reference() {
	// 	this.circuitApi.get_reference().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.edit_circuit_frm.get('reference').setValue(response.reference);
	// 	})
	// 		.catch((error: any) => {
	// 			console.error(error);
	// 			Swal.fire({
	// 	type: 'error',
	// 	title: 'Error!',
	// 	text: error,
	// 	confirmButtonColor: '#323258',
	// 	confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// });
	// 		});
	// }

	// Countries
	get_countries() {
		this.masterApi.countries().then((response: any) => {
			console.log('Response:', response);
			this.countries = response.countries
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	getUsers() {
		this.circuitApi.users().then((response: any) => {
			console.log('Response:', response);
			this.users = response.list_of_users
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Commarcial field (get staff member API)
	getCommarcial() {
		let formData = new FormData()
		this.staffMemberApiService.staff_members(formData).then((response: any) => {
			console.log('Response:', response);
			this.staff_members = response.staff_members
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// reference id
	get_root_list() {
		this.circuitApi.root_list().then((response: any) => {
			console.log('Response:', response);
			this.root_list = response.routes
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Departure Date change function for Date Block
	departureDateChange() {
		this.minimumDate = this.edit_circuit_frm.get('departure_date').value
		// console.log(this.minimumDate)
	}

	// Arrival Date change function for Date Block
	arrivalDateChange() {
		// Check Arrival Date must be greater Departure Date!
		if (new Date(this.edit_circuit_frm.get('departure_date').value) >= new Date(this.edit_circuit_frm.get('arrival_date').value)) {
			var errorMsg = ''
			this.translate.get('edit_circuit.Arrival Date must be greater Departure Date!').subscribe(val => {
				errorMsg = val
			})
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: errorMsg,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		}

		this.maximumDate = this.edit_circuit_frm.get('arrival_date').value
		// console.log(this.maximumDate)
	}

	// Get Staff Function Dropdown
	get_staff_functions() {
		this.masterApi.list_staff_function().then((response: any) => {
			console.log('Response:', response);
			// this.staff_functions = response.functions
			this.staff_functions = response.driver
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	staffChange(driver_id) {
		this.circuitApi.get_staff(this.edit_circuit_frm.get('function_name').value).then((response: any) => {
			console.log('Response:', response);
			this.driver = response.$drivers
			this.edit_circuit_frm.get('driver').setValue(driver_id);

		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Get Vehicle Dropdown
	get_vehicle() {
		let formData = new FormData()
		this.vehicleApi.list_vehicles(formData).then((response: any) => {
			console.log('Response:', response);
			this.vehilces = response.vehicles
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Cities Dropdown
	get_cities() {
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Cities Dropdown
	get_location_types() {
		this.circuitApi.location_type().then((response: any) => {
			console.log('Response:', response);
			this.location_types = response.location_type
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get circuit Details by ID
	get_circuit_by_id() {
		this.circuitApi.get_circuit_by_id(this.circuit_id).then((response: any) => {
			console.log('Response:', response);
			this.circuit = response.circuit;
			this.edit_circuit_frm.get('reference').setValue(response.circuit.circuit.refrence);
			this.edit_circuit_frm.get('client').setValue(response.circuit.circuit.client_id);
			this.edit_circuit_frm.get('path').setValue(response.circuit.circuit.route_id);

			this.edit_circuit_frm.get('location_type').setValue(response.circuit.circuit.type_location);
			this.edit_circuit_frm.get('departure_date').setValue(this.DateFormate(response.circuit.circuit.departure_date));
			this.edit_circuit_frm.get('arrival_date').setValue(this.DateFormate(response.circuit.circuit.arrival_date));

			// this.edit_circuit_frm.get('driver').setValue(response.circuit.circuit.driver_id);
			this.edit_circuit_frm.get('vehicle').setValue(response.circuit.circuit.vehicle_id);
			this.edit_circuit_frm.get('passenger_number').setValue(response.circuit.circuit.no_of_passenger);
			this.edit_circuit_frm.get('tourist_country').setValue(response.circuit.circuit.tourist_country);
			// this.edit_circuit_frm.get('file_number').setValue(response.circuit.circuit.file_number);
			// this.edit_circuit_frm.get('authorization_number').setValue(response.circuit.circuit.autorisation_number);
			this.edit_circuit_frm.get('commarcial').setValue(response.circuit.circuit.commercial);

			this.edit_circuit_frm.get('function_name').setValue(response.circuit.circuit.function_id);
			this.staffChange(response.circuit.circuit.driver_id)
			this.edit_circuit_frm.get('payment_mode').setValue(response.circuit.circuit.payment_mode);
			this.edit_circuit_frm.get('invoice_type').setValue(response.circuit.circuit.invoice_type);
			this.edit_circuit_frm.get('flat_rate').setValue(response.circuit.circuit.flat_rate);
			this.edit_circuit_frm.get('nb_km').setValue(response.circuit.circuit.no_of_km);
			this.edit_circuit_frm.get('location_description').setValue(response.circuit.circuit.location_description);

			// Array Data (Passenger)
			const control3 = <FormArray>this.edit_circuit_frm.controls['passenger_list'];
			for (let item of this.circuit.passengers_details) {
				// item.route_id ---> (route_id is parameter Name (API))
				// control3.push(this.getPassengerArray(item.route_id, item.city_id, item.city, item.schedule_date, item.actual_date))
				control3.push(this.getPassengerArray(item.passenger_id, item.mobile_no, item.full_name, item.age, item.gender))
			}

			// Array Data (Path)
			const control1 = <FormArray>this.edit_circuit_frm.controls['root_movement'];
			for (let item of this.circuit.route_details) {
				// item.route_id ---> (route_id is parameter Name (API))
				control1.push(this.getRootArray(item.route_id, item.city_id, item.city, item.schedule_date, item.actual_date))
			}

			// Array Data (Affection)
			const control2 = <FormArray>this.edit_circuit_frm.controls['affection_details'];
			for (let item of this.circuit.route_affections) {
				// item.deadline_value ---> (deadline_value is parameter Name (API))
				control2.push(this.getAffectionArray(item.id, item.vehicle_id, item.vehicle, item.from_date, item.to_date, item.km_departure, item.km_arrival, item.driver_id, item.driver, item.comment))
			}
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	DateFormate(Date) {
		return this.datepipe.transform(Date, 'yyyy-MM-dd');
	}

	// Patch Value for Affection Table
	patchValuesAffection(id, vehicle_id, vehicle, from_date, to_date, km_departure, km_arrival, comment) {
		return this.__fb.group({
			id: [id],
			vehicle_id: [vehicle_id],
			vehicle: [vehicle],
			from_date: [from_date],
			to_date: [to_date],
			km_departure: [km_departure],
			km_arrival: [km_arrival],
			comment: [comment],
		})
	}

	// Check Vehicle Avilibility for vehicle
	check_vehicle_avilibility() {
		let form_data = new FormData();
		form_data.append('vehicle_id', this.edit_circuit_frm.get('vehicle').value);
		form_data.append('departure_date', this.edit_circuit_frm.get('departure_date').value);
		form_data.append('arrival_date', this.edit_circuit_frm.get('arrival_date').value);
		// let vehicle_id = this.edit_circuit_frm.get('vehicle').value;
		// let departure_date = this.edit_circuit_frm.get('departure_date').value;
		// let arrival_date = this.edit_circuit_frm.get('arrival_date').value;
		// console.log(form_data)
		this.circuitApi.vehicle_avilibility(form_data).then((response: any) => {
			console.log('Response:', response);
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	enableFlatRate() {
		if (this.edit_circuit_frm.get('invoice_type').value == 2) {
			this.edit_circuit_frm.get('flat_rate').disable();
		} else {
			this.edit_circuit_frm.get('flat_rate').enable();
		}
	}

	// Add Passenger Button Functionality
	addPassengerRow() {
		const control = <FormArray>this.edit_circuit_frm.controls['passenger_list'];
		control.push(this.getPassengerBlankArray());
		return true;
	}
	// Delete Passenger Button Functionality
	deletePassengerRow(index, passenger_id) {
		const control = <FormArray>this.edit_circuit_frm.controls['passenger_list'];
		// if (control.length == 1) {
		// } 
		if (control.length == 1 || this.edit_circuit_frm.get('passenger_number').value == this.edit_circuit_frm.get('passenger_list').value.length) {
			console.log(this.edit_circuit_frm.get('passenger_number').value)
			console.log(this.edit_circuit_frm.get('passenger_list').value.length)
			var msg = ''
			this.translate.get('add_circuit.No of Passenger and Passenger List Row are not Equal').subscribe(val => {
				msg = val
			})
			return Swal.fire({
				type: 'warning',
				title: 'Warning!',
				text: msg,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		}
		
		// else {
		// 	control.removeAt(index);
		// }
		else {
			Swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#323258',
				cancelButtonColor: '#ff1f1f',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
			}).then(result => {
				if (result.value) {
					let form_data = new FormData();
					form_data.append('circuit_id', this.circuit_id);
					form_data.append('id', passenger_id);
					console.log(form_data.get('circuit_id'), form_data.get('id'))
					console.log(this.circuit_id);
					console.log(passenger_id);
					this.circuitApi.delete_passenger_row(form_data).then((response: any) => {
						console.log('Response:', response);
						control.removeAt(index);
						Swal.fire({
							type: 'success',
							title: 'Deleted!',
							text: response.message,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						}); 

					})
						.catch((error: any) => {
							console.error(error);
							Swal.fire({
								type: 'error',
								title: 'Error!',
								text: error,
								confirmButtonColor: '#323258',
								confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
							});
						});
				}
			})
		}
	}

	addRootRow() {
		const control = <FormArray>this.edit_circuit_frm.controls['root_movement'];
		control.push(this.getRootBlankArray());
		return true;
	}

	deleteRootRow(index, route_id) {
		const control = <FormArray>this.edit_circuit_frm.controls['root_movement'];
		if (control.length == 1) {

		}
		// else {
		// 	control.removeAt(index);
		// }

		else {
			Swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#323258',
				cancelButtonColor: '#ff1f1f',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
			}).then(result => {
				if (result.value) {
					let form_data = new FormData();
					form_data.append('circuit_id', this.circuit_id);
					form_data.append('id', route_id);
					console.log(form_data.get('circuit_id'), form_data.get('id'))
					this.circuitApi.delete_root_row(form_data).then((response: any) => {
						console.log('Response:', response);
						control.removeAt(index);
						Swal.fire({
							type: 'success',
							title: 'Deleted!',
							text: response.message,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});

					})
						.catch((error: any) => {
							console.error(error);
							Swal.fire({
								type: 'error',
								title: 'Error!',
								text: error,
								confirmButtonColor: '#323258',
								confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
							});
						});
				}
			})
		}

	}

	addAffectionRow() {
		const control = <FormArray>this.edit_circuit_frm.controls['affection_details'];
		control.push(this.getAffectionBlankArray());
		return true;
	}

	deleteAffectionRow(index, id) {
		const control = <FormArray>this.edit_circuit_frm.controls['affection_details'];
		if (control.length == 1) {

		} else {
			Swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#323258',
				cancelButtonColor: '#ff1f1f',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
			}).then(result => {
				if (result.value) {
					let form_data = new FormData();
					form_data.append('circuit_id', this.circuit_id);
					form_data.append('id', id);
					console.log(form_data.get('circuit_id'), form_data.get('id'))
					this.circuitApi.delete_affection_row(form_data).then((response: any) => {
						console.log('Response:', response);
						control.removeAt(index);
						Swal.fire({
							type: 'success',
							title: 'Deleted!',
							text: response.message,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});

					})
						.catch((error: any) => {
							console.error(error);
							Swal.fire({
								type: 'error',
								title: 'Error!',
								text: error,
								confirmButtonColor: '#323258',
								confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
							});
						});
				}
			})
		}

	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_circuit_frm.get(formControlName).errors &&
			(this.edit_circuit_frm.get(formControlName).touched ||
				this.edit_circuit_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_circuit_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_circuit_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_circuit_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_circuit_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save() {
		this.markFormGroupTouched(this.edit_circuit_frm);
		if (this.edit_circuit_frm.valid) {
			if (this.edit_circuit_frm.get('passenger_number').value != this.edit_circuit_frm.get('passenger_list').value.length) {
				console.log(this.edit_circuit_frm.get('passenger_number').value)
				console.log(this.edit_circuit_frm.get('passenger_list').value.length)
				var msg = ''
				this.translate.get('add_circuit.No of Passenger and Passenger List Row are not Equal').subscribe(val => {
					msg = val
				})
				return Swal.fire({
					type: 'warning',
					title: 'Warning!',
					text: msg,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			}
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('circuit_id', this.circuit_id);
			// form_data.append('refrence', this.edit_circuit_frm.get('reference').value);
			form_data.append('client_id', this.edit_circuit_frm.get('client').value);
			// form_data.append('route_id', this.edit_circuit_frm.get('path').value);
			form_data.append('arrival_date', this.edit_circuit_frm.get('arrival_date').value);
			form_data.append('departure_date', this.edit_circuit_frm.get('departure_date').value);
			form_data.append('type_location', this.edit_circuit_frm.get('location_type').value);
			form_data.append('function_id', this.edit_circuit_frm.get('function_name').value);
			form_data.append('driver_id', this.edit_circuit_frm.get('driver').value);
			form_data.append('vehicle_id', this.edit_circuit_frm.get('vehicle').value);
			form_data.append('payment_mode', this.edit_circuit_frm.get('payment_mode').value);
			form_data.append('invoice_type', this.edit_circuit_frm.get('invoice_type').value);
			form_data.append('flat_rate', this.edit_circuit_frm.get('flat_rate').value);

			form_data.append('passenger_number', this.edit_circuit_frm.get('passenger_number').value);
			form_data.append('tourist_country', this.edit_circuit_frm.get('tourist_country').value);
			// form_data.append('file_number', this.edit_circuit_frm.get('file_number').value);
			// form_data.append('authorization_number', this.edit_circuit_frm.get('authorization_number').value);
			form_data.append('commercial', this.edit_circuit_frm.get('commarcial').value);

			form_data.append('no_of_km', this.edit_circuit_frm.get('nb_km').value);
			form_data.append('location_description', this.edit_circuit_frm.get('location_description').value)

			// Edit API
			this.circuitApi.edit_circuit_details(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				this.router.navigate(['/manage-circuit/list-circuit'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

	// Edit Passenger Table Function
	public editPassenger() {
		if (this.edit_circuit_frm.get('passenger_number').value != this.edit_circuit_frm.get('passenger_list').value.length) {
			console.log(this.edit_circuit_frm.get('passenger_number').value)
			console.log(this.edit_circuit_frm.get('passenger_list').value.length)
			var msg = ''
			this.translate.get('add_circuit.No of Passenger and Passenger List Row are not Equal').subscribe(val => {
				msg = val
			})
			return Swal.fire({
				type: 'warning',
				title: 'Warning!',
				text: msg,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		}
		let form_data = new FormData();
		form_data.append('circuit_id', this.circuit_id);
		form_data.append('route_master', this.edit_circuit_frm.get('path').value);
		console.log(this.edit_circuit_frm.get('path').value)
		form_data.append('passengers', JSON.stringify(this.edit_circuit_frm.get('passenger_list').value));
		console.log(this.edit_circuit_frm.get('passenger_list').value);
		// Edit Path Table API
		this.circuitApi.edit_passenger_list(form_data).then((response: any) => {
			console.log('Response:', response);
			Swal.fire({
				type: 'success',
				title: 'Success',
				confirmButtonColor: '#323258',
				// cancelButtonColor: '#ff1f1f',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				text: response.message
			});
			this.router.navigate(['/manage-circuit/list-circuit'])
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Edit Path Table Function
	public editPath() {
		let form_data = new FormData();
		// form_data.append('route_id', route_id);
		// form_data.append('city_id', destination);
		// form_data.append('schedule_date', schedule_date);
		// form_data.append('actual_date', actual_date);
		form_data.append('circuit_id', this.circuit_id);
		form_data.append('route_master', this.edit_circuit_frm.get('path').value);
		console.log(this.edit_circuit_frm.get('path').value)
		form_data.append('destination', JSON.stringify(this.edit_circuit_frm.get('root_movement').value));
		// Edit Passenger Table API
		this.circuitApi.edit_root_details(form_data).then((response: any) => {
			console.log('Response:', response);
			Swal.fire({
				type: 'success',
				title: 'Success',
				confirmButtonColor: '#323258',
				// cancelButtonColor: '#ff1f1f',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				text: response.message
			});
			this.router.navigate(['/manage-circuit/list-circuit'])
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Edit Afection Table Function
	public editAfection() {
		let form_data = new FormData();
		// form_data.append('route_affection_id', id);
		// form_data.append('vehicle_id', vehicle_id);
		// form_data.append('driver_id', driver_id);
		// form_data.append('from_date', from_date);
		// form_data.append('to_date', to_date);
		// form_data.append('km_start', km_departure);
		// form_data.append('km_arrival', km_arrival);
		// form_data.append('comment', comment);
		form_data.append('circuit_id', this.circuit_id);
		form_data.append('route_master', this.edit_circuit_frm.get('path').value);
		console.log(this.edit_circuit_frm.get('path').value)
		form_data.append('affections', JSON.stringify(this.edit_circuit_frm.get('affection_details').value));

		// Edit Affection Table API
		this.circuitApi.edit_circuit_affectiona_details(form_data).then((response: any) => {
			console.log('Response:', response);
			Swal.fire({
				type: 'success',
				title: 'Success',
				confirmButtonColor: '#323258',
				// cancelButtonColor: '#ff1f1f',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				text: response.message
			});
			this.router.navigate(['/manage-circuit/list-circuit'])
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

}
