import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { CircuitApiService } from '../../shared/circuit-api.service';
import { VehicleApiService } from '../../../manage-vehicle/shared/vehicle-api.service';
@Component({
	selector: 'app-list-circuit',
	templateUrl: './list-circuit.component.html',
	styleUrls: ['./list-circuit.component.scss']
})
export class ListCircuitComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();

	circuit_List: any = [];
	users = [];
	vehilces = [];
	status_change = [];
	driver_filter = [];

	statusArr: any[] = [
		{ id: 1, name: 'Creation' },
		{ id: 2, name: 'Canceled' },
		{ id: 3, name: 'Rejected' },
		{ id: 4, name: 'Postponed' },
		{ id: 5, name: 'Accepted' },
		{ id: 6, name: 'End Circuit' }
	];

	// for filter
	from_date = '';
	to_date = '';
	driver = '';
	vehicle = '';
	status = '';
	client_id = '';
	// For clear value of filter
	clear() {
		this.from_date = '';
		this.to_date = '';
		this.driver = '';
		this.vehicle = '';
		this.status = '';
		this.client_id = '';
	}

	// circuit_date_of: string;
	// at: string;
	// driver: string = null;
	// vehicle: string = null;
	// status: string = null;
	constructor(
		private masterApi: MasterApiService,
		private vehicleApi: VehicleApiService,
		private circuitApi: CircuitApiService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			// 		ajax: 'fake-data/datatable-data.json',
			// 		columns: [
			// 			{
			// 				title: 'Id',
			// 				data: 'id'
			// 			},
			// 			{
			// 				title: 'Name',
			// 				data: 'name'
			// 			},
			// 			{
			// 				title: 'Office',
			// 				data: 'office'
			// 			},
			// 			{
			// 				title: 'Age',
			// 				data: 'age'
			// 			}, 
			// 			{
			// 				title: 'Start Date',
			// 				data: 'date'
			// 			}, 
			// 			{
			// 				title: 'Salary',
			// 				data: 'salary'
			// 			}
			// 		],
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.list_circuit();
		this.getUsers();
		this.get_vehicle();
		this.drivers_for_filter();
		// this.get_status();
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// Get User Dropdown
	getUsers() {
		this.circuitApi.users().then((response: any) => {
			console.log('Response:', response);
			this.users = response.list_of_users
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Vehicle Dropdown
	get_vehicle() {
		let formData = new FormData()
		this.vehicleApi.list_vehicles(formData).then((response: any) => {
			console.log('Response:', response);
			this.vehilces = response.vehicles
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Driver list Dropdown
	drivers_for_filter() {
		this.circuitApi.drivers_for_filter().then((response: any) => {
			console.log('Response:', response);
			this.driver_filter = response.drivers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	list_circuit() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.circuitApi.list_circuits(formData).then((response: any) => {
			console.log('Response:', response);
			this.circuit_List = response.circuit
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Status Change
	public statusChange(number, circuit_id) {
		let formData = new FormData()
		formData.append('status', number);
		formData.append('circuit_id', circuit_id);
		this.circuitApi.circuit_status(formData).then((response: any) => {
			console.log('Response:', response);
			this.list_circuit();
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		// console.log(number);
	}

	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('from_date', this.from_date);
		formData.append('to_date', this.to_date);
		formData.append('driver', this.driver);
		formData.append('vehicle', this.vehicle);
		formData.append('status', this.status);
		formData.append('client_id', this.client_id);
		formData.append('add_filter', '1');
		// console.log(this.vehicle_category);
		// console.log(this.vehicle_brand);
		// console.log(this.capacity);
		this.circuitApi.list_circuits(formData).then((response: any) => {
			console.log('Response:', response);
			this.circuit_List = response.circuit
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.list_circuit()
	}

	// Delete
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.circuitApi.delete_circuit(id).then((response: any) => {
					console.log('Response:', response);
					this.list_circuit();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
							type: 'error',
							title: 'Error!',
							text: error,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
					});
			}
		})
	}

}
