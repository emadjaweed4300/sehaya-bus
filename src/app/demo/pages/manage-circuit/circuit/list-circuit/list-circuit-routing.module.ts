import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCircuitComponent } from './list-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: ListCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListCircuitRoutingModule { }
