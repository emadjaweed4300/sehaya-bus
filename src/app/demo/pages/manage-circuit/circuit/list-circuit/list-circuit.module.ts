import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListCircuitRoutingModule } from './list-circuit-routing.module';
import { ListCircuitComponent } from './list-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDatepickerModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    ListCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    NgbDatepickerModule,
    NgbDropdownModule
  ],
  declarations: [ListCircuitComponent]
})
export class ListCircuitModule { }
