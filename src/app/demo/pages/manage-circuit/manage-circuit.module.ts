import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageCircuitRoutingModule } from './manage-circuit-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ManageCircuitRoutingModule,
  ],
  declarations: []
})
export class ManageCircuitModule { }
