import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Circuit list,add,view,edit components
			{
				path: 'list-circuit',
				loadChildren: () => import('./circuit/list-circuit/list-circuit.module').then(module => module.ListCircuitModule)
			},
			{
				path: 'add-circuit',
				loadChildren: () => import('./circuit/add-circuit/add-circuit.module').then(module => module.AddCircuitModule)
			},
			{
				path: 'view-circuit/:id',
				loadChildren: () => import('./circuit/view-circuit/view-circuit.module').then(module => module.ViewCircuitModule)
			},
			{
				path: 'edit-circuit/:id',
				loadChildren: () => import('./circuit/edit-circuit/edit-circuit.module').then(module => module.EditCircuitModule)
			}
		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManageCircuitRoutingModule { }
