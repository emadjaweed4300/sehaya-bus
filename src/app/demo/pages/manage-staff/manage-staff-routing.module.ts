import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Staff Memebers list,add,view,edit components
			{
				path: 'list-staff-members',
				loadChildren: () => import('./staff-members/list-staff-members/list-staff-members.module').then(module => module.ListStaffMembersModule)
			},
			{
				path: 'add-staff-members',
				loadChildren: () => import('./staff-members/add-staff-members/add-staff-members.module').then(module => module.AddStaffMembersModule)
			},
			{
				path: 'view-staff-members/:id',
				loadChildren: () => import('./staff-members/view-staff-members/view-staff-members.module').then(module => module.ViewStaffMembersModule)
			},
			{
				path: 'edit-staff-members/:id',
				loadChildren: () => import('./staff-members/edit-staff-members/edit-staff-members.module').then(module => module.EditStaffMembersModule)
			},

		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManageStaffRoutingModule { }
