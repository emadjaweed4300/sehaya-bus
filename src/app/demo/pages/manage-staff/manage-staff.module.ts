import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageStaffRoutingModule } from './manage-staff-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ManageStaffRoutingModule,
  ],
  declarations: []
})
export class ManageStaffModule { }
