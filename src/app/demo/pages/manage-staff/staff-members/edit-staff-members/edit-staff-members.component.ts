import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { StaffMemberApiService } from '../../shared/staff-member-api.service';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

@Component({
	selector: 'app-edit-staff-members',
	templateUrl: './edit-staff-members.component.html',
	styleUrls: ['./edit-staff-members.component.scss']
})
export class EditStaffMembersComponent implements OnInit {

	edit_staff_members_frm: FormGroup;
	staff_member_id
	user_details: any;
	cities = [];
	staff_functions = [];
	permit_types = [];

	contractArr: any[] = [
		{ id: 0, name: 'Occasional' },
		{ id: 1, name: 'Permanent' }
		
	];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService,
		private staffMemberApiService: StaffMemberApiService,
		public datepipe: DatePipe,
	) { 
		this.staff_member_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_staff_members_frm = this.__fb.group({
			name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			phone:  ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			email: [{ value: '', disabled: true }],
			// [{ value: '', disabled: true }]
			// password: ['', Validators.required],
			function_name: ['', Validators.required],
			marital_status: ['', Validators.required],
			permit_type: ['', Validators.required],
			permit_number: ['', Validators.required],
			permit_validate_date: ['', Validators.required],
			cin: ['', Validators.required],
			cnss: ['', Validators.required],
			registration_number: ['', Validators.required],
			date_of_birth: ['', Validators.required],
			city_name: ['', Validators.required],
			joining_date: ['', Validators.required],
			releasing_date: ['', Validators.required],
			contract: ['', Validators.required],
			description: ['', Validators.required],
			
		});
		this.get_cities();
		this.get_staff_functions();
		this.get_permit_typs();
		this.get_staff_member();
	}

	// Get Cities Dropdown
	get_cities(){
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Staff Function Dropdown
	get_staff_functions(){
		this.masterApi.list_staff_function().then((response: any) => {
			console.log('Response:', response);
			this.staff_functions = response.functions
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// Get Permit Typs Dropdown
	get_permit_typs(){
		this.staffMemberApiService.permit_types().then((response: any) => {
			console.log('Response:', response);
			this.permit_types = response.permit_type
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	get_staff_member(){
		this.staffMemberApiService.get_staff_members(this.staff_member_id).then((response: any) => {
			console.log('Response:', response);
			this.user_details = response.user_details;
			// name (formControlName) & name (postman)
			this.edit_staff_members_frm.get('name').setValue(response.user_details.name);
			this.edit_staff_members_frm.get('phone').setValue(response.user_details.phone);
			this.edit_staff_members_frm.get('email').setValue(response.user_details.email);
			// this.edit_staff_members_frm.get('password').setValue(response.user_details.password);
			this.edit_staff_members_frm.get('date_of_birth').setValue(this.DateFormate(response.user_details.date_of_birth));
			this.edit_staff_members_frm.get('function_name').setValue(response.user_details.function_id);
			this.edit_staff_members_frm.get('marital_status').setValue(response.user_details.marital_status);
			this.edit_staff_members_frm.get('permit_type').setValue(response.user_details.permit_type);
			this.edit_staff_members_frm.get('permit_number').setValue(response.user_details.permit_number);
			this.edit_staff_members_frm.get('permit_validate_date').setValue(this.DateFormate(response.user_details.permit_valid_date));
			this.edit_staff_members_frm.get('cin').setValue(response.user_details.CIN);
			this.edit_staff_members_frm.get('cnss').setValue(response.user_details.CNSS);
			this.edit_staff_members_frm.get('registration_number').setValue(response.user_details.registration);
			this.edit_staff_members_frm.get('city_name').setValue(response.user_details.city_id);
			this.edit_staff_members_frm.get('joining_date').setValue(this.DateFormate(response.user_details.joining_date));
			this.edit_staff_members_frm.get('releasing_date').setValue(this.DateFormate(response.user_details.releasing_date));
			this.edit_staff_members_frm.get('contract').setValue(response.user_details.contract);
			this.edit_staff_members_frm.get('description').setValue(response.user_details.description);

		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	// Date Format Function (Also add this function on get Function)
	DateFormate(Date) {
		return this.datepipe.transform(Date, 'yyyy-MM-dd');
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_staff_members_frm.get(formControlName).errors &&
			(this.edit_staff_members_frm.get(formControlName).touched ||
				this.edit_staff_members_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_staff_members_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_staff_members_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_staff_members_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_staff_members_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_staff_members_frm);
		if (this.edit_staff_members_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// id (ppostman) & staff_member_id (formControlName)
			form_data.append('staff_id', this.staff_member_id);
			form_data.append('name', this.edit_staff_members_frm.get('name').value);
			form_data.append('phone', this.edit_staff_members_frm.get('phone').value);
			form_data.append('email', this.edit_staff_members_frm.get('email').value);
			// form_data.append('password', this.edit_staff_members_frm.get('password').value);
			form_data.append('function_name', this.edit_staff_members_frm.get('function_name').value);
			form_data.append('marital_status', this.edit_staff_members_frm.get('marital_status').value);
			form_data.append('permit_type', this.edit_staff_members_frm.get('permit_type').value);
			form_data.append('permit_number', this.edit_staff_members_frm.get('permit_number').value);
			form_data.append('permit_validate_date', this.edit_staff_members_frm.get('permit_validate_date').value);
			form_data.append('CIN', this.edit_staff_members_frm.get('cin').value);
			form_data.append('CNSS', this.edit_staff_members_frm.get('cnss').value);
			form_data.append('registration', this.edit_staff_members_frm.get('registration_number').value);
			form_data.append('date_of_birth', this.edit_staff_members_frm.get('date_of_birth').value);
			form_data.append('city_id', this.edit_staff_members_frm.get('city_name').value);
			form_data.append('joining_date', this.edit_staff_members_frm.get('joining_date').value);
			form_data.append('releasing_date', this.edit_staff_members_frm.get('releasing_date').value);
			form_data.append('contract', this.edit_staff_members_frm.get('contract').value);
			console.log(this.edit_staff_members_frm.get('contract').value)
			form_data.append('description', this.edit_staff_members_frm.get('description').value);
			this.staffMemberApiService.edit_staff_members(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/manage-staff/list-staff-members'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
