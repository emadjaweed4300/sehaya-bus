import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStaffMembersComponent } from './edit-staff-members.component';

describe('EditStaffMembersComponent', () => {
  let component: EditStaffMembersComponent;
  let fixture: ComponentFixture<EditStaffMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditStaffMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStaffMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
