import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditStaffMembersRoutingModule } from './edit-staff-members-routing.module';
import { EditStaffMembersComponent } from './edit-staff-members.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditStaffMembersRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditStaffMembersComponent]
})
export class EditStaffMembersModule { }
