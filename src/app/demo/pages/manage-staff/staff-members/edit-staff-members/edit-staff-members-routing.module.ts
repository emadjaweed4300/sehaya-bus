import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditStaffMembersComponent } from './edit-staff-members.component';

const routes: Routes = [
  {
    path: '',
    component: EditStaffMembersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditStaffMembersRoutingModule { }
