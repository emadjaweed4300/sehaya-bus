import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListStaffMembersComponent } from './list-staff-members.component';

describe('ListStaffMembersComponent', () => {
  let component: ListStaffMembersComponent;
  let fixture: ComponentFixture<ListStaffMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListStaffMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListStaffMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
