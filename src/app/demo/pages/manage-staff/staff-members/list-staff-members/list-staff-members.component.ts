import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { StaffMemberApiService } from '../../shared/staff-member-api.service';

@Component({
	selector: 'app-list-staff-members',
	templateUrl: './list-staff-members.component.html',
	styleUrls: ['./list-staff-members.component.scss']
})
export class ListStaffMembersComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	staff_members_list: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();

	staff_functions = [];
	// for filter
	function_id = ''
	// For clear value of filter
	clear(){
		this.function_id = '';
	}

	constructor(
		private masterApi: MasterApiService,
		private staffMemberApiService: StaffMemberApiService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				"<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			buttons: [
				{
					extend: 'copy',
					text: '<i class="fa fa-copy" style="color: white;"></i>',
					titleAttr: 'Copy',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				},
				{
					extend: 'print',
					text: '<i class="fa fa-print" style="color: white;"></i>',
					titleAttr: 'Print',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				},
				{
					extend: 'excel',
					text: '<i class="fa fa-file-excel" style="color: white;"></i>',
					titleAttr: 'Excel',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				},
				{
					extend: 'csv',
					text: '<i class="fa fa-file-csv" style="color: white;"></i>',
					titleAttr: 'CSV',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				}
			],
			responsive: true
		};
		this.list_staff_members();
		this.get_staff_functions();
	}

	// Get Staff Function Dropdown
	get_staff_functions() {
		
		this.masterApi.list_staff_function().then((response: any) => {
			console.log('Response:', response);
			this.staff_functions = response.functions
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	list_staff_members() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.staffMemberApiService.staff_members(formData).then((response: any) => {
			console.log('Response:', response);
			this.staff_members_list = response.staff_members
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Status Change API
	statusChange(i, staff_id: string, event) {
		let formData = new FormData()
		// staff_id (Postman)
		formData.append('staff_id', staff_id)
		if (event.target.checked == true) {
			this.staffMemberApiService.staff_members_active(formData).then((response: any) => {
				console.log('Response:', response);
				this.staff_members_list[i].status = '1'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		} else {
			this.staffMemberApiService.staff_members_inactive(formData).then((response: any) => {
				console.log('Response:', response);
				this.staff_members_list[i].status = '0'
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}
	}

	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('function_id', this.function_id);
		formData.append('add_filter', '1');
		console.log(this.function_id);
		this.staffMemberApiService.staff_members(formData).then((response: any) => {
			console.log('Response:', response);
			this.staff_members_list = response.staff_members
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.list_staff_members()
	}

}
