import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListStaffMembersRoutingModule } from './list-staff-members-routing.module';
import { ListStaffMembersComponent } from './list-staff-members.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListStaffMembersRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListStaffMembersComponent]
})
export class ListStaffMembersModule { }
