import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListStaffMembersComponent } from './list-staff-members.component';

const routes: Routes = [
  {
    path: '',
    component: ListStaffMembersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListStaffMembersRoutingModule { }
