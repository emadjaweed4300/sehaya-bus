import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddStaffMembersRoutingModule } from './add-staff-members-routing.module';
import { AddStaffMembersComponent } from './add-staff-members.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddStaffMembersRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddStaffMembersComponent]
})
export class AddStaffMembersModule { }
