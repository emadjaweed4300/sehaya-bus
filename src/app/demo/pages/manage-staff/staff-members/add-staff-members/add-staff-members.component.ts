import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { StaffMemberApiService } from '../../shared/staff-member-api.service';
import { MasterApiService } from '../../../master/shared/master_api.service';

@Component({
	selector: 'app-add-staff-members',
	templateUrl: './add-staff-members.component.html',
	styleUrls: ['./add-staff-members.component.scss']
})
export class AddStaffMembersComponent implements OnInit {

	add_staff_members_frm: FormGroup;
	cities = [];
	staff_functions = [];
	permit_types = [];

	accesibilityValue: any[] = [
		{ id: '1', viewValue: 'No'  },
		{ id: '2', viewValue: 'Yes' },
	];
	accesibility = '';

	contractArr: any[] = [
		{ id: 0, name: 'Occasional' },
		{ id: 1, name: 'Permanent' }
	];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService,  
		private staffMemberApiService: StaffMemberApiService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_staff_members_frm = this.__fb.group({
			name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			phone:  ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{15}$')]],
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			// password: [{value: null, disabled: true},], 
			password:[{value: '', disabled: true}, [Validators.required]],
			function_name: ['', Validators.required],
			marital_status: ['', Validators.required],
			permit_type: ['', Validators.required],
			permit_number: ['', Validators.required],
			permit_validate_date: ['', Validators.required],
			cin: ['', Validators.required],
			cnss: ['', Validators.required],
			registration_number: ['', Validators.required],
			date_of_birth: ['', Validators.required],
			city_name: ['', Validators.required],
			joining_date: ['', Validators.required],
			releasing_date: ['', Validators.required],
			contract: ['', Validators.required],
			description: ['', Validators.required],
			accesibility: ["", Validators.required],
			
		});
		this.get_cities();
		this.get_staff_functions();
		this.get_permit_typs();
	}

	// Get Cities Dropdown
	get_cities(){
		this.masterApi.list_cities().then((response: any) => {
			console.log('Response:', response);
			this.cities = response.cities
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// Get Staff Function Dropdown
	get_staff_functions(){
		this.masterApi.list_staff_function().then((response: any) => {
			console.log('Response:', response);
			this.staff_functions = response.functions
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	// Get Permit Typs Dropdown
	get_permit_typs(){
		this.staffMemberApiService.permit_types().then((response: any) => {
			console.log('Response:', response);
			this.permit_types = response.permit_type
		})
		.catch((error: any) => {
			console.error(error);
		});	
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_staff_members_frm.get(formControlName).errors &&
			(this.add_staff_members_frm.get(formControlName).touched ||
				this.add_staff_members_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_staff_members_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_staff_members_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_staff_members_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_staff_members_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_staff_members_frm);
		if (this.add_staff_members_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// name(postman) & name (formControlName)
			form_data.append('name', this.add_staff_members_frm.get('name').value);
			form_data.append('phone', this.add_staff_members_frm.get('phone').value);
			form_data.append('email', this.add_staff_members_frm.get('email').value);
			form_data.append('password', this.add_staff_members_frm.get('password').value);
			form_data.append('accesibility', this.add_staff_members_frm.get('accesibility').value);
			// form_data.append('function_name', this.add_staff_members_frm.get('function_name').value);
			form_data.append('function_id', this.add_staff_members_frm.get('function_name').value);
			form_data.append('marital_status', this.add_staff_members_frm.get('marital_status').value);
			// form_data.append('permit_type', this.add_staff_members_frm.get('permit_type').value);
			form_data.append('permit_type', this.add_staff_members_frm.get('permit_type').value);
			form_data.append('permit_number', this.add_staff_members_frm.get('permit_number').value);
			form_data.append('permit_valid_date', this.add_staff_members_frm.get('permit_validate_date').value);
			form_data.append('CIN', this.add_staff_members_frm.get('cin').value);
			form_data.append('CNSS', this.add_staff_members_frm.get('cnss').value);
			form_data.append('registration', this.add_staff_members_frm.get('registration_number').value);
			form_data.append('date_of_birth', this.add_staff_members_frm.get('date_of_birth').value);
			// form_data.append('city_name', this.add_staff_members_frm.get('city_name').value);
			form_data.append('city_id', this.add_staff_members_frm.get('city_name').value);
			form_data.append('joining_date', this.add_staff_members_frm.get('joining_date').value);
			form_data.append('releasing_date', this.add_staff_members_frm.get('releasing_date').value);
			form_data.append('contract', this.add_staff_members_frm.get('contract').value);
			form_data.append('description', this.add_staff_members_frm.get('description').value);
			// console.log(this.add_staff_members_frm.get('password').value);
			console.log(this.add_staff_members_frm.get('accesibility').value);
			// console.log(this.add_staff_members_frm.value);
			this.staffMemberApiService.add_staff_members(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/manage-staff/list-staff-members'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

	/* Set County as Required based on DB Selected */
	onDatabseChange() {
		
		if (this.accesibility == '2') {
			console.log(this.accesibility)
			this.add_staff_members_frm.get('password').enable();
		} else {
			console.log(this.accesibility)
			this.add_staff_members_frm.get('password').disable();
		}

	}

}
