import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStaffMembersComponent } from './add-staff-members.component';

describe('AddStaffMembersComponent', () => {
  let component: AddStaffMembersComponent;
  let fixture: ComponentFixture<AddStaffMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddStaffMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStaffMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
