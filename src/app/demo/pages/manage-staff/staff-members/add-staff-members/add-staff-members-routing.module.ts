import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddStaffMembersComponent } from './add-staff-members.component';

const routes: Routes = [
  {
    path: '',
    component: AddStaffMembersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddStaffMembersRoutingModule { }
