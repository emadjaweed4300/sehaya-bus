import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { StaffMemberApiService } from '../../shared/staff-member-api.service';

@Component({
	selector: 'app-view-staff-members',
	templateUrl: './view-staff-members.component.html',
	styleUrls: ['./view-staff-members.component.scss']
})
export class ViewStaffMembersComponent implements OnInit {

	sraff_members_list: any = [];
	staff_member_id
	user_details: any;

	constructor(
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService,
		private staffMemberApiService: StaffMemberApiService
	) {
		this.staff_member_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		this.get_staff_members();
	}

	// Get All FeatureAPI
	get_staff_members(){
		this.staffMemberApiService.get_staff_members(this.staff_member_id).then((response: any) => {
			console.log('Response:', response);
			this.user_details = response.user_details;
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

}
