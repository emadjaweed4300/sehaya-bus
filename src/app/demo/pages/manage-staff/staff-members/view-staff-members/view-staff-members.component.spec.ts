import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewStaffMembersComponent } from './view-staff-members.component';

describe('ViewStaffMembersComponent', () => {
  let component: ViewStaffMembersComponent;
  let fixture: ComponentFixture<ViewStaffMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewStaffMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStaffMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
