import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewStaffMembersComponent } from './view-staff-members.component';

const routes: Routes = [
  {
    path: '',
    component: ViewStaffMembersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewStaffMembersRoutingModule { }
