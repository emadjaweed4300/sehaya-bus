import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewStaffMembersRoutingModule } from './view-staff-members-routing.module';
import { ViewStaffMembersComponent } from './view-staff-members.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ViewStaffMembersRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ViewStaffMembersComponent]
})
export class ViewStaffMembersModule { }
