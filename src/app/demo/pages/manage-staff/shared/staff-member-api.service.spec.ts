import { TestBed } from '@angular/core/testing';

import { StaffMemberApiService } from './staff-member-api.service';

describe('StaffMemberApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StaffMemberApiService = TestBed.get(StaffMemberApiService);
    expect(service).toBeTruthy();
  });
});
