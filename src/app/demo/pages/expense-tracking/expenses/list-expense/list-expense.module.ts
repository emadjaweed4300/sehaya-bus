import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListExpenseRoutingModule } from './list-expense-routing.module';
import { ListExpenseComponent } from './list-expense.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListExpenseRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListExpenseComponent]
})
export class ListExpenseModule { }
