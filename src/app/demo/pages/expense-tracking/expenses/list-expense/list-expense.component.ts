import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ExpenseCircuitApiService } from '../../shared/expense-circuit-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ClaimCircuitApiService } from '../../../manage-claim/shared/claim-circuit-api.service';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';
import { Route } from '@angular/compiler/src/core';

@Component({
	selector: 'app-list-expense',
	templateUrl: './list-expense.component.html',
	styleUrls: ['./list-expense.component.scss']
})
export class ListExpenseComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	expense_circuit_list: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	dtExportButtonOptions: any = {};

	all_claim_users = [];
	circuits = [];

	// for circuit filter
	circuit_id = ''

	// for filter
	driver_id = '';
	circuit = '';

	// For clear value of filter
	clear() {
		this.driver_id = '';
		this.circuit = '';
	}

	constructor(
		private expenseCircuitApi: ExpenseCircuitApiService,
		private __route: ActivatedRoute,
		private claimCircuitApi: ClaimCircuitApiService,
		private circuitApi: CircuitApiService,
		private router: Router,
	) {
		this.circuit_id = this.__route.snapshot.paramMap.get('setting_id');
	}

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			responsive: true
		};
		this.list_expense_circuit();
		this.get_all_claim_user();
		this.get_circuits();
		if (this.circuit_id != null) {
			this.circuitFilterAction()
		}
	}

	// Table Data not found message function
	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	get_all_claim_user() {
		this.claimCircuitApi.get_all_claim_user().then((response: any) => {
			console.log('Response:', response);
			this.all_claim_users = response.drivers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Circuit list Dropdown
	get_circuits() {
		this.circuitApi.circuits().then((response: any) => {
			console.log('Response:', response);
			this.circuits = response.circuit
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// List Data API
	list_expense_circuit() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.expenseCircuitApi.expense_circuit(formData).then((response: any) => {
			console.log('Response:', response);
			this.expense_circuit_list = response.paybill
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('circuit', this.circuit);
		formData.append('driver_id', this.driver_id);
		formData.append('add_filter', '1');
		console.log(this.circuit)
		this.expenseCircuitApi.expense_circuit(formData).then((response: any) => {
			console.log('Response:', response);
			this.expense_circuit_list = response.paybill
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	public circuitFilterAction() {
		let formData = new FormData()
		formData.append('circuit_id', this.circuit_id);
		formData.append('add_filter', '1');
		console.log(this.circuit_id)
		this.expenseCircuitApi.expense_circuit(formData).then((response: any) => {
			console.log('Response:', response);
			this.expense_circuit_list = response.paybill
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.list_expense_circuit()
	}

	// Delete
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.expenseCircuitApi.delete_expence_circuit(id).then((response: any) => {
					console.log('Response:', response);
					this.list_expense_circuit();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
							type: 'error',
							title: 'Error!',
							text: error,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
					});
			}
		})
	}

	// Share Function
	public shareAction(id: number) {
		// console.log(id)
		this.expenseCircuitApi.navigate_expense_to_cash_desk(id).then((response: any) => {
			console.log('Response:', response);
			Swal.fire({
				type: 'success',
				title: 'Success',
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				text: response.message
			});
			this.router.navigate(['/manage-cash-desk/cash-desk']);
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

}
