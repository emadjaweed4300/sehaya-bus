import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditExpenseRoutingModule } from './edit-expense-routing.module';
import { EditExpenseComponent } from './edit-expense.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditExpenseRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditExpenseComponent]
})
export class EditExpenseModule { }
