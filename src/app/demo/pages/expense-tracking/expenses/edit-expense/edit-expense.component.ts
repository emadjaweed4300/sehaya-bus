import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';
import { ExpenseCircuitApiService } from '../../shared/expense-circuit-api.service';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

@Component({
	selector: 'app-edit-expense',
	templateUrl: './edit-expense.component.html',
	styleUrls: ['./edit-expense.component.scss']
})
export class EditExpenseComponent implements OnInit {

	edit_expense_frm: FormGroup;
	drivers = [];
	circuits = [];
	operations =[];
	expense_circuit_id
	expenses: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private circuitApi: CircuitApiService,
		private expenseCircuitApi: ExpenseCircuitApiService,
		public datepipe: DatePipe
	) {
		this.expense_circuit_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		/** Form builder instance */
		this.edit_expense_frm = this.__fb.group({
			circuit: ["", Validators.required],
			driver: ["", Validators.required],
			operation:  [{ value: '', disabled: true }],
			reference: ["", Validators.required],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			opration_date: ["", Validators.required],
			description: ["", Validators.required],
		});
		this.get_drivers();
		this.get_circuits();
		this.get_operaions();
		this.get_expense_circuit();
	}

	// Get Driver list Dropdown
	get_drivers() {
		this.circuitApi.drivers_for_filter().then((response: any) => {
			console.log('Response:', response);
			this.drivers = response.drivers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	
	// Get Circuit list Dropdown
	get_circuits() {
		this.circuitApi.circuits().then((response: any) => {
			console.log('Response:', response);
			this.circuits = response.circuit
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Operation list Dropdown
	get_operaions() {
		this.expenseCircuitApi.get_operation().then((response: any) => {
			console.log('Response:', response);
			this.operations = response.expense
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get All Expense Circuit API
	get_expense_circuit(){
		this.expenseCircuitApi.get_expence_circuit(this.expense_circuit_id).then((response: any) => {
			console.log('Response:', response);
			this.expenses = response.expense;
			this.edit_expense_frm.get('circuit').setValue(response.expense.circuit_id);
			this.edit_expense_frm.get('driver').setValue(response.expense.driver_id);
			this.edit_expense_frm.get('operation').setValue(response.expense.operation_name);
			console.log(response.expense.operation)
			this.edit_expense_frm.get('reference').setValue(response.expense.reference);
			this.edit_expense_frm.get('amount').setValue(response.expense.amount);
			this.edit_expense_frm.get('opration_date').setValue(this.DateFormate(response.expense.opration_date));
			this.edit_expense_frm.get('description').setValue(response.expense.discription);
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	DateFormate(Date) {
		return this.datepipe.transform(Date, 'yyyy-MM-dd');
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_expense_frm.get(formControlName).errors &&
			(this.edit_expense_frm.get(formControlName).touched ||
				this.edit_expense_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_expense_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_expense_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_expense_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_expense_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call edit service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_expense_frm);
		if (this.edit_expense_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('expense_id', this.expense_circuit_id);
			form_data.append('circuit_id', this.edit_expense_frm.get('circuit').value);
			form_data.append('driver_id', this.edit_expense_frm.get('driver').value);
			form_data.append('operation', this.edit_expense_frm.get('operation').value);
			form_data.append('reference', this.edit_expense_frm.get('reference').value);
			form_data.append('amount', this.edit_expense_frm.get('amount').value);
			form_data.append('opration_date', this.edit_expense_frm.get('opration_date').value);
			form_data.append('discription', this.edit_expense_frm.get('description').value);

			this.expenseCircuitApi.edit_expence_circuit(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/expense-tracking/list-expense'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
