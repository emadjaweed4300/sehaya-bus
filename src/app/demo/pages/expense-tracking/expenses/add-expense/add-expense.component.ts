import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';
import Swal from 'sweetalert2';
import { ExpenseCircuitApiService } from '../../shared/expense-circuit-api.service';

@Component({
	selector: 'app-add-expense',
	templateUrl: './add-expense.component.html',
	styleUrls: ['./add-expense.component.scss']
})
export class AddExpenseComponent implements OnInit {

	add_expense_frm: FormGroup;
	drivers = [];
	circuits = [];
	operations =[];
	// Get the Circuit reference number onClikc from the list of crrcuit for the add expense for that circuit
	setting_id

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private circuitApi: CircuitApiService,
		private expenseCircuitApi: ExpenseCircuitApiService,
	) {
		this.setting_id = this.__route.snapshot.paramMap.get('setting_id');
	 }

	ngOnInit() {
		/** Form builder instance */
		this.add_expense_frm = this.__fb.group({
			circuit: [null, Validators.required],
			driver: ["", Validators.required],
			operation: ["", Validators.required],
			reference: ["", Validators.required],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			description: ["", Validators.required],
			opration_date:  ["", Validators.required],
		});
		this.add_expense_frm.get('circuit').setValue(this.setting_id);
		this.get_drivers();
		this.get_circuits();
		this.get_operaions();
	}

	// Get Driver list Dropdown
	get_drivers() {
		this.circuitApi.drivers_for_filter().then((response: any) => {
			console.log('Response:', response);
			this.drivers = response.drivers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	
	// Get Circuit list Dropdown
	get_circuits() {
		this.circuitApi.circuits().then((response: any) => {
			console.log('Response:', response);
			this.circuits = response.circuit
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Operation list Dropdown
	get_operaions() {
		this.expenseCircuitApi.get_operation().then((response: any) => {
			console.log('Response:', response);
			this.operations = response.expense
		})
		.catch((error: any) => {
			console.error(error);
		});
	}
	

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_expense_frm.get(formControlName).errors &&
			(this.add_expense_frm.get(formControlName).touched ||
				this.add_expense_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_expense_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_expense_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_expense_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_expense_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_expense_frm);
		if (this.add_expense_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('circuit_id', this.add_expense_frm.get('circuit').value);
			form_data.append('driver_id', this.add_expense_frm.get('driver').value);
			form_data.append('operation', this.add_expense_frm.get('operation').value);
			form_data.append('reference', this.add_expense_frm.get('reference').value);
			form_data.append('amount', this.add_expense_frm.get('amount').value);
			form_data.append('discription', this.add_expense_frm.get('description').value);
			form_data.append('opration_date', this.add_expense_frm.get('opration_date').value);

			this.expenseCircuitApi.add_expence_circuit(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/expense-tracking/list-expense'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
