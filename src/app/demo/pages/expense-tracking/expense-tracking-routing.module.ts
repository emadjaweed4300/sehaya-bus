import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Expense Tracking list,add,edit components
			{
				path: 'list-expense',
				loadChildren: () => import('./expenses/list-expense/list-expense.module').then(module => module.ListExpenseModule)
			},
			{
				path: 'list-expense/:setting_id',
				loadChildren: () => import('./expenses/list-expense/list-expense.module').then(module => module.ListExpenseModule)
			},
			{
				path: 'add-expense',
				loadChildren: () => import('./expenses/add-expense/add-expense.module').then(module => module.AddExpenseModule)
			},
			{
				path: 'edit-expense/:id',
				loadChildren: () => import('./expenses/edit-expense/edit-expense.module').then(module => module.EditExpenseModule)
			},
			// {
			// 	path: 'add-expense/:setting_id',
			// 	loadChildren: () => import('./expenses/add-expense/add-expense.module').then(module => module.AddExpenseModule)
			// },
		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ExpenseTrackingRoutingModule { }
