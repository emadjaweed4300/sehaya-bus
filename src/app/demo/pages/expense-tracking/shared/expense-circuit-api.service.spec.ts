import { TestBed } from '@angular/core/testing';

import { ExpenseCircuitApiService } from './expense-circuit-api.service';

describe('ExpenseCircuitApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExpenseCircuitApiService = TestBed.get(ExpenseCircuitApiService);
    expect(service).toBeTruthy();
  });
});
