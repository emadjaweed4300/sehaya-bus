import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global.service';
import { END_POINT } from 'src/app/constant';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class ExpenseCircuitApiService {

	// API path
	//   base_path = 'http://nooridev.in/hapush/sehayabus';

	data = new FormData();
	constructor(
		private http: HttpClient,
		private translate: TranslateService,
		private global: GlobalService,
		private router: Router
	) {
		this.data.append('user_id', this.global.currentUser.id);
		this.data.append('user_role', this.global.currentUser.user_role);
		this.data.append('security_key', this.global.currentUser.token)
	}

	// Http Options
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	}

	// Handle API errors
	handleError(error: HttpErrorResponse) {

		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${JSON.stringify(error.status)}, ` +
				`body was: ${JSON.stringify(error.message)}`);
		}
		// return an observable with a user-facing error message
		if (error.status == 0) {
			return throwError(
				Swal.fire(
					'The Internet?',
					'Please try again later',
					'question'
				)
			);
		}
	};

	// Expense Circuit API's
	// Add Expense Circuit
	add_expence_circuit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-expence-circuit', data)
	}
	// Expense Circuit
	expense_circuit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/expense-circuit', data)
	}
	// Expense Circuit
	// expense_circuit() {
	// 	return this._callAPI('/expense-circuit', this.data)
	// }

	// Delete Expense Circuit
	delete_expence_circuit(id) {
		return this._callAPI('/delete-expence-circuit/' + id, this.data)
	}

	// Edit Expense Circuit
	edit_expence_circuit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-expence-circuit', data)
	}
	// Get Expense Circuit
	get_staff_members(id) {
		return this._callAPI('/get-staff-member/' + id, this.data)
	}

	// Get Operations
	get_operation() {
		return this._callAPI('/get-expense-cash-movements', this.data)
	}
	
	// Get Expense Circuit By Id
	get_expence_circuit(id) {
		return this._callAPI('/get-expense-circuit/' + id, this.data)
	}

	// Navigate expense to cash desk API
	navigate_expense_to_cash_desk(id) {
		return this._callAPI('/navigate-expense-to-cash/' + id, this.data)
	} 


	 _callAPI(url: string, params?) {
		return new Promise((resolve, reject) => {
			this.http.post(END_POINT + url, params)
				.subscribe(res => {
					if (res['response'] === 'success') {
						resolve(res)
					} else {
						if (res['response_code'] == 405 || res['response_code'] == 403 || res['response_code'] == 204){
							localStorage.clear();
							return window.location.replace('/auth/signin');
						}
						reject(res['message'])
					}
				}, (this.handleError))
		})
	}
}
