import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpenseTrackingRoutingModule } from './expense-tracking-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ExpenseTrackingRoutingModule,
  ],
  declarations: []
})
export class ExpenseTrackingModule { }
