import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageCashDeskRoutingModule } from './manage-cash-desk.routing.module';

 
@NgModule({
  imports: [
    CommonModule,
    ManageCashDeskRoutingModule
  ],
  declarations: []
})
export class ManageCashDeskModule { } 
