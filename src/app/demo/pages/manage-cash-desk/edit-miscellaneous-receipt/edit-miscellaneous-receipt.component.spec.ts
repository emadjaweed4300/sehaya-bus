import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMiscellaneousReceiptComponent } from './edit-miscellaneous-receipt.component';

describe('EditMiscellaneousReceiptComponent', () => {
  let component: EditMiscellaneousReceiptComponent;
  let fixture: ComponentFixture<EditMiscellaneousReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMiscellaneousReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMiscellaneousReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
