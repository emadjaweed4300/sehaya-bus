import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditMiscellaneousReceiptComponent } from './edit-miscellaneous-receipt.component';

const routes: Routes = [
  {
    path: '',
    component: EditMiscellaneousReceiptComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditMiscellaneousReceiptRoutingModule { }
