import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddMiscellaneousReceiptComponent } from './add-miscellaneous-receipt.component';

const routes: Routes = [
  {
    path: '',
    component: AddMiscellaneousReceiptComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddMiscellaneousReceiptRoutingModule { }
