import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { CashDeskApiService } from '../shared/cash-desk-api.service';

@Component({
	selector: 'app-add-miscellaneous-receipt',
	templateUrl: './add-miscellaneous-receipt.component.html',
	styleUrls: ['./add-miscellaneous-receipt.component.scss']
})
export class AddMiscellaneousReceiptComponent implements OnInit {

	add_miscellaneous_receipt_frm: FormGroup;

	receipt_operations =[];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private cashDeskApi: CashDeskApiService,
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_miscellaneous_receipt_frm = this.__fb.group({
			operation: ["", Validators.required],
			reference: ["", Validators.required],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			description: ["", Validators.required],
			opration_date: ["", Validators.required],
		});
		this.get_operaions_receipt();
	}

	// get operation dropdown
	get_operaions_receipt() {
		this.cashDeskApi.get_receipt_operation().then((response: any) => {
			console.log('Response:', response);
			this.receipt_operations = response.reciept
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}
	
	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_miscellaneous_receipt_frm.get(formControlName).errors &&
			(this.add_miscellaneous_receipt_frm.get(formControlName).touched ||
				this.add_miscellaneous_receipt_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_miscellaneous_receipt_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_miscellaneous_receipt_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_miscellaneous_receipt_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_miscellaneous_receipt_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_miscellaneous_receipt_frm);
		if (this.add_miscellaneous_receipt_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('direction', '1');
			form_data.append('operation', this.add_miscellaneous_receipt_frm.get('operation').value);
			form_data.append('reference', this.add_miscellaneous_receipt_frm.get('reference').value);
			form_data.append('amount', this.add_miscellaneous_receipt_frm.get('amount').value);
			form_data.append('discription', this.add_miscellaneous_receipt_frm.get('description').value);
			form_data.append('opration_date', this.add_miscellaneous_receipt_frm.get('opration_date').value);
			
			this.cashDeskApi.add_miscellaneous(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/manage-cash-desk/cash-desk'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
