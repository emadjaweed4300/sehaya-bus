import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddMiscellaneousReceiptRoutingModule } from './add-miscellaneous-receipt-routing.module';
import { AddMiscellaneousReceiptComponent } from './add-miscellaneous-receipt.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    AddMiscellaneousReceiptRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    FullCalendarModule,
    NgbDropdownModule
  ],
  declarations: [AddMiscellaneousReceiptComponent]
})
export class AddMiscellaneousReceiptModule { }
