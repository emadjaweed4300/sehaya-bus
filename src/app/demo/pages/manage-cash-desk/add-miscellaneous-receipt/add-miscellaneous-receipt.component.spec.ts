import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMiscellaneousReceiptComponent } from './add-miscellaneous-receipt.component';

describe('AddMiscellaneousReceiptComponent', () => {
  let component: AddMiscellaneousReceiptComponent;
  let fixture: ComponentFixture<AddMiscellaneousReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMiscellaneousReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMiscellaneousReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
