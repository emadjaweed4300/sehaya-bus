import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMiscellaneousExpenseComponent } from './add-miscellaneous-expense.component';

describe('AddMiscellaneousExpenseComponent', () => {
  let component: AddMiscellaneousExpenseComponent;
  let fixture: ComponentFixture<AddMiscellaneousExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMiscellaneousExpenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMiscellaneousExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
