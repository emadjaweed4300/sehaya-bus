import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddMiscellaneousExpenseComponent } from './add-miscellaneous-expense.component';

const routes: Routes = [
  {
    path: '',
    component: AddMiscellaneousExpenseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddMiscellaneousExpenseRoutingModule { }
