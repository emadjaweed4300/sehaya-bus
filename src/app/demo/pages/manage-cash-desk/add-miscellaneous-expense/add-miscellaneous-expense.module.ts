import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddMiscellaneousExpenseRoutingModule } from './add-miscellaneous-expense-routing.module';
import { AddMiscellaneousExpenseComponent } from './add-miscellaneous-expense.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    AddMiscellaneousExpenseRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    FullCalendarModule,
    NgbDropdownModule
  ],
  declarations: [AddMiscellaneousExpenseComponent]
})
export class AddMiscellaneousExpenseModule { }
