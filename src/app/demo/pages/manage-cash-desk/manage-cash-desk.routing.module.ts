import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Cash Desk components
			{
				path: 'cash-desk',
				loadChildren: () => import('./cash-desk/cash-desk.module').then(module => module.CashDeskModule)
			},

			// Miscellaneous Receipt
			{
				path: 'add-miscellaneous-receipt',
				loadChildren: () => import('./add-miscellaneous-receipt/add-miscellaneous-receipt.module').then(module => module.AddMiscellaneousReceiptModule)
			},

			// Miscellaneous Expense
			{
				path: 'add-miscellaneous-expense',
				loadChildren: () => import('./add-miscellaneous-expense/add-miscellaneous-expense.module').then(module => module.AddMiscellaneousExpenseModule)
			},

			// Edit Miscellaneous Receipt
			{
				path: 'edit-miscellaneous-receipt/:id',
				loadChildren: () => import('./edit-miscellaneous-receipt/edit-miscellaneous-receipt.module').then(module => module.EditMiscellaneousReceiptModule)
			},

			// Edit Miscellaneous Expense
			{
				path: 'edit-miscellaneous-expense/:id',
				loadChildren: () => import('./edit-miscellaneous-expense/edit-miscellaneous-expense.module').then(module => module.EditMiscellaneousExpenseModule)
			},
		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManageCashDeskRoutingModule { }
