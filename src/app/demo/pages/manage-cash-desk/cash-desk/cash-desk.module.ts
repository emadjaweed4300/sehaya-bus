import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CashDeskRoutingModule } from './cash-desk-routing.module';
import { CashDeskComponent } from './cash-desk.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    CashDeskRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    FullCalendarModule,
    NgbDropdownModule
  ],
  declarations: [CashDeskComponent]
})
export class CashDeskModule { }
