import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ChartDB } from 'src/app/fack-db/chart-data';
import { CashDeskApiService } from '../shared/cash-desk-api.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
	selector: 'app-cash-desk',
	templateUrl: './cash-desk.component.html',
	styleUrls: ['./cash-desk.component.scss']
})
export class CashDeskComponent implements OnInit {

	public chartDB: any;

	directionArr: any[] = [
		{ id: 1, name: 'Receipt' },
		{ id: 2, name: 'Expense' }
	];

	current_date = (new Date()).toISOString().substring(0, 10)

	// for filter
	from_date = '';
	to_date = '';
	direction = '';
	operation = '';
	// For clear value of filter
	clear() {
		this.from_date = '';
		this.to_date = '';
		this.direction = '';
		this.operation = '';
	}

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	miscellaneous_lists: any = [];
	cash_desk_lists: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	dtExportButtonOptions: any = {};

	constructor(
		private cashDeskApi: CashDeskApiService,
		private router: Router,
	) {
		this.chartDB = ChartDB;
		// console.log(this.current_date)
	}

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				"<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			buttons: [
				{
					extend: 'copy',
					text: '<i class="fa fa-copy" style="color: white;"></i>',
					titleAttr: 'Copy',
					exportOptions: {
						columns: [0, 1, 2, 3, 4, 5]
					}
				},
				{
					extend: 'print',
					text: '<i class="fa fa-print" style="color: white;"></i>',
					titleAttr: 'Print',
					exportOptions: {
						columns: [0, 1, 2, 3, 4, 5]
					}
				},
				{
					extend: 'excel',
					text: '<i class="fa fa-file-excel" style="color: white;"></i>',
					titleAttr: 'Excel',
					exportOptions: {
						columns: [0, 1, 2, 3, 4, 5]
					}
				},
				{
					extend: 'csv',
					text: '<i class="fa fa-file-csv" style="color: white;"></i>',
					titleAttr: 'CSV',
					exportOptions: {
						columns: [0, 1, 2, 3, 4, 5]
					}
				}
			],
			responsive: true
		};
		this.get_cash_desk_deatils();
		this.miscellaneous_list();
		this.get_cash_desk_deatils();
	}

	// Table Data not found message function
	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// Get Cash Desk Card Details
	get_cash_desk_deatils() {
		let formData = new FormData()
		this.cashDeskApi.get_cash_desk_amount(formData).then((response: any) => {
			console.log('Response:', response);
			this.cash_desk_lists = response.cash_desk;
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('from_date', this.from_date);
		formData.append('to_date', this.to_date);
		formData.append('direction', this.direction);
		formData.append('operation', this.operation)
		formData.append('add_filter', '1');
		// console.log(this.vehicle_category);
		// console.log(this.vehicle_brand);
		// console.log(this.capacity);
		this.cashDeskApi.list_miscellaneous(formData).then((response: any) => {
			console.log('Response:', response);
			this.miscellaneous_lists = response.miscellaneous
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.miscellaneous_list()
	}

	// List Data API
	miscellaneous_list() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.cashDeskApi.list_miscellaneous(formData).then((response: any) => {
			console.log('Response:', response);
			this.miscellaneous_lists = response.miscellaneous
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
			});
	}

	opening_Cash_Desk() {
		const form_data = new FormData();
		form_data.append('case_date', this.current_date);
		this.cashDeskApi.case_open(form_data).then((response: any) => {
			console.log('Response:', response);
			this.get_cash_desk_deatils();
			Swal.fire({
				type: 'success',
				title: 'Success',
				confirmButtonColor: '#323258',
				// cancelButtonColor: '#ff1f1f',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				text: response.message
			});
			// this.router.navigate(['/manage-cash-desk/cash-desk'])
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// closing_Cash_Desk() {
	// 	const form_data = new FormData();
	// 	// console.log(this.cash_desk_lists.id)
	// 	form_data.append('case_id', this.cash_desk_lists.id);
	// 	this.cashDeskApi.case_close(form_data).then((response: any) => {
	// 		console.log('Response:', response);
	// 		Swal.fire({
	// 			type: 'success',
	// 			title: 'Success',
	// 			confirmButtonColor: '#323258',
	// 			// cancelButtonColor: '#ff1f1f',
	// 			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
	// 			text: response.message
	// 		});
	// 	})
	// 		.catch((error: any) => {
	// 			console.error(error);
	// 			Swal.fire({
	// 				type: 'error',
	// 				title: 'Error!',
	// 				text: error,
	// 				confirmButtonColor: '#323258',
	// 				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// 			});
	// 		});
	// }

	closing_Cash_Desk() {
		const form_data = new FormData();
		// console.log(this.cash_desk_lists.id)
		form_data.append('case_id', this.cash_desk_lists.id);
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.cashDeskApi.case_close(form_data).then((response: any) => {
					console.log('Response:', response);
					this.get_cash_desk_deatils();
					Swal.fire({
						type: 'success',
						title: 'Success!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
							type: 'error',
							title: 'Error!',
							text: error,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
					});
			}
		})
	}

	// Delete function for Receipt
	public deleteReceiptAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.cashDeskApi.delete_receipt(id).then((response: any) => {
					console.log('Response:', response);
					this.miscellaneous_list();
					this.get_cash_desk_deatils();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
					});
			}
		})
	}

	// Delete function for Expense
	public deleteExpenseAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.cashDeskApi.delete_expense(id).then((response: any) => {
					console.log('Response:', response);
					this.miscellaneous_list();
					this.get_cash_desk_deatils();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
					});
			}
		})
	}

}
