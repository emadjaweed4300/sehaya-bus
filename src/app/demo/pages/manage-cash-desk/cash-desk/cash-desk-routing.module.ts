import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CashDeskComponent } from './cash-desk.component';

const routes: Routes = [
  {
    path: '',
    component: CashDeskComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashDeskRoutingModule { }
