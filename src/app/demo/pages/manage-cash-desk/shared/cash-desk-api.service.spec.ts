import { TestBed } from '@angular/core/testing';

import { CashDeskApiService } from './cash-desk-api.service';

describe('CashDeskApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CashDeskApiService = TestBed.get(CashDeskApiService);
    expect(service).toBeTruthy();
  });
});
