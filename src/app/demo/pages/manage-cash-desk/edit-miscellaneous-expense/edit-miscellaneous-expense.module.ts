import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditMiscellaneousExpenseRoutingModule } from './edit-miscellaneous-expense-routing.module';
import { EditMiscellaneousExpenseComponent } from './edit-miscellaneous-expense.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    EditMiscellaneousExpenseRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule,
    FullCalendarModule,
    NgbDropdownModule
  ],
  declarations: [EditMiscellaneousExpenseComponent]
})
export class EditMiscellaneousExpenseModule { }
