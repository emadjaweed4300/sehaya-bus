import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CashDeskApiService } from '../shared/cash-desk-api.service';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

@Component({
	selector: 'app-edit-miscellaneous-expense',
	templateUrl: './edit-miscellaneous-expense.component.html',
	styleUrls: ['./edit-miscellaneous-expense.component.scss']
})
export class EditMiscellaneousExpenseComponent implements OnInit {

	edit_miscellaneous_expense_frm: FormGroup;

	expensse_operations = [];
	miscellaneous_expense_id
	miscellaneous_expense: any;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private cashDeskApi: CashDeskApiService,
		public datepipe: DatePipe,
		private __route: ActivatedRoute,
	) {
		this.miscellaneous_expense_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		/** Form builder instance */
		this.edit_miscellaneous_expense_frm = this.__fb.group({
			operation: [{ value: '', disabled: true }],
			reference: ["", Validators.required],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			description: ["", Validators.required],
			opration_date: [{ value: '', disabled: true }],
		});
		// this.get_operaions_expense();
		this.get_miscellaneous_expense();
	}

	// get operation dropdown
	// get_operaions_expense() {
	// 	this.cashDeskApi.get_expense_operation().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.expensse_operations = response.expense
	// 	})
	// 		.catch((error: any) => {
	// 			console.error(error);
	// 			Swal.fire({
	// 				type: 'error',
	// 				title: 'Error!',
	// 				text: error,
	// 				confirmButtonColor: '#323258',
	// 				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// 			});
	// 		});
	// }

	get_miscellaneous_expense(){
		this.cashDeskApi.get_miscellaneous_expense(this.miscellaneous_expense_id).then((response: any) => {
			console.log('Response:', response);
			this.miscellaneous_expense = response.expense;
			// name (formControlName) & name (postman)
			this.edit_miscellaneous_expense_frm.get('operation').setValue(response.expense.operation_name);
			this.edit_miscellaneous_expense_frm.get('reference').setValue(response.expense.reference);
			this.edit_miscellaneous_expense_frm.get('amount').setValue(response.expense.amount);
			this.edit_miscellaneous_expense_frm.get('opration_date').setValue(this.DateFormate(response.expense.opration_date));
			this.edit_miscellaneous_expense_frm.get('description').setValue(response.expense.discription);

		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

	DateFormate(Date) {
		return this.datepipe.transform(Date, 'yyyy-MM-dd');
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_miscellaneous_expense_frm.get(formControlName).errors &&
			(this.edit_miscellaneous_expense_frm.get(formControlName).touched ||
				this.edit_miscellaneous_expense_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_miscellaneous_expense_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_miscellaneous_expense_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_miscellaneous_expense_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_miscellaneous_expense_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_miscellaneous_expense_frm);
		if (this.edit_miscellaneous_expense_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('expense_id', this.miscellaneous_expense_id);
			form_data.append('operation', this.edit_miscellaneous_expense_frm.get('operation').value);
			form_data.append('reference', this.edit_miscellaneous_expense_frm.get('reference').value);
			form_data.append('amount', this.edit_miscellaneous_expense_frm.get('amount').value);
			form_data.append('discription', this.edit_miscellaneous_expense_frm.get('description').value);
			form_data.append('opration_date', this.edit_miscellaneous_expense_frm.get('opration_date').value);

			this.cashDeskApi.edit_miscellaneous_expense(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				this.router.navigate(['/manage-cash-desk/cash-desk'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
