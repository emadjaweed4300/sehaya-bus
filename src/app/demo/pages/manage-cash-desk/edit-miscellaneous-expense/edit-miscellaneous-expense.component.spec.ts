import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMiscellaneousExpenseComponent } from './edit-miscellaneous-expense.component';

describe('EditMiscellaneousExpenseComponent', () => {
  let component: EditMiscellaneousExpenseComponent;
  let fixture: ComponentFixture<EditMiscellaneousExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMiscellaneousExpenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMiscellaneousExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
