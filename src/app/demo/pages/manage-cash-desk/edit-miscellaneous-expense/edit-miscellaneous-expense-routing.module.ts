import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditMiscellaneousExpenseComponent } from './edit-miscellaneous-expense.component';

const routes: Routes = [
  {
    path: '',
    component: EditMiscellaneousExpenseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditMiscellaneousExpenseRoutingModule { }
