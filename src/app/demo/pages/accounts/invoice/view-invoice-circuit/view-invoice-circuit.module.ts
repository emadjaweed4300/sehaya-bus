import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewInvoiceCircuitRoutingModule } from './view-invoice-circuit-routing.module';
import { ViewInvoiceCircuitComponent } from './view-invoice-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ViewInvoiceCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ViewInvoiceCircuitComponent]
})
export class ViewInvoiceCircuitModule { }
