import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInvoiceCircuitComponent } from './view-invoice-circuit.component';

describe('ViewInvoicCircuitComponent', () => {
  let component: ViewInvoiceCircuitComponent;
  let fixture: ComponentFixture<ViewInvoiceCircuitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewInvoiceCircuitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewInvoiceCircuitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
