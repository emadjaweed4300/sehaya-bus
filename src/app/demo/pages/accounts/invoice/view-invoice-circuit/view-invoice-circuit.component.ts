import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-view-invoice-circuit',
	templateUrl: './view-invoice-circuit.component.html',
	styleUrls: ['./view-invoice-circuit.component.scss']
})
export class ViewInvoiceCircuitComponent implements OnInit {

	circuit_id
	circuit_details: any;
	passengers_details_list: any = [];
	route_affections_list: any = [];
	route_details_list: any = [];

	constructor(
		private router: Router,
		private __route: ActivatedRoute,
		private circuitApi: CircuitApiService,
	) {
		this.circuit_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		this.get_circuit();
		this.list_route_affections();
		this.list_route_details();
		this.list_passenger_details();
	}

	// Get All FeatureAPI
	get_circuit(){
		this.circuitApi.get_circuit(this.circuit_id).then((response: any) => {
			console.log('Response:', response);
			this.circuit_details = response.circuit;
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

	// List Data API
	list_route_affections() {
		this.circuitApi.get_circuit(this.circuit_id).then((response: any) => {
			console.log('Response:', response);
			this.route_affections_list = response.circuit.route_affections;
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// List Data API
	list_route_details() {
		this.circuitApi.get_circuit(this.circuit_id).then((response: any) => {
			console.log('Response:', response);
			this.route_details_list = response.circuit.route_details;
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// List Passenger Data Function
	list_passenger_details() {
		this.circuitApi.get_circuit(this.circuit_id).then((response: any) => {
			console.log('Response:', response);
			this.passengers_details_list = response.circuit.passengers_details;
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

}
