import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewInvoiceCircuitComponent } from './view-invoice-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: ViewInvoiceCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewInvoiceCircuitRoutingModule { }
