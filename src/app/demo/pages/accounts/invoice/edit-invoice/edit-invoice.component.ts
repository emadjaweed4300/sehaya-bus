import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { AccountsApiService } from '../../shared/accounts-api.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
	selector: 'app-edit-invoice',
	templateUrl: './edit-invoice.component.html',
	styleUrls: ['./edit-invoice.component.scss']
})
export class EditInvoiceComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();

	invoice_circuit_id
	client_id
	edit_invoiced_frm: FormGroup;
	invoiced_circuit

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private accountsApi: AccountsApiService,
	) {
		this.invoice_circuit_id = this.__route.snapshot.paramMap.get('id');
		this.client_id = this.__route.snapshot.paramMap.get('client_id');
	}

	ngOnInit() {
		this.edit_invoiced_frm = this.__fb.group({
			invoice_details: this.__fb.array([
				// this.getInvoiceBlankArray()
			]),
			// function_name: ["", Validators.required],
		});
		this.get_invoiced_circuits()
		// console.log(this.invoice_circuit_id);
		// console.log(this.client_id);
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// New Row Array for Affection Table
	getInvoiceBlankArray(id, refrence, client_name, departure_date, vehicle_name, gross_amount, is_invoiced, circuit_id, vehicle_id) {
		return this.__fb.group({
			id: [id],
			refrence: [{ value: refrence, disabled: true }],
			client_name: [{ value: client_name, disabled: true }],
			dept_date: [{ value: departure_date, disabled: true }],
			vehicle: [{ value: vehicle_name, disabled: true }],
			flat_rate: [gross_amount, [Validators.required, Validators.pattern("^[0-9]+$")]],
			is_invoiced: [is_invoiced],
			circuit_id: [circuit_id],
			vehicle_id: [vehicle_id],
			departure_date: [departure_date],
		});
	}

	get_invoiced_circuits() {
		const form_data = new FormData();
		form_data.append('invoice_id', this.invoice_circuit_id);
		form_data.append('client_id', this.client_id);
		this.accountsApi.get_invoiced_circuit(form_data).then((response: any) => {
			console.log('Response:', response);
			this.invoiced_circuit = response.invoice_details;
			// Array Data (Affection)
			const control2 = <FormArray>this.edit_invoiced_frm.controls['invoice_details'];
			for (let item of this.invoiced_circuit.circuit) {
				// item.deadline_value ---> (deadline_value is parameter Name (API))
				control2.push(this.getInvoiceBlankArray(item.id, item.refrence, item.client_name, item.departure_date, item.vehicle_name, item.gross_amount, item.is_invoiced, item.circuit_id, item.vehicle_id))
			}
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_invoiced_frm.get(formControlName).errors &&
			(this.edit_invoiced_frm.get(formControlName).touched ||
				this.edit_invoiced_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_invoiced_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_invoiced_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_invoiced_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_invoiced_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}


	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_invoiced_frm);
		if (this.edit_invoiced_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('invoice_id', this.invoice_circuit_id);
			form_data.append('client_id', this.client_id);
			form_data.append('invoice', JSON.stringify(this.edit_invoiced_frm.get('invoice_details').value));
			// console.log(this.edit_invoiced_frm.get('invoice_details').value);
			// console.log(this.invoice_circuit_id);
			// console.log(this.client_id);
			this.accountsApi.update_invoice(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				this.router.navigate(['/accounts/list-invoice'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}	

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
