import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { AccountsApiService } from '../../shared/accounts-api.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { END_POINT } from 'src/app/constant';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
@Component({
	selector: 'app-list-invoice',
	templateUrl: './list-invoice.component.html',
	styleUrls: ['./list-invoice.component.scss']
})
export class ListInvoiceComponent implements OnInit {

	dtExportButtonOptions: any = {};
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	invoice_list = []
	constructor(
		private __fb: FormBuilder,
		private accountsApi: AccountsApiService,
		private router: Router,
		private translate: TranslateService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				"<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			buttons: [
				{
					extend: 'copy',
					text: '<i class="fa fa-copy" style="color: white;"></i>',
					titleAttr: 'Copy',
					exportOptions: {
						columns: [0, 1, 2, 3]
					}
				},
				{
					extend: 'print',
					text: '<i class="fa fa-print" style="color: white;"></i>',
					titleAttr: 'Print',
					exportOptions: {
						columns: [0, 1, 2, 3]
					}
				},
				{
					extend: 'excel',
					text: '<i class="fa fa-file-excel" style="color: white;"></i>',
					titleAttr: 'Excel',
					exportOptions: {
						columns: [0, 1, 2, 3]
					}
				},
				{
					extend: 'csv',
					text: '<i class="fa fa-file-csv" style="color: white;"></i>',
					titleAttr: 'CSV',
					exportOptions: {
						columns: [0, 1, 2, 3]
					}
				}
			],
			responsive: true
		};
		this.getAllInvoicesList();
	}

	getAllInvoicesList() {
		this.accountsApi.invoices_list().then((response: any) => {
			console.log('Response:', response);
			this.invoice_list = response.invoice
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Download
	public downloadAction(id: number) {
		this.accountsApi.download_invoice(id).then((response: any) => {
			// END_POINT + Response URL
			console.log('Response:', response);
			let pdf = END_POINT + '/' + response.url;
			window.open(pdf, '_blank');
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// // Edit navigate function
	// edit_invoice(id, id_client) {
	// 	let invoice_id = id;
	// 	let client_id = id_client;
	// 	console.log(invoice_id);
	// 	console.log(client_id);
	// 	let formData = new FormData()
	// 	// formData.append('invoice_id', invoice_id);
	// 	// formData.append('client_id', client_id);
	// 	this.router.navigate(['/accounts/edit-invoice'])
	// }

	cancelInvocieAction(id) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You want to cancel the Invoice!",
			input: 'textarea',
			inputAttributes: {
				autocapitalize: 'off'
			},
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then((result) => {
			if (result.value) {
				let val = result.value;
				let invoice_id = id;
				// console.log(val);
				// console.log(id);
				let formData = new FormData()
				formData.append('invoice_id', invoice_id);
				formData.append('cancelation_reason', val);
				this.accountsApi.cancel_invoice(formData).then((response: any) => {
					console.log('Response:', response);
					Swal.fire({
						type: 'success',
						title: 'Success',
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
						text: response.message
					});

				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
							type: 'error',
							title: 'Error!',
							text: error,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
					});
			}
		})
	}

	invoiceCanceledReason(id) {
		let formData = new FormData()
		let invoice_id = id;
		console.log(id);
		formData.append('invoice_id', invoice_id);
		this.accountsApi.invoice_canceled_reason(formData).then((response: any) => {
			console.log('Response:', response);
			var msg = ''
			this.translate.get('list_invoice.Cancellation Reason').subscribe(val => {
				msg = val
			})
			Swal.fire({
				// type: 'success',
				title: msg,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				text: response.cancelation_reason.cancelation_reason
			});

		})
	}

}

