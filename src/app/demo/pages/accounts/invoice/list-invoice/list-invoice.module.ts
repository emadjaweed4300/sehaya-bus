import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListInvoiceRoutingModule } from './list-invoice-routing.module';
import { ListInvoiceComponent } from './list-invoice.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListInvoiceRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListInvoiceComponent]
})
export class ListInvoiceModule { }
