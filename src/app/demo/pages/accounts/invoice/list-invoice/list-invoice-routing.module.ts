import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListInvoiceComponent } from './list-invoice.component';

const routes: Routes = [
  {
    path: '',
    component: ListInvoiceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListInvoiceRoutingModule { }
