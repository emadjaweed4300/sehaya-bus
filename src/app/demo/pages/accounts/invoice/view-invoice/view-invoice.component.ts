import { Component, OnInit } from '@angular/core';
import { AccountsApiService } from '../../shared/accounts-api.service';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-view-invoice',
	templateUrl: './view-invoice.component.html',
	styleUrls: ['./view-invoice.component.scss']
})
export class ViewInvoiceComponent implements OnInit {

	id
	invoice_details: any;
	invoice_list: any = [];

	constructor(
		private __route: ActivatedRoute,
		private accountsApi: AccountsApiService
	) { 
		this.id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		this.get_invoice_details();
		this.list_invoice();
	}

	// Get All FeatureAPI
	get_invoice_details() {
		this.accountsApi.get_invoice(this.id).then((response: any) => {
			console.log('Response:', response);
			this.invoice_details = response.invoice;
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// List Data API
	list_invoice() {
		this.accountsApi.get_invoice(this.id).then((response: any) => {
			console.log('Response:', response);
			this.invoice_list = response.invoice.circuit;
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

}
