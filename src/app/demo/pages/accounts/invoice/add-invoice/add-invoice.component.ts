import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { AccountsApiService } from '../../shared/accounts-api.service';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
@Component({
	selector: 'app-add-invoice',
	templateUrl: './add-invoice.component.html',
	styleUrls: ['./add-invoice.component.scss']
})
export class AddInvoiceComponent implements OnInit {

	filter_invoice_frm: FormGroup
	dtExportButtonOptions: any = {};
	invoice_circuit_list = [];
	users = [];
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	invoice: any = []

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private accountsApi: AccountsApiService,
		private translate: TranslateService
	) { }

	ngOnInit() {
		this.filter_invoice_frm = this.__fb.group({
			dated: [""],
			at: [""],
			client: [""]
		});
		this.getUsers();
	}

	getUsers(){
		this.accountsApi.users().then((response: any) => {
			console.log('Response:', response);
			this.users = response.list_of_users
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	invoiceCircuitList(filter_status){
		let form_data = new FormData();

		if(filter_status == 1){
			form_data.append('client_id', this.filter_invoice_frm.get('client').value);
			form_data.append('from_date', this.filter_invoice_frm.get('dated').value);
			form_data.append('to_date', this.filter_invoice_frm.get('at').value);
			form_data.append('add_filter', '1');
			console.log(this.filter_invoice_frm.get('dated').value)
			this.accountsApi.invoice_circuit_list(form_data).then((response: any) => {
				console.log('Response:', response);
				this.invoice_circuit_list = response.invoice
				if (this.isDtInitialized) {
					this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
						dtInstance.destroy();
						this.dtTrigger.next();
					});
				} else {
					this.isDtInitialized = true
					this.dtTrigger.next();
				}
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		} else {
			form_data.append('remove_filter', '1');
			this.accountsApi.invoice_circuit_list(form_data).then((response: any) => {
				console.log('Response:', response);
				this.invoice_circuit_list = response.invoice
				if (this.isDtInitialized) {
					this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
						dtInstance.destroy();
						this.dtTrigger.next();
					});
				} else {
					this.isDtInitialized = true
					this.dtTrigger.next();
				}
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}

	}

	addRowIntoArray(event, invoice){
		const index: number = this.invoice.indexOf(invoice);
		if(event.target.checked == true){
			this.invoice.push(invoice)
			console.log(this.invoice)
		} else {
			this.invoice.splice(index, 1);
			console.log(this.invoice)
		}
	}

	addInvoice(){
		if(this.invoice.length !== 0){
			let form_data = new FormData();
			form_data.append('client_id', this.filter_invoice_frm.get('client').value);
			form_data.append('invoice', JSON.stringify(this.invoice));
			this.accountsApi.add_invoice(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				this.router.navigate(['/accounts/list-invoice'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		} else {
			var msg = ''
			this.translate.get('add_invoice.please select one or more invoice').subscribe(val => {
				msg = val
			})
			return Swal.fire({
				type: 'warning',
				title: 'Warning!',
				text: msg,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		}
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.filter_invoice_frm.get(formControlName).errors &&
			(this.filter_invoice_frm.get(formControlName).touched ||
				this.filter_invoice_frm.get(formControlName).dirty);
		if (errors) {
			required = this.filter_invoice_frm.get(formControlName).errors
				.required;
			patternValidate = this.filter_invoice_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.filter_invoice_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.filter_invoice_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.filter_invoice_frm);
		if (this.filter_invoice_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('user_id', null);
			form_data.append('circuit', this.filter_invoice_frm.get('circuit').value);
			form_data.append('client', this.filter_invoice_frm.get('client').value);
			form_data.append('vehicles', this.filter_invoice_frm.get('vehicles').value);
			form_data.append('departure_date', this.filter_invoice_frm.get('departure_date').value);
			form_data.append('amount', this.filter_invoice_frm.get('amount').value);
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
