import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global.service';
import { throwError } from 'rxjs';
import Swal from 'sweetalert2';
import { END_POINT } from 'src/app/constant';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class AccountsApiService {

	data = new FormData();
	constructor(
		private http: HttpClient,
		private translate: TranslateService,
		private global: GlobalService,
		private router: Router
	) {
		this.data.append('user_id', this.global.currentUser.id);
		this.data.append('user_role', this.global.currentUser.user_role);
		this.data.append('security_key', this.global.currentUser.token)
	}

	// Http Options
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	}

	// Handle API errors
	handleError(error: HttpErrorResponse) {

		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${JSON.stringify(error.status)}, ` +
				`body was: ${JSON.stringify(error.message)}`);
		}
		// return an observable with a user-facing error message
		if (error.status == 0) {
			return throwError(
				Swal.fire(
					'The Internet?',
					'Please try again later',
					'question'
				)
			);
		}
	};

	// Invoice List
	invoices_list() {
		return this._callAPI('/invoices', this.data)
	}

	// Invoice Circuit List filtered by circuit id 
	invoice_circuit_list(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/invoice-circuit', data)
	}

	// Download invoice
	download_invoice(id) {
		return this._callAPI('/download-invoice/' + id, this.data)
	}

	// Get Invoices BY ID
	get_invoice(id) {
		return this._callAPI('/get-invoice-details/' + id, this.data)
	}

	list_invoice() {
		return this._callAPI('/get-invoice-details', this.data)
	}

	// Clients
	users() {
		return this._callAPI('/users', this.data)
	}

	add_invoice(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-invoice', data)
	}

	// For getting the number of circuit for the invoice
	get_invoiced_circuit(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/get-invoiced-circuit-details', data)
	}

	// Cancel Invoice API
	cancel_invoice(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/canceled-invoice', data)
	}

	// Invoice Canceled Reason API
	invoice_canceled_reason(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/get-invoice-cancellation-reason', data)
	}

	// Update Invoice API 
	update_invoice(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/update-existing-invoice', data)
	}

	// Account API
	// Add Account AOI
	add_bank_account(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-account', data)
	}

	// Account list API
	bank_accounts() {
		return this._callAPI('/accounts', this.data)
	}

	// Get account by ID
	get_bank_account(id) {
		return this._callAPI('/get-account/' + id, this.data)
	}

	// Pay BIlls API
	// Add Account AOI
	add_pay_bill(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-paybill', data)
	}

	// Get Pay Bill BY ID
	get_pay_bill(id) {
		return this._callAPI('/get-paybill-details/' + id, this.data)
	}

	// Download pay-bills
	download_pay_bills(id) {
		return this._callAPI('/download-paybill/' + id, this.data)
	}

	// reference id
	get_payment_reference() {
		return this._callAPI('/get-payment-reference', this.data)
	}

	// Account list API
	pay_bills(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/paybills', data)
	}

	// Get account by ID
	// download_pay_bill(id) {
	// 	return this._callAPI('//' + id, this.data)
	// }

	// Get Unpaid Invoices
	unpaid_invoices(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/unpaid-invoices', data)
	}

	// Cash movement list list API
	cash_movement_lst(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/cash-movements', data)
	}

	add_cash_movement(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-cash-movement', data)
	}

	// Download Statment
	download_statement(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/get-statements', data)
	}

	_callAPI(url: string, params?) {
		return new Promise((resolve, reject) => {
			this.http.post(END_POINT + url, params)
				.subscribe(res => {
					if (res['response'] === 'success') {
						resolve(res)
					} else {
						if (res['response_code'] == 405 || res['response_code'] == 403 || res['response_code'] == 204){
							localStorage.clear();
							return window.location.replace('/auth/signin');
						}
						reject(res['message'])
					}
				}, (this.handleError))
		})
	}

}
