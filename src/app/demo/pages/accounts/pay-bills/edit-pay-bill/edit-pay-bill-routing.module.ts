import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditPayBillComponent } from './edit-pay-bill.component';


const routes: Routes = [
  {
    path: '',
    component: EditPayBillComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditPayBillRoutingModule { }
