import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';
import { AccountsApiService } from '../../shared/accounts-api.service';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-pay-bill',
	templateUrl: './edit-pay-bill.component.html',
	styleUrls: ['./edit-pay-bill.component.scss']
})
export class EditPayBillComponent implements OnInit {

	edit_pay_bill_frm: FormGroup;
	current_date = (new Date()).toISOString().substring(0, 10)
	path_table: any = {}
	bank_name = [];
	users = [];
	unpaid_invoice_list = [];
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	unPaidInvoice: any = []

	paymentModeArr: any[] = [
		{ id: 1, name: 'Cash' },
		{ id: 2, name: 'Cheeque' },
		{ id: 3, name: 'Transfer' }
	];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService,
		private circuitApi: CircuitApiService,
		private accountsApi: AccountsApiService,
		private translate: TranslateService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.edit_pay_bill_frm = this.__fb.group({
			reglement_no: [{ value: '', disabled: true }, [Validators.required]],
			payment_date: [{ value: this.current_date, disabled: true }, [Validators.required]],
			payment_mode: ["", Validators.required],
			// ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			reference: ["", Validators.required],
			bank: ["", Validators.required],
			to_submit_on: ["", Validators.required],
			client: ["", Validators.required],
		});
		this.path_table = {
			"paging": false,
			"ordering": false,
			"info": false,
			"filter": false

		}
		this.get_reference();
		this.get_banks();
		this.getUsers();
	}

	// reference id
	get_reference() {
		this.accountsApi.get_payment_reference().then((response: any) => {
			console.log('Response:', response);
			this.edit_pay_bill_frm.get('reglement_no').setValue(response.reference);
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Staff Function Dropdown
	get_banks() {
		this.accountsApi.bank_accounts().then((response: any) => {
			console.log('Response:', response);
			this.bank_name = response.accounts
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	getUsers() {
		this.circuitApi.users().then((response: any) => {
			console.log('Response:', response);
			this.users = response.list_of_users
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

		// On client select display unpaid invoice List
		unpaidInvoiceList() {
			let form_data = new FormData();
			form_data.append('client_id', this.edit_pay_bill_frm.get('client').value);
			console.log(this.edit_pay_bill_frm.get('client').value)
			this.accountsApi.unpaid_invoices(form_data).then((response: any) => {
				console.log('Response:', response);
				this.unpaid_invoice_list = response.invoice
				if (this.isDtInitialized) {
					this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
						dtInstance.destroy();
						this.dtTrigger.next();
					});
				} else {
					this.isDtInitialized = true
					this.dtTrigger.next();
				}
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		
		}
	
		addRowIntoArray(event, unPaidInvoice){
			const index: number = this.unPaidInvoice.indexOf(unPaidInvoice);
			if(event.target.checked == true){
				this.unPaidInvoice.push(unPaidInvoice)
				console.log(this.unPaidInvoice)
			} else {
				this.unPaidInvoice.splice(index, 1);
				console.log(this.unPaidInvoice)
			}
		}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_pay_bill_frm.get(formControlName).errors &&
			(this.edit_pay_bill_frm.get(formControlName).touched ||
				this.edit_pay_bill_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_pay_bill_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_pay_bill_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_pay_bill_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_pay_bill_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save() {
		this.markFormGroupTouched(this.edit_pay_bill_frm);
		if (this.edit_pay_bill_frm.valid && this.unpaid_invoice_list.length !== 0) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('payment_reference', this.edit_pay_bill_frm.get('reglement_no').value);
			form_data.append('payment_date', this.edit_pay_bill_frm.get('payment_date').value);
			form_data.append('payment_mode', this.edit_pay_bill_frm.get('payment_mode').value);
			form_data.append('paid_amount', this.edit_pay_bill_frm.get('amount').value);
			form_data.append('reference', this.edit_pay_bill_frm.get('reference').value);
			form_data.append('account_id', this.edit_pay_bill_frm.get('bank').value);
			form_data.append('submitted_date', this.edit_pay_bill_frm.get('to_submit_on').value);
			form_data.append('client_id', this.edit_pay_bill_frm.get('client').value);
			form_data.append('invoice', JSON.stringify(this.unPaidInvoice));

			// 	this.accountsApi.add_pay_bill(form_data).then((response: any) => {
			// 		console.log('Response:', response);
			// 		Swal.fire({
			// 			type: 'success',
			// 			title: 'Success',
			// 			confirmButtonColor: '#323258',
			// 			// cancelButtonColor: '#ff1f1f',
			// 			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			// 			text: response.message
			// 		});
			// 		  this.router.navigate(['/accounts/list-pay-bill'])
			// 	})
			// 	.catch((error: any) => {
			// 		console.error(error);
			// 		Swal.fire({
			// 			type: 'error',
			// 			title: 'Error!',
			// 			text: error,
			// 			confirmButtonColor: '#323258',
			// 			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			// 		});
			// 	});	
			// } else {
			// 	var msg = ''
			// 	this.translate.get('Add_Pay_Bill.please select one or more unpaid invoice').subscribe(val => {
			// 		msg = val
			// 	})
			// 	// warning-->(type), Warning!--> (title), msg --> (text)
			// 	// return Swal.fire('Warning!', msg, 'warning');
			// 	return Swal.fire({
			// 		type: 'warning',
			// 		title: 'Warning!',
			// 		text: msg,
			// 		confirmButtonColor: '#323258',
			// 		confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			// 	});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
