import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditPayBillRoutingModule } from './edit-pay-bill-routing.module';
import { EditPayBillComponent } from './edit-pay-bill.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditPayBillRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditPayBillComponent]
})
export class EditPayBillModule { }
