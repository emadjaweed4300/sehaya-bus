import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPayBillComponent } from './edit-pay-bill.component';

describe('EditPayBillComponent', () => {
  let component: EditPayBillComponent;
  let fixture: ComponentFixture<EditPayBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPayBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPayBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
