import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewPayBillComponent } from './view-pay-bill.component';


const routes: Routes = [
  {
    path: '',
    component: ViewPayBillComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewPayBillRoutingModule { }
