import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPayBillComponent } from './view-pay-bill.component';

describe('ViewPayBillComponent', () => {
  let component: ViewPayBillComponent;
  let fixture: ComponentFixture<ViewPayBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPayBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPayBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
