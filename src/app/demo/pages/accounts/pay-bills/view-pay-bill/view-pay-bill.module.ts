import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewPayBillRoutingModule } from './view-pay-bill-routing.module';
import { ViewPayBillComponent } from './view-pay-bill.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ViewPayBillRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ViewPayBillComponent]
})
export class ViewPayBillModule { }
