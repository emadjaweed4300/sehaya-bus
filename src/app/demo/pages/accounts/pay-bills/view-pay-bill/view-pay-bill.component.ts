import { Component, OnInit } from '@angular/core';
import { AccountsApiService } from '../../shared/accounts-api.service';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-view-pay-bill',
	templateUrl: './view-pay-bill.component.html',
	styleUrls: ['./view-pay-bill.component.scss']
})
export class ViewPayBillComponent implements OnInit {

	id
	pay_bill_details: any;
	pay_bill_list: any = [];

	constructor(
		private __route: ActivatedRoute,
		private accountsApi: AccountsApiService
	) { 
		this.id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		this.get_pay_bill_details();
		this.list_pay_bill();
	}

	// Get All FeatureAPI
	get_pay_bill_details() {
		this.accountsApi.get_pay_bill(this.id).then((response: any) => {
			console.log('Response:', response);
			this.pay_bill_details = response.paybill;
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// List Data API
	list_pay_bill() {
		this.accountsApi.get_pay_bill(this.id).then((response: any) => {
			console.log('Response:', response);
			this.pay_bill_list = response.paybill.invoice;
		})
		.catch((error: any) => {
			console.error(error);
		});
	}


}
