import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddPayBillRoutingModule } from './add-pay-bill-routing.module';
import { AddPayBillComponent } from './add-pay-bill.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddPayBillRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddPayBillComponent]
})
export class AddPayBillModule { }
