import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPayBillComponent } from './add-pay-bill.component';

describe('AddPayBillComponent', () => {
  let component: AddPayBillComponent;
  let fixture: ComponentFixture<AddPayBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPayBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPayBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
