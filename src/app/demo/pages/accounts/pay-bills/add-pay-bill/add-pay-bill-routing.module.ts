import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPayBillComponent } from './add-pay-bill.component';


const routes: Routes = [
  {
    path: '',
    component: AddPayBillComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddPayBillRoutingModule { }
