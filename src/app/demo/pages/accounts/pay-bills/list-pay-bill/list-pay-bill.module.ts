import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListPayBillRoutingModule } from './list-pay-bill-routing.module';
import { ListPayBillComponent } from './list-pay-bill.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListPayBillRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListPayBillComponent]
})
export class ListPayBillModule { }
