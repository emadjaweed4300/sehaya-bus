import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPayBillComponent } from './list-pay-bill.component';

describe('ListPayBillComponent', () => {
  let component: ListPayBillComponent;
  let fixture: ComponentFixture<ListPayBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPayBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPayBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
