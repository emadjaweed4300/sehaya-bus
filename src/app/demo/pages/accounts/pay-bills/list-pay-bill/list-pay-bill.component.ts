import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { AccountsApiService } from '../../shared/accounts-api.service';
import { END_POINT } from 'src/app/constant';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';

@Component({
	selector: 'app-list-pay-bill',
	templateUrl: './list-pay-bill.component.html',
	styleUrls: ['./list-pay-bill.component.scss']
})
export class ListPayBillComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	pay_bills_list: any = [];
	bank_name = [];
	users= [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	dtExportButtonOptions: any = {};

	// for filter
	client_name = '';
	bank = '';
	submitted_date = '';
	// For clear value of filter
	clear(){
		this.client_name = '';
		this.bank = '';
		this.submitted_date = '';
	}

	constructor(
		private accountsApi: AccountsApiService,
		private circuitApi: CircuitApiService,
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				"<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			buttons: [
				{
					extend: 'copy',
					text: '<i class="fa fa-copy" style="color: white;"></i>',
					titleAttr: 'Copy',
					exportOptions: {
						columns: [0, 1, 2, 3, 4, 5]
					}
				},
				{
					extend: 'print',
					text: '<i class="fa fa-print" style="color: white;"></i>',
					titleAttr: 'Print',
					exportOptions: {
						columns: [0, 1, 2, 3, 4, 5]
					}
				},
				{
					extend: 'excel',
					text: '<i class="fa fa-file-excel" style="color: white;"></i>',
					titleAttr: 'Excel',
					exportOptions: {
						columns: [0, 1, 2, 3, 4, 5]
					}
				},
				{
					extend: 'csv',
					text: '<i class="fa fa-file-csv" style="color: white;"></i>',
					titleAttr: 'CSV',
					exportOptions: {
						columns: [0, 1, 2, 3, 4, 5]
					}
				}
			],
			responsive: true
		};
		this.list_pay_bills();
		this.get_banks();
		this.getUsers();
	}

	// Table Data not found message function
	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// Get Staff Function Dropdown
	get_banks() {
		this.accountsApi.bank_accounts().then((response: any) => {
			console.log('Response:', response);
			this.bank_name = response.accounts
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	getUsers(){
		this.circuitApi.users().then((response: any) => {
			console.log('Response:', response);
			this.users = response.list_of_users
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// List Data API
	list_pay_bills() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.accountsApi.pay_bills(formData).then((response: any) => {
			console.log('Response:', response);
			this.pay_bills_list = response.paybill
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
				  dtInstance.destroy();
				  this.dtTrigger.next();
				});
			  } else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			  }
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('client', this.client_name);
		formData.append('bank', this.bank);
		formData.append('submitted_date', this.submitted_date);
		formData.append('add_filter', '1');
		console.log(this.client_name);
		console.log(this.bank);
		console.log(this.submitted_date);
		this.accountsApi.pay_bills(formData).then((response: any) => {
			console.log('Response:', response);
			this.pay_bills_list = response.paybill
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.list_pay_bills()
	}

	// Download
	public downloadAction(id: number) {
		this.accountsApi.download_pay_bills(id).then((response: any) => {
			// END_POINT + Response URL
			console.log('Response:', response);
			let pdf = END_POINT + '/' + response.url;
			window.open(pdf, '_blank');
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// delete Pay Bill
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			// if (result.value) {
			// 	this.eventApi.delete_event_circuit(id).then((response: any) => {
			// 		console.log('Response:', response);
			// 		this.list_event_circuits();
			// 		Swal.fire({
			// 			type: 'success',
			// 			title: 'Deleted!',
			// 			text: response.message,
			// 			confirmButtonColor: '#323258',
			// 			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			// 		});

			// 	})
			// 		.catch((error: any) => {
			// 			console.error(error);
			// 			Swal.fire({
			// 		type: 'error',
			// 		title: 'Error!',
			// 		text: error,
			// 		confirmButtonColor: '#323258',
			// 		confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			// 	});
			// 		});
			// }
		})
	}

}
