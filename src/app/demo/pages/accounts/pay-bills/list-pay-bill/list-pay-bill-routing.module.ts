import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPayBillComponent } from './list-pay-bill.component';


const routes: Routes = [
  {
    path: '',
    component: ListPayBillComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListPayBillRoutingModule { }
