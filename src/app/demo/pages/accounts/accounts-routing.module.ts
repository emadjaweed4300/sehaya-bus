import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Receipt list,add,view,edit components
			{
				path: 'list-receipt',
				loadChildren: () => import('./receipt/list-receipt/list-receipt.module').then(module => module.ListReceiptModule)
			},
			{
				path: 'add-receipt',
				loadChildren: () => import('./receipt/add-receipt/add-receipt.module').then(module => module.AddReceiptModule)
			},
			{
				path: 'edit-receipt',
				loadChildren: () => import('./receipt/edit-receipt/edit-receipt.module').then(module => module.EditReceiptModule)
			},

			//  Invoice list,add,edit components
			{
				path: 'list-invoice',
				loadChildren: () => import('./invoice/list-invoice/list-invoice.module').then(module => module.ListInvoiceModule)
			},
			{
				path: 'add-invoice',
				loadChildren: () => import('./invoice/add-invoice/add-invoice.module').then(module => module.AddInvoiceModule)
			},
			// Passing invoice_id (id) and client_id (client_id) from invoice list to edit invoice
			{
				path: 'edit-invoice/:id/:client_id',
				loadChildren: () => import('./invoice/edit-invoice/edit-invoice.module').then(module => module.EditInvoiceModule)
			},
			{
				path: 'view-invoice/:id',
				loadChildren: () => import('./invoice/view-invoice/view-invoice.module').then(module => module.ViewInvoiceModule)
			}, 
			{
				path: 'view-invoice-circuit/:id',
				loadChildren: () => import('./invoice/view-invoice-circuit/view-invoice-circuit.module').then(module => module.ViewInvoiceCircuitModule)
			},

			// Statements list,add,edit components
			{
				path: 'list-statement',
				loadChildren: () => import('./statement/list-statement/list-statement.module').then(module => module.ListStatementModule)
			},

			// Pay bill list,add,edit components
			{
				path: 'add-pay-bill',
				loadChildren: () => import('./pay-bills/add-pay-bill/add-pay-bill.module').then(module => module.AddPayBillModule)
			},
			{
				path: 'list-pay-bill',
				loadChildren: () => import('./pay-bills/list-pay-bill/list-pay-bill.module').then(module => module.ListPayBillModule)
			},
			{
				path: 'view-pay-bill/:id',
				loadChildren: () => import('./pay-bills/view-pay-bill/view-pay-bill.module').then(module => module.ViewPayBillModule)
			},
			{
				path: 'edit-pay-bill/:id',
				loadChildren: () => import('./pay-bills/edit-pay-bill/edit-pay-bill.module').then(module => module.EditPayBillModule)
			},

			// Bank Account list,add,view components
			{
				path: 'add-bank-account',
				loadChildren: () => import('./bank-account/add-bank-account/add-bank-account.module').then(module => module.AddBankAccountModule)
			},
			{
				path: 'list-bank-account',
				loadChildren: () => import('./bank-account/list-bank-account/list-bank-account.module').then(module => module.ListBankAccountModule)
			},
			{
				path: 'view-bank-account/:id',
				loadChildren: () => import('./bank-account/view-bank-account/view-bank-account.module').then(module => module.ViewBankAccountModule)
			}

		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AccountsRoutingModule { }
