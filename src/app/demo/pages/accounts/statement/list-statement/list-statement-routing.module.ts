import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListStatementComponent } from './list-statement.component';

const routes: Routes = [
  {
    path: '',
    component: ListStatementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListStatementRoutingModule { }
