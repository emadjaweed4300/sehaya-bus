import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AccountsApiService } from '../../shared/accounts-api.service';
import Swal from 'sweetalert2';
import { END_POINT } from 'src/app/constant';

@Component({
	selector: 'app-list-statement',
	templateUrl: './list-statement.component.html',
	styleUrls: ['./list-statement.component.scss']
})
export class ListStatementComponent implements OnInit {

	filter_statement_frm: FormGroup
	date_from: string;
	date_to: string;
	users = [];

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();

	// for filter
	from_date = '';
	to_date = '';
	client_id = '';
	// For clear value of filter
	clear(){
		this.from_date = '';
		this.to_date = '';
		this.client_id = '';
	}

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private accountsApi: AccountsApiService,
		private translate: TranslateService
	) { }

	ngOnInit() {
		this.filter_statement_frm = this.__fb.group({
			date_from: ["", Validators.required],
			date_to: ["", Validators.required],
			client: ["", Validators.required],
		});
		this.getUsers();
	}

	getUsers() {
		this.accountsApi.users().then((response: any) => {
			console.log('Response:', response);
			this.users = response.list_of_users
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Download
	// public downloadAction(id: number) {
	// 	let formData = new FormData()
	// 	formData.append('from_date', this.from_date);
	// 	formData.append('to_date', this.to_date);
	// 	formData.append('client_id', this.client);
	// 	this.accountsApi.download_statement(id).then((response: any) => {
	// 		// END_POINT + Response URL 
	// 		console.log('Response:', response);
	// 		let pdf = END_POINT + '/' + response.url;
	// 		window.open(pdf, '_blank');
	// 	})
	// 		.catch((error: any) => {
	// 			console.error(error);
	// 			Swal.fire({
	// 	type: 'error',
	// 	title: 'Error!',
	// 	text: error,
	// 	confirmButtonColor: '#323258',
	// 	confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// });
	// 		});
	// }


	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.filter_statement_frm.get(formControlName).errors &&
			(this.filter_statement_frm.get(formControlName).touched ||
				this.filter_statement_frm.get(formControlName).dirty);
		if (errors) {
			required = this.filter_statement_frm.get(formControlName).errors
				.required;
			patternValidate = this.filter_statement_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.filter_statement_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.filter_statement_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */

	// Download Statment Function
 
	public downloadAction(): void {
		this.markFormGroupTouched(this.filter_statement_frm);
		if (this.filter_statement_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('from_date', this.filter_statement_frm.get('date_from').value);
			form_data.append('to_date', this.filter_statement_frm.get('date_to').value);
			form_data.append('client_id', this.filter_statement_frm.get('client').value);
			this.accountsApi.download_statement(form_data).then((response: any) => {
				// END_POINT + Response URL 
				console.log('Response:', response);
				let pdf = END_POINT + '/' + response.url;
				console.log(pdf)
				window.open(pdf, '_blank');
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	// Cancel Function on Filter
	public cancelAction() {
		
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
