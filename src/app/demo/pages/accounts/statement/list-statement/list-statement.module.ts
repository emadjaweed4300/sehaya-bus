import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListStatementRoutingModule } from './list-statement-routing.module';
import { ListStatementComponent } from './list-statement.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListStatementRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListStatementComponent]
})
export class ListStatementModule { }
