import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListBankAccountRoutingModule } from './list-bank-account-routing.module';
import { ListBankAccountComponent } from './list-bank-account.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListBankAccountRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListBankAccountComponent]
})
export class ListBankAccountModule { }
