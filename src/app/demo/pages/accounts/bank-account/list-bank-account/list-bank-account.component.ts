import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { AccountsApiService } from '../../shared/accounts-api.service';

@Component({
  selector: 'app-list-bank-account',
  templateUrl: './list-bank-account.component.html',
  styleUrls: ['./list-bank-account.component.scss']
})
export class ListBankAccountComponent implements OnInit {

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  accounts_list: any = [];
  isDtInitialized: boolean = false
  dtTrigger: Subject<any> = new Subject();
  dtExportButtonOptions: any = {};

  constructor(
    private router: Router,
		private accountsApi: AccountsApiService
  ) { }

  ngOnInit() {
		this.dtExportButtonOptions = {
	// 		ajax: 'fake-data/datatable-data.json',
	// 		columns: [
	// 			{
	// 				title: 'Id',
	// 				data: 'id'
	// 			},
	// 			{
	// 				title: 'Name',
	// 				data: 'name'
	// 			},
	// 			{
	// 				title: 'Office',
	// 				data: 'office'
	// 			},
	// 			{
	// 				title: 'Age',
	// 				data: 'age'
	// 			}, 
	// 			{
	// 				title: 'Start Date',
	// 				data: 'date'
	// 			}, 
	// 			{
	// 				title: 'Salary',
	// 				data: 'salary'
	// 			}
	// 		],
			dom: 
      // "<'row'<'col-sm-12'Brt>>" +
      "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
         "<'row'<'col-sm-12'tr>>" +
         "<'row'<'col-sm-5'i>>",
	// 		buttons: [
	// 			'copy',
	// 			'print',
	// 			'excel',
	// 			'csv'
	// 		],
			responsive: true
		};
		this.list_accounts()
	}

	// Table Data not found message function
	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// List Data API
	list_accounts() {
		this.accountsApi.bank_accounts().then((response: any) => {
			console.log('Response:', response);
			this.accounts_list = response.accounts
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
				  dtInstance.destroy();
				  this.dtTrigger.next();
				});
			  } else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			  }
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}

}
