import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListBankAccountComponent } from './list-bank-account.component';


const routes: Routes = [
  {
    path: '',
    component: ListBankAccountComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListBankAccountRoutingModule { }
