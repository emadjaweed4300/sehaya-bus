import { Component, OnInit } from '@angular/core';
import { AccountsApiService } from '../../shared/accounts-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-bank-account',
  templateUrl: './view-bank-account.component.html',
  styleUrls: ['./view-bank-account.component.scss']
})
export class ViewBankAccountComponent implements OnInit {

  account_id
	account_details: any;

  constructor(
    private router: Router,
		private __route: ActivatedRoute,
		private accountsApi: AccountsApiService,
  ) {
    this.account_id = this.__route.snapshot.paramMap.get('id');
   }

  ngOnInit() {
    this.get_account();
  }
  // Get Account by id API
	get_account(){
		this.accountsApi.get_bank_account(this.account_id).then((response: any) => {
			console.log('Response:', response);
			this.account_details = response.account;
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
        type: 'error',
        title: 'Error!',
        text: error,
        confirmButtonColor: '#323258',
        confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
      });
		});
	}

}
