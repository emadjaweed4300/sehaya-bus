import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewBankAccountRoutingModule } from './view-bank-account-routing.module';
import { ViewBankAccountComponent } from './view-bank-account.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ViewBankAccountRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ViewBankAccountComponent]
})
export class ViewBankAccountModule { }
