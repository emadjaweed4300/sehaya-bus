import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBankAccountComponent } from './view-bank-account.component';

describe('ViewBankAccountComponent', () => {
  let component: ViewBankAccountComponent;
  let fixture: ComponentFixture<ViewBankAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBankAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBankAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
