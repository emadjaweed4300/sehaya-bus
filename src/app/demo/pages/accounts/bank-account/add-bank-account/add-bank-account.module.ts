import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddBankAccountRoutingModule } from './add-bank-account-routing.module';
import { AddBankAccountComponent } from './add-bank-account.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddBankAccountRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddBankAccountComponent]
})
export class AddBankAccountModule { }
