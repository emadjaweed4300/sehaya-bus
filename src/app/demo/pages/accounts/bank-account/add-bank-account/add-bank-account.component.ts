import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountsApiService } from '../../shared/accounts-api.service';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-add-bank-account',
	templateUrl: './add-bank-account.component.html',
	styleUrls: ['./add-bank-account.component.scss']
})
export class AddBankAccountComponent implements OnInit {

	add_bank_account_frm: FormGroup;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private accountsApi: AccountsApiService,
		private translate: TranslateService
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_bank_account_frm = this.__fb.group({
			bank_name: ["", Validators.required],
			account_number: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			// <span class="invalid-feedback">{{ 'Add_Vehicle.Please provider this format (10.00),(100.00),(1000.00) etc' | translate }}.</span>
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_bank_account_frm.get(formControlName).errors &&
			(this.add_bank_account_frm.get(formControlName).touched ||
				this.add_bank_account_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_bank_account_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_bank_account_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_bank_account_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_bank_account_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_bank_account_frm);
		if (this.add_bank_account_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('bank_name', this.add_bank_account_frm.get('bank_name').value);
			form_data.append('account_number', this.add_bank_account_frm.get('account_number').value);
			form_data.append('amount', this.add_bank_account_frm.get('amount').value);

			this.accountsApi.add_bank_account(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				this.router.navigate(['/accounts/list-bank-account'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}


}
