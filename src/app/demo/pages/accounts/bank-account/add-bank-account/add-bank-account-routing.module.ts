import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBankAccountComponent } from './add-bank-account.component';


const routes: Routes = [
  {
    path: '',
    component: AddBankAccountComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddBankAccountRoutingModule { }
