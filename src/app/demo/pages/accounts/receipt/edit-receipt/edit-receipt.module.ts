import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditReceiptRoutingModule } from './edit-receipt-routing.module';
import { EditReceiptComponent } from './edit-receipt.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditReceiptRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditReceiptComponent]
})
export class EditReceiptModule { }
