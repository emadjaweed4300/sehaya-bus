import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
	selector: 'app-edit-receipt',
	templateUrl: './edit-receipt.component.html',
	styleUrls: ['./edit-receipt.component.scss']
})
export class EditReceiptComponent implements OnInit {

	edit_receipt_frm: FormGroup;

	constructor( 
		private __fb: FormBuilder,
		private router: Router,
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.edit_receipt_frm = this.__fb.group({
			mvt_code: ["", Validators.required],
			designation: ["", Validators.required],
			account_number: ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
			meaning: ["", Validators.required],
			active: ["", Validators.required],
		});
	}

	
	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let maxLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_receipt_frm.get(formControlName).errors &&
			(this.edit_receipt_frm.get(formControlName).touched ||
				this.edit_receipt_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_receipt_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_receipt_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_receipt_frm.get(formControlName)
				.errors.minlength;
			maxLengthValidate = this.edit_receipt_frm.get(formControlName)
				.errors.maxlength;
			notEquivalentValidate = this.edit_receipt_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			maxLengthValidate: maxLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

		/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
