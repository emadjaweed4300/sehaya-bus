import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { AccountsApiService } from '../../shared/accounts-api.service';

@Component({
	selector: 'app-list-receipt',
	templateUrl: './list-receipt.component.html',
	styleUrls: ['./list-receipt.component.scss']
})
export class ListReceiptComponent implements OnInit {

	@ViewChild(DataTableDirective, {static: false})
  	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	cities_list: any = [];
	isDtInitialized:boolean = false
	cash_movement = [];
	dtTrigger: Subject<any> = new Subject();

	// for filter
	meaning = '';
	active = '';
	// For clear value of filter
	clear(){
		this.meaning = '';
		this.active = '';
	}

	constructor(
		private accountsApi: AccountsApiService
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.get_all_cash_movement_list();
	}

	get_all_cash_movement_list(){
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.accountsApi.cash_movement_lst(formData).then((response: any) => {
			console.log('Response:', response);
			this.cash_movement = response.cash_movements
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('client', this.meaning);
		formData.append('bank', this.active);
		formData.append('add_filter', '1');
		console.log(this.meaning);
		console.log(this.active);
		this.accountsApi.cash_movement_lst(formData).then((response: any) => {
			console.log('Response:', response);
			this.cash_movement = response.cash_movements
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.get_all_cash_movement_list()
	}

	// Delete
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		})
	}

}
