import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountsApiService } from '../../shared/accounts-api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-add-receipt',
	templateUrl: './add-receipt.component.html',
	styleUrls: ['./add-receipt.component.scss']
})
export class AddReceiptComponent implements OnInit {

	add_receipt_frm: FormGroup;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private accountsApi: AccountsApiService,
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_receipt_frm = this.__fb.group({
			mvt_code: ["", [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]],
			designation: ["", Validators.required],
			account_number: ["", [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(12)]],
			meaning: ["", Validators.required],
			active: ["1", Validators.required],
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let maxLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_receipt_frm.get(formControlName).errors &&
			(this.add_receipt_frm.get(formControlName).touched ||
				this.add_receipt_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_receipt_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_receipt_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_receipt_frm.get(formControlName)
				.errors.minlength;
			maxLengthValidate = this.add_receipt_frm.get(formControlName)
				.errors.maxlength;
			notEquivalentValidate = this.add_receipt_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			maxLengthValidate: maxLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_receipt_frm);
		if (this.add_receipt_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('movement_code', this.add_receipt_frm.get('mvt_code').value);
			form_data.append('designation', this.add_receipt_frm.get('designation').value);
			form_data.append('accounting_account', this.add_receipt_frm.get('account_number').value);
			form_data.append('direction', this.add_receipt_frm.get('meaning').value);
			form_data.append('active', this.add_receipt_frm.get('active').value);

			
			this.accountsApi.add_cash_movement(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/accounts/list-receipt'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});	
			
	
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
