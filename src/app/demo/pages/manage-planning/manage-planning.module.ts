import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagePlanningRoutingModule } from './manage-planning-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ManagePlanningRoutingModule,
  ],
  declarations: []
})
export class ManagePlanningModule { }
