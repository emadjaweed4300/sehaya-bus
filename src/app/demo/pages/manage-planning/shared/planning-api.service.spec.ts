import { TestBed } from '@angular/core/testing';

import { PlanningApiService } from './planning-api.service';

describe('PlanningApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanningApiService = TestBed.get(PlanningApiService);
    expect(service).toBeTruthy();
  });
});
