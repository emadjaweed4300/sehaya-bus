import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { formatDate } from '@angular/common';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';


import { FormGroup, FormBuilder, Validators,  } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { VehicleApiService } from '../../manage-vehicle/shared/vehicle-api.service';
import { PlanningApiService } from '../shared/planning-api.service';
import { CircuitApiService } from '../../manage-circuit/shared/circuit-api.service';


@Component({
	selector: 'app-planning',
	templateUrl: './planning.component.html',
	styleUrls: ['./planning.component.scss']
})
export class PlanningComponent implements OnInit {

	@ViewChild('calendar', { static: false }) calendarComponent: FullCalendarComponent; // the #calendar in the template

	calendarVisible = true;
	calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
	calendarWeekends = true;

	public notSupported = false;

	dateObj = new Date();
	yearMonth = this.dateObj.getUTCFullYear() + '-' + (this.dateObj.getUTCMonth() + 1);

	calendarEvents: EventInput[] = [

		// *ngFor="let item of vehicle_planning_list;let i=index"

		
		// {
		// 	// icon: "feather icon-calendar",
		// 	// title: '',
		// 	// start: formatDate(this.yearMonth + '-10', 'yyyy-MM-dd', 'en-US'),
		// 	// end: formatDate(this.yearMonth + '-14', 'yyyy-MM-dd', 'en-US'),
		// 	// borderColor: '#a568ff',
		// 	// backgroundColor: '#a568ff',
		// 	// textColor: '#fff',
			
		// },

		// {
		// 	icon: "feather icon-calendar",
		// 	title: 'Audi Q7 MH-01 0001',
		// 	start: formatDate(this.yearMonth + '-10', 'yyyy-MM-dd', 'en-US'),
		// 	end: formatDate(this.yearMonth + '-14', 'yyyy-MM-dd', 'en-US'),
		// 	borderColor: '#a568ff',
		// 	backgroundColor: '#a568ff',
		// 	textColor: '#fff',
			
		// },
	];

	filter_planning_form: FormGroup
	vehilces = [];
	vehicle_planning_list: any = [];
	users= [];

	// clear(){
	// }

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private vehicleApi: VehicleApiService,
		private planningApi: PlanningApiService,
		private circuitApi: CircuitApiService,
	) {
		
	 }

	ngOnInit() {
		/** Form builder instance */
		this.filter_planning_form = this.__fb.group({
			vehicle: ['', [Validators.required]],
			// client: ['', [Validators.required]],
		});

		const isIE = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent);
		if (isIE) {
			this.notSupported = true;
		};
		this.get_vehicle();
		this.getUsers();
		// this.filterAction();
	}

	// Get Vehicle Dropdown
	get_vehicle() {
		let formData = new FormData()
		this.vehicleApi.list_vehicles(formData).then((response: any) => {
			console.log('Response:', response);
			this.vehilces = response.vehicles
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	getUsers(){
		this.circuitApi.users().then((response: any) => {
			console.log('Response:', response);
			this.users = response.list_of_users
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.filter_planning_form.get(formControlName).errors &&
			(this.filter_planning_form.get(formControlName).touched ||
				this.filter_planning_form.get(formControlName).dirty);
		if (errors) {
			required = this.filter_planning_form.get(formControlName).errors
				.required;
			patternValidate = this.filter_planning_form.get(formControlName).errors
				.pattern;
			minLengthValidate = this.filter_planning_form.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.filter_planning_form.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	// Download Statment Function
	public filterAction(): void {
		this.markFormGroupTouched(this.filter_planning_form);
		if (this.filter_planning_form.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('vehicle_id', this.filter_planning_form.get('vehicle').value);
			// form_data.append('client_id', this.filter_planning_form.get('client').value);
			this.planningApi.get_vehicle_planning(form_data).then((response: any) => {
				console.log('Response:', response);
				this.vehicle_planning_list = response.planned_vehicle;
				let planningData
				this.calendarEvents = [];
				for (let item of this.vehicle_planning_list){
					planningData = 	{
						icon: "feather icon-calendar",
						title: item.name +' '+ item.brand_name +' '+ item.registration ,
						// start: formatDate(this.yearMonth + '- 2020-08-15' , 'yyyy-MM-dd', 'en-US'),
						// end: formatDate(this.yearMonth + '- 2020-08-27' , 'yyyy-MM-dd', 'en-US'),
						start: formatDate( item.departure_month + '-' + item.departure_day , 'yyyy-MM-dd', 'en-US'),
						end: formatDate( item.arrival_month + '-' + item.arrival_day , 'yyyy-MM-dd', 'en-US'),
						borderColor: '#a568ff',
						backgroundColor: '#a568ff',
						textColor: '#fff',
					}
					this.calendarEvents.push(planningData);
					this.calendarEvents = [...this.calendarEvents]
					console.log(this.calendarEvents)
				}
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
				});
		}
	}

	// // Cancel Function on Filter
	// public cancelAction() {
	// 	this.clear();
	// }

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}


}
