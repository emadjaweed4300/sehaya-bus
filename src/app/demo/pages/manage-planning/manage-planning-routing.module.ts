import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Staff Memebers list,add,view,edit components
			{
				path: 'planning',
				loadChildren: () => import('./planning/planning.module').then(module => module.PlanningModule)
			},
		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManagePlanningRoutingModule { }
