import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global.service';
import { END_POINT } from 'src/app/constant';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class VehicleApiService {

	// API path
	//   base_path = 'http://nooridev.in/hapush/sehayabus';

	data = new FormData();
	constructor(
		private http: HttpClient,
		private translate: TranslateService,
		private global: GlobalService,
		private router: Router
	) {
		this.data.append('user_id', this.global.currentUser.id);
		this.data.append('user_role', this.global.currentUser.user_role);
		this.data.append('security_key', this.global.currentUser.token)
	}

	// Http Options
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	}

	// Handle API errors
	handleError(error: HttpErrorResponse) {

		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${JSON.stringify(error.status)}, ` +
				`body was: ${JSON.stringify(error.message)}`);
		}
		// return an observable with a user-facing error message
		if (error.status == 0) {
			return throwError(
				Swal.fire(
					'The Internet?',
					'Please try again later',
					'question'
				)
			);
		}
	};

	// Vehicle API's
	// Add Vehicle
	add_vehicle(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/add-vehicle', data)
	}

	// Vehicles
	// claim_circuits() {
	// 	return this._callAPI('/complaints', this.data)
	// }
	
	// list_vehicles() {
	// 	return this._callAPI('/vehicles', this.data)
	// }
	list_vehicles(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/vehicles', data)
	}
	// Vehicle
	delete_vehicle(id) {
		return this._callAPI('/delete-vehicle/' + id, this.data)
	}

	// // Status Active
	// claim_circuits_active(data) {
	// 	data.append('user_id', this.global.currentUser.id);
	// 	data.append('user_role', this.global.currentUser.user_role);
	// 	return this._callAPI('/staff-member-active', data)
	// }
	// // Status Deactive
	// claim_circuits_inactive(data) {
	// 	data.append('user_id', this.global.currentUser.id);
	// 	data.append('user_role', this.global.currentUser.user_role);
	// 	return this._callAPI('/staff-member-inactive', data)
	// }
	// Edit Vehicle
	edit_vehicle(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-vehicle', data)
	}

	// Edit Vehicle Feature
	edit_vehicle_feature(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-vehicle-feature', data)
	}

	// Edit Vehicle Deadline
	edit_vehicle_deadline(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/edit-vehicle-deadline', data)
	}

	// Get Vehicles
	get_vehicle(id) {
		return this._callAPI('/get-vehicle/' + id, this.data)
	}

	get_feature_by_id(id){
		return this._callAPI('/get-feature/' + id, this.data)
	}

	get_deadline_by_id(id) {
		return this._callAPI('/get-deadline/' + id, this.data)
	}

	// delete feature row 
	delete_feature_row(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/delete-existing-feature', data)
	}

	// delete deadline row 
	delete_deadline_row(data) {
		data.append('user_id', this.global.currentUser.id);
		data.append('user_role', this.global.currentUser.user_role);
		data.append('security_key', this.global.currentUser.token)
		return this._callAPI('/delete-existing-deadline', data)
	}


	_callAPI(url: string, params?) {
		return new Promise((resolve, reject) => {
			this.http.post(END_POINT + url, params)
				.subscribe(res => {
					if (res['response'] === 'success') {
						resolve(res)
					} else {
						if (res['response_code'] == 405 || res['response_code'] == 403 || res['response_code'] == 204){
							localStorage.clear();
							return window.location.replace('/auth/signin');
						}
						reject(res['message'])
					}
				}, (this.handleError))
		})
	}
}
