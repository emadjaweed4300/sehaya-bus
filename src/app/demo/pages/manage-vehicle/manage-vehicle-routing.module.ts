import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// vehicle list,add,view,edit components
			{
				path: 'list-vehicle',
				loadChildren: () => import('./vehicle/list-vehicle/list-vehicle.module').then(module => module.ListVehicleModule)
			},
			{
				path: 'add-vehicle',
				loadChildren: () => import('./vehicle/add-vehicle/add-vehicle.module').then(module => module.AddVehicleModule)
			},
			{
				path: 'view-vehicle/:id',
				loadChildren: () => import('./vehicle/view-vehicle/view-vehicle.module').then(module => module.ViewVehicleModule)
			},
			{
				path: 'edit-vehicle/:id',
				loadChildren: () => import('./vehicle/edit-vehicle/edit-vehicle.module').then(module => module.EditVehicleModule)
			},

		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManageVehicleRoutingModule { }
