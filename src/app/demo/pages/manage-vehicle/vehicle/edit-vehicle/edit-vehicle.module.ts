import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditVehicleRoutingModule } from './edit-vehicle-routing.module';
import { EditVehicleComponent } from './edit-vehicle.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditVehicleRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditVehicleComponent]
})
export class EditVehicleModule { }
