import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { VehicleApiService } from '../../shared/vehicle-api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-edit-vehicle',
	templateUrl: './edit-vehicle.component.html',
	styleUrls: ['./edit-vehicle.component.scss']
})
export class EditVehicleComponent implements OnInit {

	edit_vehicle_frm: FormGroup;

	vehicle_id
	vehicle: any

	vehicle_categorys = [];
	brands = [];
	Vehicle_Providers = [];
	features = [];
	deadlines = [];
	feature: any;
	deadline: any;

	// checkboxData = [
	// 	{ id: 100, name: 'wifi', isDisabled: true },
	// 	{ id: 200, name: 'AC', isDisabled: true },
	// 	{ id: 300, name: 'Online Payment', isDisabled: true },
	// 	// { id: 400, name: 'Sound System' }
	// ];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService,
		private vehicleApi: VehicleApiService,
	) {
		this.vehicle_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		/** Form builder instance */
		this.edit_vehicle_frm = this.__fb.group({
			vehicle_category: ["", Validators.required],
			vehicle_name: ["", Validators.required],
			// registration_number: ["", Validators.required],
			registration_number: ["", [Validators.required]],
			acquisition_date: ["", Validators.required],
			// acquisition_cost: ["", Validators.required],
			acquisition_cost:['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			provider: ["", Validators.required],
			brand: ["", Validators.required],
			capacity: ["", Validators.required],
			file_number: ['', [Validators.required]],
			authorization_number: ['', [Validators.required]],
			features: this.__fb.array([
				// this.getFeatures()
			]),
			deadlines: this.__fb.array([
				// this.getDeadlines()
			]),
			description: ["", Validators.required],
			// nb_month: ["", Validators.required],
			// nb_killometer: ["", Validators.required],
		});
		this.get_Vehicle_Category();
		this.get_Brand_Name();
		this.get_Vehicle_Provider();
		this.get_Features();
		this.get_Deadlines();
		this.get_vehicle_by_id();
	}

	getFeatures() {
		return this.__fb.group({
			// mapping_feature_id: ["0"],
			id: ["0"], 
			unit_id: [""],
			feature_id: ["", Validators.required],
			quantity: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			unit: ["", Validators.required],
		})
	}

	getDeadlines() {
		return this.__fb.group({
			id: ["0"],
			deadline_id: ["", Validators.required],
			value: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			deadline_type: [''],
			// type: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			type: [""],
			organization: ["", Validators.required],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
		})
	}

	addFeatureRow() {
		const control = <FormArray>this.edit_vehicle_frm.controls['features'];
		control.push(this.getFeatures());
		return true;
	}

	deleteFeatureRow(index, id) {
		const control = <FormArray>this.edit_vehicle_frm.controls['features'];
		if (control.length == 1) {
		} 
		else {
				Swal.fire({
					title: 'Are you sure?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#323258',
					cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
				}).then(result => {
					if (result.value) {
						let form_data = new FormData();
						form_data.append('vehicle_id', this.vehicle_id);
						form_data.append('feature_mapping_id', id);
						console.log(form_data.get('vehicle_id'),form_data.get('feature_mapping_id'))
						this.vehicleApi.delete_feature_row(form_data).then((response: any) => {
							console.log('Response:', response);
							control.removeAt(index);
							Swal.fire({
								type: 'success',
								title: 'Deleted!',
								text: response.message,
								confirmButtonColor: '#323258',
								confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
							});
		
						})
						.catch((error: any) => {
							console.error(error);
							Swal.fire({
								type: 'error',
								title: 'Error!',
								text: error,
								confirmButtonColor: '#323258',
								confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
							});
						});
					}
				})
			}
	}


	addDeadlineRow() {
		const control = <FormArray>this.edit_vehicle_frm.controls['deadlines'];
		control.push(this.getDeadlines());
		return true;
	}

	deleteDeadlineRow(index, id) {
		const control = <FormArray>this.edit_vehicle_frm.controls['deadlines'];
		if (control.length == 1) {

		} 
		else {
			Swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#323258',
				cancelButtonColor: '#ff1f1f',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
				cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
			}).then(result => {
				if (result.value) {
					let form_data = new FormData();
					form_data.append('vehicle_id', this.vehicle_id);
					form_data.append('deadline_mapping_id', id);
					console.log(form_data.get('vehicle_id'),form_data.get('deadline_mapping_id'))
					this.vehicleApi.delete_deadline_row(form_data).then((response: any) => {
						console.log('Response:', response);
						control.removeAt(index);
						Swal.fire({
							type: 'success',
							title: 'Deleted!',
							text: response.message,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
	
					})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
							type: 'error',
							title: 'Error!',
							text: error,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
					});
				}
			})
		}
	}

	// Get Vehicle Category Dropdown
	get_Vehicle_Category() {
		this.masterApi.vehicle_categorys().then((response: any) => {
			console.log('Response:', response);
			this.vehicle_categorys = response.vehicle_category
		})
		.catch((error: any) => {
			console.error(error);
		});
	}


	// Get Brand Name Dropdown
	get_Brand_Name() {
		this.masterApi.brands().then((response: any) => {
			console.log('Response:', response);
			this.brands = response.brands
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Vehicle Provider Dropdown
	get_Vehicle_Provider() {
		this.masterApi.vehicle_providers().then((response: any) => {
			console.log('Response:', response);
			this.Vehicle_Providers = response.providers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Features Dropdown
	get_Features() {
		this.masterApi.features().then((response: any) => {
			console.log('Response:', response);
			this.features = response.features
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Deadline Dropdown
	get_Deadlines() {
		this.masterApi.list_deadline().then((response: any) => {
			console.log('Response:', response);
			this.deadlines = response.deadlines
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Vehicle Details by ID
	get_vehicle_by_id() {
		this.vehicleApi.get_vehicle(this.vehicle_id).then((response: any) => {
			console.log('Response:', response);
			this.vehicle = response.vehicle;
			this.edit_vehicle_frm.get('vehicle_category').setValue(response.vehicle.details.vehicle_category);
			this.edit_vehicle_frm.get('vehicle_name').setValue(response.vehicle.details.name);
			this.edit_vehicle_frm.get('registration_number').setValue(response.vehicle.details.registration);
			// this.edit_vehicle_frm.get('designation').setValue(response.vehicle.details.designation);
			this.edit_vehicle_frm.get('provider').setValue(response.vehicle.details.provider);
			this.edit_vehicle_frm.get('brand').setValue(response.vehicle.details.vehicle_brand);
			this.edit_vehicle_frm.get('capacity').setValue(response.vehicle.details.capacity);
			this.edit_vehicle_frm.get('acquisition_date').setValue(response.vehicle.details.acquisition_date);
			this.edit_vehicle_frm.get('acquisition_cost').setValue(response.vehicle.details.acquisition_cost);
			this.edit_vehicle_frm.get('file_number').setValue(response.vehicle.details.file_number);
			this.edit_vehicle_frm.get('authorization_number').setValue(response.vehicle.details.autorisation_number);
			this.edit_vehicle_frm.get('description').setValue(response.vehicle.details.discription);
			// Array Data (Feature)
			const control1 = <FormArray>this.edit_vehicle_frm.controls['features'];
			for (let item of this.vehicle.feature) {
				// item.id ---> (id is parameter Name (API))
				control1.push(this.patchValuesFeature(item.id, item.feature_id, item.quantity, item.unit, item.unit_id))
			}

			// Array Data (Deadline)
			const control2 = <FormArray>this.edit_vehicle_frm.controls['deadlines'];
			for (let item of this.vehicle.deadline) {
				// item.deadline_value ---> (deadline_value is parameter Name (API))
				control2.push(this.patchValuesDeadline(item.id, item.deadline_id, item.deadline_value, item.deadline_type, item.organization, item.amount, item.type))
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// on feature select automatically fill the Unit value
	featureChanged(feature_id, unit, unit_id) {
		if (feature_id !== '') {
			this.vehicleApi.get_feature_by_id(feature_id).then((response: any) => {
				console.log('Response:', response);
				this.feature = response.feature.unit;
				unit.setValue(response.feature.unit);
				unit_id.setValue(response.feature.unit_id);
			})
		}
	}

	// on deadline select automatically fill the type value (Month/KM)
	// deadlineChanged(deadline_id, type, type_id) {
	// 	if (deadline_id !== '') {
	// 		this.vehicleApi.get_deadline_by_id(deadline_id).then((response: any) => {
	// 			console.log('Response:', response);
	// 			this.deadline = response.deadline.deadline_type;
	// 			type.setValue(response.deadline.deadline_type);
	// 			type_id.setValue(response.deadline.type_id);
	// 		})
	// 	}
	// }
	deadlineChanged(deadline_id, deadline_type, type) {
		if (deadline_id !== '') {
			this.vehicleApi.get_deadline_by_id(deadline_id).then((response: any) => {
				console.log('Response:', response);
				this.deadline = response.deadline.deadline_type;
				deadline_type.setValue(response.deadline.type);
				type.setValue(response.deadline.deadline_type);
			})
		}
	}

	// Patch Value for feature Table
	// id ---> (Parameter Name (API))
	patchValuesFeature(id, feature_id, quantity, unit, unit_id) {
		return this.__fb.group({
			// id --> ( Declear by developer) [id] ---> (Parameter Name (API))
			id: [id],
			feature_id: [feature_id],
			quantity: [quantity],
			// unit: [{ value: unit, disabled: true }],
			unit: [unit],
			unit_id: [unit_id]
		})
	}

	// Patch Value for deadline Table
	// deadline_value ---> (Parameter Name (API))
	patchValuesDeadline(id, deadline_id, deadline_value, deadline_type, organization, amount, type) {
		return this.__fb.group({
			// value --> ( Declear by developer) [deadline_value] ---> (Parameter Name (API))
			id: [id],
			deadline_id: [deadline_id],
			value: [deadline_value],
			deadline_type: [type],
			organization: [organization],
			amount: [amount],
			type: [deadline_type],
			// type: [{ value: deadline_type, disabled: true }],
		})
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_vehicle_frm.get(formControlName).errors &&
			(this.edit_vehicle_frm.get(formControlName).touched ||
				this.edit_vehicle_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_vehicle_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_vehicle_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_vehicle_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_vehicle_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_vehicle_frm);
		if (this.edit_vehicle_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('vehicle_id', this.vehicle_id);
			form_data.append('vehicle_category', this.edit_vehicle_frm.get('vehicle_category').value);
			form_data.append('vehicle_name', this.edit_vehicle_frm.get('vehicle_name').value);
			form_data.append('registration', this.edit_vehicle_frm.get('registration_number').value);
			form_data.append('designation', null);
			form_data.append('provider_id', this.edit_vehicle_frm.get('provider').value);
			form_data.append('vehicle_brand', this.edit_vehicle_frm.get('brand').value);
			form_data.append('capacity', this.edit_vehicle_frm.get('capacity').value);
			form_data.append('acquisition_date', this.edit_vehicle_frm.get('acquisition_date').value);
			form_data.append('acquisition_cost', this.edit_vehicle_frm.get('acquisition_cost').value);
			form_data.append('file_number', this.edit_vehicle_frm.get('file_number').value);
			form_data.append('authorization_number', this.edit_vehicle_frm.get('authorization_number').value);
			form_data.append('discription', this.edit_vehicle_frm.get('description').value);
			// Edit API
			this.vehicleApi.edit_vehicle(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/manage-vehicle/list-vehicle'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});

		}
	}

	// Edit funtion of Features
	public editFeature(): void {
		console.log(this.edit_vehicle_frm.get('features').value);
		this.markFormGroupTouched(this.edit_vehicle_frm);
		if (this.edit_vehicle_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// form_data.append('id', id);
			// form_data.append('feature_id', feature_id);
			// form_data.append('quantity', quantity);
			// form_data.append('unit_id', unit_id);
			// form_data.append('unit', unit);
			form_data.append('vehicle_id', this.vehicle_id);
			form_data.append('features', JSON.stringify(this.edit_vehicle_frm.get('features').value));
			// Edit API
			this.vehicleApi.edit_vehicle_feature(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/manage-vehicle/list-vehicle'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});

		}
	}

	// Edit funtion of Deadline
	public editDeadline(): void {
		console.log(this.edit_vehicle_frm.get('deadlines').value); 
		this.markFormGroupTouched(this.edit_vehicle_frm);
		if (this.edit_vehicle_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// form_data.append('id', id);
			// form_data.append('deadline_id', deadline_id);
			// form_data.append('value', value);
			// form_data.append('organization', organization);
			// form_data.append('amount', amount);
			// form_data.append('deadline_type', deadline_type);
			// form_data.append('type', type);
			form_data.append('vehicle_id', this.vehicle_id);
			form_data.append('deadlines', JSON.stringify(this.edit_vehicle_frm.get('deadlines').value));
			// Edit API
			this.vehicleApi.edit_vehicle_deadline(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/manage-vehicle/list-vehicle'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});

		}
	}


	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

	// // Check Box function
	// triggerSomeEvent(id: any, isDisabled) {
	// 	console.log(this.checkboxData[id].isDisabled);
	// 	this.checkboxData[id].isDisabled = !isDisabled;
	//     return;
	// }

}
