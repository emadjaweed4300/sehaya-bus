import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { VehicleApiService } from '../../shared/vehicle-api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-add-vehicle',
	templateUrl: './add-vehicle.component.html',
	styleUrls: ['./add-vehicle.component.scss']
})
export class AddVehicleComponent implements OnInit {

	add_vehicle_frm: FormGroup;

	vehicle_categorys = [];
	brands = [];
	Vehicle_Providers = [];
	features = [];
	deadlines = [];
	feature: any;
	deadline: any;

	// checkboxData = [
	// 	{ id: 100, name: 'wifi', isDisabled: true },
	// 	{ id: 200, name: 'AC', isDisabled: true },
	// 	{ id: 300, name: 'Online Payment', isDisabled: true },
	// 	// { id: 400, name: 'Sound System' }
	// ];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private masterApi: MasterApiService,
		private vehicleApi: VehicleApiService,
	) { }

	ngOnInit() {
		/** Form builder instance */
		this.add_vehicle_frm = this.__fb.group({
			vehicle_category: ["", Validators.required],
			vehicle_name: ["", Validators.required],
			// registration_number: ["", Validators.required],
			registration_number: ["", [Validators.required]],
			acquisition_date: ["", Validators.required],
			// acquisition_cost: ["", Validators.required],
			acquisition_cost:['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
			provider: ["", Validators.required], 
			brand: ["", Validators.required],
			capacity: ["", Validators.required],
			file_number: ['', [Validators.required]],
			authorization_number: ['', [Validators.required]],
			features: this.__fb.array([
				this.getFeatures()
			]),
			deadlines: this.__fb.array([
				this.getDeadlines()
			]),
			description: ["", Validators.required],
			// nb_month: ["", Validators.required],
			// nb_killometer: ["", Validators.required],
		});
		this.get_Vehicle_Category();
		this.get_Brand_Name();
		this.get_Vehicle_Provider();
		this.get_Features();
		this.get_Deadlines();
	}

	getFeatures() { 
		return this.__fb.group({
			feature_id: ["", Validators.required],
			quantity: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			unit: ["", Validators.required],
			// unit: [{ value: "", disabled: true }, Validators.required],
			unit_id: [""],
		})
	}


	getDeadlines() {
		return this.__fb.group({
			deadline_id: ["", Validators.required],
			value: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
			deadline_type: ["", Validators.required],
			// deadline_type: [{ value: "", disabled: true }, Validators.required],
			type: [""],
			organization: ["", Validators.required],
			amount: ['', [Validators.required, Validators.pattern('^(?!0,?\d)([0-9]{2}[0-9]{0,}(\.[0-9]{2}))$')]],
		})
	}

	addFeatureRow() {
		const control = <FormArray>this.add_vehicle_frm.controls['features'];
		control.push(this.getFeatures());
		return true;
	}

	deleteFeatureRow(index) {
		const control = <FormArray>this.add_vehicle_frm.controls['features'];
		if (control.length == 1) {

		} else {
			control.removeAt(index);
		}
	}


	addDeadlineRow() {
		const control = <FormArray>this.add_vehicle_frm.controls['deadlines'];
		control.push(this.getDeadlines());
		return true;
	}

	deleteDeadlineRow(index) {
		const control = <FormArray>this.add_vehicle_frm.controls['deadlines'];
		if (control.length == 1) {

		} else {
			control.removeAt(index);
		}
	}

	// Get Vehicle Category Dropdown
	get_Vehicle_Category() {
		this.masterApi.vehicle_categorys().then((response: any) => {
			console.log('Response:', response);
			this.vehicle_categorys = response.vehicle_category
		})
		.catch((error: any) => {
			console.error(error);
		});
	}


	// Get Brand Name Dropdown
	get_Brand_Name() {
		this.masterApi.brands().then((response: any) => {
			console.log('Response:', response);
			this.brands = response.brands
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Vehicle Provider Dropdown
	get_Vehicle_Provider() {
		this.masterApi.vehicle_providers().then((response: any) => {
			console.log('Response:', response);
			this.Vehicle_Providers = response.providers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Features Dropdown
	get_Features() {
		this.masterApi.features().then((response: any) => {
			console.log('Response:', response);
			this.features = response.features
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Deadline Dropdown
	get_Deadlines() {
		this.masterApi.list_deadline().then((response: any) => {
			console.log('Response:', response);
			this.deadlines = response.deadlines
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// on feature select automatically fill the Unit value
	featureChanged(feature_id, unit, unit_id) {
		if (feature_id !== '') {
			this.vehicleApi.get_feature_by_id(feature_id).then((response: any) => {
				console.log('Response:', response);
				this.feature = response.feature.unit;
				unit.setValue(response.feature.unit);
				unit_id.setValue(response.feature.unit_id);
			})
		}
		console.log(unit)
	}

	// on deadline select automatically fill the type value (Month/KM)
	deadlineChanged(deadline_id, deadline_type, type) {
		if (deadline_id !== '') {
			this.vehicleApi.get_deadline_by_id(deadline_id).then((response: any) => {
				console.log('Response:', response);
				this.deadline = response.deadline.deadline_type;
				deadline_type.setValue(response.deadline.deadline_type);
				type.setValue(response.deadline.type);
			})
		}
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.add_vehicle_frm.get(formControlName).errors &&
			(this.add_vehicle_frm.get(formControlName).touched ||
				this.add_vehicle_frm.get(formControlName).dirty);
		if (errors) {
			required = this.add_vehicle_frm.get(formControlName).errors
				.required;
			patternValidate = this.add_vehicle_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.add_vehicle_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.add_vehicle_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call add service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.add_vehicle_frm);
		if (this.add_vehicle_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			form_data.append('vehicle_category', this.add_vehicle_frm.get('vehicle_category').value);
			form_data.append('vehicle_name', this.add_vehicle_frm.get('vehicle_name').value);
			form_data.append('registration', this.add_vehicle_frm.get('registration_number').value);
			form_data.append('designation', null);
			form_data.append('provider_id', this.add_vehicle_frm.get('provider').value);
			form_data.append('vehicle_brand', this.add_vehicle_frm.get('brand').value);
			form_data.append('capacity', this.add_vehicle_frm.get('capacity').value);
			form_data.append('acquisition_date', this.add_vehicle_frm.get('acquisition_date').value);
			form_data.append('acquisition_cost', this.add_vehicle_frm.get('acquisition_cost').value);
			form_data.append('file_number', this.add_vehicle_frm.get('file_number').value);
			form_data.append('authorization_number', this.add_vehicle_frm.get('authorization_number').value);
			form_data.append('discription', this.add_vehicle_frm.get('description').value);
			// feature_id (API Array parameter Name) & features (Array formControlName)
			form_data.append('features', JSON.stringify(this.add_vehicle_frm.get('features').value));
			console.log(this.add_vehicle_frm.get('features').value)
			form_data.append('deadlines', JSON.stringify(this.add_vehicle_frm.get('deadlines').value));
			// Add API
			this.vehicleApi.add_vehicle(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				this.router.navigate(['/manage-vehicle/list-vehicle'])
			})
				.catch((error: any) => {
					console.error(error);
					Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
				});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

	// Check Box function
	// triggerSomeEvent(id: any, isDisabled) {
	// 	console.log(this.checkboxData[id].isDisabled);
	// 	this.checkboxData[id].isDisabled = !isDisabled;
	//     return;
	// }

}
