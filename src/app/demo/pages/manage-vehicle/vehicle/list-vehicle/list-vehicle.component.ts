import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { VehicleApiService } from '../../shared/vehicle-api.service';

@Component({
	selector: 'app-list-vehicle',
	templateUrl: './list-vehicle.component.html',
	styleUrls: ['./list-vehicle.component.scss']
})
export class ListVehicleComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	vehicleList: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	vehicle_categorys = [];
	brands = [];
	vehicle_carriers = [];

	// for filter
	vehicle_category = '';
	vehicle_brand = '';
	capacity = '';
	vehicle_carrier = '';
	// For clear value of filter
	clear(){
		this.vehicle_category = '';
		this.vehicle_brand = '';
		this.capacity = '';
		this.vehicle_carrier = '';
	}

	constructor(
		private masterApi: MasterApiService,
		private vehicleApi: VehicleApiService,
	) { }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				"<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			buttons: [
				{
					extend: 'copy',
					text: '<i class="fa fa-copy" style="color: white;"></i>',
					titleAttr: 'Copy',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				},
				{
					extend: 'print',
					text: '<i class="fa fa-print" style="color: white;"></i>',
					titleAttr: 'Print',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				},
				{
					extend: 'excel',
					text: '<i class="fa fa-file-excel" style="color: white;"></i>',
					titleAttr: 'Excel',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				},
				{
					extend: 'csv',
					text: '<i class="fa fa-file-csv" style="color: white;"></i>',
					titleAttr: 'CSV',
					exportOptions: {
						columns: [0, 1, 2, 3, 4]
					}
				}
			],
			responsive: true
		};
		this.list_vehicle();
		this.get_Brand_Name();
		this.get_Vehicle_Category();
		this.get_vehicle_carrier();
	}

	// Get Vehicle Category Dropdown
	get_Vehicle_Category() {
		this.masterApi.vehicle_categorys().then((response: any) => {
			console.log('Response:', response);
			this.vehicle_categorys = response.vehicle_category
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Brand Name Dropdown
	get_Brand_Name() {
		this.masterApi.brands().then((response: any) => {
			console.log('Response:', response);
			this.brands = response.brands
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Vehicle Carrier (Provider) Dropdown
	get_vehicle_carrier() {
		this.masterApi.vehicle_providers().then((response: any) => {
			console.log('Response:', response);
			this.vehicle_carriers = response.providers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	list_vehicle() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.vehicleApi.list_vehicles(formData).then((response: any) => {
			console.log('Response:', response);
			this.vehicleList = response.vehicles
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}


	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('category', this.vehicle_category);
		formData.append('brand', this.vehicle_brand);
		formData.append('carrier', this.vehicle_carrier);
		formData.append('add_filter', '1');
		console.log(this.vehicle_category);
		console.log(this.vehicle_brand);
		console.log(this.vehicle_carrier);
		this.vehicleApi.list_vehicles(formData).then((response: any) => {
			console.log('Response:', response);
			this.vehicleList = response.vehicles
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.list_vehicle()
	}


	// delete Brand
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.vehicleApi.delete_vehicle(id).then((response: any) => {
					console.log('Response:', response);
					this.list_vehicle();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
					});
			}
		})
	}

}
