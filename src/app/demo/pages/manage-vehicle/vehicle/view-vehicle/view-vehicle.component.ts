import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { VehicleApiService } from '../../shared/vehicle-api.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-view-vehicle',
	templateUrl: './view-vehicle.component.html',
	styleUrls: ['./view-vehicle.component.scss']
})
export class ViewVehicleComponent implements OnInit {

	vehicle_id
	vehicle_details: any;
	vehicle_deadline_list: any = [];
	vehicle_feature_list: any = [];


	constructor(
		private router: Router,
		private __route: ActivatedRoute,
		private vehicleApi: VehicleApiService,
	) {
		this.vehicle_id = this.__route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		this.get_vehicle();
		this.list_vehicle_deadline();
		this.list_vehicle_feature();
	}

	// Get All FeatureAPI
	get_vehicle(){
		this.vehicleApi.get_vehicle(this.vehicle_id).then((response: any) => {
			console.log('Response:', response);
			this.vehicle_details = response.vehicle;
		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
		});
	}

	// List Data API
	list_vehicle_deadline() {
		this.vehicleApi.get_vehicle(this.vehicle_id).then((response: any) => {
			console.log('Response:', response);
			this.vehicle_deadline_list = response.vehicle.deadline;
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// List Data API
	list_vehicle_feature() {
		this.vehicleApi.get_vehicle(this.vehicle_id).then((response: any) => {
			console.log('Response:', response);
			this.vehicle_feature_list = response.vehicle.feature;
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

}
