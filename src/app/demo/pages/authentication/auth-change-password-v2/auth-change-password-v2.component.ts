import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
	selector: 'app-auth-change-password-v2',
	templateUrl: './auth-change-password-v2.component.html',
	styleUrls: ['./auth-change-password-v2.component.scss']
})
export class AuthChangePasswordV2Component implements OnInit {

	change_Password_Form: FormGroup;

	constructor(
		private formBuilder: FormBuilder
	) { }

	ngOnInit() {
		this.change_Password_Form = this.formBuilder.group({
			current_password: [null, [Validators.required]],
			new_password: [null, [Validators.required]],
			confirm_password: [null, [Validators.required, (control) => this.Match(control, this.change_Password_Form, 'new_password')]],
		});
	}

	// Passwrod Match Function
	Match(fc: FormControl, za: FormGroup, zb: string) {
		if (fc.value === null) {
			return (null)
		}
		if (fc.value !== za.get(zb).value) {
			return { Match: true }
		} else {
			return (null)
		}
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minlengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.change_Password_Form.get(formControlName).errors &&
			(this.change_Password_Form.get(formControlName).touched ||
				this.change_Password_Form.get(formControlName).dirty);
		if (errors) {
			required = this.change_Password_Form.get(formControlName).errors
				.required;
			patternValidate = this.change_Password_Form.get(formControlName).errors
				.pattern;
			minlengthValidate = this.change_Password_Form.get(formControlName)
				.errors.minLength;
			notEquivalentValidate = this.change_Password_Form.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minlengthValidate: minlengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	changePassword() {
		this.markFormGroupTouched(this.change_Password_Form);
		if (this.change_Password_Form.valid) {
		}
	}

	/**
	   * Marks all controls in a form group as touched
	   * @param formGroup The form group to touch
	   */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
