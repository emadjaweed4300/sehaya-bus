import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthChangePasswordV2RoutingModule } from './auth-change-password-v2-routing.module';
import { AuthChangePasswordV2Component } from './auth-change-password-v2.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AuthChangePasswordV2Component],
  imports: [
    CommonModule,
    AuthChangePasswordV2RoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AuthChangePasswordV2Module { }
