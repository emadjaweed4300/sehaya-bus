import { TestBed } from '@angular/core/testing';

import { AuthenticationApiServiceService } from './authentication-api-service.service';

describe('AuthenticationApiServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthenticationApiServiceService = TestBed.get(AuthenticationApiServiceService);
    expect(service).toBeTruthy();
  });
});
