import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { END_POINT } from '../../../../constant';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';


@Injectable({
	providedIn: 'root'
})
export class AuthenticationApiServiceService {
	private _global: any;

	constructor(
		private http: HttpClient
	) { }

	_getUserId() {
		return { UserID: this._global.currentUser.UserID }
	}

	getLanguage() {
		return { lang: this._global.selectedLanguage }
	}

	CT_login(data) {
		return this._callAPI('/client-login', data)
	}

	staff_login(data) {
		return this._callAPI('/staff-login', data)
	}

	admin_login(data) {
		return this._callAPI('/admin-login', data)
	}

	tourism_company_reset(data) {
		return this._callAPI('/reset-password', data)
	}

	_callAPI(url: string, params?) {
		return new Promise((resolve, reject) => {
		  this.http.post(END_POINT + url, params)
			.subscribe(res => {
			  if (res['response'] === 'success') {
				resolve(res)
			  } else {
				reject(res['message'])
			  }
			}, (this.handleError))
		})
	  }

	    // Handle API errors
  handleError(error: HttpErrorResponse) {

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${JSON.stringify(error.status)}, ` +
        `body was: ${JSON.stringify(error.message)}`);
    }
    // return an observable with a user-facing error message
		if(error.status == 0){
			return throwError(
				Swal.fire(
				  'The Internet?',
				  'Please try again later',
				  'question'
				)
			  );
		}
  };

}
