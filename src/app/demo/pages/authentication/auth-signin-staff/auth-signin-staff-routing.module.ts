import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthSigninStaffComponent} from './auth-signin-staff.component';

const routes: Routes = [
  {
    path: '',
    component: AuthSigninStaffComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthSigninStaffRoutingModule { }
