import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthSigninStaffRoutingModule } from './auth-signin-staff-routing.module';
import { AuthSigninStaffComponent } from './auth-signin-staff.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [AuthSigninStaffComponent],
  imports: [
    CommonModule,
    AuthSigninStaffRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ]
})
export class AuthSigninStaffModule { }
