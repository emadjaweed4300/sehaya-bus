import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthSigninStaffComponent } from './auth-signin-staff.component';

describe('AuthSigninStaffComponent', () => {
  let component: AuthSigninStaffComponent;
  let fixture: ComponentFixture<AuthSigninStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthSigninStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthSigninStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
