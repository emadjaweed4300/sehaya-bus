import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
	selector: 'app-auth-signin-v2', 
	templateUrl: './auth-signin-v2.component.html',
	styleUrls: ['./auth-signin-v2.component.scss']
})
export class AuthSigninV2Component implements OnInit {

	loginForm: FormGroup;

	constructor(
		private authenticationService: AuthenticationService,
		private formBuilder: FormBuilder
	) { }

	ngOnInit(): void {
		this.loginForm = this.formBuilder.group({
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			// password: ['', [Validators.required, Validators.minLength(4)]],
			password: [null, [Validators.required]],
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minlengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.loginForm.get(formControlName).errors &&
			(this.loginForm.get(formControlName).touched ||
				this.loginForm.get(formControlName).dirty);
		if (errors) {
			required = this.loginForm.get(formControlName).errors
				.required;
			patternValidate = this.loginForm.get(formControlName).errors
				.pattern;
			minlengthValidate = this.loginForm.get(formControlName)
				.errors.minLength;
			notEquivalentValidate = this.loginForm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minlengthValidate: minlengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	login() {
		this.markFormGroupTouched(this.loginForm);
		if (this.loginForm.valid) {
			this.authenticationService.CT_login(this.loginForm.get('email').value, this.loginForm.get('password').value);
		}
	}

	/**
	   * Marks all controls in a form group as touched
	   * @param formGroup The form group to touch
	   */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
