import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { AuthenticationApiServiceService } from '../shared/authentication-api-service.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-auth-reset-password',
	templateUrl: './auth-reset-password.component.html',
	styleUrls: ['./auth-reset-password.component.scss']
})
export class AuthResetPasswordComponent implements OnInit {

	reset_Admin_Form: FormGroup;

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private authAPIService: AuthenticationApiServiceService,
	) { }

	ngOnInit() {
		this.reset_Admin_Form = this.__fb.group({
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.reset_Admin_Form.get(formControlName).errors &&
			(this.reset_Admin_Form.get(formControlName).touched ||
				this.reset_Admin_Form.get(formControlName).dirty);
		if (errors) {
			required = this.reset_Admin_Form.get(formControlName).errors
				.required;
			patternValidate = this.reset_Admin_Form.get(formControlName).errors
				.pattern;
			minLengthValidate = this.reset_Admin_Form.get(formControlName)
				.errors.minLength;
			notEquivalentValidate = this.reset_Admin_Form.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	reset_Admin_Password() {
		this.markFormGroupTouched(this.reset_Admin_Form);
		if (this.reset_Admin_Form.valid) {
			const form_data = new FormData();
			form_data.append('type', 'admin');
			form_data.append('email', this.reset_Admin_Form.get('email').value);
			this.authAPIService.tourism_company_reset(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				  });
				  this.router.navigate(['/auth/admin'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	   * Marks all controls in a form group as touched
	   * @param formGroup The form group to touch
	   */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
