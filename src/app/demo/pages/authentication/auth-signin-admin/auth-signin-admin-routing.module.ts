import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthSigninAdminComponent} from './auth-signin-admin.component';

const routes: Routes = [
  {
    path: '',
    component: AuthSigninAdminComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthSigninAdminRoutingModule { }
