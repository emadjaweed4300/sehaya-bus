import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthSigninAdminRoutingModule } from './auth-signin-admin-routing.module';
import { AuthSigninAdminComponent } from './auth-signin-admin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    AuthSigninAdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [AuthSigninAdminComponent]
})
export class AuthSigninAdminModule { }
