import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthSigninAdminComponent } from './auth-signin-admin.component';

describe('AuthSigninAdminComponent', () => {
  let component: AuthSigninAdminComponent;
  let fixture: ComponentFixture<AuthSigninAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthSigninAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthSigninAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
