import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
	selector: 'app-auth-signin-admin',
	templateUrl: './auth-signin-admin.component.html',
	styleUrls: ['./auth-signin-admin.component.scss']
})
export class AuthSigninAdminComponent implements OnInit {

	admin_login_Form: FormGroup;

	constructor(
		private formBuilder: FormBuilder,
		private authenticationService: AuthenticationService,
	) { }

	ngOnInit(): void {
		this.admin_login_Form = this.formBuilder.group({
			email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
			// password: ['', [Validators.required, Validators.minLength(4)]],
			password: [null, [Validators.required]],
		});
	}

	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minlengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.admin_login_Form.get(formControlName).errors &&
			(this.admin_login_Form.get(formControlName).touched ||
				this.admin_login_Form.get(formControlName).dirty);
		if (errors) {
			required = this.admin_login_Form.get(formControlName).errors
				.required;
			patternValidate = this.admin_login_Form.get(formControlName).errors
				.pattern;
			minlengthValidate = this.admin_login_Form.get(formControlName)
				.errors.minLength;
			notEquivalentValidate = this.admin_login_Form.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minlengthValidate: minlengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	admin_login() {
		this.markFormGroupTouched(this.admin_login_Form);
		if (this.admin_login_Form.valid) {
			this.authenticationService.admin_login(this.admin_login_Form.get('email').value, this.admin_login_Form.get('password').value);
		}
	}

	/**
	   * Marks all controls in a form group as touched
	   * @param formGroup The form group to touch
	   */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
