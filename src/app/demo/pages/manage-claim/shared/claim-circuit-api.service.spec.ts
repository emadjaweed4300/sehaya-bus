import { TestBed } from '@angular/core/testing';

import { ClaimCircuitApiService } from './claim-circuit-api.service';

describe('ClaimCircuitApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClaimCircuitApiService = TestBed.get(ClaimCircuitApiService);
    expect(service).toBeTruthy();
  });
});
