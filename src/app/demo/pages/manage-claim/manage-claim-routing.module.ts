import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		children: [
			// Claim Circuit list,add,view,edit components
			{
				path: 'list-claim-circuit',
				loadChildren: () => import('./claim-circuit/list-claim-circuit/list-claim-circuit.module').then(module => module.ListClaimCircuitModule)
			},
			{
				path: 'list-claim-circuit/:setting_id',
				loadChildren: () => import('./claim-circuit/list-claim-circuit/list-claim-circuit.module').then(module => module.ListClaimCircuitModule)
			},
			{
				path: 'add-claim-circuit',
				loadChildren: () => import('./claim-circuit/add-claim-circuit/add-claim-circuit.module').then(module => module.AddClaimCircuitModule)
			},
			// {
			// 	path: 'add-claim-circuit/:setting_id',
			// 	loadChildren: () => import('./claim-circuit/add-claim-circuit/add-claim-circuit.module').then(module => module.AddClaimCircuitModule)
			// },
			{
				path: 'edit-claim-circuit/:id',
				loadChildren: () => import('./claim-circuit/edit-claim-circuit/edit-claim-circuit.module').then(module => module.EditClaimCircuitModule)
			},

		] 
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManageClaimRoutingModule { }
