import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { ClaimCircuitApiService } from '../../shared/claim-circuit-api.service';
import Swal from 'sweetalert2';
import { CircuitApiService } from '../../../manage-circuit/shared/circuit-api.service';

@Component({
	selector: 'app-edit-claim-circuit',
	templateUrl: './edit-claim-circuit.component.html',
	styleUrls: ['./edit-claim-circuit.component.scss']
})
export class EditClaimCircuitComponent implements OnInit {

	edit_claim_circuit_frm: FormGroup;
	claim_circuit_id
	staff_functions = [];
	claim_types = [];
	circuits = [];
	staffs = [];
	complaints: any;

	priorityArr: any[] = [
		{ id: 1, name: 'Low' },
		{ id: 2, name: 'Medium' },
		{ id: 3, name: 'High' }
	];

	constructor(
		private __fb: FormBuilder,
		private router: Router,
		private __route: ActivatedRoute,
		private masterApi: MasterApiService, 
		private claimCircuitApi: ClaimCircuitApiService, 
		private circuitApi: CircuitApiService,
	) {
		this.claim_circuit_id = this.__route.snapshot.paramMap.get('id');
	 }

	ngOnInit() {
		/** Form builder instance */
		this.edit_claim_circuit_frm = this.__fb.group({
			circuit: ["", Validators.required],
			driver: ["", Validators.required],
			reclamination_type: ["", Validators.required],
			claim_priority: ["", Validators.required],
			description: ["", Validators.required],
			// function_name: [{ value: '', disabled: true }, Validators.required],
		});
		this.get_staff_functions();
		this.get_Claim_Type();
		this.get_circuits();
		this.get_claim_circuits();
		console.log(this.claim_circuit_id)
	}

	// Get Staff Function Dropdown
	get_staff_functions() {
		this.circuitApi.drivers_for_filter().then((response: any) => {
			console.log('Response:', response);
			this.staffs = response.drivers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Staff Function Dropdown
	// get_staff_functions(){
	// 	this.masterApi.list_staff_function().then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.staff_functions = response.functions

	// 	})
	// 	.catch((error: any) => {
	// 		console.error(error);
	// 		Swal.fire({
	// 				type: 'error',
	// 				title: 'Error!',
	// 				text: error,
	// 				confirmButtonColor: '#323258',
	// 				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// 			});
	// 	});	
	// }

	// Get Claim_Type Dropdown
	get_Claim_Type() {
		this.masterApi.list_claim_types().then((response: any) => {
			console.log('Response:', response);
			this.claim_types = response.claim_type
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// Get Circuit list Dropdown
	get_circuits() {
		this.circuitApi.circuits().then((response: any) => {
			console.log('Response:', response);
			this.circuits = response.circuit
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	// staffChange(driver_id) {
	// 	this.circuitApi.get_staff(this.edit_claim_circuit_frm.get('function_name').value).then((response: any) => {
	// 		console.log('Response:', response);
	// 		this.staffs = response.$drivers
	// 		this.edit_claim_circuit_frm.get('driver').setValue(driver_id);

	// 	})
	// 		.catch((error: any) => {
	// 			console.error(error);
	// 			Swal.fire({
	// 				type: 'error',
	// 				title: 'Error!',
	// 				text: error,
	// 				confirmButtonColor: '#323258',
	// 				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// 			});
	// 		});
	// }


	get_claim_circuits(){
		this.claimCircuitApi.get_claim_circuits(this.claim_circuit_id).then((response: any) => {
			console.log('Response:', response);
			this.complaints = response.claim;
			// name (formControlName) & name (postman)
			this.edit_claim_circuit_frm.get('circuit').setValue(response.claim.circuit_id);
			// this.edit_claim_circuit_frm.get('function_name').setValue(response.claim.function_id);
			// this.staffChange(response.claim.driver_id)
			this.edit_claim_circuit_frm.get('driver').setValue(response.claim.driver_id);
			this.edit_claim_circuit_frm.get('reclamination_type').setValue(response.claim.claim_id);
			this.edit_claim_circuit_frm.get('claim_priority').setValue(response.claim.severity);
			this.edit_claim_circuit_frm.get('description').setValue(response.claim.discription);

		})
		.catch((error: any) => {
			console.error(error);
			Swal.fire({
				type: 'error',
				title: 'Error!',
				text: error,
				confirmButtonColor: '#323258',
				confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
			});
		});
	}


	/** Helper functions */
	/**
	 * @name checkValidation
	 * @param formControlName
	 */
	public checkValidation(formControlName: string) {
		let errors = false;
		let required = false;
		let patternValidate = false;
		let minLengthValidate = false;
		let notEquivalentValidate = false;
		errors =
			this.edit_claim_circuit_frm.get(formControlName).errors &&
			(this.edit_claim_circuit_frm.get(formControlName).touched ||
				this.edit_claim_circuit_frm.get(formControlName).dirty);
		if (errors) {
			required = this.edit_claim_circuit_frm.get(formControlName).errors
				.required;
			patternValidate = this.edit_claim_circuit_frm.get(formControlName).errors
				.pattern;
			minLengthValidate = this.edit_claim_circuit_frm.get(formControlName)
				.errors.minlength;
			notEquivalentValidate = this.edit_claim_circuit_frm.get(formControlName)
				.errors.notEquivalent;
		}

		const errorAll = {
			errors: errors,
			required: required,
			patternValidate: patternValidate,
			minLengthValidate: minLengthValidate,
			notEquivalentValidate: notEquivalentValidate
		};
		// console.log(errorAll);
		return errorAll;
	}

	/**
	 * @name save
	 * @description This function will be called on submit and
	 * used to call edit service method
	 */
	public save(): void {
		this.markFormGroupTouched(this.edit_claim_circuit_frm);
		if (this.edit_claim_circuit_frm.valid) {
			/** Prepare form data */
			const form_data = new FormData();
			// id (ppostman) & staff_member_id (formControlName)
			form_data.append('complaint_id', this.claim_circuit_id);
			form_data.append('circuit_id', this.edit_claim_circuit_frm.get('circuit').value);
			// form_data.append('function_id', this.edit_claim_circuit_frm.get('function_name').value);
			form_data.append('driver_id', this.edit_claim_circuit_frm.get('driver').value);
			form_data.append('claim_type', this.edit_claim_circuit_frm.get('reclamination_type').value);
			form_data.append('severity', this.edit_claim_circuit_frm.get('claim_priority').value);
			form_data.append('discription', this.edit_claim_circuit_frm.get('description').value);
			this.claimCircuitApi.edit_claim_circuits(form_data).then((response: any) => {
				console.log('Response:', response);
				Swal.fire({
					type: 'success',
					title: 'Success',
					confirmButtonColor: '#323258',
					// cancelButtonColor: '#ff1f1f',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
					text: response.message
				});
				  this.router.navigate(['/manage-claim/list-claim-circuit'])
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
		}
	}

	/**
	 * Marks all controls in a form group as touched
	 * @param formGroup The form group to touch
	 */
	private markFormGroupTouched(formGroup: FormGroup) {
		Object.values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
			if (control.controls) {
				this.markFormGroupTouched(control);
			}
		});
	}

}
