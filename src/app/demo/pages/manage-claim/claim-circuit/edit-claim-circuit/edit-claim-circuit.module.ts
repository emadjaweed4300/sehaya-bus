import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditClaimCircuitRoutingModule } from './edit-claim-circuit-routing.module';
import { EditClaimCircuitComponent } from './edit-claim-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    EditClaimCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [EditClaimCircuitComponent]
})
export class EditClaimCircuitModule { }
