import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditClaimCircuitComponent } from './edit-claim-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: EditClaimCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditClaimCircuitRoutingModule { }
