import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditClaimCircuitComponent } from './edit-claim-circuit.component';

describe('EditClaimCircuitComponent', () => {
  let component: EditClaimCircuitComponent;
  let fixture: ComponentFixture<EditClaimCircuitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditClaimCircuitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditClaimCircuitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
