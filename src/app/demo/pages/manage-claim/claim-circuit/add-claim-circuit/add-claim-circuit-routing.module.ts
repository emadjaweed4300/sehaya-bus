import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddClaimCircuitComponent } from './add-claim-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: AddClaimCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddClaimCircuitRoutingModule { }
