import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClaimCircuitComponent } from './add-claim-circuit.component';

describe('AddClaimCircuitComponent', () => {
  let component: AddClaimCircuitComponent;
  let fixture: ComponentFixture<AddClaimCircuitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClaimCircuitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClaimCircuitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
