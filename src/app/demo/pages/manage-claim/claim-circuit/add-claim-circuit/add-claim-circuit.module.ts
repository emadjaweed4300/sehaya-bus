import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddClaimCircuitRoutingModule } from './add-claim-circuit-routing.module';
import { AddClaimCircuitComponent } from './add-claim-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    AddClaimCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [AddClaimCircuitComponent]
})
export class AddClaimCircuitModule { }
