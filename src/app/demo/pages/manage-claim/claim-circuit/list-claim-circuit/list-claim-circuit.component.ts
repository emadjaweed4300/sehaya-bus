import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MasterApiService } from '../../../master/shared/master_api.service';
import { ClaimCircuitApiService } from '../../shared/claim-circuit-api.service';

@Component({
	selector: 'app-list-claim-circuit',
	templateUrl: './list-claim-circuit.component.html',
	styleUrls: ['./list-claim-circuit.component.scss']
})
export class ListClaimCircuitComponent implements OnInit {

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dtExportButtonOptions: any = {};
	claim_circuit_list: any = [];
	isDtInitialized: boolean = false
	dtTrigger: Subject<any> = new Subject();
	staff_functions = [];
	claim_types = [];
	all_claim_users = [];

	priorityArr: any[] = [
		{ id: 1, name: 'Low' },
		{ id: 2, name: 'Medium' },
		{ id: 3, name: 'High' }
	];

	// for circuit filter
	circuit_id = ''

	// for filter
	claim_id = '';
	priority_id = '';
	driver_id = '';
	// For clear value of filter
	clear(){
		this.claim_id = '';
		this.priority_id = '';
		this.driver_id = '';
	}


	constructor(
		private masterApi: MasterApiService,
		private claimCircuitApi: ClaimCircuitApiService,
		private __route: ActivatedRoute,
	) {
		this.circuit_id = this.__route.snapshot.paramMap.get('setting_id');
	 }

	ngOnInit() {
		this.dtExportButtonOptions = {
			dom:
				// "<'row'<'col-sm-12'Brt>>" +
				"<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-5'i>>",
			// 		buttons: [
			// 			'copy',
			// 			'print',
			// 			'excel',
			// 			'csv'
			// 		],
			responsive: true
		};
		this.list_claim_circuit();
		this.get_staff_functions();
		this.get_Claim_Type();
		this.get_all_claim_user();
		if (this.circuit_id != null) {
			this.circuitFilterAction()
		}
	}

	// Get Staff Function Dropdown
	get_staff_functions() {
		this.masterApi.list_staff_function().then((response: any) => {
			console.log('Response:', response);
			this.staff_functions = response.functions
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	get_Claim_Type() {
		this.masterApi.list_claim_types().then((response: any) => {
			console.log('Response:', response);
			this.claim_types = response.claim_type
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	get_all_claim_user() {
		this.claimCircuitApi.get_all_claim_user().then((response: any) => {
			console.log('Response:', response);
			this.all_claim_users = response.drivers
		})
		.catch((error: any) => {
			console.error(error);
		});
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	// List Data API
	list_claim_circuit() {
		let formData = new FormData()
		formData.append('remove_filter', '1');
		this.claimCircuitApi.claim_circuits(formData).then((response: any) => {
			console.log('Response:', response);
			this.claim_circuit_list = response.complaints
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Filter Function
	public filterAction() {
		let formData = new FormData()
		formData.append('claim_type', this.claim_id);
		formData.append('priority', this.priority_id);
		formData.append('driver_id', this.driver_id);
		formData.append('add_filter', '1');
		this.claimCircuitApi.claim_circuits(formData).then((response: any) => {
			console.log('Response:', response);
			this.claim_circuit_list = response.complaints
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	public circuitFilterAction() {
		let formData = new FormData()
		formData.append('circuit_id', this.circuit_id);
		formData.append('add_filter', '1');
		console.log(this.circuit_id)
		this.claimCircuitApi.claim_circuits(formData).then((response: any) => {
			console.log('Response:', response);
			this.claim_circuit_list = response.complaints
			if (this.isDtInitialized) {
				this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.destroy();
					this.dtTrigger.next();
				});
			} else {
				this.isDtInitialized = true
				this.dtTrigger.next();
			}
		})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
			});
	}

	// Cancel Function on Filter
	public cancelAction() {
		this.clear();
		this.list_claim_circuit()
	}

	// Status API
	// statusChange(i, complain_id: string, event) {
	// 	console.log(complain_id)
	// 	let formData = new FormData()
	// 	formData.append('id', complain_id)
	// 	if (event.target.checked == true) {
	// 		this.claimCircuitApi.claim_circuits_active(formData).then((response: any) => {
	// 			console.log('Response:', response);
	// 			// on Inactive status edit disbale. 
	// 			this.claim_circuit_list[i].status = '1'
	// 		})
	// 			.catch((error: any) => {
	// 				console.error(error);
	// 				Swal.fire({
	// 	type: 'error',
	// 	title: 'Error!',
	// 	text: error,
	// 	confirmButtonColor: '#323258',
	// 	confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// });
	// 			});
	// 	} else {
	// 		this.claimCircuitApi.claim_circuits_inactive
	// 			(formData).then((response: any) => {
	// 				console.log('Response:', response);
	// 				// on Active status edit Enabale. 
	// 				this.claim_circuit_list[i].status = '0'
	// 			})
	// 			.catch((error: any) => {
	// 				console.error(error);
	// 				Swal.fire({
	// 	type: 'error',
	// 	title: 'Error!',
	// 	text: error,
	// 	confirmButtonColor: '#323258',
	// 	confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
	// });
	// 			});
	// 	}
	// }

	// Delete
	public deleteAction(id: number) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#323258',
			cancelButtonColor: '#ff1f1f',
			confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>',
			cancelButtonText: '<i class="feather icon-x" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i>',
		}).then(result => {
			if (result.value) {
				this.claimCircuitApi.delete_claim_circuit(id).then((response: any) => {
					console.log('Response:', response);
					this.list_claim_circuit();
					Swal.fire({
						type: 'success',
						title: 'Deleted!',
						text: response.message,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});

				})
					.catch((error: any) => {
						console.error(error);
						Swal.fire({
							type: 'error',
							title: 'Error!',
							text: error,
							confirmButtonColor: '#323258',
							confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
						});
					});
			}
		})
	}

}
