import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListClaimCircuitComponent } from './list-claim-circuit.component';

describe('ListClaimCircuitComponent', () => {
  let component: ListClaimCircuitComponent;
  let fixture: ComponentFixture<ListClaimCircuitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListClaimCircuitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListClaimCircuitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
