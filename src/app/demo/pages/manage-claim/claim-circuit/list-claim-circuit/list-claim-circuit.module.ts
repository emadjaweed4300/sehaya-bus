import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListClaimCircuitRoutingModule } from './list-claim-circuit-routing.module';
import { ListClaimCircuitComponent } from './list-claim-circuit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    ListClaimCircuitRoutingModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TranslateModule
  ],
  declarations: [ListClaimCircuitComponent]
})
export class ListClaimCircuitModule { }
