import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListClaimCircuitComponent } from './list-claim-circuit.component';

const routes: Routes = [
  {
    path: '',
    component: ListClaimCircuitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListClaimCircuitRoutingModule { }
