import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageClaimRoutingModule } from './manage-claim-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ManageClaimRoutingModule,
  ],
  declarations: []
})
export class ManageClaimModule { }
