export class User {
    id: string;
    email: string;
    user_role: string;
    user_type: string;
    first_name: string;
    last_name: string;
    token: string;
}