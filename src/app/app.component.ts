import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { User } from './models/User';
import { AuthenticationService } from './services/authentication.service';

import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from './services/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  currentUser: User;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    // Language Service
    public translate: TranslateService,
    private global: GlobalService
  ) {
    translate.addLangs(['English', 'French']);
    translate.setDefaultLang('English');
    // By default language French
    // translate.addLangs(['French', 'English']);
    // translate.setDefaultLang('French');
    
    // const browserLang = translate.getBrowserLang();
    // translate.use(browserLang.match(/en/fr) ? browserLang : 'en');
  }

  ngOnInit() {
    this.authenticationService.currentUser.subscribe(x => {
      if (x) {
        this.global.currentUser = x;
        console.log(this.global.currentUser);
        // this.router.navigate(['/dashboard/analytics']);
      }
    });
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return
      }
      window.scrollTo(0, 0);
    });
  }
}
