import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/User';
import { Router } from '@angular/router';

import { AuthenticationApiServiceService } from '../demo/pages/authentication/shared/authentication-api-service.service';
import Swal from 'sweetalert2';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(
      private router: Router,
      private authAPIService: AuthenticationApiServiceService,
    ) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    // Tourism client login
    CT_login(email: string, password: string) {
        let formData = new FormData();
        formData.append('email', email);
        formData.append('password', password);
        this.authAPIService.CT_login(formData).then((response: any) => {
            console.log('Response:', response);
            let user = {
                id: response.client.id,
                email: response.client.email,
                user_role: response.client.user_role,
                user_type: response.client.user_type,
                first_name: response.client.first_name,
                last_name: response.client.last_name,
                token: response.security_codes.security_key
              }
              localStorage.setItem('currentUser', JSON.stringify(user));
              this.currentUserSubject.next(user);
              this.router.navigate(['/dashboard/analytics'])
        })
            .catch((error: any) => {
                console.error(error);
                Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
            });
    }

    // Staff member login
    staff_login(email: string, password: string) {
        let formData = new FormData();
        formData.append('email', email);
        formData.append('password', password);
        this.authAPIService.staff_login(formData).then((response: any) => {
            console.log('Response:', response);
            let user = {
                id: response.staff_member.id,
                email: response.staff_member.email,
                user_role: response.staff_member.user_role,
                user_type: response.staff_member.user_type,
                first_name: response.staff_member.first_name,
                last_name: response.staff_member.last_name,
                token: response.security_codes.security_key
              }
              localStorage.setItem('currentUser', JSON.stringify(user));
              this.currentUserSubject.next(user);
              this.router.navigate(['/dashboard/analytics'])
        })
            .catch((error: any) => {
                console.error(error);
                Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
            });
    }

    // admin member login
    admin_login(email: string, password: string) {
        let formData = new FormData();
        formData.append('email', email);
        formData.append('password', password);
        this.authAPIService.admin_login(formData).then((response: any) => {
            console.log('Response:', response);
            let user = {
                id: response.admin.id,
                email: response.admin.email,
                user_role: response.admin.user_role,
                user_type: response.admin.user_type,
                first_name: response.admin.first_name,
                last_name: response.admin.last_name,
                token: response.security_codes.security_key
              }
              localStorage.setItem('currentUser', JSON.stringify(user));
              this.currentUserSubject.next(user);
              this.router.navigate(['/dashboard/analytics'])
        })
            .catch((error: any) => {
                console.error(error);
                Swal.fire({
					type: 'error',
					title: 'Error!',
					text: error,
					confirmButtonColor: '#323258',
					confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
				});
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        // localStorage.clear();
        this.currentUserSubject.next(null);
        // this.router.navigate(['/auth/signin']);
        // It use for the Hard Reload the Page
        window.location.replace('/auth/signin');
        console.log("logout")
    }
}