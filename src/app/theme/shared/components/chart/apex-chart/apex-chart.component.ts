import { Component, Input, OnInit } from '@angular/core';
import ApexCharts from 'apexcharts/dist/apexcharts.common.js';
import { ApexChartService } from './apex-chart.service';
import { END_POINT } from 'src/app/constant';
import { DashboardApiService } from 'src/app/demo/dashboard/shared/dashboard-api.service';
import Swal from 'sweetalert2';
import { GlobalService } from 'src/app/services/global.service';

@Component({
	selector: 'app-apex-chart',
	templateUrl: './apex-chart.component.html',
	styleUrls: ['./apex-chart.component.scss']
})
export class ApexChartComponent implements OnInit {
	@Input() chartID: string;
	@Input() chartConfig: any;
	@Input() xAxis: any;
	@Input() newData: any;

	public chart: any;
	// cash_in: any = [];
	// expense: any = [];
	// profit: any = [];
	// month: any = [];

	// for Series Data 
	graphData: any = [];
	// for categories data
	xaxis: any = [];
	data = new FormData();
	constructor(private apexEvent: ApexChartService, private dashboardApi: DashboardApiService, private global: GlobalService,) {
		this.data.append('user_id', this.global.currentUser.id);
		this.data.append('user_role', this.global.currentUser.user_role);
		this.data.append('security_key', this.global.currentUser.token)
	}

	ngOnInit() {
		let th = this;
		setTimeout(() => {

			$.ajax({
				url: END_POINT + '/' + 'new-idea',
				type: 'POST',
				dataType: 'json',
				data: { user_id: this.global.currentUser.id, user_role: this.global.currentUser.user_role, security_key: this.global.currentUser.token },
			})
				.done(function (data) {
					// th.cash_in = data['dashboard']['cash_in'];
					// th.profit = data['dashboard']['profit'];
					// th.expense = data['dashboard']['expense'];
					// th.month = data['dashboard']['months'];

					// for Series Data 
					th.graphData = data['dashboard']['series'];
					// for categories data
					th.xaxis = data['dashboard']['xaxis'];
					// Graph Array structure
					var options = {
						chart: {
							height: 350,
							type: 'bar',
						},
						plotOptions: {
							bar: {
								horizontal: false,
								columnWidth: '55%',
								endingShape: 'rounded'
							},
						},
						dataLabels: {
							enabled: false
						},
						colors: ['#0e9e4a', '#323258', '#ff5252'],
						stroke: {
							show: true,
							width: 2,
							colors: ['transparent']
						},
						// Series Data
						series: th.graphData,
						// categories data
						xaxis: th.xaxis,
						yaxis: {
							title: {
								text: 'mad (thousands)'
							}
						},
						fill: {
							opacity: 1

						},
						tooltip: {
							y: {
								formatter: (val) => 'MAD ' + val + ' thousands'
							}
						}
					}
					th.chart = new ApexCharts(document.querySelector('#' + th.chartID), options);
					th.chart.render();
				});
		});

		this.apexEvent.changeTimeRange.subscribe(() => {
			if (this.xAxis) {
				this.chart.updateOptions({
					xaxis: this.xAxis
				});
			}
		});

		this.apexEvent.changeSeriesData.subscribe(() => {
			if (this.newData) {
				this.chart.updateSeries([{
					data: this.newData
				}]);
			}
		});
	}

}
