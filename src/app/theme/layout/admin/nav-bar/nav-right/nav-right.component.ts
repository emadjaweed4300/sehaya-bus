import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/services/authentication.service';

import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileApiService } from 'src/app/demo/pages/profile-details/shared/profile-api.service';
import Swal from 'sweetalert2';
import { END_POINT } from 'src/app/constant';
import { GlobalService } from 'src/app/services/global.service';


@Component({
	selector: 'app-nav-right',
	templateUrl: './nav-right.component.html',
	styleUrls: ['./nav-right.component.scss'],
	providers: [NgbDropdownConfig]
})
export class NavRightComponent implements OnInit {

	profile_details: any = [];
	imageSrc: any;

	constructor(
		private authenticationService: AuthenticationService,
		public translate: TranslateService,
		private router: Router,
		private __route: ActivatedRoute,
		private profileApiService: ProfileApiService,
		public global: GlobalService,
	) {
		// translate.addLangs(['en', 'fr']);
		// 		translate.setDefaultLang('en');
		// 		const browserLang = translate.getBrowserLang();
		// 		translate.use(browserLang.match(/en/fr) ? browserLang : 'en');
	}

	ngOnInit() { 
		this.get_profile_deatils();
	}

	logout() {
		this.authenticationService.logout();
	}

	// Get User name & Profile picture
	get_profile_deatils(){
		if(this.global.currentUser.user_role == '2' || this.global.currentUser.user_role == '3'){
			this.profileApiService.get_profile_details().then((response: any) => {
				console.log('Response:', response);
				this.profile_details = response.user_details;
				this.imageSrc = END_POINT + '/' + this.profile_details.companies_logo
				
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
			});
		} else if(this.global.currentUser.user_role == '1'){
			this.profileApiService.get_admin_profile_details().then((response: any) => {
				console.log('Response:', response);
				this.profile_details = response.admin;
				this.imageSrc = END_POINT + '/' + this.profile_details.companies_logo
				
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
			});
			
		}
		// Staff Member
		else if(this.global.currentUser.user_role == '4'){
			this.profileApiService.get_staff_member_profile_details().then((response: any) => {
				console.log('Response:', response);
				this.profile_details = response.staff_member;
				this.imageSrc = END_POINT + '/' + this.profile_details.companies_logo
				
			})
			.catch((error: any) => {
				console.error(error);
				Swal.fire({
						type: 'error',
						title: 'Error!',
						text: error,
						confirmButtonColor: '#323258',
						confirmButtonText: '<i class="feather icon-check" style="font-size: x-large;" data-toggle="tooltip" data-placement="bottom" title="Confirm"></i>'
					});
			});
			
		}
	}

}

